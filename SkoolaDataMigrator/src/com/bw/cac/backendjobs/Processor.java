package com.bw.cac.backendjobs;

import java.io.File;
import java.io.PrintStream;

public class Processor
{
  public static void main(String[] args)
  {
    try
    {
      
      System.out.println("Processor called: ");
      String userDir = System.getProperty("user.dir");
      File logFile = new File(userDir + File.separator + "COURSE_SHEET.log");
      File excelFile = new File(userDir + File.separator + "COURSE_SHEET.xls");
      if (!logFile.exists()) {
        logFile.createNewFile();
      }
      ExcelHandler handler = new ExcelHandler();
      boolean result = handler.deleteCessationCompanies(excelFile, logFile);
      System.out.println("Result: " + result);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
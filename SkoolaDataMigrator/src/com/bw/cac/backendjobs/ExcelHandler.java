package com.bw.cac.backendjobs;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.Pattern;
import jxl.read.biff.BiffException;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;

import org.apache.commons.io.FileUtils;
import org.hibernate.cfg.Configuration;

import com.dg.skoola.ServiceLocator;
import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.Course;
import com.dg.skoola.entity.CourseType;
import com.dg.skoola.entity.School;
import com.dg.skoola.entity.Subject;
import com.dg.skoola.entity.SubjectGroup;
import com.dg.skoola.enumeration.CourseCategoryConstant;
import com.dg.skoola.enumeration.CourseTypeConstant;
import com.dg.skoola.enumeration.SchoolTypeConstant;
import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;


public class ExcelHandler {
	private static SkoolaService sService = null;



	public boolean deleteCessationCompanies(File excelFile, File logFile) {
		new Configuration().configure("DGDataConfig.xml").buildSessionFactory();
		ServiceLocator servicelocator = ServiceLocator.getInstance();
		sService = servicelocator.getSkoolaService();

		if (!excelFile.exists()) {
			System.out.println("Error, File " + excelFile.getAbsolutePath()
					+ " does not exist!");
			return false;
		}

		Workbook excelWorkbook = null;
		try {
			excelWorkbook = Workbook.getWorkbook(excelFile);
			Sheet sheet = excelWorkbook.getSheet(0);
			int startRow = 0;
			int startCol = 0;
			OUTER: for (int i = 0; i < sheet.getRows(); i++) {

				for (int j = 0; j < sheet.getColumns(); j++) {
					Cell cell = sheet.getCell(i, j);
					if (cell.getContents().trim().equalsIgnoreCase("S/N")) {
						startRow = cell.getRow();
						startCol = cell.getColumn();
						System.out.println("Start row, col = " + startRow + ":"
								+ startCol);
						break OUTER;

					}
				}
			}

			StringBuilder sb = new StringBuilder();
			for (int currentRow = startRow + 1; currentRow < sheet.getRows(); currentRow++) {

				try {
					String sn = sheet.getCell(startCol, currentRow)
							.getContents().trim();
					String courseName = sheet
							.getCell(startCol + 1, currentRow).getContents() == null ? "" : sheet
							.getCell(startCol + 1, currentRow).getContents()
							.trim();
					String universityLogo = sheet
							.getCell(startCol + 2, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 2, currentRow)
							.getContents().trim();
					String universityName = sheet
							.getCell(startCol + 3, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 3, currentRow)
							.getContents().trim();
					String countryName = sheet
							.getCell(startCol + 4, currentRow).getContents() == null ? "" :  sheet
							.getCell(startCol + 4, currentRow).getContents()
							.trim();
					String courseSummary = sheet
							.getCell(startCol + 5, currentRow).getContents() == null ? "" :  sheet
							.getCell(startCol + 5, currentRow).getContents()
							.trim();
					String tuitionFees = sheet
							.getCell(startCol + 6, currentRow).getContents() == null ? "" :  sheet
							.getCell(startCol + 6, currentRow).getContents()
							.trim();
					String startDate = sheet
							.getCell(startCol + 7, currentRow).getContents() == null ? "" :  sheet
							.getCell(startCol + 7, currentRow).getContents()
							.trim();
					String venue = sheet
							.getCell(startCol + 8, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 8, currentRow).getContents()
							.trim();
					String courseDuration = sheet
							.getCell(startCol + 9, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 9, currentRow).getContents()
							.trim();
					String entryRequirements = sheet
							.getCell(startCol + 10, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 10, currentRow).getContents()
							.trim();
					String courseSubjectGroupName = sheet
							.getCell(startCol + 11, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 11, currentRow).getContents()
							.trim();
					String courseSubjectName = sheet
							.getCell(startCol + 12, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 12, currentRow).getContents()
							.trim();
					String ieltsRequired = sheet
							.getCell(startCol + 13, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 13, currentRow).getContents()
							.trim();
					String programModules = sheet
							.getCell(startCol + 14, currentRow).getContents() == null ? "" :  sheet.getCell(startCol + 14, currentRow).getContents()
							.trim();
					
					System.out.println(" courseName  = " + courseName);
					System.out.println(" univeristy logo  = " + universityLogo);
					System.out.println(" university name  = " + universityName);
					System.out.println(" country  = " + countryName);
					System.out.println(" courseSummary  = " + courseSummary);
					System.out.println(" tuitionFees  = " + tuitionFees);
					System.out.println(" startDate  = " + startDate);
					System.out.println(" venue  = " + venue);
					System.out.println(" courseDuration  = " + courseDuration);
					System.out.println(" entryRequirements  = " + entryRequirements);
					System.out.println(" subjectGroup  = " + courseSubjectGroupName);
					System.out.println(" subject  = " + courseSubjectName);
					System.out.println(" ieltsRequired  = " + ieltsRequired);
					System.out.println(" programModules  = " + programModules);
	
					if((universityName == null || universityName.isEmpty()) 
							&& (countryName == null || countryName.isEmpty()) 
							&& (courseName == null || courseName.isEmpty() )
							&& (universityLogo == null || universityLogo.isEmpty() )
							&& (courseSummary == null || courseSummary.isEmpty() )
							&& (tuitionFees == null || tuitionFees.isEmpty()) 
							&& (startDate == null || startDate.isEmpty()) 
							&& (venue == null || venue.isEmpty()) 
							&& (courseDuration == null || courseDuration.isEmpty()) 
							&& (entryRequirements == null || entryRequirements.isEmpty()) 
							&& (courseSubjectGroupName == null || courseSubjectGroupName.isEmpty()) 
							&& (courseSubjectName == null || courseSubjectName.isEmpty())
							&& (ieltsRequired == null || ieltsRequired.isEmpty()) 
							&& (programModules == null || programModules.isEmpty())){
							System.out.println("skipped  :" + courseName + " ");
							continue;
						}

					CourseType currentCourseType = null;
					
					Country country = getCountryByName(countryName);
					
					if(country == null){
						country = new Country();
						country.setName(countryName);
						country.setCode("");
						
						country = (Country) sService.createNewRecord(country);
						sb.append(country.getName() + " added >> \t");
					}else{
						sb.append(country.getName() + " exists >> \t");
					}
					
					SubjectGroup subjectGroup = getSubjectGroupBySubjectGroupName(courseSubjectGroupName);
					if(subjectGroup == null){
						subjectGroup = new SubjectGroup();
						subjectGroup.setDescription("");
						subjectGroup.setDisplayName("");
						subjectGroup.setName(courseSubjectGroupName);
						
						subjectGroup = (SubjectGroup) sService.createNewRecord(subjectGroup);
					}
					
					Subject subject = getSubjectBySubjectName(courseSubjectName);
					if(subject == null){
						subject = new Subject();
						subject.setCode("");
						subject.setDescription("");
						subject.setDisplayName(courseSubjectName.toUpperCase());
						subject.setIsActive(true);
						subject.setName(courseSubjectName);
						subject.setSubjectGroup(subjectGroup);
						
						subject = (Subject) sService.createNewRecord(subject);
						
						
					}
					
					
					
					School school = getSchoolByName(universityName);
					if(school == null){
						school = new School();
						school.setName(universityName);
						school.setLogoUrl(universityLogo);
						school.setCountry(country);
						
						school = (School)sService.createNewRecord(school);
						sb.append(school.getName() + " added >> \t");
					}else{
						if (school.getCountry() == null){
							school.setCountry(country);
							sService.updateRecord(school);
						}
						
						sb.append(school.getName() + " exists >> \t");
					}
					
					Course course = getCourseByName(courseName);
					if(course == null){
						//List<String> coursePrices = new ArrayList<String>();
						String schoolFees = "0.00";
						Long skoolaSchoolTuition = 0l;
						String mydata = tuitionFees;
						//java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^\\$(([1-9]\\d{0,2}(,\\d{3})*)|(([1-9]\\d*)?\\d))(\\.\\d\\d)?$");
						java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("US\\$\\s*(([1-9][0-9]*)(,\\s*[0-9]{3})*)(\\.[0-9]+)?");
						Matcher matcher = pattern.matcher(mydata);
						if (matcher.find())
						{
						  // coursePrices.add((matcher.group(1)));
						   System.out.println(matcher.group(1));
						   schoolFees = matcher.group(1);
						}
						if(schoolFees != null && !schoolFees.isEmpty()){
							BigDecimal schoolFeesAmount = new BigDecimal(schoolFees.replaceAll(",", ""));
							skoolaSchoolTuition = schoolFeesAmount.longValue();
						}
						
						System.out.println(" skoolaSchoolTuition "  +  skoolaSchoolTuition);
						 
						
						
						course = new Course();
						
						course.setCourseType(currentCourseType);
						course.setDuration(courseDuration);
						course.setFeeInCents(Long.valueOf(skoolaSchoolTuition * 100));
						course.setFeeInWords(schoolFees);
						course.setCourseRequirements(entryRequirements);
						course.setCourseSummary(courseSummary);
						course.setCourseVenue(venue);
						course.setIeltsRequired(ieltsRequired.isEmpty() ? false : true);
						course.setProgramModules(programModules);
						course.setName(courseName);
						course.setSchool(school); 
						course.setSubject(subject);
						course.setCountry(country);
						
						course = (Course) sService.createNewRecord(course);
						
						sb.append(course.getName() + " added >> \n");
						
					}else{
						sb.append(" course = " + courseName + " already exits \n");
						continue;
					}
					
					
				} catch (Exception ex) {
					ex.printStackTrace();
					continue;
				}
			}
			FileUtils.writeStringToFile(logFile, sb.toString(), "UTF-8");
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			excelWorkbook.close();
		}

		return true;
	}

	
	private CourseType getCourseTypeByName(String courseTypeName) {
		CourseType courseType = null;
		
		String hql = "select s from CourseType s where s.name ='"+courseTypeName.trim()+"'";
		courseType = (CourseType) sService.getUniqueRecordByHQL(hql);
		
		return courseType;
	}

	private School getSchoolByName(String schoolName){
		School school = null;
		try{
			String hql = "select s from School s where upper(s.name) = upper('"+schoolName.trim()+"')";
			school = (School) sService.getUniqueRecordByHQL(hql);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return school;
	}
	
	private Country getCountryByName(String countryName){
		Country country = null;
		try{
			String hql = "select c from Country c where upper(c.name) = upper('"+countryName.trim()+"')";
			country = (Country) sService.getUniqueRecordByHQL(hql);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return country;
	}
	
	
	private Course getCourseByName(String courseName){
		Course course = null;
		try{
			String hql = "select c from Course c where upper(c.name) = upper('"+courseName.trim()+"')";
			course = (Course) sService.getUniqueRecordByHQL(hql);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return course;
	}
	
	
	private Subject getSubjectBySubjectName(String subjectName){
		Subject subject = null;
		try{
			String hql = "select s from Subject s where upper(s.name) = upper('"+subjectName.trim()+"')";
			subject = (Subject) sService.getUniqueRecordByHQL(hql);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return subject;
	}
	
	
	private SubjectGroup getSubjectGroupBySubjectGroupName(String subjectGroupName){
		SubjectGroup subjectGroup = null;
		try{
			String hql = "select s from SubjectGroup s where upper(s.name) = upper('"+subjectGroupName.trim()+"')";
			subjectGroup = (SubjectGroup) sService.getUniqueRecordByHQL(hql);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return subjectGroup;
	}
	
	
	
}
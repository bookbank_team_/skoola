Compile Stylus Code to CSS

Fontend CSS
stylus -u rupture -w skoola.styl -o ../skoola.css -c

Backend CSS
stylus -u rupture -w admin-style.styl -o ../../admin/css/style.css -c

    Requirements:
        * Rupture (https://github.com/jenius/rupture)

// license-header java merge-point
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package com.dg.skoola.entity;

/**
 * This contains the attributes of the entity (with getter and setter methods)
  * 
 * <p>
  * @author Neme I
 * @see Entity object
 * @since  $date
 */
public class Image
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 6431601336420678859L;
	
	//default no args constructor
	public Image(){}
	

    private java.lang.String name;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getName()
    {
        return this.name;
    }

    public void setName(java.lang.String name)
    {
        this.name = name;
    }

    private byte[] image;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see byte[] object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public byte[] getImage()
    {
        return this.image;
    }

    public void setImage(byte[] image)
    {
        this.image = image;
    }

    private java.lang.Long imageId;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.Long object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.Long getImageId()
    {
        return this.imageId;
    }

    public void setImageId(java.lang.Long imageId)
    {
        this.imageId = imageId;
    }

    private java.lang.String description;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getDescription()
    {
        return this.description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    private java.lang.Long id;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.Long object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.Long getId()
    {
        return this.id;
    }

    public void setId(java.lang.Long id)
    {
        this.id = id;
    }

    private com.dg.skoola.entity.ImageType imageType;

 /**
	 * <p>
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.entity.ImageType object
	 * @since $date
	 * 
	 * 
	 * 
	 */
   
    public com.dg.skoola.entity.ImageType getImageType()
    {
        return this.imageType;
    }

    public void setImageType(com.dg.skoola.entity.ImageType imageType)
    {
        this.imageType = imageType;
    }

    private com.dg.skoola.entity.School school;

 /**
	 * <p>
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.entity.School object
	 * @since $date
	 * 
	 * 
	 * 
	 */
   
    public com.dg.skoola.entity.School getSchool()
    {
        return this.school;
    }

    public void setSchool(com.dg.skoola.entity.School school)
    {
        this.school = school;
    }

    /**
     * Returns <code>true</code> if the argument is an Image instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Image))
        {
            return false;
        }
        final Image that = (Image)object;
        if (this.id == null || that.getId() == null || !this.id.equals(that.getId()))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (id == null ? 0 : id.hashCode());

        return hashCode;
    }


// HibernateEntity.vsl merge-point
}
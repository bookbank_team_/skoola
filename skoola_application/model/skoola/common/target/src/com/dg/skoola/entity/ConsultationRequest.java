// license-header java merge-point
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package com.dg.skoola.entity;

/**
 * This contains the attributes of the entity (with getter and setter methods)
  * 
 * <p>
  * @author Neme I
 * @see Entity object
 * @since  $date
 */
public class ConsultationRequest
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 955943210703584205L;
	
	//default no args constructor
	public ConsultationRequest(){}
	

    private java.lang.String firstname;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getFirstname()
    {
        return this.firstname;
    }

    public void setFirstname(java.lang.String firstname)
    {
        this.firstname = firstname;
    }

    private java.lang.String lastname;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getLastname()
    {
        return this.lastname;
    }

    public void setLastname(java.lang.String lastname)
    {
        this.lastname = lastname;
    }

    private java.lang.String email;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getEmail()
    {
        return this.email;
    }

    public void setEmail(java.lang.String email)
    {
        this.email = email;
    }

    private java.lang.String phoneNumber;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getPhoneNumber()
    {
        return this.phoneNumber;
    }

    public void setPhoneNumber(java.lang.String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    private com.dg.skoola.enumeration.ConsultationStatusConstant consultationStatus;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.enumeration.ConsultationStatusConstant object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public com.dg.skoola.enumeration.ConsultationStatusConstant getConsultationStatus()
    {
        return this.consultationStatus;
    }

    public void setConsultationStatus(com.dg.skoola.enumeration.ConsultationStatusConstant consultationStatus)
    {
        this.consultationStatus = consultationStatus;
    }

    private java.sql.Timestamp dateCreated;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.sql.Timestamp object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.sql.Timestamp getDateCreated()
    {
        return this.dateCreated;
    }

    public void setDateCreated(java.sql.Timestamp dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    private java.sql.Timestamp consultationDate;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.sql.Timestamp object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.sql.Timestamp getConsultationDate()
    {
        return this.consultationDate;
    }

    public void setConsultationDate(java.sql.Timestamp consultationDate)
    {
        this.consultationDate = consultationDate;
    }

    private com.dg.skoola.enumeration.ConsultationModeConstant consultationMode;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.enumeration.ConsultationModeConstant object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public com.dg.skoola.enumeration.ConsultationModeConstant getConsultationMode()
    {
        return this.consultationMode;
    }

    public void setConsultationMode(com.dg.skoola.enumeration.ConsultationModeConstant consultationMode)
    {
        this.consultationMode = consultationMode;
    }

    private java.lang.String comments;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getComments()
    {
        return this.comments;
    }

    public void setComments(java.lang.String comments)
    {
        this.comments = comments;
    }

    private java.lang.Long id;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.Long object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.Long getId()
    {
        return this.id;
    }

    public void setId(java.lang.Long id)
    {
        this.id = id;
    }

    private com.dg.skoola.entity.PortalUser createdBy;

 /**
	 * <p>
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.entity.PortalUser object
	 * @since $date
	 * 
	 * 
	 * 
	 */
   
    public com.dg.skoola.entity.PortalUser getCreatedBy()
    {
        return this.createdBy;
    }

    public void setCreatedBy(com.dg.skoola.entity.PortalUser createdBy)
    {
        this.createdBy = createdBy;
    }

    private com.dg.skoola.entity.PortalUser consultationAnchor;

 /**
	 * <p>
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.entity.PortalUser object
	 * @since $date
	 * 
	 * 
	 * 
	 */
   
    public com.dg.skoola.entity.PortalUser getConsultationAnchor()
    {
        return this.consultationAnchor;
    }

    public void setConsultationAnchor(com.dg.skoola.entity.PortalUser consultationAnchor)
    {
        this.consultationAnchor = consultationAnchor;
    }

    private com.dg.skoola.entity.Course course;

 /**
	 * <p>
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see com.dg.skoola.entity.Course object
	 * @since $date
	 * 
	 * 
	 * 
	 */
   
    public com.dg.skoola.entity.Course getCourse()
    {
        return this.course;
    }

    public void setCourse(com.dg.skoola.entity.Course course)
    {
        this.course = course;
    }

    /**
     * Returns <code>true</code> if the argument is an ConsultationRequest instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof ConsultationRequest))
        {
            return false;
        }
        final ConsultationRequest that = (ConsultationRequest)object;
        if (this.id == null || that.getId() == null || !this.id.equals(that.getId()))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (id == null ? 0 : id.hashCode());

        return hashCode;
    }


// HibernateEntity.vsl merge-point
}
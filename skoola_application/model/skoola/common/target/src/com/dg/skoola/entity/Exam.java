// license-header java merge-point
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package com.dg.skoola.entity;

/**
 * This contains the attributes of the entity (with getter and setter methods)
  * 
 * <p>
  * @author Neme I
 * @see Entity object
 * @since  $date
 */
public class Exam
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = -3507146846484844084L;
	
	//default no args constructor
	public Exam(){}
	

    private java.lang.String overview;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getOverview()
    {
        return this.overview;
    }

    public void setOverview(java.lang.String overview)
    {
        this.overview = overview;
    }

    private java.lang.String testFormat;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getTestFormat()
    {
        return this.testFormat;
    }

    public void setTestFormat(java.lang.String testFormat)
    {
        this.testFormat = testFormat;
    }

    private java.lang.Long examFeeInKobo;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.Long object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.Long getExamFeeInKobo()
    {
        return this.examFeeInKobo;
    }

    public void setExamFeeInKobo(java.lang.Long examFeeInKobo)
    {
        this.examFeeInKobo = examFeeInKobo;
    }

    private java.lang.String faqs;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getFaqs()
    {
        return this.faqs;
    }

    public void setFaqs(java.lang.String faqs)
    {
        this.faqs = faqs;
    }

    private java.lang.String examNameAbbreivated;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getExamNameAbbreivated()
    {
        return this.examNameAbbreivated;
    }

    public void setExamNameAbbreivated(java.lang.String examNameAbbreivated)
    {
        this.examNameAbbreivated = examNameAbbreivated;
    }

    private java.lang.String examNameInFull;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getExamNameInFull()
    {
        return this.examNameInFull;
    }

    public void setExamNameInFull(java.lang.String examNameInFull)
    {
        this.examNameInFull = examNameInFull;
    }

    private java.lang.String examFeeInFigures;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.String object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.String getExamFeeInFigures()
    {
        return this.examFeeInFigures;
    }

    public void setExamFeeInFigures(java.lang.String examFeeInFigures)
    {
        this.examFeeInFigures = examFeeInFigures;
    }

    private java.lang.Boolean active = java.lang.Boolean.valueOf(false);

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.Boolean object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.Boolean getActive()
    {
        return this.active;
    }

    public void setActive(java.lang.Boolean active)
    {
        this.active = active;
    }

    private java.lang.Long id;

     /**
	      * 
	 * 
	 * @author Neme I
	 * 
	 * @see java.lang.Long object
	 * @since $date
	 * 
	 * 
	 * 
	 */
    public java.lang.Long getId()
    {
        return this.id;
    }

    public void setId(java.lang.Long id)
    {
        this.id = id;
    }

    /**
     * Returns <code>true</code> if the argument is an Exam instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Exam))
        {
            return false;
        }
        final Exam that = (Exam)object;
        if (this.id == null || that.getId() == null || !this.id.equals(that.getId()))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (id == null ? 0 : id.hashCode());

        return hashCode;
    }


// HibernateEntity.vsl merge-point
}
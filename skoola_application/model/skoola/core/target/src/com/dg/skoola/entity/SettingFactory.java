// license-header java merge-point
/**
 * Attention: Generated source! Do not modify by hand!
 * Generated by HibernateEntityFactory.vsl
 */
package com.dg.skoola.entity;

/**
 * Is able to find and create objects of type Setting.
 * The Hibernate <em>subclass</em> inheritance strategy is followed here.
      * 
 * <p>
 * @author Neme I
 * @see  Setting object
 * @since  $date
 */
public abstract class SettingFactory
{
   /**
    * Creates a(n) Setting object.
    *
    * @param name
    * @param value
    * @param description
    * @return {@link Setting} the created object
    */
    public static Setting create (java.lang.String name, java.lang.String value, java.lang.String description)
    {
        Setting object = new Setting();

        object.setName (name);
        object.setValue (value);
        object.setDescription (description);

        return object;
    }

    /**
     *
     * Finds Setting object by its primary key.
     * In Hibernate, this is just a call to load().
     *
     */
    public static Setting findByPrimaryKey (org.hibernate.Session session, java.lang.Long id)
        throws org.hibernate.HibernateException
    {
        Setting object = (Setting) session.load(Setting.class, id);
        return object;
    }

	
	public static java.util.Collection loadAll(org.hibernate.Session session)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT setting from com.dg.skoola.entity.Setting  setting");
		return query.list();
	}
	
	public static java.util.Collection loadAllInRange(org.hibernate.Session session,int pageIndex, int pageSize)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT setting from com.dg.skoola.entity.Setting  setting");
		query.setFirstResult(pageIndex);
        query.setMaxResults(pageSize);
		return query.list();
	}
	
	
}
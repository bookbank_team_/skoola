// license-header java merge-point
/**
 * Attention: Generated source! Do not modify by hand!
 * Generated by HibernateEntityFactory.vsl
 */
package com.dg.skoola.entity;

/**
 * Is able to find and create objects of type Image.
 * The Hibernate <em>subclass</em> inheritance strategy is followed here.
      * 
 * <p>
 * @author Neme I
 * @see  Image object
 * @since  $date
 */
public abstract class ImageFactory
{
   /**
    * Creates a(n) Image object.
    *
    * @param name
    * @param image
    * @param imageId
    * @param description
    * @return {@link Image} the created object
    */
    public static Image create (java.lang.String name, byte[] image, java.lang.Long imageId, java.lang.String description)
    {
        Image object = new Image();

        object.setName (name);
        object.setImage (image);
        object.setImageId (imageId);
        object.setDescription (description);

        return object;
    }

    /**
     *
     * Finds Image object by its primary key.
     * In Hibernate, this is just a call to load().
     *
     */
    public static Image findByPrimaryKey (org.hibernate.Session session, java.lang.Long id)
        throws org.hibernate.HibernateException
    {
        Image object = (Image) session.load(Image.class, id);
        return object;
    }

	
	public static java.util.Collection loadAll(org.hibernate.Session session)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT image from com.dg.skoola.entity.Image  image");
		return query.list();
	}
	
	public static java.util.Collection loadAllInRange(org.hibernate.Session session,int pageIndex, int pageSize)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT image from com.dg.skoola.entity.Image  image");
		query.setFirstResult(pageIndex);
        query.setMaxResults(pageSize);
		return query.list();
	}
	
	
}
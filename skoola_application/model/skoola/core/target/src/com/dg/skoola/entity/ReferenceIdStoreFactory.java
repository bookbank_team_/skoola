// license-header java merge-point
/**
 * Attention: Generated source! Do not modify by hand!
 * Generated by HibernateEntityFactory.vsl
 */
package com.dg.skoola.entity;

/**
 * Is able to find and create objects of type ReferenceIdStore.
 * The Hibernate <em>subclass</em> inheritance strategy is followed here.
      * 
 * <p>
 * @author Neme I
 * @see  ReferenceIdStore object
 * @since  $date
 */
public abstract class ReferenceIdStoreFactory
{
   /**
    * Creates a(n) ReferenceIdStore object.
    *
    * @param referenceId
    * @param dateGenerated
    * @param consumed
    * @param referenceIdType
    * @return {@link ReferenceIdStore} the created object
    */
    public static ReferenceIdStore create (java.lang.String referenceId, java.sql.Timestamp dateGenerated, boolean consumed, com.dg.skoola.enumeration.ReferenceIdType referenceIdType)
    {
        ReferenceIdStore object = new ReferenceIdStore();

        object.setReferenceId (referenceId);
        object.setDateGenerated (dateGenerated);
        object.setConsumed (consumed);
        object.setReferenceIdType (referenceIdType);

        return object;
    }

    /**
     *
     * Finds ReferenceIdStore object by its primary key.
     * In Hibernate, this is just a call to load().
     *
     */
    public static ReferenceIdStore findByPrimaryKey (org.hibernate.Session session, java.lang.Long id)
        throws org.hibernate.HibernateException
    {
        ReferenceIdStore object = (ReferenceIdStore) session.load(ReferenceIdStore.class, id);
        return object;
    }

	
	public static java.util.Collection loadAll(org.hibernate.Session session)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT referenceIdStore from com.dg.skoola.entity.ReferenceIdStore  referenceIdStore");
		return query.list();
	}
	
	public static java.util.Collection loadAllInRange(org.hibernate.Session session,int pageIndex, int pageSize)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT referenceIdStore from com.dg.skoola.entity.ReferenceIdStore  referenceIdStore");
		query.setFirstResult(pageIndex);
        query.setMaxResults(pageSize);
		return query.list();
	}
	
	
}
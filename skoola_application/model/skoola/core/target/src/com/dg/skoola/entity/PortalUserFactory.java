// license-header java merge-point
/**
 * Attention: Generated source! Do not modify by hand!
 * Generated by HibernateEntityFactory.vsl
 */
package com.dg.skoola.entity;

/**
 * Is able to find and create objects of type PortalUser.
 * The Hibernate <em>subclass</em> inheritance strategy is followed here.
      * 
 * <p>
 * @author Neme I
 * @see  PortalUser object
 * @since  $date
 */
public abstract class PortalUserFactory
{
   /**
    * Creates a(n) PortalUser object.
    *
    * @param firstname
    * @param lastname
    * @param email
    * @param phoneNumber
    * @param password
    * @param passWordEncrypted
    * @param passwordReset
    * @param userName
    * @param gender
    * @param dateCreated
    * @param lastPasswordResetDate
    * @param userId
    * @param passwordSalt
    * @param passwordEncryptionAlgo
    * @param active
    * @return {@link PortalUser} the created object
    */
    public static PortalUser create (java.lang.String firstname, java.lang.String lastname, java.lang.String email, java.lang.String phoneNumber, java.lang.String password, java.lang.Boolean passWordEncrypted, java.lang.Boolean passwordReset, java.lang.String userName, java.lang.String gender, java.sql.Timestamp dateCreated, java.sql.Timestamp lastPasswordResetDate, java.lang.String userId, java.lang.String passwordSalt, java.lang.String passwordEncryptionAlgo, java.lang.Boolean active)
    {
        PortalUser object = new PortalUser();

        object.setFirstname (firstname);
        object.setLastname (lastname);
        object.setEmail (email);
        object.setPhoneNumber (phoneNumber);
        object.setPassword (password);
        object.setPassWordEncrypted (passWordEncrypted);
        object.setPasswordReset (passwordReset);
        object.setUserName (userName);
        object.setGender (gender);
        object.setDateCreated (dateCreated);
        object.setLastPasswordResetDate (lastPasswordResetDate);
        object.setUserId (userId);
        object.setPasswordSalt (passwordSalt);
        object.setPasswordEncryptionAlgo (passwordEncryptionAlgo);
        object.setActive (active);

        return object;
    }

    /**
     *
     * Finds PortalUser object by its primary key.
     * In Hibernate, this is just a call to load().
     *
     */
    public static PortalUser findByPrimaryKey (org.hibernate.Session session, java.lang.Long id)
        throws org.hibernate.HibernateException
    {
        PortalUser object = (PortalUser) session.load(PortalUser.class, id);
        return object;
    }

	
	public static java.util.Collection loadAll(org.hibernate.Session session)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT portalUser from com.dg.skoola.entity.PortalUser  portalUser");
		return query.list();
	}
	
	public static java.util.Collection loadAllInRange(org.hibernate.Session session,int pageIndex, int pageSize)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT portalUser from com.dg.skoola.entity.PortalUser  portalUser");
		query.setFirstResult(pageIndex);
        query.setMaxResults(pageSize);
		return query.list();
	}
	
	
}
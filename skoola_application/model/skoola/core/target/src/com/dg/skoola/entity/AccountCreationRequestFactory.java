// license-header java merge-point
/**
 * Attention: Generated source! Do not modify by hand!
 * Generated by HibernateEntityFactory.vsl
 */
package com.dg.skoola.entity;

/**
 * Is able to find and create objects of type AccountCreationRequest.
 * The Hibernate <em>subclass</em> inheritance strategy is followed here.
      * 
 * <p>
 * @author Neme I
 * @see  AccountCreationRequest object
 * @since  $date
 */
public abstract class AccountCreationRequestFactory
{
   /**
    * Creates a(n) AccountCreationRequest object.
    *
    * @param firstName
    * @param lastName
    * @param email
    * @param phoneNumber
    * @param activationCode
    * @param gender
    * @param dateCreated
    * @param activated
    * @param resendActivationCode
    * @param password
    * @param passwordSalt
    * @param passwordEncryptionAlgo
    * @return {@link AccountCreationRequest} the created object
    */
    public static AccountCreationRequest create (java.lang.String firstName, java.lang.String lastName, java.lang.String email, java.lang.String phoneNumber, java.lang.String activationCode, java.lang.String gender, java.sql.Timestamp dateCreated, java.lang.Boolean activated, java.lang.Boolean resendActivationCode, java.lang.String password, java.lang.String passwordSalt, java.lang.String passwordEncryptionAlgo)
    {
        AccountCreationRequest object = new AccountCreationRequest();

        object.setFirstName (firstName);
        object.setLastName (lastName);
        object.setEmail (email);
        object.setPhoneNumber (phoneNumber);
        object.setActivationCode (activationCode);
        object.setGender (gender);
        object.setDateCreated (dateCreated);
        object.setActivated (activated);
        object.setResendActivationCode (resendActivationCode);
        object.setPassword (password);
        object.setPasswordSalt (passwordSalt);
        object.setPasswordEncryptionAlgo (passwordEncryptionAlgo);

        return object;
    }

    /**
     *
     * Finds AccountCreationRequest object by its primary key.
     * In Hibernate, this is just a call to load().
     *
     */
    public static AccountCreationRequest findByPrimaryKey (org.hibernate.Session session, java.lang.Long id)
        throws org.hibernate.HibernateException
    {
        AccountCreationRequest object = (AccountCreationRequest) session.load(AccountCreationRequest.class, id);
        return object;
    }

	
	public static java.util.Collection loadAll(org.hibernate.Session session)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT accountCreationRequest from com.dg.skoola.entity.AccountCreationRequest  accountCreationRequest");
		return query.list();
	}
	
	public static java.util.Collection loadAllInRange(org.hibernate.Session session,int pageIndex, int pageSize)
		throws org.hibernate.HibernateException
	{
		org.hibernate.Query query = session.createQuery("SELECT accountCreationRequest from com.dg.skoola.entity.AccountCreationRequest  accountCreationRequest");
		query.setFirstResult(pageIndex);
        query.setMaxResults(pageSize);
		return query.list();
	}
	
	
}
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraph.org/schema/"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
<meta property="og:title" content="Mobile App Visual Flowcharts">
<meta property="fb:page_id" content="43929265776">        
        
        <title>Skoola.com</title>
        <meta name="viewport" content="width=device-width">
        <!-- <meta name="viewport" content="width=device-width, user-scalable=no" /> -->
        
    <style type="text/css">
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTaOCaDZZVv73zpFSwE4Va2k.woff2) format('woff2');              unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTdWlIHla9B101mdmTHF3-q0.woff2) format('woff2');              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTVMRqIICSqEqsEoDxn8ddME.woff2) format('woff2');              unicode-range: U+02BC, U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200B-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTaWHppw2c1XOp6B2yhU8z7c.woff2) format('woff2');              unicode-range: U+1F00-1FFF;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTeji7H8UD0RUWSM-55zrR4g.woff2) format('woff2');              unicode-range: U+0370-03FF;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTcw0n1X1lV_hRH3yZFpIE9Q.woff2) format('woff2');              unicode-range: U+0102-0103, U+1EA0-1EF1, U+20AB;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTSYtBUPDK3WL7KRKS_3q7OE.woff2) format('woff2');              unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 300;              src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v10/DXI1ORHCpsQm3Vp6mXoaTRampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNSqOCaDZZVv73zpFSwE4Va2k.woff2) format('woff2');              unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNStWlIHla9B101mdmTHF3-q0.woff2) format('woff2');              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNSlMRqIICSqEqsEoDxn8ddME.woff2) format('woff2');              unicode-range: U+02BC, U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200B-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNSqWHppw2c1XOp6B2yhU8z7c.woff2) format('woff2');              unicode-range: U+1F00-1FFF;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNSuji7H8UD0RUWSM-55zrR4g.woff2) format('woff2');              unicode-range: U+0370-03FF;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNSsw0n1X1lV_hRH3yZFpIE9Q.woff2) format('woff2');              unicode-range: U+0102-0103, U+1EA0-1EF1, U+20AB;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNSiYtBUPDK3WL7KRKS_3q7OE.woff2) format('woff2');              unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;}
	@font-face {font-family: 'Open Sans';              font-style: normal;              font-weight: 600;              src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://fonts.gstatic.com/s/opensans/v10/MTP_ySUJH_bn48VBG8sNShampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;}
</style>        
               <style type="text/css">
            /* Facebook/Google+ Modals */
            #social-proxy { background:#fff; -webkit-box-shadow: 4px 4px 8px 2px rgba(0,0,0,.2); box-shadow: 4px 4px 8px 2px rgba(0,0,0,.2); padding-bottom:35px; z-index:1000; }
            #social-proxy_close { display:block; position:absolute; top:0; right:0; height:30px; width:32px; background:transparent url(http://cdn-images.mailchimp.com/awesomebar-sprite.png) 0 -200px; text-indent:-9999px; outline:none; font-size:1px; }
            #social-proxy_close:hover { background-position:0 -240px; }
            body { padding-bottom:50px !important; }
        </style> </head>
    
    <body style="min-width: 600px; padding-top: 20px; margin: 0px; background: #EAECED; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; color: #777; line-height: 30px;">

        <table style="width: 570px; margin: 0px auto;" cellpadding="0" cellspacing="0">

            <!-- Header -->
            <tbody><tr>
                <td>
                    <table width="100%" style="margin-bottom: 10px;background: #fff;      padding: 0px;      border-radius: 4px;      overflow: hidden;" bgcolor="#fff">
                        <tbody><tr>
                            <td style="font-size:12px;font-weight:200;">
                                <a href="skoola.com" style="float:left">
                                    <img src="http://skoola.com/assets/img/skoola-logo.png" height="50px;">
                                </a>
                                <h2 style="float:left; font-weight: 200; font-size: 24px; color: #555; line-height: 50px; margin:0 0 0 20px;padding:0px;"><small> Study abroad made simple </small></h2>
                            </td>
                            
                        </tr>
                    </tbody></table>
                </td>
            </tr>



            <!-- Feature 1 -->
            <tr>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:#fff;padding: 0px; border-radius:4px; overflow:hidden; margin-bottom: 5px;">
                        <tbody>
                        <tr>
                            <td style="padding: 30px 70px 50px 70px; /* text-align:center; */"><h4 style="font-weight: 200; font-size: 26px; line-height: 38px; color: #444; line-height: 36px; margin-bottom: 30px;">
							Hello ${userName},</h4>

<p style="padding-bottom: 30px; color: #888">${mailContent}
</p>

</td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>


            <!-- Feature 2 -->
            



            <!-- Feature 3 -->
            


            <!-- Feature 4 -->
            




            <!-- Bundle message -->
            <tr>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:#fff;padding: 0px;border-radius:4px; margin-bottom: 10px;">
                        <!-- Intro Section -->
                        <tbody><tr>
                            <td style="padding: 30px 70px; /* text-align:center; */"><p mg:edit="bundle_blurb" style="padding-bottom: 0px;font-size: 12px;">We are the number 1 and most trusted platform for overseas study. Thousands of students use Skoola to find the right the right schools; make applications & get admitted to the best schools in Europe, US, Asia and South America.<br>Visit <a href="skoola.com">skoola.com</a> For more information. <br><br>
-The Skoola Team</p>
</td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>



            <!-- Social Links -->
            
            
            
            <!-- UI8 Branding -->
            
        


            <!-- Footer -->
            <tr>
                <td style="padding-top:20px;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tbody><tr style="font-size:10px; color: #777;">
                            <td align="middle" valign="top">Copyright © 2016 SKOOLA.COM, All rights reserved.</td>
                            <!--<td align="right" valign="top">-->
                                <!--<a href="skoola.com" style="font-weight:200 !important;color:#aaa;text-decoration:none;border-bottom:1px solid #999;">Unsubscribe</a>-->
                            <!--</td>-->
                        </tr>
                    </tbody></table>
                </td>
            </tr>


            

        </tbody></table>
        
        

</body></html>
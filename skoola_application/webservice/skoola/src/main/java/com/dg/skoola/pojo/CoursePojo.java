package com.dg.skoola.pojo;

public class CoursePojo {
	
	private long id;
	private String name;
	private String courseDuration;
	private String courseSummary;
	private boolean ieltsRequired;
	private String courseRequirements;
	private String programModules;
	private String courseVenue;
	private String courseFeeInCents;
	private String courseSubjectName;
	private String courseSubjectGroupName;
	private String courseSubjectGroupId;
	private String courseType;
	private String courseSchoolName;
	private String courseSubjectId;
	private String courseFeeInWords;
	private String courseTypeId;
	private String courseSchoolId;
	private String courseSchoolLogoUrl;
	private String courseFeeInNaira;
	private String courseStartDate;
	
	
	public String getCourseSubjectId() {
		return courseSubjectId;
	}
	public void setCourseSubjectId(String courseSubjectId) {
		this.courseSubjectId = courseSubjectId;
	}
	
	
	
	public String getCourseTypeId() {
		return courseTypeId;
	}
	public void setCourseTypeId(String courseTypeId) {
		this.courseTypeId = courseTypeId;
	}
	public String getCourseSchoolId() {
		return courseSchoolId;
	}
	public void setCourseSchoolId(String courseSchoolId) {
		this.courseSchoolId = courseSchoolId;
	}
	public String getCourseSchoolLogoUrl() {
		return courseSchoolLogoUrl;
	}
	public void setCourseSchoolLogoUrl(String courseSchoolLogoUrl) {
		this.courseSchoolLogoUrl = courseSchoolLogoUrl;
	}
	public String getCourseFeeInNaira() {
		return courseFeeInNaira;
	}
	public void setCourseFeeInNaira(String courseFeeInNaira) {
		this.courseFeeInNaira = courseFeeInNaira;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCourseDuration() {
		return courseDuration;
	}
	public void setCourseDuration(String courseDuration) {
		this.courseDuration = courseDuration;
	}

	public String getCourseFeeInCents() {
		return courseFeeInCents;
	}
	public void setCourseFeeInCents(String courseFeeInCents) {
		this.courseFeeInCents = courseFeeInCents;
	}
	
	public String getCourseType() {
		return courseType;
	}
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCourseSummary() {
		return courseSummary;
	}
	public void setCourseSummary(String courseSummary) {
		this.courseSummary = courseSummary;
	}
	public boolean isIeltsRequired() {
		return ieltsRequired;
	}
	public void setIeltsRequired(boolean ieltsRequired) {
		this.ieltsRequired = ieltsRequired;
	}
	public String getCourseRequirements() {
		return courseRequirements;
	}
	public void setCourseRequirements(String courseRequirements) {
		this.courseRequirements = courseRequirements;
	}
	public String getProgramModules() {
		return programModules;
	}
	public void setProgramModules(String programModules) {
		this.programModules = programModules;
	}
	public String getCourseVenue() {
		return courseVenue;
	}
	public void setCourseVenue(String courseVenue) {
		this.courseVenue = courseVenue;
	}

	public String getCourseSchoolName() {
		return courseSchoolName;
	}
	public void setCourseSchoolName(String courseSchoolName) {
		this.courseSchoolName = courseSchoolName;
	}
	public String getCourseStartDate() {
		return courseStartDate;
	}
	public void setCourseStartDate(String courseStartDate) {
		this.courseStartDate = courseStartDate;
	}
	public String getCourseSubjectGroupName() {
		return courseSubjectGroupName;
	}
	public void setCourseSubjectGroupName(String courseSubjectGroupName) {
		this.courseSubjectGroupName = courseSubjectGroupName;
	}
	public String getCourseSubjectName() {
		return courseSubjectName;
	}
	public void setCourseSubjectName(String courseSubjectName) {
		this.courseSubjectName = courseSubjectName;
	}
	public String getCourseFeeInWords() {
		return courseFeeInWords;
	}
	public void setCourseFeeInWords(String courseFeeInWords) {
		this.courseFeeInWords = courseFeeInWords;
	}
	public String getCourseSubjectGroupId() {
		return courseSubjectGroupId;
	}
	public void setCourseSubjectGroupId(String courseSubjectGroupId) {
		this.courseSubjectGroupId = courseSubjectGroupId;
	}

	
	
	
	
	

}

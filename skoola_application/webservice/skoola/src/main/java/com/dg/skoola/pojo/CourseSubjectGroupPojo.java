package com.dg.skoola.pojo;

public class CourseSubjectGroupPojo {
	
	private String id;
	private String type;
	private String courseSubjectGroupName;
	private String courseSubjectGroupDisplayName;
	private String numberOfCourses;
	private String numberOfUniversities;

	
	
	
	public String getCourseSubjectGroupName() {
		return courseSubjectGroupName;
	}
	public void setCourseSubjectGroupName(String courseSubjectGroupName) {
		this.courseSubjectGroupName = courseSubjectGroupName;
	}
	public String getCourseSubjectGroupDisplayName() {
		return courseSubjectGroupDisplayName;
	}
	public void setCourseSubjectGroupDisplayName(
			String courseSubjectGroupDisplayName) {
		this.courseSubjectGroupDisplayName = courseSubjectGroupDisplayName;
	}
	public String getNumberOfCourses() {
		return numberOfCourses;
	}
	public void setNumberOfCourses(String numberOfCourses) {
		this.numberOfCourses = numberOfCourses;
	}
	public String getNumberOfUniversities() {
		return numberOfUniversities;
	}
	public void setNumberOfUniversities(String numberOfUniversities) {
		this.numberOfUniversities = numberOfUniversities;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	

}

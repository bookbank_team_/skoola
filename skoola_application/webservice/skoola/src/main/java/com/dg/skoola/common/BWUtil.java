package com.dg.skoola.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;


import com.google.gson.Gson;
/**
 * @author  Nwanya Justin
 * @version 1.0
 * @since   2015-08-26 
*/
 
public class BWUtil {
	
	public static boolean isInvalidLongString(String value) {
		return value == null || value.isEmpty() || value.length() > 256;
	}
	public static boolean isInvalidShortString(String value) {
		return value == null || value.isEmpty() || value.length() > 30;
	}
	public static boolean isInValidEmail(String text) {
		return !text.toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	}
	public static long getLongValue(String longValue)
	  {
		  long value = 0l;
		  try{
			  value = Long.parseLong(longValue);
		  }catch(NumberFormatException ex)
		  {
		  }
		  return value;
	  }
	public static int getIntegerValue(String intValue)
	  {
		  int value = 0;
		  try{
			  value = Integer.parseInt(intValue);
		  }catch(NumberFormatException ex)
		  {
		  }
		  return value;
	  }
	public static String getStringValue(String value)
	  {
		  if(value == null){
			  return "";
		  }
		  return value;
	  }
	public static String showErrorMessage(String message)
	{
		String errorMessage = "";
		if( message != null && !message.isEmpty())
		{
			errorMessage="<div class='alert alert-danger' role='alert'>"
					    + "<button type='button' class='close' data-dismiss='alert'><i class='ace-icon fa fa-times'></i></button>"
					    + "<strong>Error: </strong>"
	                 +message+"</div>";	
		}
		return errorMessage;
	}
	public static String showSuccessMessage(String message)
	{
		String errorMessage = "";
		if( message != null && !message.isEmpty())
		{
			errorMessage="<div class='alert alert-success' role='alert'>"
					 + "<button type='button' class='close' data-dismiss='alert'><i class='ace-icon fa fa-times'></i></button>"
					 +"<strong>Success: </strong>"
		             +message+"</div>";   
		}
		return errorMessage;
	}
	public static boolean isInvalidPhoneNumber(String value) {
		return value == null || value.isEmpty() || !value.matches("^[0-9]{6,}$");
		//!value.matches("^[0-9]{6,}$")
	}
	public static String showInformationMessage(String message)
	{
		String infoMessage = "";
		if( message != null && !message.isEmpty())
		{
			infoMessage="<div class='alert alert-info' role='alert'>"
					 + "<button type='button' class='close' data-dismiss='alert'><i class='ace-icon fa fa-times'></i></button>"
		              +"<strong>Info: </strong> "+message+"</div>";

		}
		return infoMessage;
	}
	public static boolean isValidImageFileExtension(String fileName)
	{
			boolean isValid = false;
			String extension = fileName.substring(fileName.lastIndexOf('.')+1,fileName.length());
			String[] extList = {"jpg","jpeg","png","gif"};
			for(String ext : extList)
			{
				if(ext.equalsIgnoreCase(extension))
				{
					return true;
				}
			}
			return isValid;
	}
	public static boolean isNotXlsx(File file) {
		String ext = FilenameUtils.getExtension(file.getName());
		return !ext.equalsIgnoreCase("xlsx");
	}
	public static byte[] getImageThumbnail(InputStream inputStream,int width,int height){
		try {
			BufferedImage img = ImageIO.read(inputStream);
			BufferedImage thumbImg = Scalr.resize(img,Method.QUALITY,Mode.AUTOMATIC, width,width, Scalr.OP_ANTIALIAS);
			ByteArrayOutputStream data = new ByteArrayOutputStream();
			ImageIO.write(thumbImg, "png", data);
			return data.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	/*public static FileTypeConstant getFileType(String fileName){
		String extension = fileName.substring(fileName.lastIndexOf('.')+1,fileName.length());
		if(extension.equalsIgnoreCase(FileTypeConstant.BMP.toString())){
			return FileTypeConstant.BMP;
		}else if(extension.equalsIgnoreCase(FileTypeConstant.JPEG.toString())){
			return  FileTypeConstant.JPEG;
		}else if(extension.equalsIgnoreCase(FileTypeConstant.JPG.toString())){
			return  FileTypeConstant.JPG;
		}else if(extension.equalsIgnoreCase(FileTypeConstant.PNG.toString())){
			return  FileTypeConstant.PNG;
		}else return  FileTypeConstant.GIF;
	}*/
	
	private static String priceWithDecimal (Double price) {
	    DecimalFormat formatter = new DecimalFormat("###,###,###.00");
	    return formatter.format(price);
	}

  private static String priceWithoutDecimal (Double price) {
	    DecimalFormat formatter = new DecimalFormat("###,###,###.##");
	    return formatter.format(price);
	}

	public static String getFormattedKoboPrice(long value) {
		Double price = (double) value;
		price = price / 100;
	    String toShow = priceWithoutDecimal(price);
	    if (toShow.indexOf(".") > 0) {
	        return priceWithDecimal(price);
	    } else {
	        return priceWithoutDecimal(price);
	    }
	}
	public static Timestamp getMonthStartDayTimestampByMonthValue(int monthValue, int year)
	{
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		cal.set(currentYear,monthValue, 1,0,0,0);
		Timestamp timestamp = new Timestamp(cal.getTime().getTime());
		return timestamp;
	}
	public static Timestamp getMonthEndDayTimestampByMonthValue(int monthValue, int year)
	{
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		int endDay = getNumberOfDaysInMonth(monthValue);
		cal.set(currentYear,monthValue, endDay,23,59,59);
		Timestamp timestamp = new Timestamp(cal.getTime().getTime());
		return timestamp;
	}
	private static int getNumberOfDaysInMonth(int monthValue)
	 {
		 int numberOfDays = 0;
		 switch(monthValue)
		 {
		  case 3:
		  case 5:
		  case 8:
		  case 10:
			  numberOfDays = 30;
			  break;
		  case 1:
			  numberOfDays = 28;
			  break;
		  default: numberOfDays = 31;
		 }
		 return numberOfDays;
	 }
	public static String getFormattedDateMonth(Timestamp timestamp)
	  {
	    String formattedDate = "";
	    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, yyyy");
	    formattedDate = formatter.format(timestamp);
	    return formattedDate.toUpperCase();
	  }
	public static String getMonthName(Timestamp end)
	{
		
		Date date = new Date(end.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		//int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		//int year = cal.get(Calendar.YEAR);
		String endMonth = getMonthName(month);	
		//String periodName = endMonth+" "+day+", "+year;
		String periodName = endMonth;
		return periodName;
	}
	public static String getMonthNameYear(Timestamp end)
	{
		
		Date date = new Date(end.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		//int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		String endMonth = getMonthName(month);	
		//String periodName = endMonth+" "+day+", "+year;
		String periodName = endMonth+" "+year;
		return periodName;
	}
	private static String getMonthName(int monthValue)
	{
		String monthName = "";
		switch(monthValue)
		{
		  case 0: monthName = "JANUARY";break;
		  case 1: monthName = "FEBUARY";break;
		  case 2: monthName = "MARCH";break;
		  case 3: monthName = "APRIL";break;
		  case 4: monthName = "MAY";break;
		  case 5: monthName = "JUNE";break;
		  case 6: monthName = "JULY";break;
		  case 7: monthName = "AUGUST";break;
		  case 8: monthName = "SEPTEMBER";break;
		  case 9: monthName = "OCTOBER";break;
		  case 10: monthName = "NOVEMBER";break;
		  case 11: monthName = "DECEMBER";break;
		}
		return monthName;
	}
	/*public static void sendGSONObjects(Object[] response, ResourceResponse resp) {
	    Gson gson = new Gson();
	    String jsonString = gson.toJson(response);
	    resp.setContentType("text/json");
	    PrintWriter pw = null;
	    try{
	      pw = resp.getWriter();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    if (pw != null) {
	      pw.write(jsonString);
	      pw.flush();
	      pw.close();
	    }
	  }*/

}

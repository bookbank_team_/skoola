package com.dg.skoola.util;
/*package com.dg.skoola.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ims.enumeration.DocumentType;
import com.ims.ws.common.IMSJSONResponseObject;

public class WSUtil {

	public static String constructClientResponse(boolean status,String message){
		String response = "";
		try{
			JsonObject responseObject = new JsonObject();
			responseObject.addProperty("successful", status);
	        responseObject.addProperty("message", message);
	        response =  new Gson().toJson(responseObject);
		}catch(Exception ex){
			
		}
		return response;
	}
	public static String sendResponseToClient(boolean status,String message,String data){
		String response = "";
		try{
			IMSJSONResponseObject clientResponse = new IMSJSONResponseObject();
	        clientResponse.setSuccessful(status);
	        clientResponse.setMessage(message);
	        clientResponse.setData(data);
	        return new Gson().toJson(clientResponse);
		}catch(Exception ex){
			
		}
		return response;
	}
	public static DocumentType getDocumentType(String type){
		if(type.endsWith(Constant.DOC_DEGREE_FILE)){
			return DocumentType.MEDICAL_DEGREE_FILE;
		}else if(type.endsWith(Constant.DOC_REG_FILE)){
			return DocumentType.MEDICAL_REGISTRATION_FILE;
		}else if(type.endsWith(Constant.ID_CARD_FILE)){
			return DocumentType.IDENTITY_CARD;
		}else return DocumentType.OTHER_FILE;
	}
	
	public static byte[] createImageThumbnail(byte[] imageData,int width,int height){
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
			BufferedImage img = ImageIO.read(bais);
			BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC, width,height, Scalr.OP_ANTIALIAS);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(thumbImg,"png",os);
			
			return os.toByteArray();
		} catch (IOException e) {
			
			e.printStackTrace();
			return null;
		}
	}
}
*/
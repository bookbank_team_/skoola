package com.dg.skoola.careerconsultation;

import java.sql.Timestamp;
import java.util.Date;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.dg.skoola.ServiceLocator;
import com.dg.skoola.common.CustomService;
import com.dg.skoola.common.SkoolaJSONRequestObject;
import com.dg.skoola.common.SkoolaJSONResponseObject;
import com.dg.skoola.entity.ConsultationRequest;
import com.dg.skoola.entity.Course;
import com.dg.skoola.entity.Feeschedule;
import com.dg.skoola.entity.PaymentHistory;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.ProcessType;
import com.dg.skoola.enumeration.ConsultationModeConstant;
import com.dg.skoola.enumeration.ConsultationStatusConstant;
import com.dg.skoola.enumeration.PaymentStatusConstant;
import com.dg.skoola.payment.PaymentHandler;
import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;

import sun.misc.BASE64Encoder;

@Path("/career_consultation")
@WebService
public class CareerConsultationHandler {
	
	//private HashValidator hashValidator = HashValidator.getInstance();
	private Logger log = Logger.getLogger(CareerConsultationHandler.class);
	private SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
	private CustomService skoolaCustomService = CustomService.getInstance();
	private BASE64Encoder encoder = new BASE64Encoder();
	private Gson gson = new Gson();
	private JSONParser jsonParser = new JSONParser();
	
	

    @POST @Path("/create_consultation_request")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String createConsultationRequest(String data) {
    	SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
    	SkoolaJSONResponseObject responseObject =  new SkoolaJSONResponseObject();
    	String wsData = skoolaJSONRequestObject.getData();
    	//ConsultationModeConstant.
        try {
			if (wsData == null) {
			 
				System.out.println(" hash validation failed");
			    responseObject = new SkoolaJSONResponseObject();
			    responseObject.setSuccessful(false);
			    responseObject.setMessage( "Hash Validation Failed");

			    return gson.toJson(responseObject);
			} else {

			    System.out.println(" hash validation passed");
			    JSONParser parser = new JSONParser();
			    JSONObject requestObject = (JSONObject) parser.parse(wsData);

			    String action = (String) requestObject.get("action");
			    String portalUserId = (String) requestObject.get("portalUserId");
			    String courseId = (String) requestObject.get("courseId");
			    String consultationMode = (String) requestObject.get("consultationMode");

			    System.out.println("action = " + action );
			    System.out.println("portalUserId = " + portalUserId );
			    System.out.println("courseId = " + courseId );
			    System.out.println("consultationMode = " + consultationMode );
			    
			    if(action.equalsIgnoreCase("createConsultationRequest") && !portalUserId.isEmpty() && !portalUserId.isEmpty() && !courseId.isEmpty() && !consultationMode.isEmpty()){
			    	
			    	ConsultationModeConstant cMode = null;
			    	if(consultationMode.equalsIgnoreCase(ConsultationModeConstant.IN_HOUSE.getValue())){
			    		cMode = ConsultationModeConstant.IN_HOUSE;
			    	}else if(consultationMode.equalsIgnoreCase(ConsultationModeConstant.VIRTUAL.getValue())){
			    		cMode = ConsultationModeConstant.VIRTUAL;
			    	}
			    	
			    	PortalUser portalUser = (PortalUser) skoolaService.getRecordById(PortalUser.class, Long.valueOf(portalUserId));
			    	
			    	if(portalUser == null){
			    	    responseObject = new SkoolaJSONResponseObject();
					    responseObject.setSuccessful(false);
					    responseObject.setMessage( "User cannot be null, contact support");

					    return gson.toJson(responseObject);
			    	}
			    	
			    	Course course = (Course) skoolaService.getRecordById(Course.class, Long.valueOf(courseId));

			    	if(course == null){
			    		responseObject = new SkoolaJSONResponseObject();
					    responseObject.setSuccessful(false);
					    responseObject.setMessage( "Course cannot be null, contact support");

					    return gson.toJson(responseObject);
			    	}
			    	
			    	
			    	ConsultationRequest consultationRequest = new ConsultationRequest();
			    	
			    	consultationRequest.setDateCreated(new Timestamp(new Date().getTime()));
			    	consultationRequest.setConsultationMode(cMode);
			    	consultationRequest.setConsultationStatus(ConsultationStatusConstant.NOT_PAID);
			    	consultationRequest.setCourse(course);
			    	consultationRequest.setCreatedBy(portalUser);
			    	consultationRequest.setEmail(portalUser.getEmail());
			    	consultationRequest.setFirstname(portalUser.getFirstname());
			    	consultationRequest.setLastname(portalUser.getLastname());
			    	consultationRequest.setPhoneNumber(portalUser.getPhoneNumber());
			    	
			    	
			    	consultationRequest = (ConsultationRequest) skoolaService.createNewRecord(consultationRequest);
			    	
			    	JSONObject jsonObject = new JSONObject();
			    	jsonObject.put("consultationRequestId", consultationRequest.getId().toString());
			    	
			    	responseObject = new SkoolaJSONResponseObject();
			    	responseObject.setData(jsonObject.toJSONString());
				    responseObject.setSuccessful(true);
				    responseObject.setMessage( "Successful");

				    return gson.toJson(responseObject);
			    	
			    }
			    
			    
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	return new Gson().toJson(responseObject);
    }
	
	


}

package com.dg.skoola.util;

/**
 * Created by JB on 26/09/2015.
 */
public class Constant {
	
	public static final String NOTIFICATION_EMAIL_ADDRESS = "NOTIFICATION_EMAIL_ADDRESS";
	public static final String NOTIFICATION_EMAIL_PASSWORD = "NOTIFICATION_EMAIL_PASSWORD";
	public static final String NOTIFICATION_SMTP_HOST = "NOTIFICATION_SMTP_HOST";
	public static final String MAIL_ENGINE_URL = "MAIL_ENGINE_URL";
}

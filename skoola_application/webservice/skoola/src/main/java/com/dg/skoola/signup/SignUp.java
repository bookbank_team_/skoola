package com.dg.skoola.signup;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import sun.misc.BASE64Encoder;














import com.dg.skoola.ServiceLocator;
import com.dg.skoola.common.CustomEmailEngine;
import com.dg.skoola.common.CustomService;
import com.dg.skoola.common.Notifier;
import com.dg.skoola.common.SkoolaConstants;
import com.dg.skoola.common.SkoolaJSONRequestObject;
import com.dg.skoola.common.SkoolaJSONResponseObject;
import com.dg.skoola.common.SkoolaUtil;
import com.dg.skoola.entity.AccountCreationRequest;
import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.PortalUserRoleMap;
import com.dg.skoola.entity.Role;
import com.dg.skoola.enumeration.RoleTypeConstant;
import com.dg.skoola.service.SkoolaService;
import com.dg.skoola.util.SendMail;
import com.google.gson.Gson;
import com.sun.xml.internal.fastinfoset.algorithm.BuiltInEncodingAlgorithm.WordListener;

@Path("/signup")
@WebService
public class SignUp {
	
	private Logger log = Logger.getLogger(SignUp.class);
	private SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
	private CustomService skoolaCustomService = CustomService.getInstance();
	private BASE64Encoder encoder = new BASE64Encoder();
	private Gson gson = new Gson();
	private JSONParser jsonParser = new JSONParser();
	
	 	@GET @Path("/test")
	    @Consumes({MediaType.TEXT_PLAIN})
	    @Produces({MediaType.TEXT_PLAIN})
	    public String test(String data,@Context HttpServletRequest request) {
	    	System.out.println(" data = " + data);
	    	SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
	    	System.out.println( " inside test  ");
	    	SkoolaJSONResponseObject SkoolaJSONResponseObject =  new SkoolaJSONResponseObject();
	    	SkoolaJSONResponseObject.setData("Welcome to Skoola SearchHandler ");
	    	SkoolaJSONResponseObject.setSuccessful(true);
	       
	    	try {
	    		String serverURL = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/skoola/skoola_api/signup/test";
	    		SkoolaJSONResponseObject.setData(serverURL);
				skoolaService.getAllRecords(Country.class);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	    	return new Gson().toJson(SkoolaJSONResponseObject);
	    }
	    
	 	
	 	
	 	@POST @Path("/create_signup_request")
	    @Consumes({MediaType.TEXT_PLAIN})
	    @Produces({MediaType.TEXT_PLAIN})
	    public String createSignUpRequest(String data,@Context HttpServletRequest request) {
	 		System.out.println(" inside create signup request ");
	    	SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
	    	SkoolaJSONResponseObject skoolaJSONResponseObject =  new SkoolaJSONResponseObject();

	       
	    	try {
	    		
	    		if(!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)){
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());
	    		
	    		if(jsonObject == null){
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		String firstName = (String) jsonObject.get("firstName");
	    		String lastName = (String) jsonObject.get("lastName");
	    		String email = (String) jsonObject.get("email");
	    		String phoneNumber = (String) jsonObject.get("phoneNumber");
	    		String password = (String) jsonObject.get("password");
	    		
	    		if(firstName == null || lastName == null || email == null  || phoneNumber == null || password == null || 
	    			firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || phoneNumber.isEmpty() || password.isEmpty() ){
	    			
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		
	    		
	    		AccountCreationRequest testAccountCreationRequest = CustomService.getAccountCreationRequestByEmailAddress(email);
	    		
	    		if(testAccountCreationRequest != null){
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Sorry, this email address is already registered. Please use another one.");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		testAccountCreationRequest = CustomService.getAccountCreationRequestByPhoneNumber(phoneNumber);
	    		
	    		if(testAccountCreationRequest != null){
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Sorry, this phone number is already registered. Please use another one.");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		PortalUser testPortalUser = CustomService.getPortalUserByEmailAddress(email);
	    		
	    		if(testPortalUser != null){
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Sorry, this email address is already registered. Please use another one.");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		testPortalUser = CustomService.getPortalUserByPhoneNumber(phoneNumber);
	    		
	    		if(testPortalUser != null){
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Sorry, this phone number is already registered. Please use another one.");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		String passwordSalt = UUID.randomUUID().toString();
	    		String encryptionAlgorithm = SkoolaConstants.PASSWORD_ENCRYPTION_ALGORITHM;
	    		String digestInput = password.trim() + passwordSalt;
	    		byte[] passwordDigest = SkoolaUtil.getMessageDigest(encryptionAlgorithm, digestInput);
	    		String hexPasswordDigest = Hex.encodeHexString(passwordDigest);
	    		String activationCode = UUID.randomUUID().toString();
	    		
	    		AccountCreationRequest accountCreationRequest = new AccountCreationRequest();
	    		accountCreationRequest.setActivated(false);
	    		accountCreationRequest.setActivationCode(activationCode);
	    		accountCreationRequest.setDateCreated(new Timestamp(new Date().getTime()));
	    		accountCreationRequest.setEmail(email);
	    		accountCreationRequest.setFirstName(firstName);
	    		accountCreationRequest.setLastName(lastName);
	    		accountCreationRequest.setPassword(hexPasswordDigest);
	    		accountCreationRequest.setPasswordSalt(passwordSalt);
	    		accountCreationRequest.setPasswordEncryptionAlgo(encryptionAlgorithm);
	    		accountCreationRequest.setPhoneNumber(phoneNumber);
	    		accountCreationRequest.setResendActivationCode(false);
	    		
	    		accountCreationRequest = (AccountCreationRequest) skoolaService.createNewRecord(accountCreationRequest);
	    		
	    		String settingBaseUrl = CustomService.getSettingValue("NINJA_APP_BASE_URL", "http://localhost:8080/", true);
	    		
	    		String baseURL = settingBaseUrl+"complete_registration_request";
	    		String activationURL = baseURL + "?code="+activationCode;
	    		System.out.println(" activationURL  =" + activationURL);
	    		String userName = WordUtils.capitalizeFully(accountCreationRequest.getFirstName()) +" " + WordUtils.capitalizeFully(accountCreationRequest.getLastName());
	    	//	String messageContent = getSignupRequestEmailMessage(activationURL);
	    		String subject = "Skoola.com Account Activation";
	    		String template = SkoolaConstants.ACTIVATION_EMAIL_TEMPLATE;

	    		
	    		 HashMap<String, Object> emailParam = new HashMap<String, Object>();
		         emailParam.put("userName", userName);
		         emailParam.put("activationURL", activationURL);		        
		        // Notifier.getNotifier().sendEmail(emailParam, email,subject, template);
		        // new SendMail().sendMailInHTMLTemplate(email, subject, emailParam, template);
		         new CustomEmailEngine().sendMessageWithHTMLTemplate(email, subject, emailParam, template);
		         
		         skoolaJSONResponseObject.setData("");
		         skoolaJSONResponseObject.setMessage("Signup Request, Successful");
		         skoolaJSONResponseObject.setSuccessful(true);
		         
		         
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	    	return new Gson().toJson(skoolaJSONResponseObject);
	    }
	 	
	 	
	 	private String getSignupRequestEmailMessage(String accountActivationURL){
	 		
	 		String message = "<span>  To complete your registration on Skoola.com , <a href=\""+accountActivationURL+"\"  \\>Click Here. <\\a><\\span>"
	 				+ " <br> <span> If you did not initiate this process, kindly ignore this email.<\\span>" ;
	 		
	 		return message;
	 	}
	    
	 	
	 	@POST @Path("/activate_user_account")
	    @Consumes({MediaType.TEXT_PLAIN})
	    @Produces({MediaType.TEXT_PLAIN})
	    public String activateUserAccount(String data) {
	 		System.out.println(" inside create signup request ");
	    	SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
	    	SkoolaJSONResponseObject skoolaJSONResponseObject =  new SkoolaJSONResponseObject();

	       
	    	try {
	    		
	    		if(!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)){
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		
	    		JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());
	    		
	    		if(jsonObject == null){
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		String activationCode = (String) jsonObject.get("activationCode");
	    		
	    		
	    		if(activationCode == null ||activationCode.isEmpty() ){
	    			
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Invalid Activation Code!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		AccountCreationRequest accr = CustomService.getAccountCreationRequestByActivationCode(activationCode);
	    		
	    		if(accr == null ){
	    			
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Invalid Activation Code!");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		if(accr.getActivated()){
	    			
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setMessage("Your account has aready been activated. Use your email address and password to login.");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		String uniqueUserId = "";
	    		PortalUser testPortalUser = null;
	    		
	    		do{
	    			uniqueUserId = UUID.randomUUID().toString();			
	    			testPortalUser = CustomService.getPortalUserByUserId(uniqueUserId);
	    		
	    		}while(testPortalUser != null);
	    		
	    		PortalUser portalUser = new PortalUser();
	    		portalUser.setDateCreated(new Timestamp(new Date().getTime()));
	    		portalUser.setEmail(accr.getEmail());
	    		portalUser.setFirstname(accr.getFirstName());
	    		portalUser.setLastname(accr.getLastName());
	    		portalUser.setPassword(accr.getPassword());
	    		portalUser.setPassWordEncrypted(true);
	    		portalUser.setPasswordEncryptionAlgo(accr.getPasswordEncryptionAlgo());
	    		portalUser.setPasswordSalt(accr.getPasswordSalt());
	    		portalUser.setPhoneNumber(accr.getPhoneNumber());
	    		portalUser.setUserId(uniqueUserId);
	    		portalUser.setUserName(accr.getEmail());
	    		
	    		portalUser = (PortalUser) skoolaService.createNewRecord(portalUser);
	    		
	    		Role studentRole = CustomService.getRoleByName(RoleTypeConstant.STUDENT.getValue());

	    		if(studentRole == null){

	    			// Performing rollback
	    			skoolaService.deleteRecord(portalUser);
	    			
	    			skoolaJSONResponseObject.setMessage("Sorry, your request cannot be completed at the moment. Please contact support.");
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    			
	    		}
	    		
	    		PortalUserRoleMap portalUserRoleMap = new PortalUserRoleMap();
	    		portalUserRoleMap.setPortalUser(portalUser);
	    		portalUserRoleMap.setRole(studentRole);
	    		
	    		portalUserRoleMap = (PortalUserRoleMap) skoolaService.createNewRecord(portalUserRoleMap);
	    		
	    		
	    		// Update Account Activation Record.
	    		accr.setActivated(true);
	    		skoolaService.updateRecord(accr);
	    		
	    		
	    		skoolaJSONResponseObject.setMessage("Your account has been activated. Use your email address and password to login to your account.");
    			skoolaJSONResponseObject.setSuccessful(true);
    			return gson.toJson(skoolaJSONResponseObject);
	    		
	    		
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    	}
	    	
	    	return new Gson().toJson(skoolaJSONResponseObject);
	    }

}

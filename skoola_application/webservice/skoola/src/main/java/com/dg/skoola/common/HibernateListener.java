package com.dg.skoola.common;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.dg.skoola.HibernateUtils;

public class HibernateListener implements ServletContextListener {

	private Logger log = Logger.getLogger(getClass());

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

		try {
			log.info(":.... Hibernate Listerner Destroyed - Skoola Service ...");
		} catch (HibernateException e) {
			e.printStackTrace();
			log.error(e);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			HibernateUtils.getSessionFactory();
			log.info(":.... Hibernate Listerner Initialized - Skoola Service ...");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}
}

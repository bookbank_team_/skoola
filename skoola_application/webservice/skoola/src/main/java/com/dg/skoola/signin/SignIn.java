package com.dg.skoola.signin;

import java.io.File;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import sun.misc.BASE64Encoder;

import com.dg.skoola.ServiceLocator;
import com.dg.skoola.common.CustomService;
import com.dg.skoola.common.SkoolaConstants;
import com.dg.skoola.common.SkoolaJSONRequestObject;
import com.dg.skoola.common.SkoolaJSONResponseObject;
import com.dg.skoola.common.SkoolaUtil;
import com.dg.skoola.entity.AccountCreationRequest;
import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.PortalUserRoleMap;
import com.dg.skoola.entity.Role;
import com.dg.skoola.enumeration.RoleTypeConstant;
import com.dg.skoola.pojo.ActiveSessionPojo;
import com.dg.skoola.search.SearchHandler;
import com.dg.skoola.service.SkoolaService;
import com.dg.skoola.signup.SignUp;
import com.google.gson.Gson;


@Path("/signin")
@WebService
public class SignIn {
	
	private Logger log = Logger.getLogger(SignIn.class);
	private SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
	private CustomService skoolaCustomService = CustomService.getInstance();
	private BASE64Encoder encoder = new BASE64Encoder();
	private Gson gson = new Gson();
	private JSONParser jsonParser = new JSONParser();
	
	 @GET @Path("/test")
	    @Consumes({MediaType.TEXT_PLAIN})
	    @Produces({MediaType.TEXT_PLAIN})
	    public String test(String data) {
	    	System.out.println(" data = " + data);
	    	SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
	    	System.out.println( " inside test  ");
	    	SkoolaJSONResponseObject SkoolaJSONResponseObject =  new SkoolaJSONResponseObject();
	    	SkoolaJSONResponseObject.setData("Welcome to Skoola SignIn ");
	    	SkoolaJSONResponseObject.setSuccessful(true);
	       
	    	try {
				skoolaService.getAllRecords(Country.class);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	    	return new Gson().toJson(SkoolaJSONResponseObject);
	    }
	 
	 
	 
	 @POST @Path("/user_signin")
	    @Consumes({MediaType.TEXT_PLAIN})
	    @Produces({MediaType.TEXT_PLAIN})
	    public String userSigin(String data) {
	 		System.out.println(" inside create signup request ");
	    	SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
	    	SkoolaJSONResponseObject skoolaJSONResponseObject =  new SkoolaJSONResponseObject();
	       
	    	try {
	    		
	    		if(!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)){
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		
	    		JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());
	    		
	    		if(jsonObject == null){
	    			skoolaJSONResponseObject.setMessage("Invalid Request!");
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		String email = (String) jsonObject.get("email");
	    		String password = (String) jsonObject.get("password");
	    		
	    		log.info("password = " + password);
	    		log.info("email = " + email);
	    		
	    		PortalUser portalUser = CustomService.getPortalUserByEmailAddress(email);
	    		
	    		if(portalUser == null){
	    			skoolaJSONResponseObject.setMessage("Sorry, your email address or password is invalid!");
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		
	    		String digestInput = password + portalUser.getPasswordSalt();
	    		byte[] testPasswordArray = SkoolaUtil.getMessageDigest(portalUser.getPasswordEncryptionAlgo(), digestInput);
	    		String passwordHex = Hex.encodeHexString(testPasswordArray);
	    		
	    		if(!passwordHex.equalsIgnoreCase(portalUser.getPassword())){
	    			skoolaJSONResponseObject.setMessage("Sorry, your email address or password is invalid!!");
	    			skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
	    			skoolaJSONResponseObject.setSuccessful(false);
	    			return gson.toJson(skoolaJSONResponseObject);
	    		}
	    		

	    		ActiveSessionPojo sessionPojo = new ActiveSessionPojo();
	    		sessionPojo.setEmail(portalUser.getEmail());
	    		sessionPojo.setFirstName(WordUtils.capitalizeFully(portalUser.getFirstname()));
	    		sessionPojo.setLastName(WordUtils.capitalizeFully(portalUser.getLastname()));
	    		sessionPojo.setPhoneNumber(portalUser.getPhoneNumber());
	    		sessionPojo.setPortalUserId(portalUser.getId().toString());
	    		sessionPojo.setUserUniqueId(portalUser.getUserId());
	    		sessionPojo.setActive(true);
	    		sessionPojo.setIsAdmin(false);
	    		sessionPojo.setIsDataEntry(false);
	    		sessionPojo.setIsStudent(false);
	    		
	    		Collection<Role> roleCol = CustomService.getAllPortalUserRolePortalUserId(portalUser.getId());
	    		
	    		if(roleCol != null && !roleCol.isEmpty()){
	    			for(Role role : roleCol){
	    				if(role.getName() == RoleTypeConstant.ADMINISTRATOR){
	    					sessionPojo.setIsAdmin(true);
	    				}else if(role.getName() == RoleTypeConstant.DATA_ENTRY){
	    					sessionPojo.setIsDataEntry(true);
	    				}else if(role.getName() == RoleTypeConstant.STUDENT){
	    					sessionPojo.setIsStudent(true);
	    				}
	    			}
	    		}
	    		
	    		
	    		
	    		skoolaJSONResponseObject.setData(gson.toJson(sessionPojo));
	    		skoolaJSONResponseObject.setMessage("Welcome " + WordUtils.capitalizeFully(portalUser.getFirstname()) + " " + WordUtils.capitalizeFully(portalUser.getLastname()));
	    		skoolaJSONResponseObject.setSuccessful(true);
	    		
	    		return gson.toJson(skoolaJSONResponseObject);
	    		
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    	}
	    	
	    	return gson.toJson(skoolaJSONResponseObject);
	    }

	    

}

package com.dg.skoola.common;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.bw.utils.email.EmailSender;
import com.bw.utils.email.EmailSenderFactory;
import com.dg.skoola.service.SkoolaService;
import com.dg.skoola.util.Constant;


/**
 * @author holy__000
 *
 */
public class Notifier {

	private String SMS_GATWAY = "";
	private String SMS_USERNAME = "";
	private String SMS_PASSWORD = "";
	private String emailAddress = "";
	private String emailPassword = "";
	private CustomService customService = CustomService.getInstance();
	private SkoolaService skoolaService = customService.service;
	
    //OnPosCustomService opservice = OnPosCustomService.getInstance();
	/**
	 * private constructor
	 */
	private Notifier() {
		emailAddress = customService.getSettingValue(Constant.NOTIFICATION_EMAIL_ADDRESS,
				"skoola.nigeria@gmail.com", true);
		emailPassword = customService.getSettingValue(Constant.NOTIFICATION_EMAIL_PASSWORD,
				"*#sk00l@#", true);
	}

	private static class InstanceHolder {
		
		private static Notifier instance = new Notifier();
	}

	public static Notifier getNotifier() {
		return InstanceHolder.instance;
	}

	/**
	 * @param paramMap
	 * @param ftlName
	 *

	public void sendEmail(HashMap<String, String> paramMap, String emailAddress, String subject, String ftlName) {
		new EmailSender(ftlName).sendMailWithHTMLTemplate(emailAddress, subject, paramMap);

	}*/

	public void sendEmail(Map<String, Object> paramMap, String receiver, String subject, String ftlName) {
		EmailSender es = EmailSenderFactory.getInstance().getEmailSender();
		File templatePath = new File(System.getProperty("user.dir")+ File.separator +"resources"+File.separator+"emailtemplates");
		if (!templatePath.exists()) {
			templatePath.mkdir();
		}
		System.out.println("========= Dir: "+templatePath.getAbsolutePath()+File.separator+ftlName);
		File file = new File(templatePath.getAbsolutePath()+File.separator+ftlName);
		if(file.exists()){
			System.out.println("========= File Exist ============= ");
		}else{
			System.out.println("========= File Doesnot Exist ============= ");
		} 
		es.setSenderEmail(emailAddress);
		es.setSenderPassword(emailPassword);
		es.sendEmail(subject, receiver, paramMap, templatePath.getAbsolutePath(), ftlName);
	}
  
	
	public void sendEmail(String emailAddress, String subject, String message) {
		EmailSender es = EmailSenderFactory.getInstance().getEmailSender();
		es.setSenderEmail(emailAddress);
		es.setSenderPassword(emailPassword);
		es.sendEmail(subject, emailAddress, message, emailAddress, emailPassword);
	}

	/**
	 * @param recipients
	 * @param from
	 * @param message
	 * @param title
	 */
	/*public void sendSms(String[] recipients, String from, String message) {
		getSmsSender().sendSms(recipients, message,"ONPOS");
	} */

	/**
	 * @param recipient
	 * @param from
	 * @param message
	 * @param title
	 
	public void sendSms(String recipient, String from, String message, String title) {
		getSmsSender().sendSms(recipient, message, from);
	}

	
	 * @return
	 */
	/*private SMSSender getSmsSender() {
		if (SMS_GATWAY.isEmpty() || SMS_PASSWORD.isEmpty() || SMS_USERNAME.isEmpty()) {
			SMS_GATWAY = opservice.getSettingValue("SMS_GATWAY", "http://api.smartsmssolutions.com/smsapi.php?", true);
			SMS_PASSWORD = opservice.getSettingValue("SMS_PASSWORD", "ng2$laba", true);
			SMS_USERNAME = opservice.getSettingValue("SMS_USERNAME", "ngalaba", true);
		}
		Builder builder = new Builder(SMS_USERNAME,SMS_PASSWORD,SMS_GATWAY);
		SMSSender smsSender = SMSSenderFactory.getInstance().getSmsSender(builder);
		return smsSender;
	} */
}

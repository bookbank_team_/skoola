package com.dg.skoola.common;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;

public class TestEmailClass {
	
	public static void main(String[] args) {
		try {
			Properties props = System.getProperties();
			props.put("mail.smtps.host","smtp.mailgun.org");
			props.put("mail.smtps.auth","true");
			Session session = Session.getInstance(props, null);
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("noreply@skoola.com"));
			msg.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse("jemeokafor@gmail.com", false));
			msg.setSubject("Hello");
			msg.setText("Testing some Mailgun awesomness");
			msg.setSentDate(new Date());
			SMTPTransport t =
			    (SMTPTransport)session.getTransport("smtps");
			t.connect("smtp.mailgun.com", "noreply@skoola.com", "*#n0r3ply#");
			t.sendMessage(msg, msg.getAllRecipients());
			System.out.println(" Email sent!!!");
			System.out.println("Response: " + t.getLastServerResponse());
			t.close();
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SendFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

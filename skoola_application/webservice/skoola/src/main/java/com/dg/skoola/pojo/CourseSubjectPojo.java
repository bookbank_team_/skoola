package com.dg.skoola.pojo;

public class CourseSubjectPojo {
	
	private String subjectId;
	private String subjectName;
	private String subjectDisplayName;
	
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getSubjectDisplayName() {
		return subjectDisplayName;
	}
	public void setSubjectDisplayName(String subjectDisplayName) {
		this.subjectDisplayName = subjectDisplayName;
	}
	

}

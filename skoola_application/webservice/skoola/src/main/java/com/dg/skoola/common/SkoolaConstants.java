package com.dg.skoola.common;

public class SkoolaConstants {
	
	public static final String API_KEY = "XKl0b3E_#p%M";
	public static final String PASSWORD_ENCRYPTION_ALGORITHM = "SHA-512";
	public static final String SUPER_ADMIN_EMAIL_ADDRESS = "super_admin@skoola.com";
	public static final String SUPER_ADMIN_DEFAULT_PASSWORD = "password**";
	
	public static final String EMAIL_TEMPLATE_SIGNUP_REQUEST = "default_email_template.ftl";
	public static final String ACTIVATION_EMAIL_TEMPLATE = "activation_email_template.ftl";
	
	public static final String EMAIL_TEMPLATE_DEFAULT = "default_email_template.ftl";
	
	public static final String NIGERIA_PHONE_CODE = "+234";
	
	public static final String PROCESS_TYPE_BOOK_VIRTUAL_CONSULTATION = "BOOK VIRTUAL CONSULTATION";
	public static final String PROCESS_TYPE_BOOK_IN_HOUSE_CONSULTATION = "BOOK IN HOUSE CONSULTATION";
	public static final String PROCESS_TYPE_BOOK_ACCOMMODATION_SERVICES = "BOOK ACCOMMODATION SERVICES";
	
	
	
	
}

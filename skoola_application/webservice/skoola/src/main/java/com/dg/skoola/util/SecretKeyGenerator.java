package com.dg.skoola.util;
/*package com.dg.skoola.util;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.UUID;


*//**
 * Created by Chidera on 8/14/2015.
 *//*
public class SecretKeyGenerator {

    private static SecureRandom random = new SecureRandom();

    public static String generateKeySeed() {
        return Base64.encodeBase64String(new BigInteger(256, random).toString().getBytes());
    }

    public static SecretKeySpec generateSecretKey(String seed) {
        SecretKeySpec secretKey = null;
        try {
            String secretKeySeed = seed;
            secretKey = new SecretKeySpec(secretKeySeed.getBytes("UTF-8"), "HmacSHA256");

        }catch (Exception e) {
            e.printStackTrace();
        }
        return secretKey;
    }


    public static void generateSecretKeyAndSaveToKeyStore(String alias, KeyStore keyStore, String keyPassword, String storePassword) {
        SecretKeySpec secretKeySpec = generateSecretKey("");

        try {
            KeyStore defaultKeyStore;
            if (keyStore == null)
                defaultKeyStore = KeyStore.getInstance("JCEKS");
            else
                defaultKeyStore = keyStore;

            defaultKeyStore.load(null, null);

            KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(secretKeySpec);

            defaultKeyStore.setEntry(alias, secretKeyEntry, new KeyStore.PasswordProtection(keyPassword.toCharArray()));
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            defaultKeyStore.store(bOut, storePassword.toCharArray());

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String generateApiAlias() {
        return UUID.randomUUID().toString();
    }
}
*/
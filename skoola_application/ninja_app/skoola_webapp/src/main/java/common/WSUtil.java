package common;


import java.net.URLEncoder;

import javax.ws.rs.core.MediaType;

import ninja.utils.NinjaProperties;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Johnbosco Eme-Okafor
 *
 */

public class WSUtil {
	private static  Logger log = Logger.getLogger(WSUtil.class);

	
	public  String sendDataToWebservice(NinjaProperties ninjaProperties,String methodURL , String data)
    {
		String wsOutput = "";
		
		try {
			
			if(data == null || data.isEmpty()){
				return wsOutput;
			}
			//data = URLEncoder.encode(data, "UTF-8"); 
			String serviceCallURL = getWebserviceURL(ninjaProperties) + methodURL ;
			log.info("Calling webservice method at >>>>>> " + serviceCallURL);
			

			Client client = Client.create();

			
			WebResource webResource = client.resource(serviceCallURL);

			ClientResponse response = webResource
		            .accept(MediaType.TEXT_PLAIN)
		            .post(ClientResponse.class,data);

			if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
			}


			wsOutput = response.getEntity(String.class);

			
		  } catch (Exception e) {

			e.printStackTrace();

		  }

		return wsOutput;

    }


	
	private  String getWebserviceURL(NinjaProperties ninjaProperties){
		
		
		log.info("Is Ninja in Dev Mode = " + ninjaProperties.isDev());
		String webserviceURL = "";
		
		if(ninjaProperties.isDev()){
			webserviceURL = ninjaProperties.get("wsurl");
		}else if(ninjaProperties.isProd()){
			webserviceURL = ninjaProperties.get("wsurl"); 
		}
		return webserviceURL;
		
	}
	



}

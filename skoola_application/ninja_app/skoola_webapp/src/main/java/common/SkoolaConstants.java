package common;

import java.util.Collection;

public class SkoolaConstants {
	
	public static final String API_KEY = "XKl0b3E_#p%M";
	
	public static final String WS_URL_CREATE_SIGNUP_REQUEST = "";
	
	public static final String NIGERIA_PHONE_CODE = "+234";
	
	// Ninja Cache Session Active Session Map key
	
	public static final String ACTIVE_SESSION_MAP = "ACTIVE_SESSION_MAP";
	
	public static final String SEARCH_MAXIMUM_RESULTSET_SIZE = "6";
	
	//Service URLs
	
	public static final String URL_SIGNUP_CREATE_SIGNUP_REQUEST = "/signup/create_signup_request";
	public static final String URL_CREATE_UPDATE_APPLICATION_SETTINGS = "/setting/create_update_default_super_admin_user";
	public static final String URL_SIGNUP_ACTIVATE_USER_ACCOUNT = "/signup/activate_user_account";	
	public static final String URL_USER_SIGNIN = "/signin/user_signin";	
	public static final String URL_SEARCH_SKOOLA_COURSES = "/search/get_all_skoola_courses";
	public static final String URL_GET_ALL_COUNTRY = "/search/get_all_country";
	public static final String URL_GET_ALL_COURSE_TYPE = "/search/get_all_course_type";
	public static final String URL_GET_ALL_SUBJECT_GROUP_STATISTICS = "/search/get_all_subject_group_statistics";
	public static final String URL_GET_COURSES_BY_PARAMETERS = "/search/get_courses_by_parameters";
	public static final String URL_GET_ALL_SUBJECT_TYPE = "/search/get_all_course_subject_type";
	public static final String URL_GET_ALL_SKOOLA_COURSE_SUBJECT_CATEGORIES = "/search/get_all_skoola_course_subject_categories";
	public static final String URL_GET_PAGINATED_SKOOLA_SCHOOLS = "/search/get_paginated_skoola_schools";
	public static final String URL_GET_ALL_SUBJECT_GROUP_TYPE = "/search/get_all_course_subject_group_type";
	public static final String URL_GET_COURSE_DETAIL = "/search/get_course_detail";
	public static final String URL_CREATE_CONSULTATION_REQUEST = "/career_consultation/create_consultation_request";
	public static final String URL_GET_FEESCHEDULE_BY_CODE = "/payment/get_feeschedule_by_code";
	public static final String URL_CREATE_PAYMENT_HISTORY = "/payment/create_payment_history";	
	public static final String URL_UPDATE_PAYMENT_HISTORY_WITH_PAYSTACK_ACCESSCODE = "/payment/update_payment_history_with_paystack_accesscode";
	public static final String URL_UPDATE_PAYMENT_HISTORY_WITH_PAYSTACK_PAYMENT_RESPONSE = "/payment/update_payment_history_with_paystack_response";
	public static final String URL_GET_SETTING_BY_NAME = "/payment/get_setting_by_name";	
	public static final String URL_GET_PAGINATED_CONSULTATION_REQUEST_BY_PORTALUSER_USERID = "/useraccounthandler/get_paginated_consultation_request_by_portaluser_userid";
	public static final String URL_GET_PAGINATED_PAYMENT_HISTORY_BY_PORTALUSER_USERID = "/useraccounthandler/get_paginated_payment_history_by_portaluser_userid";
	public static final String URL_GET_PAGINATED_APPLICATION_USERS_RESULT = "/backendaccounthandler/get_paginated_application_users_result";

	public static final String URL_GET_PAGINATED_CAREER_CONSULTATION_APPLICATION_REQUESTS_RESULT = "/backendaccounthandler/get_paginated_career_consultation_requests_by_status";
	public static final String URL_GET_ADMIN_DASHBOARD_STATISTICS = "/backendaccounthandler/get_admin_dashboard_statistics";
	public static final String URL_GET_PAGINATED_SKOOLA_COURSES = "/backendaccounthandler/get_paginated_skoola_courses";
	
	public static final String URL_CREATE_OR_UPDATE_COURSE_DETAIL = "/backendaccounthandler/create_or_update_skoola_course";
	
	


	
	
	//Pages
	// Signup
	public static final String HOME_PAGE = "views/SkoolaHomeController/index.ftl.html";
	public static final String SIGNUP_REGISTRATION_PAGE = "views/SignUp/user-registration-page.ftl.html";
	public static final String SIGNUP_REGISTRATION_CONFIRMATION_PAGE = "views/SignUp/user-registration-confirmation-page.ftl.html";
	public static final String SIGNUP_ACCOUNT_ACTIVATION_PAGE = "views/SignUp/user-account-activation-page.ftl.html";
	public static final String COURSE_DETAIL_PAGE = "views/CourseDetail/course.ftl.html";
	public static final String CONSULTATION_APPLICATION_PAGE = "views/ConsultationApplication/career-consultation-application.ftl.html";
	
	// Admin
	public static final String ADMIN_DASHBOARD = "views/Admin/admin-dashboard.ftl.html";
	public static final String ADMIN_APPLICATION_USERS = "views/Admin/application-users.ftl.html";
	public static final String ADMIN_BACKEND_USERS = "views/Admin/back-end-users.ftl.html";
	public static final String ADMIN_CAREER_CONSULTATION_APPLICATION = "views/Admin/career-consultation-applications.ftl.html";
	public static final String ADMIN_SKOOLA_COURSES = "views/Admin/courses.ftl.html";
	public static final String ADMIN_SKOOLA_EXAMS = "views/Admin/exams.ftl.html";
	public static final String ADMIN_SKOOLA_COUNTRIES = "views/Admin/countries.ftl.html";
	public static final String ADMIN_SKOOLA_UNIVERSITIES = "views/Admin/universities.ftl.html";
	
	public static final String ADMIN_EDIT_SKOOLA_COURSE = "views/Admin/edit/edit-course.ftl.html";
	
	// Signin
	public static final String SIGNIN_PAGE = "views/Login/user-login-page.ftl.html";
	
	//Account
	public static final String ACCOUNT_PROFILE_PAGE = "views/UserAccount/account-profile.ftl.html";
	
	// Search Result
	public static final String SEARCH_RESULT_PAGE = "views/CourseSearchResults/search-result.ftl.html";
	
	// Payment Callback URLs
	
	public static final String PAYMENT_CALLBACK_URL = "/payment_callback";
	
	
	public static final String REQUEST_TYPE_GO_BACK = "GO_BACK";
	
	public static final String LAST_OPERATION_URL = "LAST_OPERATION_URL";

	public static final String CACHE_VARIABLE_EXPIRATION_TIME = "10mn";

	public static final String FEES_SCHEDULE_CODE_VIRTUAL_CAREER_CONSULTATION = "1004";
	public static final String FEES_SCHEDULE_CODE_IN_HOUSE_CAREER_CONSULTATION = "1005";

	public static final String PREVIOUS_PAGE_URL = "previousPageURL";
	public static final String CURRENT_PAGE_URL = "currentPageURL";
	
	public static final String PROCESS_TYPE_BOOK_VIRTUAL_CONSULTATION = "BOOK VIRTUAL CONSULTATION";
	public static final String PROCESS_TYPE_BOOK_IN_HOUSE_CONSULTATION = "BOOK IN HOUSE CONSULTATION";
	public static final String PROCESS_TYPE_BOOK_ACCOMMODATION_SERVICES = "BOOK ACCOMMODATION SERVICES";
	
	
	// Current Page Key
	public static final String CURRENT_PAGE_KEY = "CURRENT_PAGE";
	
	// User Account Pages
	public static final String USER_ACCOUNT_PAGE_DASHBOARD = "Pages/dash-board.ftl.html";
	public static final String USER_ACCOUNT_PAGE_MY_CAREER_CONSULTATION = "Pages/career-consultation-requests.ftl.html";
	public static final String USER_ACCOUNT_PAGE_MY_PAYMENT_HISTORY = "Pages/payment-history.ftl.html";
	public static final String USER_ACCOUNT_PAGE_MY_MESSAGES = "Pages/messages.ftl.html";
	
	
	
	
	
	
	

}

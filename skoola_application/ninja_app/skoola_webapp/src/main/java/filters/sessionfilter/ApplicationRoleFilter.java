package filters.sessionfilter;

import java.util.HashMap;

import pojo.ActiveSessionPojo;

import com.google.inject.Inject;

import common.SkoolaConstants;
import ninja.Context;
import ninja.Filter;
import ninja.FilterChain;
import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.Session;

public class ApplicationRoleFilter implements Filter{
	
	
	@Inject
	NinjaCache ninjaCache;

	@Override
	public Result filter(FilterChain filterChain, Context context) {
		//Session session  = context.getSession();
		ActiveSessionPojo activeSessionPojo = (ActiveSessionPojo) context.getAttribute("activeSessionPojo");
        /*HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
    	if(userSessionMap != null){
    		activeSessionPojo = userSessionMap.get(session.getId());
    	}*/
    	if (activeSessionPojo == null) {

            return Results.html().template(SkoolaConstants.SIGNIN_PAGE);

        } else {
        	
        	String actionURL = context.getRequestPath();
        	System.out.println(" action URL = " + actionURL);
        	
        	if(activeSessionPojo.getIsStudent() != null && activeSessionPojo.getIsStudent()){
        		if(actionURL.equalsIgnoreCase("/admin_dashboard")
        		   || actionURL.equalsIgnoreCase("/admin_application_users") 
        		   || actionURL.equalsIgnoreCase("/admin_backend_users")
        		   || actionURL.equalsIgnoreCase("/admin_consultation_applications")
        		   || actionURL.equalsIgnoreCase("/admin_skoola_courses")
        		   || actionURL.equalsIgnoreCase("/admin_skoola_exams")
        		   || actionURL.equalsIgnoreCase("/admin_skoola_universities")
        		   || actionURL.equalsIgnoreCase("/admin_skoola_countries")
        		   
        		
        		){
        			
        			System.out.println(" rendering forbidden page ");
        			return Results.forbidden().template("views/system/403forbidden.ftl.html");
        		}
        	}else if(activeSessionPojo.getIsAdmin() != null && activeSessionPojo.getIsAdmin()){
        		
        	}else if(activeSessionPojo.getIsDataEntry() != null && activeSessionPojo.getIsDataEntry()){
        		
        	}
        	
        	System.out.println(" returning filter chain ");
            return filterChain.next(context);
        }
	}

}

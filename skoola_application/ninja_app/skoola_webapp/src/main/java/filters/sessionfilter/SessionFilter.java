package filters.sessionfilter;

import java.util.HashMap;

import com.google.inject.Inject;

import common.SkoolaConstants;
import ninja.Context;
import ninja.Filter;
import ninja.FilterChain;
import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.Session;
import pojo.ActiveSessionPojo;

public class SessionFilter  implements Filter{

	@Inject
	NinjaCache ninjaCache;
	
	@Override
	public Result filter(FilterChain chain, Context context) {
		
		Session session  = context.getSession();
		ActiveSessionPojo activeSessionPojo = null;
        HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
    	if(userSessionMap != null){
    		activeSessionPojo = userSessionMap.get(session.getId());
    	}
    
		session.put(SkoolaConstants.LAST_OPERATION_URL, context.getRoute().getUrl());
		// if we got no cookies we break:
        if (activeSessionPojo == null) {

            return Results.html().template(SkoolaConstants.SIGNIN_PAGE);

        } else {
        	
        	context.setAttribute("activeSessionPojo", activeSessionPojo);
            return chain.next(context);
        }
	}

}

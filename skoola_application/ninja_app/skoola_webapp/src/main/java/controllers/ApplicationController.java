/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import ninja.Result;
import ninja.Results;
import ninja.utils.NinjaProperties;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.SecurityUtil;
import common.WSUtil;



@Singleton
public class ApplicationController {


	@Inject 
    NinjaProperties ninjaProperties;


	private WSUtil wsUtil = new WSUtil();

    public Result index() {

        return Results.html();

    }
    
    public Result testSkoolaService() {
        
        
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = "/SearchHandler/test";
       
        String apiKey = "api key";      
        String jsonData = new Gson().toJson("");
        
       
        
        String hashInput = apiKey + jsonData;
        String hash = SecurityUtil.generateHmacDigest(hashInput);
        
        
        requestObject.setApi_key(apiKey);
        requestObject.setData(jsonData);
        requestObject.setHash(hash);
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        return Results.json().render(responseObject);

    }
    
    public static class SimplePojo {

        public String content;
        
    }
}

package controllers.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.mail.internet.AddressException;

import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.Router;
import ninja.cache.NinjaCache;
import ninja.postoffice.Mail;
import ninja.postoffice.Postoffice;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;

import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;
/*import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;*/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pojo.ActiveSessionPojo;
import pojo.CountryPojo;
import pojo.CoursePojo;
import pojo.CourseSubjectGroupPojo;
import pojo.CourseSubjectPojo;
import pojo.CourseTypePojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.SkoolaSearchResultPojo;
import pojo.form.SigninFormPojo;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import common.AppUtil;
import common.SkoolaConstants;
import common.WSUtil;
import controllers.SkoolaHomeController;
import controllers.signup.SignUpController;

@Singleton
public class SearchController {
	
	private Logger log = Logger.getLogger(SearchController.class);
	@Inject 
    NinjaProperties ninjaProperties;
	
	@Inject
	Router router;
	
	@Inject
    Provider<Mail> mailProvider;

    @Inject
    Postoffice postoffice;

	private WSUtil wsUtil = new WSUtil();
	private AppUtil appUtil = new AppUtil();
	
	@Inject 
	NinjaCache ninjaCache;
	

    
   /* public Result getAllSkoolaCourses(FlashScope flashScope,Session session) {
 
    	
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_SEARCH_SKOOLA_COURSES; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
       // router.getr.
        
        Collection<CoursePojo> coursePojoCol = new ArrayList<CoursePojo>();
        
        String skoolaCoursesJSONString = session.get("skoolaCoursesJSONString");
        JSONObject jObject = null;
        
        
        try {
	        
        	if(skoolaCoursesJSONString == null || skoolaCoursesJSONString.isEmpty()){
	        	
		        String requestString = new Gson().toJson(requestObject);
		        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
		        log.info(" response  = " + response);
		        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
		        log.info(" response object = " + responseObject );
		        if(!responseObject.getSuccessful()){
		        	flashScope.error(responseObject.getMessage());
		        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
		        }
		        
		        jObject =  new JSONObject(responseObject.getData());
		        
	        }else{
	        	jObject = new JSONObject(skoolaCoursesJSONString);
	        	//log.info( "loaded courses from session ");
	        }

    		
    		JSONArray jArray = (JSONArray) jObject.get("course_list");
    		
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = new JSONObject(jArray.getString(i));
    			CoursePojo pojo = new Gson().fromJson(jsonObject.toString(), CoursePojo.class);
    			//log.info( pojo.getName());
    			coursePojoCol.add(pojo);
    		}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
        return Results.json().render(coursePojoCol);

    }
    */
    
    
    public Result getAllCountry(FlashScope flashScope,Session session) {
    	 
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_COUNTRY; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
        }
        Collection<CountryPojo> countryCol = new ArrayList<CountryPojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try{

    		jObject = new JSONObject(responseObject.getData());   		
    		jArray = (JSONArray) jObject.get("country_list");
    		
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = new JSONObject(jArray.getString(i));
    			CountryPojo pojo = new CountryPojo();
    			pojo.setDescription("");
    			pojo.setCountryId(jsonObject.getString("id"));
    			pojo.setName(jsonObject.getString("name"));
    			
    			countryCol.add(pojo);
    		}
    		
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        return Results.html().render(jObject);
    }
    
    
    
    public Result getAllCourseType(FlashScope flashScope,Session session) {
   	 
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_COURSE_TYPE; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
        }
        Collection<CourseTypePojo> courseTypeCol = new ArrayList<CourseTypePojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try{

    		jObject = new JSONObject(responseObject.getData());   		
    		jArray = (JSONArray) jObject.get("course_type_list");
    		
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = new JSONObject(jArray.getString(i));
    			CourseTypePojo pojo = new CourseTypePojo();
    			pojo.setDescription("");
    			pojo.setId(jsonObject.getString("id"));
    			pojo.setName(jsonObject.getString("name"));
    			
    			courseTypeCol.add(pojo);
    		}
    		
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        return Results.html().render(jObject);
    }
    
    
    public Result getAllSubjectType(FlashScope flashScope,Session session) {
      	 log.info(" inside get all subject type ");
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_TYPE; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        log.info(" response &&&&&&&&&&&&&&&&&& = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
       
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
        }
        Collection<CourseSubjectPojo> courseSubjectCol = new ArrayList<CourseSubjectPojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try{

    		jObject = new JSONObject(responseObject.getData());  
    		
    		jArray = (JSONArray) jObject.get("course_subject_type_list");
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = jArray.getJSONObject(i);
    			
    			CourseSubjectPojo pojo = new CourseSubjectPojo();
    			pojo.setSubjectDisplayName(jsonObject.getString("name"));
    			pojo.setId(jsonObject.getString("id"));
    			pojo.setSubjectName(jsonObject.getString("name"));
    			pojo.setType("Subject");
    			
    			courseSubjectCol.add(pojo);
    		}
    		
    		log.info(" courseSubjectCol = " + courseSubjectCol);
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        return Results.json().render(courseSubjectCol);
    }
    
    
    
    public Result getAllSubjectGroupType(FlashScope flashScope,Session session) {
     	 log.info(" inside subject group type ...");
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_GROUP_TYPE; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
        }
        Collection<CourseSubjectGroupPojo> courseSubjectGroupCol = new ArrayList<CourseSubjectGroupPojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try{

    		jObject = new JSONObject(responseObject.getData());   		
    		jArray = (JSONArray) jObject.get("course_subject_group_type_list");
    		
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = jArray.getJSONObject(i);
    			
    			
    			
    			CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
    			pojo.setCourseSubjectGroupDisplayName(jsonObject.getString("name"));
    			pojo.setId(jsonObject.getString("id"));
    			pojo.setCourseSubjectGroupName(jsonObject.getString("name"));
    			pojo.setType(jsonObject.getString("type"));
    			
    			courseSubjectGroupCol.add(pojo);
    		}
    		
    		log.info( " size of subject group col  = " + courseSubjectGroupCol);
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        return Results.json().render(courseSubjectGroupCol);
    }
    
    
    
    
    
	 public Result showCourseDetail(Context context,FlashScope flashScope) {
		    SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
			String methodUrl = SkoolaConstants.URL_GET_COURSE_DETAIL; 
			String apiKey = SkoolaConstants.API_KEY; 
			Session session  =  context.getSession();
			Result result = Results.html();
			
			
        try {
			
			requestObject.setApi_key(apiKey);
			requestObject.setHash("");
			
			String subjectId = "";
	        String type = "";
	        String countryId = "";
	        String courseTypeId = "";
	        String minimumPrice = "";
	        String maximumPrice = "";
	        String searchIndex = "";
			String courseId = "";


			
			 String requestType = (String) context.getAttribute(SkoolaConstants.REQUEST_TYPE_GO_BACK);

	        if(requestType != null && requestType.equalsIgnoreCase(SkoolaConstants.REQUEST_TYPE_GO_BACK)){
	        	
	        	courseId = (String) ninjaCache.get("show_course_details_course_id");
				
	        }else{
				
				 subjectId = context.getParameter("_subjectId");
		         type = context.getParameter("_type");
		         countryId = context.getParameter("_countryId");
		         courseTypeId = context.getParameter("_courseTypeId");
		         minimumPrice = context.getParameter("_searchParamMinimumPrice");
		         maximumPrice = context.getParameter("_searchParamMaximumPrice");
		         searchIndex = context.getParameter("_searchParamSearchIndex");
				 courseId = context.getParameter("_courseId");
				 ninjaCache.add("show_course_details_course_id", courseId);
				
				
				
				ninjaCache.add("subjectId",subjectId);
				ninjaCache.add("type",type);
				ninjaCache.add("countryId",countryId);
				ninjaCache.add("courseTypeId",courseTypeId);
				ninjaCache.add("minimumPrice",minimumPrice);
				ninjaCache.add("maximumPrice",maximumPrice);
				ninjaCache.add("searchIndex",searchIndex);
	        }
		       
			
			
			
			if(courseId == null || courseId.isEmpty()){
				flashScope.error("Error!!! select a valid course ID or contact support.");
			 	return result;
			}
			
			JSONObject jsonObject = new JSONObject();
			try {
				
				jsonObject.put("courseId", courseId);
				
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			requestObject.setData(jsonObject.toString());
			String requestString = new Gson().toJson(requestObject);
			
			String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
			SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
			if(!responseObject.getSuccessful()){
				flashScope.error(responseObject.getMessage());
				
				return result;
			}
			
			
			CoursePojo coursePojo = new Gson().fromJson(responseObject.getData(), CoursePojo.class);
			if(coursePojo == null){
				System.out.println(" ######################### course pojo is null ");
			}
			ninjaCache.add(session.getId()+"_coursePojo", coursePojo,SkoolaConstants.CACHE_VARIABLE_EXPIRATION_TIME);
			
			ActiveSessionPojo activeSessionPojo = null;
	        HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
	    	if(userSessionMap != null){
	    		activeSessionPojo = userSessionMap.get(session.getId());
	    	}
	    	if(activeSessionPojo != null){
	        	result.render(activeSessionPojo);
	        }
			
			
	    	result = new AppUtil().handleRetrieveCourseCategoriesData(result,context, flashScope,ninjaCache, ninjaProperties);
			result.html().template(SkoolaConstants.COURSE_DETAIL_PAGE);
			result.render(coursePojo);
			return result;
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return result;
        
    }
  
	 public Result getCoursesBySearchParameters(Context context,FlashScope flashScope,Session session) {
	   	 
		
	        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
	        String methodUrl = SkoolaConstants.URL_GET_COURSES_BY_PARAMETERS; 
	        String apiKey = SkoolaConstants.API_KEY; 
	        
	        String countriesJSONString2 = session.get("countriesJSONString");
	     
	        
	        Result result = Results.html();
	        
	        requestObject.setApi_key(apiKey);
	        requestObject.setHash("");
	        
	        String subjectId = "";
	        String type = "";
	        String countryId = "";
	        String courseTypeId = "";
	        String minimumPrice = "";
	        String maximumPrice = "";
	        String searchIndex = "";
	        
	        String requestType = (String) context.getAttribute(SkoolaConstants.REQUEST_TYPE_GO_BACK);
	        ///log.info(" request type = " + requestType);
	        
	        if(requestType != null && requestType.equalsIgnoreCase(SkoolaConstants.REQUEST_TYPE_GO_BACK)){
	        	log.info(" 1 >>> ");
	        	subjectId = ((String) (ninjaCache.get("subjectId") == null ? "" : ninjaCache.get("subjectId"))).trim();
				type = ((String) (ninjaCache.get("type") == null ? "" :ninjaCache.get("type"))).trim();
				countryId = ((String) (ninjaCache.get("countryId") == null ? "" :ninjaCache.get("countryId"))).trim();
				courseTypeId = ((String) (ninjaCache.get("courseTypeId") == null ? "" :ninjaCache.get("courseTypeId"))).trim();
				minimumPrice = ((String) (ninjaCache.get("minimumPrice") == null ? "" :ninjaCache.get("minimumPrice"))).trim();
				maximumPrice = ((String) (ninjaCache.get("maximumPrice") == null ? "" :ninjaCache.get("maximumPrice"))).trim();
				searchIndex = ((String) (ninjaCache.get("searchIndex") == null ? "" :ninjaCache.get("searchIndex"))).trim();
	        }else{
	        	log.info(" 2 >>> ");
	        	subjectId = context.getParameter("subjectId") == null ? "" :context.getParameter("subjectId").trim();
	 	        type = context.getParameter("type") == null ? "" :context.getParameter("type").trim();
	 	        countryId = context.getParameter("countryId") == null ? "" :context.getParameter("countryId").trim();
	 	        courseTypeId = context.getParameter("courseTypeId") == null ? "" :context.getParameter("courseTypeId").trim();
	 	        minimumPrice = context.getParameter("searchParamMinimumPrice") == null ? "" :context.getParameter("searchParamMinimumPrice").trim();
	 	        maximumPrice = context.getParameter("searchParamMaximumPrice") == null ? "" :context.getParameter("searchParamMaximumPrice").trim();
	 	        searchIndex = context.getParameter("searchParamSearchIndex") == null ? "" :context.getParameter("searchParamSearchIndex").trim();
	        }
	       
	        
	        
	        String courseTypeJSONString = session.get("courseTypeJSONString");
	        String countriesJSONString =  session.get("countriesJSONString");
	        String subjectGroupStatJSONString = (String) ninjaCache.get("subjectGroupStatisticsJSONString");
	        
	        Collection<CountryPojo> countryCol = new ArrayList<CountryPojo>();
	        Collection<CourseTypePojo> courseTypeCol = new ArrayList<CourseTypePojo>();
	        Collection<CourseSubjectGroupPojo> courseSubjectGroupStatPojoCol = new ArrayList<CourseSubjectGroupPojo>();
	        
	        try {
				JSONObject countriesJObject = new JSONObject(countriesJSONString);
				JSONObject courseTypeJObject = new JSONObject(courseTypeJSONString);
				JSONObject subjectGroupStatJObject = new JSONObject(subjectGroupStatJSONString);
				
				JSONArray countriesJArray = (JSONArray) countriesJObject.get("country_list");
				//log.info("country jArray = "+ countriesJArray);
				for(int i = 0; i < countriesJArray.length(); i++){
					JSONObject jObject = countriesJArray.getJSONObject(i);
					
					CountryPojo pojo = new CountryPojo();
					pojo.setDescription("");
					pojo.setCountryId(jObject.getString("id"));
					pojo.setName(jObject.getString("name"));
					
					countryCol.add(pojo);
				}
				
				
				JSONArray courseTypeJArray = (JSONArray) courseTypeJObject.get("course_type_list");
	    		//log.info("jArray = " + courseTypeJArray);
	    		for(int i = 0; i < courseTypeJArray.length(); i++){
	    			//log.info(" object = " + jArray.getJSONObject(i));
	    			
	    			JSONObject jObject = courseTypeJArray.getJSONObject(i);
	    			//log.info("jsonobject = " + jObject);
	    			CourseTypePojo pojo = new CourseTypePojo();
	    			pojo.setDescription("");
	    			pojo.setId(jObject.getString("id"));
	    			pojo.setName(jObject.getString("name"));
	    			
	    			courseTypeCol.add(pojo);
	    		}
	    		
	    		JSONArray subjectGroupStatJArray = (JSONArray) subjectGroupStatJObject.get("subject_group_statistics_list");
	    		//log.info("jArray = " + courseTypeJArray);
	    		for(int i = 0; i < subjectGroupStatJArray.length(); i++){
	    			//log.info(" object = " + jArray.getJSONObject(i));
	    			
	    			JSONObject jsonObject = subjectGroupStatJArray.getJSONObject(i);
	        		
	    			CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
	    			pojo.setCourseSubjectGroupDisplayName(jsonObject.getString("displayName"));
	    			pojo.setCourseSubjectGroupName(jsonObject.getString("name"));
	    			pojo.setId(jsonObject.getString("id"));
	    			pojo.setNumberOfCourses(jsonObject.getString("numberOfCourses"));
	    			pojo.setNumberOfUniversities(jsonObject.getString("numberOfUniversities"));
	    			pojo.setType(jsonObject.getString("type"));
	    			
	    			courseSubjectGroupStatPojoCol.add(pojo);
	
	    		}
	    		
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	    	
	        
	        result.template(SkoolaConstants.HOME_PAGE);
	        result.render("countryCol",countryCol);
	        result.render("courseTypeCol",courseTypeCol);
	        result.render("courseSubjectGroupStatPojoCol",courseSubjectGroupStatPojoCol);   
	        
	        /*if((subjectId == null || subjectId.isEmpty()) && (type == null || type.isEmpty()) &&
	        		(countryId == null || countryId.isEmpty()) && (courseTypeId == null || courseTypeId.isEmpty())){
	        	flashScope.error("Error!!! please enter valid search parameters to proceed.");
	        	 return result;
	        }*/
	        
	        JSONObject jsonObject = new JSONObject();
	        try {
	        	
				jsonObject.put("subjectId", subjectId);
				jsonObject.put("type", type);
				jsonObject.put("countryId", countryId);
				jsonObject.put("courseTypeId", courseTypeId);
				jsonObject.put("minimumTuition", minimumPrice);
				jsonObject.put("maximumTuition", maximumPrice);
				jsonObject.put("searchIndex", searchIndex);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	        log.info(" search json object " + jsonObject);
	        
	        
	        requestObject.setData(jsonObject.toString());
	        String requestString = new Gson().toJson(requestObject);
	       
	        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
	        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
	        if(!responseObject.getSuccessful()){
	        	flashScope.error(responseObject.getMessage());
	        	return result;
	        }
	        
	        try {
	        	
	        	Collection<CoursePojo> searchResultPojo = new ArrayList<CoursePojo>();
				JSONObject jsonResponseObject = new JSONObject(responseObject.getData());

				SkoolaSearchResultPojo resultPojo = new SkoolaSearchResultPojo();
				
				resultPojo.setSubjectId(jsonResponseObject.getString("subjectId"));
				resultPojo.setCourseTypeId(jsonResponseObject.getString("courseTypeId"));
				resultPojo.setType(jsonResponseObject.getString("type"));
				resultPojo.setMinimumTuition(jsonResponseObject.getString("minimumTuition"));
				resultPojo.setMaximumTuition(jsonResponseObject.getString("maximumTuition"));
				resultPojo.setSearchIndex(jsonResponseObject.getString("searchIndex"));
				resultPojo.setCountryId(jsonResponseObject.getString("countryId"));
				resultPojo.setSearchResultCount(jsonResponseObject.getString("search_result_count"));
				
				resultPojo.setSubjectName(jsonResponseObject.getString("searchParameterSubjectName"));
				resultPojo.setCourseTypeName(jsonResponseObject.getString("searchParameterCourseTypeName"));
				resultPojo.setCountryName(jsonResponseObject.getString("searchParameterCountryName"));
				
				
				JSONArray jArray = jsonResponseObject.getJSONArray("search_result");
				
				for(int i = 0; i < jArray.length(); i++){

					JSONObject jObj = jArray.getJSONObject(i);
					
					CoursePojo cPojo = new CoursePojo();
					cPojo.setCourseSubjectName(jObj.getString("courseSubject"));
					cPojo.setCourseSubjectId(jObj.getString("courseSubjectId"));
					cPojo.setCourseDuration(jObj.getString("courseDuration"));
					cPojo.setCourseFeeInWords(jObj.getString("courseFeeInDollars"));
					cPojo.setCourseFeeInNaira(jObj.getString("courseFeeInWords"));
					cPojo.setId(Long.valueOf(jObj.getString("courseId")));
					cPojo.setCourseSchoolName(jObj.getString("schoolName"));
					cPojo.setCourseType(jObj.getString("courseTypeName"));
					cPojo.setCourseTypeId(jObj.getString("courseTypeId"));
					cPojo.setName(jObj.getString("courseName"));
					cPojo.setCourseSchoolId(jObj.getString("schoolId"));
					cPojo.setCourseSchoolLogoUrl(jObj.getString("schoolLogoURL"));
					String courseFeeInNaira = jObj.getString("courseFeeInNaira");
					if(courseFeeInNaira == null || courseFeeInNaira.equalsIgnoreCase("0") || courseFeeInNaira.isEmpty()){
						cPojo.setCourseFeeInNaira(null);
					}else{
						cPojo.setCourseFeeInNaira(jObj.getString("courseFeeInNaira"));
					}
					searchResultPojo.add(cPojo);
	
				}
				
				resultPojo.setSearchResultCol(searchResultPojo);
				
				 
		        String nextPageURL = router.getReverseRoute(SearchController.class, "getCoursesBySearchParameters");
		        appUtil.updateSessionURL(session, "getCoursesBySearchParameters",nextPageURL);
		        
		        ActiveSessionPojo activeSessionPojo = null;
		        HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		    	if(userSessionMap != null){
		    		activeSessionPojo = userSessionMap.get(session.getId());
		    	}
	    		if(activeSessionPojo != null){
		        	result.render(activeSessionPojo);
		        }
		    	
				result.render("searchResultPojo",resultPojo);
				result.template(SkoolaConstants.SEARCH_RESULT_PAGE);
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        return result;
	    }
	 

	 public Result goBackToCourseSearchResult(Context context,FlashScope flashScope,Session session) {
		 Result result = Results.html();
		 
			context.setAttribute(SkoolaConstants.REQUEST_TYPE_GO_BACK, SkoolaConstants.REQUEST_TYPE_GO_BACK);
			
			
			result = getCoursesBySearchParameters(context,flashScope,session);
			
		 
		 return result;
	 }
	 
	 
	 public Result handleGoBackToCourseDetail(Context context,FlashScope flashScope,Session session) {
		 Result result = Results.html();
		 
			context.setAttribute(SkoolaConstants.REQUEST_TYPE_GO_BACK, SkoolaConstants.REQUEST_TYPE_GO_BACK);
			
			
			result = showCourseDetail(context,flashScope);
			
		 
		 return result;
	 }
	 
	 
	 public Result applyForACourse(Context context,FlashScope flashScope) {
		 Session session = context.getSession();
		 Result result = Results.html();
		 result = showCourseDetail(context, flashScope);
		 result.redirect("/go_to_carreer_consultation");
		
		 return result;
	 }
	 
	 
	 /*public void sendMail() {
		    
	        Mail mail = mailProvider.get();

	        // fill the mail with content:
	        mail.setSubject("Test Mail");

	        mail.setFrom("iotradingviewtwo@gmail.com");


	        mail.setCharset("utf-8");
	       

	        mail.addTo("jemeokafor@gmail.com");
	      


	        mail.setBodyHtml("Body ");

	        mail.setBodyText("Body Text ");

	        // finally send the mail
	        try {
	            postoffice.send(mail);
	        } catch (EmailException | AddressException e) {
	            // ...
	        	e.printStackTrace();
	        } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }*/
}



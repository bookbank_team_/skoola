package controllers.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.AppUtil;
import common.SkoolaConstants;
import common.WSUtil;
import controllers.account.AccountController;

import filters.sessionfilter.ApplicationRoleFilter;
import filters.sessionfilter.SessionFilter;
import ninja.Context;
import ninja.FilterWith;

import ninja.Context;

import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;
import pojo.ActiveSessionPojo;
import pojo.ApplicationUserPojo;
import pojo.ConsultationRequestPojo;
import pojo.CoursePojo;
import pojo.CourseSubjectGroupPojo;
import pojo.CourseSubjectPojo;
import pojo.CourseTypePojo;

import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.form.SigninFormPojo;

@Singleton
public class AdminController {
	
	private Logger log = Logger.getLogger(AdminController.class);
	@Inject 
    NinjaProperties ninjaProperties;
	@Inject
	NinjaCache ninjaCache;
	
	private WSUtil wsUtil = new WSUtil();

	private AppUtil appUtil = new AppUtil();
	
	
	@FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
	public Result handleShowAdminDashBoard(FlashScope flashScope,Context context) {
		
		SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ADMIN_DASHBOARD_STATISTICS; 
        String apiKey = SkoolaConstants.API_KEY;      
        Result result = Results.html();
        ActiveSessionPojo activeSessionPojo = (ActiveSessionPojo) context.getAttribute("activeSessionPojo");
        result.render(activeSessionPojo);
        //result = appUtil.renderApplicationSession(result, context,ninjaCache);
       
       
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

		
        String requestString = new Gson().toJson(requestObject);
        
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);	        
         
        try {
			JSONObject jObject  = new JSONObject(responseObject.getData());
			String totalSignUpsToday = jObject.getString("totalSignUpsToday");
			String totalSignUpsThisWeek = jObject.getString("totalSignUpsThisWeek");
			String totalPaidConsultationsToday = jObject.getString("totalPaidConsultationsToday");
			String totalPaidConsultatioinsThisWeek = jObject.getString("totalPaidConsultatioinsThisWeek");
			
			 
	        System.out.println(" totalSignUpsToday = " + totalSignUpsToday);
			System.out.println(" totalSignUpsThisWeek = " + totalSignUpsThisWeek);
			System.out.println(" totalPaidConsultationsToday = " + totalPaidConsultationsToday);
			System.out.println(" totalPaidConsultatioinsThisWeek = " + totalPaidConsultatioinsThisWeek);
			
			result.render("totalSignUpsToday", totalSignUpsToday);
			result.render("totalSignUpsThisWeek", totalSignUpsThisWeek);
			result.render("totalPaidConsultationsToday", totalPaidConsultationsToday);
			result.render("totalPaidConsultatioinsThisWeek", totalPaidConsultatioinsThisWeek);
			result.html().template(SkoolaConstants.ADMIN_DASHBOARD);
        }catch(Exception ex){
        	ex.printStackTrace();
        }	 	
		
		 return result;
       

    }
 
	
	/*public Result handleShowMyCareerConsultationRequests(FlashScope flashScope,SigninFormPojo signinFormPojo,Context context) {
		Session session = context.getSession();
		

		ActiveSessionPojo activeSessionPojo = null;
	     HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		 	if(userSessionMap != null){
		 		activeSessionPojo = userSessionMap.get(session.getId());
		 	}
		
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_CONSULTATION_REQUEST_BY_PORTALUSER_USERID; 
        String apiKey = SkoolaConstants.API_KEY;      
        Result result = Results.html();
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
    	String userId = activeSessionPojo.getUserUniqueId();
		String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");	    		
		
		
		JSONObject jsonRequestObject = new JSONObject();
		try {
			jsonRequestObject.put("userId",userId);
			jsonRequestObject.put("searchIndex", searchIndex);
>>>>>>> e57a1d0bb16b23776ff4cabb1cbcaae3107c30cb
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
<<<<<<< HEAD
        
		
		
		 return result;
       

    }
 
	

	@FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
=======
		
		
		
		requestObject.setData(jsonRequestObject.toString());
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
       
        Collection<ConsultationRequestPojo> consultationRequestCol = new ArrayList<ConsultationRequestPojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        try{

	        if(responseObject.getSuccessful()){
	             	
	    		jObject = new JSONObject(responseObject.getData());   		
	    		jArray = (JSONArray) jObject.get("consultation_request_result");
	    		
	    		searchIndex = (String) jObject.get("search_index");
	    		searchResultCount = (String) jObject.get("search_result_count");
	    		
	    		for(int i = 0; i < jArray.length(); i++){
	    			JSONObject jsonObject = jArray.getJSONObject(i);
	    			
	    			ConsultationRequestPojo pojo = new ConsultationRequestPojo();
	    			pojo.setConsultationDate((String)jsonObject.get("consultationDate"));
	    			pojo.setConsultationMode(jsonObject.getString("consultationMode"));
	    			pojo.setConsultationStatus(jsonObject.getString("consultationStatus"));
	    			pojo.setDateCreated(jsonObject.getString("dateCreated"));
	    			pojo.setCourseName(jsonObject.getString("courseName"));
	    			pojo.setId(jsonObject.getString("id"));
	    			
	    			consultationRequestCol.add(pojo);
	    		}
	    		result.render("consultationRequestCol",consultationRequestCol);
        	}

   		 result.template(SkoolaConstants.ACCOUNT_PROFILE_PAGE);
   		 result.render(activeSessionPojo);  		 
   		 result.render("searchIndex",searchIndex);
   		 result.render("SEARCH_MAXIMUM_RESULTSET_SIZE",SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
   		 result.render("searchResultCount",searchResultCount);   		 
   		 result.render(SkoolaConstants.CURRENT_PAGE_KEY, SkoolaConstants.USER_ACCOUNT_PAGE_MY_CAREER_CONSULTATION);
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }

		 return result;

   }*/
	
	
    

    public Result handleShowApplicationUsers(FlashScope flashScope,Context context) {
			Session session = context.getSession(); 
		    SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
	        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_APPLICATION_USERS_RESULT; 
	        String apiKey = SkoolaConstants.API_KEY;      
	        Result result = Results.html();

	        result = appUtil.renderApplicationSession(result, context,ninjaCache);
	       
	        
	        String requestURI = context.getRoute().getUri();
	        Boolean isAdminRoleType = false;
	        
	        if(requestURI.equalsIgnoreCase("/admin_application_users")){
	        	isAdminRoleType = false;
	        }else if(requestURI.equalsIgnoreCase("/admin_backend_users")){
	        	isAdminRoleType = true;
	        }
	       


	        requestObject.setApi_key(apiKey);
	        requestObject.setData("");
	        requestObject.setHash("");

			String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");	    		
			
			
			JSONObject jsonRequestObject = new JSONObject();
			try {
				jsonRequestObject.put("searchIndex", searchIndex);

				jsonRequestObject.put("isAdminRoleType", isAdminRoleType);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			requestObject.setData(jsonRequestObject.toString());
	        String requestString = new Gson().toJson(requestObject);
	        
	        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
	        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
	        System.out.println(" appuser response string  = " + response);
	        Collection<ApplicationUserPojo> applicationUserCol = new ArrayList<ApplicationUserPojo>();
	        
	        JSONArray jArray = new JSONArray();
	        JSONObject jObject = new JSONObject();
	        String searchResultCount = "0";
	        try{

		        if(responseObject.getSuccessful()){
		             	
		    		jObject = new JSONObject(responseObject.getData());   		
		    		jArray = (JSONArray) jObject.get("application_user_result");
		    		
		    		searchIndex = (String) jObject.get("search_index");
		    		searchResultCount = (String) jObject.get("search_result_count");
		    		
		    		for(int i = 0; i < jArray.length(); i++){
		    			JSONObject jsonObject = jArray.getJSONObject(i);
		    			
		    			ApplicationUserPojo pojo = new ApplicationUserPojo();
		    			pojo.setDateCreated((String)jsonObject.get("dateCreated"));
		    			pojo.setEmail(jsonObject.getString("email"));
		    			pojo.setFirstName(jsonObject.getString("firstName"));
		    			pojo.setLastName(jsonObject.getString("lastName"));
		    			pojo.setPhoneNumber(jsonObject.getString("phoneNumber"));
		    			pojo.setRole(jsonObject.getString("role"));
		    			pojo.setUserName(jsonObject.getString("userName"));
		    			pojo.setStatus(jsonObject.getString("status"));
		    			pojo.setUserId(jsonObject.getString("userId").toUpperCase());
		    			
		    			applicationUserCol.add(pojo);
		    		}
		    		result.render("applicationUserCol",applicationUserCol);
	        	}

	    		
	        }catch(Exception ex){
	        	ex.printStackTrace();
	        }
    
	        
		 result.template(SkoolaConstants.ADMIN_APPLICATION_USERS);
		 result.render("searchIndex",searchIndex);
		 result.render("searchResultCount",searchResultCount);

		 result.render("SEARCH_MAXIMUM_RESULTSET_SIZE",SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE); //
		 return result;
    }
    
    
    
	@FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
    public Result handleShowConsultationApplication(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_CAREER_CONSULTATION_APPLICATION_REQUESTS_RESULT; 
        String apiKey = SkoolaConstants.API_KEY;      
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context,ninjaCache);
       
        
      
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

		String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");
		String requestStatus = context.getParameter("requestStatus") == null ? "" : context.getParameter("requestStatus");
		
		System.out.println(" search index = " + searchIndex);
		System.out.println(" request status = " + requestStatus);
		
		
		JSONObject jsonRequestObject = new JSONObject();
		try {
			jsonRequestObject.put("searchIndex", searchIndex);
			jsonRequestObject.put("requestStatus", requestStatus);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		requestObject.setData(jsonRequestObject.toString());
        String requestString = new Gson().toJson(requestObject);
        
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);	        
        Collection<ConsultationRequestPojo> consultationRequestCol = new ArrayList<ConsultationRequestPojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        try{

	        if(responseObject.getSuccessful()){
	             	
	    		jObject = new JSONObject(responseObject.getData());   		
	    		jArray = (JSONArray) jObject.get("consultation_request_result");
	    		
	    		searchIndex = (String) jObject.get("search_index");
	    		searchResultCount = (String) jObject.get("search_result_count");
	    		requestStatus = (String) jObject.get("request_status");
	    		
	    		for(int i = 0; i < jArray.length(); i++){
	    			JSONObject jsonObject = jArray.getJSONObject(i);
	    			System.out.println(" jsonobjec of consultation request : " + jsonObject);
	    			ConsultationRequestPojo pojo = new ConsultationRequestPojo();
	    			pojo.setDateCreated((String)jsonObject.get("dateCreated"));
	    			pojo.setEmail(jsonObject.getString("email"));
	    			pojo.setFirstName(jsonObject.getString("firstName"));
	    			pojo.setLastName(jsonObject.getString("lastName"));
	    			pojo.setPhoneNumber(jsonObject.getString("phoneNumber"));
	    			pojo.setConsultationStatus(jsonObject.getString("consultationStatus"));
	    			pojo.setConsultationMode(jsonObject.getString("consultationMode"));
	    			pojo.setCourseName(jsonObject.getString("courseName"));
	    			pojo.setSubjectName(jsonObject.getString("courseSubject"));
	    			pojo.setId(jsonObject.getString("id"));
	    			
	    			consultationRequestCol.add(pojo);
	    		}
	    		result.render("consultationRequestCol",consultationRequestCol);
        	}

    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }

			
		 result.template(SkoolaConstants.ADMIN_CAREER_CONSULTATION_APPLICATION);
		 result.render("searchIndex",searchIndex);
		 result.render("searchResultCount",searchResultCount);
		 result.render("requestStatus",requestStatus);
		 result.render("SEARCH_MAXIMUM_RESULTSET_SIZE",SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
		 return result;
    }
    
	@FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
    public Result handleShowSkoolaCourses(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_SKOOLA_COURSES; 
        String apiKey = SkoolaConstants.API_KEY;      
        Result result = Results.html();
        
        result = appUtil.renderApplicationSession(result, context,ninjaCache);
		result = getAllCourseType(result, flashScope, session);
		result = getAllSubjectType(result, flashScope, session);
		result = appUtil.handleGetAllSkoolaSchools(result, context, flashScope, ninjaCache, ninjaProperties);
       
        
      
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
       

		String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");
		String status = context.getParameter("status") == null ? "" : context.getParameter("status");
		String subjectIdParameter = context.getParameter("subjectIdParameter") == null ? "" : context.getParameter("subjectIdParameter");
		String courseTypeIdParameter = context.getParameter("courseTypeIdParameter") == null ? "" : context.getParameter("courseTypeIdParameter");
		String schoolIdParameter = context.getParameter("schoolIdParameter") == null ? "" : context.getParameter("schoolIdParameter");
		
		System.out.println(" ***************************** school parameter = " + schoolIdParameter);
		
		JSONObject jsonRequestObject = new JSONObject();
		try {
			jsonRequestObject.put("searchIndex", searchIndex);
			jsonRequestObject.put("status", status);
			jsonRequestObject.put("subjectIdParameter", subjectIdParameter);
			jsonRequestObject.put("courseTypeIdParameter", courseTypeIdParameter);
			jsonRequestObject.put("schoolIdParameter", schoolIdParameter);
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		requestObject.setData(jsonRequestObject.toString());
        String requestString = new Gson().toJson( requestObject);
        
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);	        
        Collection<CoursePojo> courseCol = new ArrayList<CoursePojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        
        try{

	        if(responseObject.getSuccessful()){
	             	
	    		jObject = new JSONObject(responseObject.getData());   		
	    		jArray = (JSONArray) jObject.get("course_result");
	    		
	    		searchIndex = (String) jObject.get("search_index");
	    		searchResultCount = String.valueOf(jObject.get("resultCount"));
	    		
	    		
	    		for(int i = 0; i < jArray.length(); i++){
	    			JSONObject jsonObject = jArray.getJSONObject(i);
	    			
	    			Long id = jsonObject.getString("courseId") == null ? 0l : Long.valueOf(jsonObject.getString("courseId"));
	    			String name = jsonObject.getString("courseName") == null ? "" : jsonObject.getString("courseName");
	    			String courseDuration = jsonObject.getString("courseDuration") == null ?"" : jsonObject.getString("courseDuration");
	    			String courseSummary = jsonObject.getString("courseSummary") == null ? "" : jsonObject.getString("courseSummary");
	    			Boolean ieltRequired = jsonObject.getBoolean("ieltsRequired") == false ? false : true;
	    			String courseRequirements = jsonObject.getString("courseRequirements") == null ? "" : jsonObject.getString("courseRequirements");
	    			String programModules = jsonObject.getString("programModules") == null ? "" : jsonObject.getString("programModules");
	    			String courseVenue = jsonObject.getString("courseVenue") == null ? "" : jsonObject.getString("courseVenue").replace(",", " ").replace("\n", " ");
	    			String courseSubject = jsonObject.getString("courseSubject") == null ? "" : jsonObject.getString("courseSubject");
	    			String courseSubjectId = jsonObject.getString("courseSubjectId") == null ? "" : jsonObject.getString("courseSubjectId");
	    			String courseSubjectGroupName = jsonObject.getString("courseSubjectGroupName") == null ? "" : jsonObject.getString("courseSubjectGroupName");
	    			String courseTypeName = jsonObject.getString("courseTypeName") == null ? "" : jsonObject.getString("courseTypeName");
	    			String schoolId = jsonObject.getString("schoolId") == null ? "" : jsonObject.getString("schoolId");
	    			String schoolLogoURL = jsonObject.getString("schoolLogoURL") == null ? "" : jsonObject.getString("schoolLogoURL");
	    			String schoolName = jsonObject.getString("schoolName") == null ? "" : jsonObject.getString("schoolName");
	    			String feeInNaira = jsonObject.getString("feeInNaira") == null ? "" : jsonObject.getString("feeInNaira");
	    			String courseFeeInWords = jsonObject.getString("courseFeeInWords") == null ? "" : jsonObject.getString("courseFeeInWords");
	    			
	    		
	    			CoursePojo pojo = new CoursePojo();
	    			pojo.setId(id);
	    			pojo.setName(name);
	    			pojo.setCourseDuration(courseDuration);
	    			pojo.setCourseSummary(courseSummary);
	    			pojo.setIeltsRequired(ieltRequired);
	    			pojo.setCourseRequirements(courseRequirements);
	    			pojo.setProgramModules(programModules);
	    			pojo.setCourseVenue(courseVenue);
	    			pojo.setCourseSubjectName(courseSubject);
	    			pojo.setCourseSubjectId(courseSubjectId);
	    			pojo.setCourseSubjectGroupName(courseSubjectGroupName);
	    			pojo.setCourseType(courseTypeName);
	    			pojo.setCourseSchoolId(schoolId);
	    			pojo.setCourseSchoolLogoUrl(schoolLogoURL);
	    			pojo.setCourseSchoolName(schoolName);
	    			pojo.setCourseFeeInNaira(feeInNaira);
	    			pojo.setCourseFeeInWords(courseFeeInWords);
	    			
	    			courseCol.add(pojo);
	    		}
	    		
        	}

    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
	 	
        System.out.println(" size of courseCol = " + courseCol.size());
		 result.render("searchIndex",searchIndex);
		 result.render("courseCol",courseCol);
		 result.render("status",status);
		 result.render("searchResultCount",searchResultCount);
		 result.render("subjectIdParameter",subjectIdParameter);
		 result.render("courseTypeIdParameter",courseTypeIdParameter);
		 result.render("schoolIdParameter",schoolIdParameter);
		 result.render("SEARCH_MAXIMUM_RESULTSET_SIZE",SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
		 result.template(SkoolaConstants.ADMIN_SKOOLA_COURSES);		 
		 return result;
    }
    
    
    

    
    
	@FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
	
    public Result getAllSubjectType(Result result,FlashScope flashScope,Session session) {
     	
       SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
       String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_TYPE; 
       String apiKey = SkoolaConstants.API_KEY;      
       
       requestObject.setApi_key(apiKey);
       requestObject.setData("");
       requestObject.setHash("");
       
       String requestString = new Gson().toJson(requestObject);
      
       String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
      
       if(!responseObject.getSuccessful()){
       	flashScope.error(responseObject.getMessage());
       	return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
       }
       Collection<CourseSubjectPojo> courseSubjectCol = new ArrayList<CourseSubjectPojo>();
       
       JSONArray jArray = new JSONArray();
       JSONObject jObject = new JSONObject();
       try{

   		jObject = new JSONObject(responseObject.getData());  
   		
   		jArray = (JSONArray) jObject.get("course_subject_type_list");
   		for(int i = 0; i < jArray.length(); i++){
   			JSONObject jsonObject = jArray.getJSONObject(i);
   			
   			CourseSubjectPojo pojo = new CourseSubjectPojo();
   			pojo.setSubjectDisplayName(jsonObject.getString("name"));
   			pojo.setId(jsonObject.getString("id"));
   			pojo.setSubjectName(jsonObject.getString("name"));
   			pojo.setType("Subject");
   			
   			courseSubjectCol.add(pojo);
   		}
   		
   		
       }catch(Exception ex){
       	ex.printStackTrace();
       }
       result.render("courseSubjectCol", courseSubjectCol);
       return result;
   }
	
	
	  
		@FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
		
		public Result getAllSkoolaCourseSubjectCategories(Result result,FlashScope flashScope,Session session) {
	     	
	       SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
	       String methodUrl = SkoolaConstants.URL_GET_ALL_SKOOLA_COURSE_SUBJECT_CATEGORIES; 
	       String apiKey = SkoolaConstants.API_KEY;      
	       
	       requestObject.setApi_key(apiKey);
	       requestObject.setData("");
	       requestObject.setHash("");
	       
	       String requestString = new Gson().toJson(requestObject);
	      
	       String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
	       SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
	      
	       if(!responseObject.getSuccessful()){
	       	flashScope.error(responseObject.getMessage());
	       	return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
	       }
	       Collection<CourseSubjectGroupPojo> courseSubjectGroupCol = new ArrayList<CourseSubjectGroupPojo>();
	       
	       JSONArray jArray = new JSONArray();
	       JSONObject jObject = new JSONObject();
	       try{

	   		jObject = new JSONObject(responseObject.getData());  
	   		
	   		jArray = (JSONArray) jObject.get("course_subject_group_list");
	   		for(int i = 0; i < jArray.length(); i++){
	   			
	   			JSONObject jsonObject = jArray.getJSONObject(i);
	   			
	   			String courseSubjectGroupDisplayName = jsonObject.getString("name") == null ? "" : jsonObject.getString("name");
	   			String courseSubjectGroupName = jsonObject.getString("name") == null ? "" : jsonObject.getString("name");
	   			String id = jsonObject.getString("id") == null ? "" : jsonObject.getString("id");
	   			
	   			CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
	   			pojo.setCourseSubjectGroupDisplayName(courseSubjectGroupDisplayName);
	   			pojo.setCourseSubjectGroupName(courseSubjectGroupName);
	   			pojo.setId(id);
	   			
	   			
	   			courseSubjectGroupCol.add(pojo);
	   		}
	   		
	   		
	       }catch(Exception ex){
	       	ex.printStackTrace();
	       }
	       result.render("courseSubjectGroupCol", courseSubjectGroupCol);
	       return result;
	   }
		
	
	
	
    public Result getAllSkoolaSchools(Result result,FlashScope flashScope,Session session) {
     	
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_TYPE; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        requestObject.setData(new JSONObject().toString());
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
       
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        }
        Collection<CourseSubjectPojo> courseSubjectCol = new ArrayList<CourseSubjectPojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try{

    		jObject = new JSONObject(responseObject.getData());  
    		
    		jArray = (JSONArray) jObject.get("course_subject_type_list");
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = jArray.getJSONObject(i);
    			
    			CourseSubjectPojo pojo = new CourseSubjectPojo();
    			pojo.setSubjectDisplayName(jsonObject.getString("name"));
    			pojo.setId(jsonObject.getString("id"));
    			pojo.setSubjectName(jsonObject.getString("name"));
    			pojo.setType("Subject");
    			
    			courseSubjectCol.add(pojo);
    		}
    		
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        result.render("courseSubjectCol", courseSubjectCol);
        return result;
    }
    
    
    
    public Result getAllCourseType(Result result,FlashScope flashScope,Session session) {
      	 
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_COURSE_TYPE; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	 return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        }
        Collection<CourseTypePojo> courseTypeCol = new ArrayList<CourseTypePojo>();
        
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try{

    		jObject = new JSONObject(responseObject.getData());   		
    		jArray = (JSONArray) jObject.get("course_type_list");
    		
    		for(int i = 0; i < jArray.length(); i++){
    			JSONObject jsonObject = jArray.getJSONObject(i);
    			CourseTypePojo pojo = new CourseTypePojo();
    			pojo.setDescription("");
    			pojo.setId(jsonObject.getString("id"));
    			pojo.setName(jsonObject.getString("name"));
    			
    			courseTypeCol.add(pojo);
    		}
    		
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        
        result.render("courseTypeCol", courseTypeCol);
        return result;
    

    }
    
    public Result handleShowBackEndUsers(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		ActiveSessionPojo activeSessionPojo = null;
	     HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		 	if(userSessionMap != null){
		 		activeSessionPojo = userSessionMap.get(session.getId());
		 	}

	 	 Result result = Results.html();	 	
		 result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
		 result.render(activeSessionPojo);
		 return result;
    }
   
  
    public Result handleShowSkoolaExams(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		ActiveSessionPojo activeSessionPojo = null;
	     HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		 	if(userSessionMap != null){
		 		activeSessionPojo = userSessionMap.get(session.getId());
		 	}

	 	 Result result = Results.html();	 	
		 result.template(SkoolaConstants.ADMIN_SKOOLA_EXAMS);
		 result.render(activeSessionPojo);
		 return result;
    }
    public Result handleShowSkoolaUniversities(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		ActiveSessionPojo activeSessionPojo = null;
	     HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		 	if(userSessionMap != null){
		 		activeSessionPojo = userSessionMap.get(session.getId());
		 	}

	 	 Result result = Results.html();	 	
		 result.template(SkoolaConstants.ADMIN_SKOOLA_UNIVERSITIES);
		 result.render(activeSessionPojo);
		 return result;
    }
    public Result handleShowSkoolaCountries(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		ActiveSessionPojo activeSessionPojo = null;
	     HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		 	if(userSessionMap != null){
		 		activeSessionPojo = userSessionMap.get(session.getId());
		 	}

	 	 Result result = Results.html();	 	
		 result.template(SkoolaConstants.ADMIN_SKOOLA_COUNTRIES);
		 result.render(activeSessionPojo);
		 return result;
    }
    
    
    public Result handleShowEditCourse(FlashScope flashScope,Context context) {
		Session session = context.getSession(); 
		 Result result = Results.html();
		 result = appUtil.renderApplicationSession(result, context,ninjaCache);	
		 result = getAllCourseType(result, flashScope, session);
		 result = getAllSubjectType(result, flashScope, session);
		 result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);
		 result = appUtil.handleGetAllSkoolaSchools(result, context, flashScope, ninjaCache, ninjaProperties);
		 
 	
	 	String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
	 	String status = context.getParameter("status") == null ? "" : context.getParameter("status");
	 	String subjectIdParameter = context.getParameter("subjectIdParameter") == null ? "" : context.getParameter("subjectIdParameter");
	 	String courseTypeIdParameter = context.getParameter("courseTypeIdParameter") == null ? "" : context.getParameter("courseTypeIdParameter");
	 	String schoolIdParameter = context.getParameter("schoolIdParameter") == null ? "" : context.getParameter("schoolIdParameter");
	 	String courseId = context.getParameter("courseId") == null ? "" : context.getParameter("courseId");
	 	System.out.println(" school id param  = " + schoolIdParameter);
	 	
	 	System.out.println(" #######################  status = " + status );
	 	
	 	if(courseId == null || courseId.isEmpty()){
	 		flashScope.error("Invalid request : course Id is not available.");
	 		return Results.redirect("/admin_skoola_courses");
	 	}
	 	
	 	
	 	
	 	SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_COURSE_DETAIL; 
        String apiKey = SkoolaConstants.API_KEY;      
	        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        JSONObject jsonObject = new JSONObject();
        try {
			jsonObject.put("courseId", courseId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	        
        requestObject.setData(jsonObject.toString());       
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
       
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	return Results.redirect("/admin_skoola_courses");
        }
		
       
        CoursePojo pojo = null;
		try {
			pojo = new Gson().fromJson(responseObject.getData(), CoursePojo.class);
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        if(pojo == null){
        	flashScope.error("Invalide Response : ");
        	return Results.redirect("/admin_skoola_courses");
        }
		 	
        System.out.println( " json data = " + responseObject.getData());
		 result.render("coursePojo", pojo);	
		 result.render("course_searchIndex", searchIndex);
		 result.render("course_status", status);
		 result.render("course_subjectIdParameter", subjectIdParameter);
		 result.render("course_courseTypeIdParameter", courseTypeIdParameter);
		 result.render("course_schoolIdParameter", schoolIdParameter);
		 result.render("course_courseId", courseId);
		 result.template(SkoolaConstants.ADMIN_EDIT_SKOOLA_COURSE);
		 return result;
    }
   
    
    
    public Result handleCreateORUpdateSkoolaCourse(FlashScope flashScope,Context context) {
		 System.out.println(" 0");
    	 Session session = context.getSession(); 
		 Result result = Results.html();
		 result = appUtil.renderApplicationSession(result, context,ninjaCache);	
		 result = getAllCourseType(result, flashScope, session);
		 result = getAllSubjectType(result, flashScope, session);
		 result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);
		 result = appUtil.handleGetAllSkoolaSchools(result, context, flashScope, ninjaCache, ninjaProperties);
		 
		 System.out.println(" 1 ");
 	
	 	String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
	 	String status = context.getParameter("status") == null ? "" : context.getParameter("status");
	 	String subjectIdParameter = context.getParameter("subjectIdParameter") == null ? "" : context.getParameter("subjectIdParameter");
	 	String courseTypeIdParameter = context.getParameter("courseTypeIdParameter") == null ? "" : context.getParameter("courseTypeIdParameter");
	 	String schoolIdParameter = context.getParameter("schoolIdParameter") == null ? "" : context.getParameter("schoolIdParameter");
	 	String courseId = context.getParameter("courseId") == null ? "" : context.getParameter("courseId");
	 	
	 	String cu_course_name = context.getParameter("cu_course_name") == null ? "" : context.getParameter("cu_course_name");
	 	String cu_course_type = context.getParameter("cu_course_type") == null ? "" : context.getParameter("cu_course_type");	 	
	 	String cu_course_subject_group = context.getParameter("cu_course_subject_group") == null ? "" : context.getParameter("cu_course_subject_group");
	 	String cu_course_subject = context.getParameter("cu_course_subject") == null ? "" : context.getParameter("cu_course_subject");
	 	String cu_course_school = context.getParameter("cu_course_school") == null ? "" : context.getParameter("cu_course_school");
	 	String cu_course_venue = context.getParameter("cu_course_venue") == null ? "" : context.getParameter("cu_course_venue");
	 	String cu_course_duration = context.getParameter("cu_course_duration") == null ? "" : context.getParameter("cu_course_duration");
	 	String cu_course_fee_in_figures = context.getParameter("cu_course_fee_in_figures") == null ? "" : context.getParameter("cu_course_fee_in_figures");
	 	String cu_course_fee_in_dollars = context.getParameter("cu_course_fee_in_dollars") == null ? "" : context.getParameter("cu_course_fee_in_dollars");
	 	String cu_course_requirements = context.getParameter("cu_course_requirements") == null ? "" : context.getParameter("cu_course_requirements");
	 	String cu_ielt_required = context.getParameter("cu_ielt_required") == null ? "" : context.getParameter("cu_ielt_required");
	 	String cu_course_summary = context.getParameter("cu_course_summary") == null ? "" : context.getParameter("cu_course_summary");
	 	String cu_course_module = context.getParameter("cu_course_module") == null ? "" : context.getParameter("cu_course_module");
	 	
	 	 System.out.println(" cu_course_type  =" + cu_course_type);
	 	 
	 	System.out.println(" cu_course_subject = " + cu_course_subject);
	 	
	 	System.out.println(" cu_course_name = " + cu_course_name);
	 	
	 	System.out.println(" subjectIdParameter =  " + subjectIdParameter);
	 	
	 	System.out.println(" courseTypeIdParameter = " + courseTypeIdParameter);
	 	
	 	System.out.println(" schoolIdParameter = " + schoolIdParameter);
	 	
	 	if(cu_course_type.isEmpty() || cu_course_subject.isEmpty() || cu_course_name.isEmpty() 
	 			
	 			|| (cu_course_school.isEmpty()) ){
	 		flashScope.error("Please Enter valid valid values in the required fields.");
	 		System.out.println("$$$$$$$$$$$$$$$$$$$$$ returning because parameter is empty ");
	 		return handleShowEditCourse( flashScope, context);
	 	}
	 	
	 	
	 	 System.out.println(" 3 ");
	 	
	 	SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_CREATE_OR_UPDATE_COURSE_DETAIL; 
        String apiKey = SkoolaConstants.API_KEY; 
        
        System.out.println(" 4 ");
	        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        JSONObject jsonObject = new JSONObject();
        try {
        	jsonObject.put("courseId", courseId);
			jsonObject.put("cu_course_name", cu_course_name);
			jsonObject.put("cu_course_type", cu_course_type);
			jsonObject.put("cu_course_subject_group", cu_course_subject_group);
			jsonObject.put("cu_course_subject", cu_course_subject);
			jsonObject.put("cu_course_school", cu_course_school);
			jsonObject.put("cu_course_venue", cu_course_venue);
			jsonObject.put("cu_course_duration", cu_course_duration);
			jsonObject.put("cu_course_fee_in_figures", cu_course_fee_in_figures);
			jsonObject.put("cu_course_fee_in_dollars", cu_course_fee_in_dollars);
			jsonObject.put("cu_course_requirements", cu_course_requirements);
			jsonObject.put("cu_ielt_required", cu_ielt_required);
			jsonObject.put("cu_course_summary", cu_course_summary);
			jsonObject.put("cu_course_module", cu_course_module);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	        
        System.out.println(" 5 ");
        requestObject.setData(jsonObject.toString());       
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
       
        System.out.println(" 6 ");
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	return Results.redirect("/admin_skoola_courses");
        }
		

        System.out.println(" 7 ");

        flashScope.success("Course was updated successfully !!!");
		 result.render("searchIndex", searchIndex);	
		 result.render("status", status);
		 result.render("subjectIdParameter", subjectIdParameter);
		 result.render("courseTypeIdParameter", courseTypeIdParameter);
		 result.render("schoolIdParameter", schoolIdParameter);
		 result.render("courseId", courseId);		
		 return handleShowSkoolaCourses(flashScope, context);
    }

}

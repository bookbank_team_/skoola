package controllers.signin;

import java.util.HashMap;

import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pojo.ActiveSessionPojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.form.SigninFormPojo;
import pojo.form.SignupFormPojo;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.SkoolaConstants;
import common.WSUtil;
import controllers.signup.SignUpController;

@Singleton
public class SigninController {
	
	private Logger log = Logger.getLogger(SignUpController.class);
	@Inject
	NinjaCache ninjaCache;
	
	@Inject 
    NinjaProperties ninjaProperties;
	private WSUtil wsUtil = new WSUtil();
	
    
    public Result userSignin(Context context,FlashScope flashScope,SigninFormPojo signinFormPojo) {
    	
    	
    	Session session  = context.getSession();
    	String email = signinFormPojo.getEmail();
    	String password = signinFormPojo.getPassword();
    	Result result = Results.html();
    	
    	JSONObject obj = new JSONObject();
        obj.put("email", email);
        obj.put("password", password);
       
    	
    	if(email == null || email.isEmpty()){
    		flashScope.error("Enter a valid email.");
    		return Results.html().template(SkoolaConstants.SIGNIN_PAGE).render(signinFormPojo);
    	}
    	
    	if(password == null || password.isEmpty()){
    		flashScope.error("Enter a valid password.");
    		return Results.html().template(SkoolaConstants.SIGNIN_PAGE).render(signinFormPojo);
    	}
    	
 
        
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_USER_SIGNIN; 
        String apiKey = SkoolaConstants.API_KEY;      
        
        requestObject.setApi_key(apiKey);
        requestObject.setData(obj.toJSONString());
        requestObject.setHash("");
        
        String requestString = new Gson().toJson(requestObject);
       
        String response = wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
        
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        
       
        System.out.println(" response object = " + responseObject );
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());      	
        	 return Results.html().template(SkoolaConstants.SIGNIN_PAGE).render(signinFormPojo);
        }

        flashScope.success(responseObject.getMessage());      
        ActiveSessionPojo activeSessionPojo = new Gson().fromJson(responseObject.getData(), ActiveSessionPojo.class);
        HashMap<String , ActiveSessionPojo > userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if(userSessionMap == null){
        	userSessionMap = new HashMap<String ,ActiveSessionPojo>();
        	userSessionMap.put(session.getId(), activeSessionPojo);
        	ninjaCache.add(SkoolaConstants.ACTIVE_SESSION_MAP, userSessionMap);
        }else{
        	
        	ninjaCache.delete(SkoolaConstants.ACTIVE_SESSION_MAP);
        	userSessionMap.put(session.getId(), activeSessionPojo);
        	ninjaCache.add(SkoolaConstants.ACTIVE_SESSION_MAP, userSessionMap);
        }
        
        result.render(activeSessionPojo);
        if(activeSessionPojo.getIsStudent() != null && activeSessionPojo.getIsStudent()){
        
	        String lastURL = session.get(SkoolaConstants.LAST_OPERATION_URL);
	        if(lastURL != null && lastURL.equalsIgnoreCase("/go_to_carreer_consultation")){
	        	//Results.redirect("/go_to_carreer_consultation");
	        	System.out.println(" redirecting... ");
	        	return Results.redirectTemporary("/go_to_carreer_consultation");
	        	
	        }else{
	        	return result.html().template(SkoolaConstants.HOME_PAGE);
	        }
        }else{
        	System.out.println(" redirecting ...");
         return	Results.redirect("/admin_dashboard");
        	//result.html().template(SkoolaConstants.ADMIN_DASHBOARD);
        }
        

    }
    
    
    
    public Result userSignout(FlashScope flashScope,Session session) {
 
       // 
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if(userSessionMap != null){
        	System.out.println(" user session map is not null ");
        	if(userSessionMap.containsKey(session.getId())){
        		System.out.println(" user session map contains session id key ");
        		userSessionMap.remove(session.getId());
        		System.out.println(" clearing session now ");
        		session.clear();
        	}else{
        		System.out.println(" user session map does not contain session id");
        	}
        }else{
        	System.out.println(" user session map is  null ");
        }
       
        flashScope.success("You are signed out!");
        return Results.html().template(SkoolaConstants.HOME_PAGE);

    }
    

}

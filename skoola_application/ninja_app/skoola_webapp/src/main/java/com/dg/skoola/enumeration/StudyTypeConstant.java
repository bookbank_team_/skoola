package com.dg.skoola.enumeration;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class StudyTypeConstant
  implements Serializable, Comparable
{
  private static final long serialVersionUID = -7753910252431047897L;
  public static final StudyTypeConstant FULL_TIME = new StudyTypeConstant("FULL TIME");

  public static final StudyTypeConstant PART_TIME = new StudyTypeConstant("PART TIME");

  public static final StudyTypeConstant ONLINE = new StudyTypeConstant("ONLINE");
  private String value;
  private static final Map values = new LinkedHashMap(3, 1.0F);
  private static List literals = new ArrayList(3);
  private static List names = new ArrayList(3);
  private static List valueList = new ArrayList(3);

  private StudyTypeConstant(String value)
  {
    this.value = value;
  }

  protected StudyTypeConstant()
  {
  }

  public String toString()
  {
    return String.valueOf(this.value);
  }

  public static StudyTypeConstant fromString(String value)
  {
    StudyTypeConstant typeValue = (StudyTypeConstant)values.get(value);
    if (typeValue == null)
    {
      throw new IllegalArgumentException("invalid value '" + value + "', possible values are: " + literals);
    }
    return typeValue;
  }

  public String getValue()
  {
    return this.value;
  }

  public int compareTo(Object that)
  {
    return this == that ? 0 : getValue().compareTo(((StudyTypeConstant)that).getValue());
  }

  public static List literals()
  {
    return literals;
  }

  public static List names()
  {
    return names;
  }

  public static List values()
  {
    return valueList;
  }

  public boolean equals(Object object)
  {
    return (this == object) || (((object instanceof StudyTypeConstant)) && (((StudyTypeConstant)object).getValue().equals(getValue())));
  }

  public int hashCode()
  {
    return getValue().hashCode();
  }

  private Object readResolve()
    throws ObjectStreamException
  {
    return fromString(this.value);
  }

  static
  {
    values.put(FULL_TIME.value, FULL_TIME);
    valueList.add(FULL_TIME);
    literals.add(FULL_TIME.value);
    names.add("FULL_TIME");
    values.put(PART_TIME.value, PART_TIME);
    valueList.add(PART_TIME);
    literals.add(PART_TIME.value);
    names.add("PART_TIME");
    values.put(ONLINE.value, ONLINE);
    valueList.add(ONLINE);
    literals.add(ONLINE.value);
    names.add("ONLINE");
    valueList = Collections.unmodifiableList(valueList);
    literals = Collections.unmodifiableList(literals);
    names = Collections.unmodifiableList(names);
  }
}
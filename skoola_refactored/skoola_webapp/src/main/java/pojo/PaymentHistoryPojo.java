package pojo;

public class PaymentHistoryPojo {
	
	private String paymentStatus;
	private String amountInNaira;
	private String dateCreated;
	private String referenceId;
	private String processName;
	private String id;
	
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getAmountInNaira() {
		return amountInNaira;
	}
	public void setAmountInNaira(String amountInNaira) {
		this.amountInNaira = amountInNaira;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	

}

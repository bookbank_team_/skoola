package pojo;

import java.util.Collection;

public class SkoolaSearchResultPojo {	
	
	private Collection<CoursePojo> searchResultCol;
	private String searchResultCount;
	private String subjectId;
	private String countryId;
	private String courseTypeId;
	private String type;
	private String minimumTuition;
	private String maximumTuition;
	private String searchIndex;
	private String subjectName;
	private String countryName;
	private String courseTypeName;
	
	
	public Collection<CoursePojo> getSearchResultCol() {
		return searchResultCol;
	}
	public void setSearchResultCol(Collection<CoursePojo> searchResultCol) {
		this.searchResultCol = searchResultCol;
	}
	
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCourseTypeId() {
		return courseTypeId;
	}
	public void setCourseTypeId(String courseTypeId) {
		this.courseTypeId = courseTypeId;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getMinimumTuition() {
		return minimumTuition;
	}
	public void setMinimumTuition(String minimumTuition) {
		this.minimumTuition = minimumTuition;
	}
	public String getMaximumTuition() {
		return maximumTuition;
	}
	public void setMaximumTuition(String maximumTuition) {
		this.maximumTuition = maximumTuition;
	}

	
	public String getSearchResultCount() {
		return searchResultCount;
	}
	public void setSearchResultCount(String searchResultCount) {
		this.searchResultCount = searchResultCount;
	}
	public String getSearchIndex() {
		return searchIndex;
	}
	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}
	
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCourseTypeName() {
		return courseTypeName;
	}
	public void setCourseTypeName(String courseTypeName) {
		this.courseTypeName = courseTypeName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	

}

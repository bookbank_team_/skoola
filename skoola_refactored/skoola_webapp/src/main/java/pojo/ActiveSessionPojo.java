package pojo;

import java.io.Serializable;

public class ActiveSessionPojo  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5224163199569808299L;
	private Boolean active;
	private String portalUserId;
	private String userUniqueId;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private Boolean isAdmin;
	private Boolean isDataEntry;
	private Boolean isStudent;
	
	
	public String getPortalUserId() {
		return portalUserId;
	}
	public void setPortalUserId(String portalUserId) {
		this.portalUserId = portalUserId;
	}
	public String getUserUniqueId() {
		return userUniqueId;
	}
	public void setUserUniqueId(String userUniqueId) {
		this.userUniqueId = userUniqueId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Boolean getIsDataEntry() {
		return isDataEntry;
	}
	public void setIsDataEntry(Boolean isDataEntry) {
		this.isDataEntry = isDataEntry;
	}
	public Boolean getIsStudent() {
		return isStudent;
	}
	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}
	
	
	
	

}

package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
 
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
 
public class HttpUtils {
static {
try {
TrustManager[] trustAllCerts = { new X509TrustManager() {
public X509Certificate[] getAcceptedIssuers() {
return null;
}
 
public void checkClientTrusted(X509Certificate[] certs,
String authType) {
}
 
public void checkServerTrusted(X509Certificate[] certs,
String authType) {
}
} };
SSLContext sc = SSLContext.getInstance("SSL");
 
HostnameVerifier hv = new HostnameVerifier() {
public boolean verify(String arg0, SSLSession arg1) {
return true;
}
};
sc.init(null, trustAllCerts, new SecureRandom());
 
HttpsURLConnection
.setDefaultSSLSocketFactory(sc.getSocketFactory());
HttpsURLConnection.setDefaultHostnameVerifier(hv);
} catch (Exception localException) {
}
}
 
private static HttpUtils httpUtil;
 
private HttpUtils() {
}
 
public static HttpUtils getHttpUtil() {
if (httpUtil == null)
httpUtil = new HttpUtils();
 
return httpUtil;
}


 
public String doPost(String url, String data) throws Exception {
URL urlObj = new URL(url);
HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();

conn.setDoOutput(true);
conn.addRequestProperty("Content-Type", "application/json");
conn.addRequestProperty("Accept", "application/json");
//conn.addRequestProperty("Authorization", "sk_test_470832b29008cc54862a18aff3718b198061779a");
OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
 
writer.write(data);
writer.flush();
 
String line;
StringBuffer buffer = new StringBuffer();
BufferedReader reader = new BufferedReader(new InputStreamReader(
conn.getInputStream()));
while ((line = reader.readLine()) != null) {
buffer.append(line);
}
writer.close();
reader.close();
int status = conn.getResponseCode();
conn.disconnect();
return status + "";
}
 
public String doGet(String url) throws Exception {
 
// configure the SSLContext with a TrustManager
 
URL urlObj = new URL(url);
HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
conn.setDoOutput(true);
 
String line;
StringBuffer buffer = new StringBuffer();
BufferedReader reader = new BufferedReader(new InputStreamReader(
conn.getInputStream()));
while ((line = reader.readLine()) != null) {
buffer.append(line);
}
reader.close();
conn.disconnect();
 
return buffer.toString();
}
 


public static HttpClient wrapClient(HttpClient base) {
	try {
        SSLContext ctx = SSLContext.getInstance("TLS");
        X509TrustManager tm = new X509TrustManager() {

            public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        X509HostnameVerifier verifier = new X509HostnameVerifier() {

            @Override
            public void verify(String string, SSLSocket ssls) throws IOException {
            }

            @Override
            public void verify(String string, X509Certificate xc) throws SSLException {
            }

            @Override
            public void verify(String string, String[] strings, String[] strings1) throws SSLException {
            }

            @Override
            public boolean verify(String string, SSLSession ssls) {
                return true;
            }
        };
        ctx.init(null, new TrustManager[]{tm}, null);
        SSLSocketFactory ssf = new SSLSocketFactory(ctx);
        ssf.setHostnameVerifier(verifier);
        ClientConnectionManager ccm = base.getConnectionManager();
        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", ssf, 443));
        return new DefaultHttpClient(ccm, base.getParams());
    } catch (Exception ex) {
        ex.printStackTrace();
        return null;
    }
}

/*
public static void main(String[] args)
		  throws Exception
		  {
		    String httpsURL = "https://api.paystack.co/transaction/verify/7PVGX8MEk85tgeEpVDtD";
		    URL myurl = new URL(httpsURL);
		   
		    HttpsURLConnection conn = (HttpsURLConnection)myurl.openConnection();
		    conn.setDoOutput(true);
		    conn.addRequestProperty("Content-Type", "application/json");
		    conn.addRequestProperty("Accept", "application/json");
		    InputStream ins = conn.getInputStream();
		    InputStreamReader isr = new InputStreamReader(ins);
		    BufferedReader in = new BufferedReader(isr);

		    String inputLine;

		    while ((inputLine = in.readLine()) != null)
		    {
		      System.out.println(inputLine);
		    }

		    in.close();
		  }*/


public static void main(String[] args) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, ClientProtocolException, IOException{
	
	
	TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
        @Override
        public boolean isTrusted(X509Certificate[] certificate, String authType) {
            return true;
        }
    };
    SSLSocketFactory sf = new SSLSocketFactory(acceptingTrustStrategy,
      SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    SchemeRegistry registry = new SchemeRegistry();
    registry.register(new Scheme("https", 8443, sf));
    ClientConnectionManager ccm = new PoolingClientConnectionManager(registry);

    DefaultHttpClient httpClient = new DefaultHttpClient(ccm);

    String urlOverHttps =
      "https://api.paystack.co";
    HttpGet getMethod = new HttpGet(urlOverHttps);
    HttpResponse response = httpClient.execute(getMethod);
   System.out.println(" respose code = " + response.getStatusLine().getStatusCode());
}


}
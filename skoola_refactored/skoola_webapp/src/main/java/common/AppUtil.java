package common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import pojo.ActiveSessionPojo;
import pojo.CourseSubjectGroupPojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.SkoolaSchoolPojo;

import com.google.gson.Gson;
import com.google.inject.Inject;
import dataservice.search.SearchHandler;

import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;

public class AppUtil {

    private Logger log = Logger.getLogger(AppUtil.class);

    //private WSUtil wsUtil = new WSUtil();

    @Inject
    NinjaCache ninjaCache;

    @Inject
    NinjaProperties ninjaProperties;

    public void updateSessionURL(Session session, String methodURL, String nextPageURL) {

        String currentPageURL = session.get(SkoolaConstants.CURRENT_PAGE_URL);
        log.info(" before currentPageURL" + currentPageURL);
        if (currentPageURL != null) {
            session.put(SkoolaConstants.PREVIOUS_PAGE_URL, currentPageURL);
        }

        currentPageURL = nextPageURL;
        session.put(SkoolaConstants.CURRENT_PAGE_URL, currentPageURL);
        log.info("after currentPageURL" + currentPageURL);

    }

    public Result handleRetrieveCourseCategoriesData(Result loadedResult, Context context, FlashScope flashScope, NinjaCache ninjaCache, NinjaProperties ninjaProperties) {
        Result result = loadedResult;
        System.out.println(" inside = ********************** you know what  ");
        Collection<CourseSubjectGroupPojo> courseSubjectGroupStatPojoCol = new ArrayList<CourseSubjectGroupPojo>();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String apiKey = SkoolaConstants.API_KEY;
        try {

            String subjectGroupStatisticsJSONString = "";
            try {
                subjectGroupStatisticsJSONString = (String) ninjaCache.get("subjectGroupStatisticsJSONString");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            JSONArray subjectGroupStatisticsJArray = new JSONArray();
            JSONObject subjectGroupStatisticsJObject = new JSONObject();

            String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_GROUP_STATISTICS;

            requestObject.setApi_key(apiKey);
            requestObject.setData("");
            requestObject.setHash("");

            if (subjectGroupStatisticsJSONString != null && !subjectGroupStatisticsJSONString.isEmpty()) {

                subjectGroupStatisticsJObject = new JSONObject(subjectGroupStatisticsJSONString);
                //log.infoln("getting values from session 1");
            } else {
                String requestString = new Gson().toJson(requestObject);

                String response = SearchHandler.getAllSubjectGroupStatistics(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
                // log.infoln(" response  = " + response);
                SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
                if (responseObject != null && !responseObject.getSuccessful()) {
                    flashScope.error(responseObject.getMessage());
                    return Results.html().template(SkoolaConstants.HOME_PAGE);
                }

                subjectGroupStatisticsJObject = new JSONObject(responseObject.getData());
                ninjaCache.set("subjectGroupStatisticsJSONString", responseObject.getData());
                //session.put("subjectGroupStatisticsJSONString", responseObject.getData());
            }

            subjectGroupStatisticsJArray = (JSONArray) subjectGroupStatisticsJObject.get("subject_group_statistics_list");
            for (int i = 0; i < subjectGroupStatisticsJArray.length(); i++) {

                JSONObject jsonObject = subjectGroupStatisticsJArray.getJSONObject(i);

                CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
                pojo.setCourseSubjectGroupDisplayName(jsonObject.getString("displayName"));
                pojo.setCourseSubjectGroupName(jsonObject.getString("name"));
                pojo.setId(jsonObject.getString("id"));
                pojo.setNumberOfCourses(jsonObject.getString("numberOfCourses"));
                pojo.setNumberOfUniversities(jsonObject.getString("numberOfUniversities"));
                pojo.setType(jsonObject.getString("type"));

                courseSubjectGroupStatPojoCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result.render("courseSubjectGroupStatPojoCol", courseSubjectGroupStatPojoCol);
    }

    public Result handleGetAllSkoolaSchools(Result loadedResult, Context context, FlashScope flashScope, NinjaCache ninjaCache, NinjaProperties ninjaProperties) {
        Result result = loadedResult;

        Collection<SkoolaSchoolPojo> skoolaSchoolsCol = new ArrayList<SkoolaSchoolPojo>();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String apiKey = SkoolaConstants.API_KEY;
        try {

            String skoolaSchoolsJSONString = "";
            try {
                skoolaSchoolsJSONString = (String) ninjaCache.get("skoolaSchoolsJSONString");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            JSONArray skoolaSchoolsJArray = new JSONArray();
            JSONObject skoolaSchoolsJObject = new JSONObject();

            String methodUrl = SkoolaConstants.URL_GET_PAGINATED_SKOOLA_SCHOOLS;

            requestObject.setApi_key(apiKey);
            requestObject.setData("");
            requestObject.setHash("");

            if (skoolaSchoolsJSONString != null && !skoolaSchoolsJSONString.isEmpty()) {

                skoolaSchoolsJObject = new JSONObject(skoolaSchoolsJSONString);
                //log.infoln("getting values from session 1");
            } else {
                String requestString = new Gson().toJson(requestObject);

                String response = SearchHandler.getAllSkoolaSchools(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
                // log.infoln(" response  = " + response);
                SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
                if (responseObject != null && !responseObject.getSuccessful()) {
                    flashScope.error(responseObject.getMessage());
                    return Results.html().template(SkoolaConstants.HOME_PAGE);
                }

                skoolaSchoolsJObject = new JSONObject(responseObject.getData());
                ninjaCache.set("skoolaSchoolsJSONString", responseObject.getData());
                //session.put("subjectGroupStatisticsJSONString", responseObject.getData());
            }

            skoolaSchoolsJArray = (JSONArray) skoolaSchoolsJObject.get("school_search_result");
            for (int i = 0; i < skoolaSchoolsJArray.length(); i++) {

                JSONObject jsonObject = skoolaSchoolsJArray.getJSONObject(i);

                SkoolaSchoolPojo pojo = new SkoolaSchoolPojo();
                pojo.setName(jsonObject.getString("name"));

                pojo.setAddress(jsonObject.getString("address"));
                pojo.setYearEstablished(jsonObject.getString("yearEstablished"));
                pojo.setLogoUrl(jsonObject.getString("logoUrl"));
                pojo.setId(jsonObject.getString("id"));
                pojo.setCountry(jsonObject.getString("country"));

                skoolaSchoolsCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result.render("skoolaSchoolsCol", skoolaSchoolsCol);
    }

    public Result renderApplicationSession(Result result, Context context, NinjaCache ninjaCache) {

        try {
            Session session = context.getSession();
            ActiveSessionPojo activeSessionPojo = null;
            HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
            if (userSessionMap != null) {
                activeSessionPojo = userSessionMap.get(session.getId());
            }

            if (activeSessionPojo == null) {
                return Results.redirect(SkoolaConstants.HOME_PAGE);
            } else {

                return result.render(activeSessionPojo);

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Results.redirect(SkoolaConstants.HOME_PAGE);
        }

    }

}

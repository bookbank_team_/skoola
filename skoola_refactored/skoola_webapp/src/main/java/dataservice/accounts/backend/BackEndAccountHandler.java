package dataservice.accounts.backend;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.dg.skoola.ServiceLocator;
import com.dg.skoola.entity.ConsultationRequest;
import com.dg.skoola.entity.Course;
import com.dg.skoola.entity.CourseType;
import com.dg.skoola.entity.PaymentHistory;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.ProcessType;
import com.dg.skoola.entity.Role;
import com.dg.skoola.entity.School;
import com.dg.skoola.entity.Subject;
import com.dg.skoola.entity.SubjectGroup;
import com.dg.skoola.enumeration.ConsultationStatusConstant;
import com.dg.skoola.enumeration.RoleTypeConstant;

import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;
import common.SkoolaConstants;
import dataservice.common.CustomService;
import dataservice.common.SkoolaUtil;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

import sun.misc.BASE64Encoder;


public  class BackEndAccountHandler {

    //private HashValidator hashValidator = HashValidator.getInstance();
    private static Logger log = Logger.getLogger(BackEndAccountHandler.class);
    private static SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
    private static CustomService skoolaCustomService = CustomService.getInstance();
    private static BASE64Encoder encoder = new BASE64Encoder();
    private static Gson gson = new Gson();
    private static JSONParser jsonParser = new JSONParser();
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");


    
    
    public static String getPaginatedApplicationUsersResult(String data) {
        System.out.println("get_paginated_application_users_result >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }
            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            String searchIndex = (String) jsonObject.get("searchIndex");
            Boolean isAdminRoleType = (Boolean) jsonObject.get("isAdminRoleType");
            String roleName = "";

            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }

            Role roleType = CustomService.getRoleByName(RoleTypeConstant.STUDENT.getValue());

            if (roleType == null) {

                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<PortalUser> queryResult = new ArrayList<PortalUser>();
            Long resultCount = 0l;

            if (isAdminRoleType) {
                queryResult = CustomService.getAllPortalUsersNotStudents(roleType.getId(), Long.valueOf(searchIndex));

                resultCount = CustomService.getCountOfPortalUsersNotStudents(roleType.getId());

                roleName = "Administrator";

            } else {
                queryResult = CustomService.getAllPortalUsersByRoleId(roleType.getId(), Long.valueOf(searchIndex));

                resultCount = CustomService.getCountOfPortalUsersByRoleId(roleType.getId());

                roleName = "Student";
            }

            System.out.println(" queryResultCount = " + queryResult == null ? "0" : queryResult.size());
            System.out.println(" count = " + resultCount);
            if (resultCount == null) {
                resultCount = 0l;
            }

            JSONObject jsonResultObject = new JSONObject();
            JSONArray jArray = new JSONArray();

            jsonResultObject.put("application_user_result", jArray);
            jsonResultObject.put("search_index", searchIndex);
            jsonResultObject.put("search_result_count", resultCount.toString());

            if (queryResult == null || queryResult.isEmpty()) {
                skoolaJSONResponseObject.setMessage("Sorry!Nothing found");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            for (PortalUser portalUser : queryResult) {

                String dateCreated = portalUser.getDateCreated() == null ? "N/A" : sdf.format(portalUser.getDateCreated());
                String email = portalUser.getEmail() == null ? "" : portalUser.getEmail();
                String firstName = portalUser.getFirstname() == null ? "N/A" : WordUtils.capitalizeFully(portalUser.getFirstname());
                String lastName = portalUser.getLastname() == null ? "N/A" : WordUtils.capitalizeFully(portalUser.getLastname());
                String phoneNumber = portalUser.getPhoneNumber() == null ? "N/A" : (portalUser.getPhoneNumber());
                String role = roleName;
                String status = "N/A";
                String userId = portalUser.getUserId() == null ? "N/A" : portalUser.getUserId();
                String userName = portalUser.getUserName() == null ? "N/A" : portalUser.getUserName();
                String activated = portalUser.getActive() == null ? "true" : portalUser.getActive() ? "true" : "false";
                String passwordReset = portalUser.getPasswordReset() == null ? "false" : portalUser.getPasswordReset() ? "true" : "false";

                JSONObject jObj = new JSONObject();

                jObj.put("dateCreated", dateCreated);
                jObj.put("email", email);
                jObj.put("firstName", firstName);
                jObj.put("lastName", lastName);
                jObj.put("phoneNumber", phoneNumber);
                jObj.put("role", role);
                jObj.put("status", status);
                jObj.put("userId", userId);
                jObj.put("userName", userName);
                jObj.put("activated",activated);
                jObj.put("passwordReset",passwordReset);
                

                jArray.add(jObj);

            }

            System.out.println(" >>>>>>>>>>>>>>>> completed application user search ");

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

    
    public static String getPaginatedCareerConsultationRequestByStatus(String data) {
        System.out.println("get_paginated_application_users_result >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }
            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            String searchIndex = (String) jsonObject.get("searchIndex");
            String status = (String) jsonObject.get("requestStatus");

            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }

            ConsultationStatusConstant requestStatus = null;

            if (status.equalsIgnoreCase(ConsultationStatusConstant.BOOKED.getValue())) {
                requestStatus = ConsultationStatusConstant.BOOKED;
            } else if (status.equalsIgnoreCase(ConsultationStatusConstant.CANCELED.getValue())) {
                requestStatus = ConsultationStatusConstant.CANCELED;
            } else if (status.equalsIgnoreCase(ConsultationStatusConstant.COMPLETED.getValue())) {
                requestStatus = ConsultationStatusConstant.COMPLETED;
            } else if (status.equalsIgnoreCase(ConsultationStatusConstant.NOT_PAID.getValue())) {
                requestStatus = ConsultationStatusConstant.NOT_PAID;
            } else if (status.equalsIgnoreCase(ConsultationStatusConstant.PAID.getValue())) {
                requestStatus = ConsultationStatusConstant.PAID;
            } else {
                status = "ALL";
            }

            Collection<ConsultationRequest> queryResult = new ArrayList<ConsultationRequest>();
            Long resultCount = 0l;

            queryResult = CustomService.getPaginatedConsultationRequestListByConsultationStatusConstant(requestStatus, resultCount);

            resultCount = CustomService.getCountOfConsultationRequestListByConsultationStatusConstant(requestStatus);

            System.out.println(" queryResultCount = " + queryResult == null ? "0" : queryResult.size());
            System.out.println(" count = " + resultCount);
            if (resultCount == null) {
                resultCount = 0l;
            }

            JSONObject jsonResultObject = new JSONObject();
            JSONArray jArray = new JSONArray();

            jsonResultObject.put("consultation_request_result", jArray);
            jsonResultObject.put("search_index", searchIndex);
            jsonResultObject.put("request_status", status);
            jsonResultObject.put("search_result_count", resultCount.toString());

            if (queryResult == null || queryResult.isEmpty()) {
                skoolaJSONResponseObject.setMessage("Sorry!Nothing found");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            for (ConsultationRequest consultationRequest : queryResult) {

                Course course = (Course) skoolaService.getRecordById(Course.class, consultationRequest.getCourse().getId());
                Subject subject = null;
                if (course != null) {
                    subject = (Subject) skoolaService.getRecordById(Subject.class, course.getSubject().getId());
                }

                String courseSubject = subject == null ? "" : subject.getName();
                String courseName = course == null ? "" : course.getName();

                String dateCreated = consultationRequest.getDateCreated() == null ? "N/A" : sdf.format(consultationRequest.getDateCreated());
                String email = consultationRequest.getEmail() == null ? "" : consultationRequest.getEmail();
                String firstName = consultationRequest.getFirstname() == null ? "N/A" : WordUtils.capitalizeFully(consultationRequest.getFirstname());
                String lastName = consultationRequest.getLastname() == null ? "N/A" : WordUtils.capitalizeFully(consultationRequest.getLastname());
                String phoneNumber = consultationRequest.getPhoneNumber() == null ? "N/A" : (consultationRequest.getPhoneNumber());
                String consultationStatus = consultationRequest.getConsultationStatus().getValue();
                String consultationMode = consultationRequest.getConsultationMode() == null ? "N/A" : consultationRequest.getConsultationMode().getValue();
                String id = consultationRequest.getId().toString();

                JSONObject jObj = new JSONObject();

                jObj.put("dateCreated", dateCreated);
                jObj.put("email", email);
                jObj.put("firstName", firstName);
                jObj.put("lastName", lastName);
                jObj.put("phoneNumber", phoneNumber);
                jObj.put("consultationStatus", consultationStatus);
                jObj.put("consultationMode", consultationMode);
                jObj.put("courseName", courseName);
                jObj.put("courseSubject", courseSubject);
                jObj.put("id", id);

                jArray.add(jObj);

            }

            System.out.println(" >>>>>>>>>>>>>>>> completed consultation Request search ");

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gson.toJson(skoolaJSONResponseObject);
    }

  
    public static String getAdminDashBoardStatistics(String data) {
        System.out.println("get_admin dashboard statistics >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            LocalDateTime now = LocalDateTime.now();

            Timestamp weekStartTime = new Timestamp(now.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withDayOfWeek(DateTimeConstants.MONDAY).minusDays(1).toDate().getTime());
            Timestamp weekEndTime = new Timestamp(now.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withDayOfWeek(DateTimeConstants.SATURDAY).toDate().getTime());

            Timestamp dayStartTime = new Timestamp(now.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate().getTime());
            Timestamp dayEndTime = new Timestamp(now.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate().getTime());

            Long totalSignUpsToday = CustomService.getUserSignUpCountByDateRange(dayStartTime, dayEndTime);
            Long totalSignUpsThisWeek = CustomService.getUserSignUpCountByDateRange(weekStartTime, weekEndTime);

            Long totalPaidConsultationsToday = CustomService.getConsultationRequestCountByDateRangeAndConsultationStatus(dayStartTime, dayEndTime, ConsultationStatusConstant.PAID);
            Long totalPaidConsultatioinsThisWeek = CustomService.getConsultationRequestCountByDateRangeAndConsultationStatus(weekStartTime, weekEndTime, ConsultationStatusConstant.PAID);

            //System.out.println(" totalSignUpsToday = " + totalSignUpsToday);
            //System.out.println(" totalSignUpsThisWeek = " + totalSignUpsThisWeek);
            //System.out.println(" totalPaidConsultationsToday = " + totalPaidConsultationsToday);
            //System.out.println(" totalPaidConsultatioinsThisWeek = " + totalPaidConsultatioinsThisWeek);
            if (totalSignUpsToday == null) {
                totalSignUpsToday = 0l;
            }

            if (totalSignUpsThisWeek == null) {
                totalSignUpsThisWeek = 0l;
            }

            if (totalPaidConsultationsToday == null) {
                totalPaidConsultationsToday = 0l;
            }

            if (totalPaidConsultatioinsThisWeek == null) {
                totalPaidConsultatioinsThisWeek = 0l;
            }

            JSONObject jsonResultObject = new JSONObject();

            jsonResultObject.put("totalSignUpsToday", totalSignUpsToday.toString());
            jsonResultObject.put("totalSignUpsThisWeek", totalSignUpsThisWeek.toString());
            jsonResultObject.put("totalPaidConsultationsToday", totalPaidConsultationsToday.toString());
            jsonResultObject.put("totalPaidConsultatioinsThisWeek", totalPaidConsultatioinsThisWeek.toString());

            System.out.println(" >>>>>>>>>>>>>>>> completed count generation ");

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gson.toJson(skoolaJSONResponseObject);
    }

    public static void main(String[] arg) {
        /*LocalDate now = LocalDate.now();
    		TemporalField fieldISO = WeekFields.of(Locale.getDefault()).dayOfWeek();
    		now = now.with(fieldISO, 1);
    		System.out.println(Timestamp.valueOf(now.atStartOfDay()));
    		System.out.println(Timestamp.valueOf(now.with(fieldISO, 7).atTime(23, 59, 59)));
    		
    		System.out.println(now.atStartOfDay());
    		System.out.println(now.atTime(23, 59));*/
    }

  
    public static String getPaginatedSkoolaCourses(String data) {
        System.out.println("get_paginated_skoola courses >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }
            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            String searchIndex = (String) jsonObject.get("searchIndex");
            String status = (String) jsonObject.get("status");
            String subjectIdParameter = (String) jsonObject.get("subjectIdParameter");
            String courseTypeIdParameter = (String) jsonObject.get("courseTypeIdParameter");
            String schoolIdParameter = (String) jsonObject.get("schoolIdParameter");

            Subject subjectParameter = null;
            CourseType courseTypeParameter = null;
            School schoolParameter = null;
            Long searchIndexValue = null;
            Long subjectIdValue = null;
            Long courseTypeIdValue = null;
            Long schoolIdValue = null;

            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndexValue = 0l;
            } else {
                searchIndexValue = Long.valueOf(searchIndex);
            }

            if (subjectIdParameter != null && !subjectIdParameter.isEmpty()) {
                subjectParameter = (Subject) skoolaService.getRecordById(Subject.class, Long.valueOf(subjectIdParameter));
                subjectIdValue = subjectParameter.getId();
            }

            if (courseTypeIdParameter != null && !courseTypeIdParameter.isEmpty()) {
                courseTypeParameter = (CourseType) skoolaService.getRecordById(CourseType.class, Long.valueOf(courseTypeIdParameter));
                courseTypeIdValue = courseTypeParameter.getId();
            }

            if (schoolIdParameter != null && !schoolIdParameter.isEmpty()) {
                schoolParameter = (School) skoolaService.getRecordById(School.class, Long.valueOf(schoolIdParameter));
                schoolIdValue = schoolParameter.getId();
            }

            Collection<Course> queryResult = new ArrayList<Course>();
            Long resultCount = 0l;

            System.out.println(" search index value  = " + searchIndexValue);
            queryResult = CustomService.getPaginatedSkoolaCoursesByIndexSubjectIdAndCourseTypeId(searchIndexValue, subjectIdValue, courseTypeIdValue, schoolIdValue);

            resultCount = CustomService.getCountOfSkoolaCoursesBySubjectIdAndCourseTypeId(subjectIdValue, courseTypeIdValue, schoolIdValue);

            System.out.println(" queryResultCount = " + queryResult == null ? "0" : queryResult.size());
            System.out.println(" count = " + resultCount);
            if (resultCount == null) {
                resultCount = 0l;
            }

            JSONObject jsonResultObject = new JSONObject();
            JSONArray jArray = new JSONArray();

            jsonResultObject.put("course_result", jArray);
            jsonResultObject.put("search_index", searchIndex);
            jsonResultObject.put("courseStatus", status);
            jsonResultObject.put("subjectIdParameter", subjectIdParameter);
            jsonResultObject.put("courseTypeIdParameter", courseTypeIdParameter);
            jsonResultObject.put("resultCount", resultCount);

            if (queryResult == null || queryResult.isEmpty()) {
                skoolaJSONResponseObject.setMessage("Sorry!Nothing found");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            String dollarExchangeRate = CustomService.getSettingValue("DOLLAR_EXCHANGE_RAGE", "325", true);
            for (Course course : queryResult) {
                try {
                    CourseType courseTypeObj = null;
                    Subject subjectObj = null;
                    School school = null;

                    try {
                        courseTypeObj = (CourseType) skoolaService.getRecordById(CourseType.class, course.getCourseType().getId());

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    try {
                        subjectObj = subjectParameter = (Subject) skoolaService.getRecordById(Subject.class, course.getSubject().getId());
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    try {
                        school = (School) skoolaService.getRecordById(School.class, course.getSchool().getId());
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    SubjectGroup subjectGroup = null;
                    if (subjectGroup == null) {
                        subjectGroup = (SubjectGroup) skoolaService.getRecordById(SubjectGroup.class, subjectObj.getSubjectGroup().getId());
                    }

                    Long id = course.getId();
                    String name = course.getName();
                    String courseDuration = course.getDuration() == null ? "" : course.getDuration();
                    String courseSummary = course.getCourseSummary() == null ? "" : course.getCourseSummary();
                    Boolean ieltsRequired = course.getIeltsRequired() == null ? false : course.getIeltsRequired();
                    String courseRequirements = course.getCourseRequirements() == null ? "" : course.getCourseRequirements();
                    String programModules = course.getProgramModules() == null ? "" : course.getProgramModules();
                    String courseVenue = course.getCourseVenue() == null ? "" : course.getCourseVenue().replace("\n", " ");
                    String courseFeeInDollar = course.getFeeInCents() == null ? "" : String.valueOf((course.getFeeInCents() / 100));
                    String courseSubjectName = subjectObj == null ? "" : subjectObj.getName();
                    String courseSubjectGroupName = subjectGroup == null ? "" : subjectGroup.getName();
                    String courseType = courseTypeObj == null ? "" : courseTypeObj.getName();
                    String courseSchoolName = school == null ? "" : school.getName();
                    String courseSubjectId = subjectObj == null ? "" : subjectObj.getId().toString();
                    String courseFeeInWords = course.getFeeInWords() == null ? "" : course.getFeeInWords();
                    String courseTypeId = courseType == null ? "" : courseTypeObj.getId().toString();
                    String courseSchoolId = school == null ? "" : school.getId().toString();
                    String courseSchoolLogoUrl = school == null ? "" : school.getLogoUrl();
                    String formattedCourseFee = SkoolaUtil.convertFromCentsToNaira(course.getFeeInCents(), dollarExchangeRate);
                    String courseFeeInNaira = formattedCourseFee;

                    JSONObject jObj = new JSONObject();
                    jObj.put("courseSummary", courseSummary);
                    jObj.put("ieltsRequired", ieltsRequired);
                    jObj.put("courseRequirements", courseRequirements);
                    jObj.put("programModules", programModules);
                    jObj.put("courseVenue", courseVenue);
                    jObj.put("courseSubjectGroupName", courseSubjectGroupName);
                    jObj.put("courseSubject", courseSubjectName);
                    jObj.put("courseSubjectId", courseSubjectId);
                    jObj.put("schoolName", courseSchoolName);
                    jObj.put("schoolId", courseSchoolId);
                    jObj.put("schoolLogoURL", courseSchoolLogoUrl);
                    jObj.put("courseTypeName", courseType);
                    jObj.put("courseTypeId", courseTypeId);
                    jObj.put("courseName", name);
                    jObj.put("courseId", id.toString());
                    jObj.put("courseDuration", courseDuration);
                    jObj.put("courseFeeInWords", courseFeeInWords);
                    jObj.put("courseFeeInDollars", courseFeeInDollar);
                    jObj.put("feeInNaira", courseFeeInNaira);

                    jArray.add(jObj);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    continue;
                }

            }

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gson.toJson(skoolaJSONResponseObject);
    }

 
    public static String createOrUpdateSkoolaCourse(String data) {
        System.out.println("create_or_update_skoola_course >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }
            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            String courseId = (String) jsonObject.get("courseId");
            String cu_course_name = (String) jsonObject.get("cu_course_name");
            String cu_course_type = (String) jsonObject.get("cu_course_type");
            String cu_course_subject_group = (String) jsonObject.get("cu_course_subject_group");
            String cu_course_subject = (String) jsonObject.get("cu_course_subject");
            String cu_course_venue = (String) jsonObject.get("cu_course_venue");
            String cu_course_duration = (String) jsonObject.get("cu_course_duration");
            String cu_course_fee_in_figures = (String) jsonObject.get("cu_course_fee_in_figures");
            String cu_course_fee_in_dollars = (String) jsonObject.get("cu_course_fee_in_dollars");
            String cu_course_requirements = (String) jsonObject.get("cu_course_requirements");
            String cu_ielt_required = (String) jsonObject.get("cu_ielt_required");
            String cu_course_summary = (String) jsonObject.get("cu_course_summary");
            String cu_course_module = (String) jsonObject.get("cu_course_module");
            String cu_course_school = (String) jsonObject.get("cu_course_school");
           

            Course course = null;
            CourseType courseType = null;
            Subject subject = null;
            SubjectGroup subjectGroup = null;
            School school = null;
            Boolean isIELTSRequired = false;

            if (cu_ielt_required != null && cu_ielt_required.equalsIgnoreCase("1")) {
                isIELTSRequired = true;
            }

            subject = (Subject) skoolaService.getRecordById(Subject.class, Long.valueOf(cu_course_subject));
            subjectGroup = (SubjectGroup) skoolaService.getRecordById(SubjectGroup.class, Long.valueOf(cu_course_subject_group));
            courseType = (CourseType) skoolaService.getRecordById(CourseType.class, Long.valueOf(cu_course_type));
            school  = (School) skoolaService.getRecordById(School.class, Long.valueOf(cu_course_school));

            try {
                if (courseId != null && !courseId.isEmpty()) {

                    course = (Course) skoolaService.getRecordById(Course.class, Long.valueOf(courseId));

                    if (course == null) {
                        skoolaJSONResponseObject.setMessage("Select a valid Skoola Course.");
                        skoolaJSONResponseObject.setSuccessful(false);
                        return gson.toJson(skoolaJSONResponseObject);
                    }

                }

                if (courseType == null) {
                    skoolaJSONResponseObject.setMessage("Select a valid Skoola courseType.");
                    skoolaJSONResponseObject.setSuccessful(false);
                    return gson.toJson(skoolaJSONResponseObject);
                }

                if (subject == null) {
                    skoolaJSONResponseObject.setMessage("Select a valid Skoola Course Subject.");
                    skoolaJSONResponseObject.setSuccessful(false);
                    return gson.toJson(skoolaJSONResponseObject);
                }

                if (subjectGroup == null) {
                    skoolaJSONResponseObject.setMessage("Select a valid Skoola Course Subject Group.");
                    skoolaJSONResponseObject.setSuccessful(false);
                    return gson.toJson(skoolaJSONResponseObject);
                }
            } catch (Exception e1) {

                e1.printStackTrace();

                skoolaJSONResponseObject.setMessage("Enter valid update parameters");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);

            }

            Long tuitionFeeInDollars = null;

            try {
                tuitionFeeInDollars = Long.valueOf(cu_course_fee_in_dollars);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();

                skoolaJSONResponseObject.setMessage("Enter valid values for the course tuition fees");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            if (course == null) {
                course = new Course();
            }

            course.setName(WordUtils.capitalizeFully(cu_course_name));
            course.setCourseType(courseType);
            course.setSubject(subject);
            course.setCourseVenue(cu_course_venue);
            course.setDuration(cu_course_duration);
            course.setFeeInWords(cu_course_fee_in_figures);
            course.setFeeInCents(tuitionFeeInDollars * 100);
            course.setCourseRequirements(cu_course_requirements);
            course.setIeltsRequired(isIELTSRequired);
            course.setCourseSummary(cu_course_summary);
            course.setProgramModules(cu_course_module);
            course.setSchool(school);

            if (course.getId() == null) {
                course = (Course) skoolaService.createNewRecord(course);
            } else {
                skoolaService.updateRecord(course);
            }

            skoolaJSONResponseObject.setMessage("Process completed successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData("");
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gson.toJson(skoolaJSONResponseObject);
    }

}

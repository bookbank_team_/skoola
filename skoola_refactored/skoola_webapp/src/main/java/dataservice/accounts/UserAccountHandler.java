package dataservice.accounts;

import java.text.SimpleDateFormat;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.dg.skoola.ServiceLocator;
import com.dg.skoola.entity.ConsultationRequest;
import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.Course;
import com.dg.skoola.entity.CourseType;
import com.dg.skoola.entity.PaymentHistory;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.ProcessType;
import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;
import common.SkoolaConstants;
import dataservice.common.CustomService;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

import sun.misc.BASE64Encoder;

public class UserAccountHandler {

    //private HashValidator hashValidator = HashValidator.getInstance();
    private static Logger log = Logger.getLogger(UserAccountHandler.class);
    private static SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
    private static CustomService skoolaCustomService = CustomService.getInstance();
    private static BASE64Encoder encoder = new BASE64Encoder();
    private static Gson gson = new Gson();
    private static JSONParser jsonParser = new JSONParser();
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

    public static String test(String data) {
        System.out.println(" data = " + data);
        SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        System.out.println(" inside test  ");
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();
        skoolaJSONResponseObject.setData("Welcome to Skoola SearchHandler ");
        skoolaJSONResponseObject.setSuccessful(true);

        try {
            Collection<Country> col = skoolaService.getAllRecords(Country.class);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new Gson().toJson(skoolaJSONResponseObject);
    }

    public static String getPaginatedPaymentHistoryByPortalUserUserId(String data) {
        System.out.println(" get_paginated_payment_history_by_portaluser_userid >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }
            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());
            String userId = (String) jsonObject.get("userId");
            String searchIndex = (String) jsonObject.get("searchIndex");
            System.out.println(" userId" + userId);
            System.out.println(" searchIndex =" + searchIndex);

            if ((userId == null || userId.isEmpty()) && (searchIndex == null || searchIndex.isEmpty())) {

                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }

            PortalUser portalUser = CustomService.getPortalUserByUserId(userId);
            if (portalUser == null) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<PaymentHistory> queryResult = CustomService.getPaginatedPaymentHistoryListByPortalUserIdAndSearchIndex(portalUser.getId(), Long.valueOf(searchIndex));

            Long resultCount = CustomService.getCountOfPaymentHistoryListByPortalUserId(portalUser.getId());

            System.out.println(" queryResultCount = " + queryResult == null ? "0" : queryResult.size());
            System.out.println(" count = " + resultCount);
            if (resultCount == null) {
                resultCount = 0l;
            }

            JSONObject jsonResultObject = new JSONObject();
            JSONArray jArray = new JSONArray();

            jsonResultObject.put("payment_history_result", jArray);
            jsonResultObject.put("search_index", searchIndex);
            jsonResultObject.put("search_result_count", resultCount.toString());

            if (queryResult == null || queryResult.isEmpty()) {
                skoolaJSONResponseObject.setMessage("Sorry!Nothing found");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            for (PaymentHistory paymentHistory : queryResult) {

                ProcessType processType = (ProcessType) skoolaService.getRecordById(ProcessType.class, paymentHistory.getProcessType().getId());

                String paymentStatus = paymentHistory.getPaymentStatus().getValue();
                String amountInNaira = String.valueOf(paymentHistory.getAmountInKobo() / 100);
                String dateCreated = sdf.format(paymentHistory.getDateCreated());
                String referenceId = paymentHistory.getReferenceId();
                String processName = processType.getName().getValue();

                JSONObject jObj = new JSONObject();

                jObj.put("paymentStatus", paymentStatus);
                jObj.put("amountInNaira", amountInNaira);
                jObj.put("dateCreated", dateCreated);
                jObj.put("referenceId", referenceId);
                jObj.put("processName", processName);
                jObj.put("id", paymentHistory.getId().toString());

                jArray.add(jObj);

            }

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

    public static String getPaginatedConsultationRequestByPortalUserUserId(String data) {
        System.out.println(" get_paginated_consultation_request_by_portaluser_userid >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }
            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());
            String userId = (String) jsonObject.get("userId");
            String searchIndex = (String) jsonObject.get("searchIndex");
            System.out.println(" userId" + userId);
            System.out.println(" searchIndex =" + searchIndex);

            if ((userId == null || userId.isEmpty()) && (searchIndex == null || searchIndex.isEmpty())) {

                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }

            PortalUser portalUser = CustomService.getPortalUserByUserId(userId);
            if (portalUser == null) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<ConsultationRequest> queryResult = CustomService.getPaginatedConsultationRequestListByPortalUserIdAndSearchIndex(portalUser.getId(), Long.valueOf(searchIndex));

            Long resultCount = CustomService.getCountOfConsultationRequestListByPortalUserId(portalUser.getId());

            System.out.println(" queryResultCount = " + queryResult == null ? "0" : queryResult.size());
            System.out.println(" count = " + resultCount);
            if (resultCount == null) {
                resultCount = 0l;
            }

            if (queryResult == null || queryResult.isEmpty()) {
                skoolaJSONResponseObject.setMessage("Sorry! Nothing found!");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            JSONObject jsonResultObject = new JSONObject();
            JSONArray jArray = new JSONArray();

            for (ConsultationRequest consultationRequest : queryResult) {

                Course course = (Course) skoolaService.getRecordById(Course.class, consultationRequest.getCourse().getId());

                String consultationMode = consultationRequest.getConsultationMode().getValue();
                String dateCreated = sdf.format(consultationRequest.getDateCreated());
                String consultationDate = consultationRequest.getConsultationDate() == null ? "N/A" : sdf.format(consultationRequest.getConsultationDate());
                String consultationStatus = consultationRequest.getConsultationStatus().getValue();
                String courseName = course.getName();

                JSONObject jObj = new JSONObject();

                jObj.put("consultationMode", consultationMode);
                jObj.put("consultationDate", consultationDate);
                jObj.put("dateCreated", dateCreated);
                jObj.put("consultationStatus", consultationStatus);
                jObj.put("courseName", courseName);
                jObj.put("id", consultationRequest.getId().toString());

                jArray.add(jObj);

            }

            jsonResultObject.put("consultation_request_result", jArray);
            jsonResultObject.put("search_index", searchIndex);
            jsonResultObject.put("search_result_count", resultCount.toString());

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gson.toJson(skoolaJSONResponseObject);
    }

}

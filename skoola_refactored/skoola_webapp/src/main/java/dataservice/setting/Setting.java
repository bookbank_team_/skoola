package dataservice.setting;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
//import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import sun.misc.BASE64Encoder;

import com.dg.skoola.ServiceLocator;

import com.dg.skoola.entity.AccountCreationRequest;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.PortalUserRoleMap;
import com.dg.skoola.entity.Role;
import com.dg.skoola.enumeration.RoleTypeConstant;
import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;
import dataservice.common.CustomService;
import dataservice.common.SkoolaConstants;
import dataservice.common.SkoolaUtil;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

public class Setting {

    private static Logger log = Logger.getLogger(Setting.class);
    private static SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
    private static CustomService skoolaCustomService = CustomService.getInstance();
    private static BASE64Encoder encoder = new BASE64Encoder();
    private static Gson gson = new Gson();
    private static JSONParser jsonParser = new JSONParser();

  
    public static String createUpdateDefaultSuperAdminUser(String data) {
        System.out.println(" inside create super admin settings ");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            if (jsonObject == null) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            String action = (String) jsonObject.get("action");

            if ((action == null || action.isEmpty()) && !action.equalsIgnoreCase("create_update_super_admin_user")) {

                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            String superAdminEmailAddress = SkoolaConstants.SUPER_ADMIN_EMAIL_ADDRESS;
            String superAdminPassword = SkoolaConstants.SUPER_ADMIN_DEFAULT_PASSWORD;

            PortalUser testPortalUser = CustomService.getPortalUserByEmailAddress(superAdminEmailAddress);

            if (testPortalUser != null) {
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                skoolaJSONResponseObject.setMessage("Sorry, this email address is already registered. Please use another one.");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            String passwordSalt = UUID.randomUUID().toString();
            String encryptionAlgorithm = SkoolaConstants.PASSWORD_ENCRYPTION_ALGORITHM;
            String digestInput = superAdminPassword.trim() + passwordSalt;
            byte[] passwordDigest = SkoolaUtil.getMessageDigest(encryptionAlgorithm, digestInput);
            String hexPasswordDigest = Hex.encodeHexString(passwordDigest);
            //String activationCode = UUID.randomUUID().toString();
            String uniqueUserId = "";

            do {
                uniqueUserId = UUID.randomUUID().toString();
                testPortalUser = CustomService.getPortalUserByUserId(uniqueUserId);

            } while (testPortalUser != null);

            PortalUser portalUser = new PortalUser();
            portalUser.setDateCreated(new Timestamp(new Date().getTime()));
            portalUser.setEmail(superAdminEmailAddress);
            portalUser.setFirstname("Super");
            portalUser.setLastname("Administrator");
            portalUser.setGender("Male");
            portalUser.setPassword(hexPasswordDigest);
            portalUser.setPassWordEncrypted(true);
            portalUser.setPasswordEncryptionAlgo(encryptionAlgorithm);
            portalUser.setPasswordSalt(passwordSalt);
            portalUser.setPhoneNumber("08030972463");
            portalUser.setUserId(uniqueUserId);
            portalUser.setUserName(superAdminEmailAddress);

            portalUser = (PortalUser) skoolaService.createNewRecord(portalUser);

            Role dataEntryRole = CustomService.getRoleByName(RoleTypeConstant.DATA_ENTRY.getValue());
            Role administratorRole = CustomService.getRoleByName(RoleTypeConstant.ADMINISTRATOR.getValue());

            PortalUserRoleMap portalUserRoleMap = new PortalUserRoleMap();
            portalUserRoleMap.setRole(dataEntryRole);
            portalUserRoleMap.setPortalUser(portalUser);
            portalUserRoleMap = (PortalUserRoleMap) skoolaService.createNewRecord(portalUserRoleMap);

            portalUserRoleMap = new PortalUserRoleMap();
            portalUserRoleMap.setRole(administratorRole);
            portalUserRoleMap.setPortalUser(portalUser);
            portalUserRoleMap = (PortalUserRoleMap) skoolaService.createNewRecord(portalUserRoleMap);

            skoolaJSONResponseObject.setData("");
            skoolaJSONResponseObject.setMessage("Signup Request, Successful");
            skoolaJSONResponseObject.setSuccessful(true);

        } catch (Exception e) {

            e.printStackTrace();
        }
        return new Gson().toJson(skoolaJSONResponseObject);
    }

}

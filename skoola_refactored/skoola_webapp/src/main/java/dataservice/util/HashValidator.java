package dataservice.util;
/*package com.dg.skoola.util;

import com.ims.entity.ApiAlias;
import org.apache.log4j.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.codec.binary.Base64;

*//**
 * Created by Chidera on 8/14/2015.
 *//*
public class HashValidator {

    private static DBUtil dbHandler = new DBUtil();
    private Logger log = Logger.getLogger(getClass());

    private static HashValidator instance = null;

    private Collection<ApiAlias> apiAliases = new ArrayList<ApiAlias>();

    private HashValidator() {

    }

    public static HashValidator getInstance() {
        if (instance == null) {
            instance = new HashValidator();
            instance.setApiAliases(dbHandler.getAllRecords(ApiAlias.class));
        }
        return instance;
    }

    public boolean isValidRequestData(String data, String hashInBase64, String apiKey) {
        boolean result = true;
        try {
            String seed = "1234";

            outer:
            for (ApiAlias alias : apiAliases) {
                if (alias.getAlias().equalsIgnoreCase(apiKey) && alias.getIsActive()) {
                    seed = alias.getKey();
                    break outer;
                }
            } 
            if (seed.isEmpty()) {
                result = false;
                return result;
            }
            
           
            SecretKeySpec secretKeySpec = SecretKeyGenerator.generateSecretKey(seed);

            Mac hMac = Mac.getInstance("HmacSHA256");
            hMac.init(secretKeySpec);

            String computedHashInBase64 = Base64.encodeBase64String(hMac.doFinal(data.getBytes()));

            if (computedHashInBase64.equalsIgnoreCase(hashInBase64)) {
            	//System.out.println("hash is matched");
                result = true;
            }

        } catch (Exception e) {
            log.error(e);
        }

        return result;
    }

    public Collection<ApiAlias> getApiAliases() {
        return apiAliases;
    }

    public void setApiAliases(Collection<ApiAlias> apiAliases) {
        this.apiAliases = apiAliases;
    }
}
*/
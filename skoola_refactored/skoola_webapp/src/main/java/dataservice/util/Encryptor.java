package dataservice.util;
/**
 * 
 *//*
package com.dg.skoola.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;



import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;


*//**
 * @author Chidera
 *
 *//*
public class Encryptor {
	
	private static Logger logger = Logger.getLogger(Encryptor.class);
	
	public static String computeHash(String data, String algo) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(data.getBytes());
        
        byte byteData[] = md.digest();
        
        String digestedData = Base64.encodeBase64String(byteData);
        
		return digestedData;
	}
	
	public static String computeHashWithKey(String key, String algo, byte[] data) {
		String computedHashInBase64 = "";
		
		try {
			SecretKeySpec secretKeySpec = SecretKeyGenerator.generateSecretKey(key);
	
	        Mac hMac = Mac.getInstance(algo);
	        hMac.init(secretKeySpec);
	
	        computedHashInBase64 = Base64.encodeBase64String(hMac.doFinal(data));
	        
		} catch (Exception e) {
			logger.error(e);
		}
        
        return computedHashInBase64;
	}

}
*/
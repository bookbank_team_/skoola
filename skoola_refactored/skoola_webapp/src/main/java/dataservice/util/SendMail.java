package dataservice.util;
import dataservice.common.CustomService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;



import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class SendMail {

	Logger logger = Logger.getLogger(SendMail.class);
	CustomService skoolaService = CustomService.getInstance();
	private String senderEmail = skoolaService.getSettingValue(Constant.NOTIFICATION_EMAIL_ADDRESS, "skoola.nigeria@gmail.com", true);
	private String password = skoolaService.getSettingValue(Constant.NOTIFICATION_EMAIL_PASSWORD, "*#sk00l@*#", true);
	
	public static final String taskMailSubject = "";

	private String smtpHost = "smtp.gmail.com";
	private String smtpPort = "587";
	private Properties emailProperties;
	private Session mailSession;
	private MimeMessage emailMessage;

	public SendMail(String subject, String message, String receipient[], String senderName, File attachment) {

		// this.senderEmail = "info@ngalaba.com.ng";
		// this.password = "*ng1nf0#";

		this.senderEmail =  skoolaService.getSettingValue(Constant.NOTIFICATION_EMAIL_ADDRESS, "skoola.nigeria@gmail.com", true);

		this.password = skoolaService.getSettingValue(Constant.NOTIFICATION_EMAIL_PASSWORD, "*#sk00l@*#", true);

		System.out.println("-----------------------------SenderEmail: " + this.senderEmail);

		System.out.println("in sendEmail constructor");
		logger.debug("in sendEmail constructor");
		// runSSL();
		try {
			System.out.println("2");
			sendEmailToUserWithAttachment(receipient, subject, message, attachment);
			System.out.println("2.5");
		} catch (AddressException e) {

			e.printStackTrace();
		} catch (MessagingException e) {

			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void sendEmailToUserWithAttachment(String[] recipient, String mailSubject, String mailBody, File attachment) throws AddressException, MessagingException {
		System.out.println(smtpPort);
		//setMailServerPropertiesForSendWithAttachment(smtpPort);
		System.out.println("3");
		createEmailMessageWithAttachment(recipient, mailSubject, mailBody, attachment);
		System.out.println("4");
		sendEmail(smtpHost, senderEmail, password);
		System.out.println("4");
	}

	/*public void setMailServerPropertiesForSendWithAttachment(String port) {

		String emailPort = port;// gmail's smtp port

		emailProperties = System.getProperties();
		emailProperties.put("mail.smtp.port", emailPort);
		emailProperties.put("mail.smtp.auth", "true");
		emailProperties.put("mail.smtp.starttls.enable", "true");
		emailProperties.put("mail.smtp.EnableSSL.enable", "true");

		emailProperties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		emailProperties.setProperty("mail.smtp.socketFactory.fallback", "false");
		emailProperties.setProperty("mail.smtp.port", "465");
		emailProperties.setProperty("mail.smtp.socketFactory.port", "465");
		System.setProperties(emailProperties);
	}*/

	public void createEmailMessageWithAttachment(String[] recipent, String mailSubject, String mailBody, File attachment2) throws AddressException, MessagingException {
		String[] toEmails = recipent;
		String emailSubject = mailSubject;
		String emailBody = mailBody;

		mailSession = Session.getDefaultInstance(emailProperties, null);
		emailMessage = new MimeMessage(mailSession);

		for (int i = 0; i < toEmails.length; i++) {
			emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
		}

		emailMessage.setSubject(emailSubject);
		if (attachment2 == null) {
			emailMessage.setContent(emailBody, "text/html");// for a html email
		} else {
			BodyPart bb = new MimeBodyPart();
			bb.setContent(emailBody, "text/html");
			MimeMultipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(bb);
			emailMessage.setText(emailBody, "text/html");// for a html email
			MimeBodyPart messageBodyPart1 = new MimeBodyPart();
			try {

				messageBodyPart1.attachFile(attachment2.getAbsoluteFile());
				multipart.addBodyPart(messageBodyPart1);

				emailMessage.setContent(multipart);
			} catch (IOException e1) {

				e1.printStackTrace();
			}
		}
	}

	public SendMail() {

	}
	public SendMail(String hostEmail,String hostPassword) {
       this.senderEmail = hostEmail;
       this.password = hostPassword;
	}

	public void sendMailInHTMLTemplate(String recieverEmail, String mailSubject, Map<String, Object> inputs, String templateFileName,String tempLocation) {
		System.out.println("mail sender called");
		File templatePath = new File(tempLocation);
		if (!templatePath.exists()) {
			templatePath.mkdir();
		}
		try {
			// System.out.println("template path: "+templateDirPath);
			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(templatePath);
			Template template = cfg.getTemplate(templateFileName);

			Writer out = new StringWriter();
			template.process(inputs, out);
			String[] recipient = { recieverEmail };
			System.out.println("1");
			sendEmailToUser(recipient, mailSubject, out.toString());

		} catch (TemplateException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	public void sendMailInHTMLTemplate(String recieverEmail, String mailSubject, Map<String, Object> inputs, String templateFileName) {
		System.out.println("mail sender called");
		
		File templatePath = new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + "emailtemplates");
		if (!templatePath.exists()) {
			templatePath.mkdir();
		}
		try {
			// System.out.println("template path: "+templateDirPath);
			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(templatePath);
			Template template = cfg.getTemplate(templateFileName);

			Writer out = new StringWriter();
			template.process(inputs, out);
			String[] recipient = { recieverEmail };
			System.out.println("1");
			sendEmailToUser(recipient, mailSubject, out.toString());

		} catch (TemplateException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void sendMessageWithHTMLTemplate(String recieptMail, String subject, Map<String, String> rootMap, String templateFileName) {
		File templatePath = new File(System.getProperty("user.dir") + File.separator + "emailtemplates");
		if (!templatePath.exists()) {
			templatePath.mkdir();
		}
		try {

			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(templatePath);
			Template template = cfg.getTemplate(templateFileName);
			Writer out = new StringWriter();
			template.process(rootMap, out);
			String[] recipient = { recieptMail };
			System.out.println("1");
			sendEmailToUser(recipient, subject, out.toString());

		} catch (TemplateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendEmailToUser(final String[] recipient, final String mailSubject, final String mailBody) {
		Thread t = new Thread() {

			public void run() {
				System.out.println(">>>>Send mail thread started..." + recipient[0]);
				// System.out.println(smtpPort);
				Properties props = new Properties();
				props = setMailServerProperties(props);
				 System.out.println("3");
				try {
					createEmailMessage(recipient, mailSubject, mailBody,props);
					 System.out.println("4");
					//sendEmail(smtpHost, senderEmail, password);
					// System.out.println("4");
				} catch (Exception e) {
					System.out.println("Unable to send mail: " + e.getMessage());
				}
			}
		};
		t.start();

	}

	public void sendEmail(String mailServerHost, String sender, String accountPassword) throws AddressException, MessagingException {

		String emailHost = mailServerHost;
		String fromUser = sender;// just the id alone without @gmail.com
		String fromUserEmailPassword = accountPassword;

		Transport transport = mailSession.getTransport("smtp");

		transport.connect(emailHost, fromUser, fromUserEmailPassword);
		transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
		transport.close();
		System.out.println("Email sent successfully.");
	}

	public void createEmailMessage(String[] recipent, String mailSubject, String mailBody,Properties props) throws AddressException, MessagingException {


		try {
			Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("iotradingview@gmail.com","8Centrin03");
					}
				});
			
			String[] toEmails = recipent;
			String emailSubject = mailSubject;
			String emailBody = mailBody;
			Message emailMessage = new MimeMessage(session);

			for (int i = 0; i < toEmails.length; i++) {
				emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
			}

			emailMessage.setSubject(emailSubject);
			emailMessage.setContent(emailBody, "text/html");// for a html email



			Transport.send(emailMessage);

			System.out.println("Email Sent Successfully!!!");
		} catch (Exception e) {
			System.out.println("Email NOT Sent!!!");
			e.printStackTrace();
		}


	}

	public Properties setMailServerProperties(Properties props) {
		
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "587");		
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
			
		return props;
	}

}



package dataservice.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.css.RGBColor;

import com.dg.skoola.ServiceLocator;
import com.dg.skoola.entity.AccountCreationRequest;
import com.dg.skoola.entity.ConsultationRequest;
import com.dg.skoola.entity.Course;
import com.dg.skoola.entity.Exam;
import com.dg.skoola.entity.Feeschedule;
import com.dg.skoola.entity.Image;
import com.dg.skoola.entity.ImageType;
import com.dg.skoola.entity.PaymentHistory;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.PortalUserRoleMap;
import com.dg.skoola.entity.ProcessType;
import com.dg.skoola.entity.Role;
import com.dg.skoola.entity.School;
import com.dg.skoola.entity.Setting;
import com.dg.skoola.entity.Subject;
import com.dg.skoola.enumeration.ConsultationStatusConstant;
import com.dg.skoola.service.SkoolaService;

public class CustomService {

    private static CustomService customService = null;
    public static SkoolaService service = ServiceLocator.getInstance().getSkoolaService();
    static Logger logger = Logger.getLogger(CustomService.class);
    private static String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

    public static CustomService getInstance() {
        if (customService == null) {
            customService = new CustomService();
        }
        return customService;
    }

    public static Collection<Subject> getSkoolaSubjectBySubjectCategoryId(Long subjectCategoryId) {
        Collection<Subject> resultCol = new ArrayList<Subject>();
        try {

            String hql = "select s from Subject s where s.subjectGroup.id ='" + subjectCategoryId + "'";
            resultCol = service.getAllRecordsByHQL(hql);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultCol;
    }

    public static Collection<Subject> getSkoolaCourseSubjectByCourseCategoryIdAndSearchIndexParameter(Long courseCategoryId,Long searchIndex) {
        Collection<Subject> resultCol = new ArrayList<Subject>();
        try {
            
            if(courseCategoryId == null || courseCategoryId == 0){
                resultCol = service.getAllRecords(Subject.class,searchIndex.intValue(),Long.valueOf(maximumResultSetSize).intValue());
            }else{
                
                String hql = "select s from Subject s where s.subjectGroup.id="+courseCategoryId;
                resultCol = service.getAllRecordsByHQL(hql,searchIndex.intValue(),Long.valueOf(maximumResultSetSize).intValue());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultCol;
    }
    
    
     public static Long getCountOfSkoolaCourseSubjectByCourseCategoryId(Long courseCategoryId) {
        Long count = 0l;
        try {
            
            if(courseCategoryId == null || courseCategoryId == 0){
                String hql = "select count(s) from Subject s";
                count = (Long) service.getUniqueRecordByHQL(hql);
            }else{
                
                String hql = "select count(s) from Subject s where s.subjectGroup.id="+courseCategoryId;
                count = (Long) service.getUniqueRecordByHQL(hql);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }
     
     
    public static Long getCountOfSkoolaCourseSubjectGroup() {
        Long count = 0l;
        try {
            
        
                String hql = "select count(s) from SubjectGroup s ";
                count = (Long) service.getUniqueRecordByHQL(hql);
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }
    
     public static Long getCountOfSkoolaCountries() {
        Long count = 0l;
        try {
                String hql = "select count(s) from Country s ";
                count = (Long) service.getUniqueRecordByHQL(hql);
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }
     
      public static Long getCountOfSkoolaSchools() {
        Long count = 0l;
        try {
                String hql = "select count(s) from School s ";
                count = (Long) service.getUniqueRecordByHQL(hql);
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }
      
      
      
    public static Collection<School> getSchoolsByCountryIdAndSearchIndex(Long countryId,Integer searchIndex) {
       Collection<School> schoolCol = new ArrayList<School>();
       
        try {
                String hql = "";
                if(countryId != null){
                     hql = "select s from School s where s.country.id = "+countryId;
                }else{
                     hql = "select s from School s";
                }
                
                schoolCol =service.getAllRecordsByHQL(hql,searchIndex,Long.valueOf(maximumResultSetSize).intValue());
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return schoolCol;
    }
    
     public static Long getSchoolsCountByCountryId(Long countryId) {
      Long count = 0l;
       
        try {
                String hql = "";
                if(countryId != null){
                     hql = "select count(s) from School s where s.country.id = "+countryId;
                }else{
                     hql = "select count(s) from School s";
                }
                
                count =(Long) service.getUniqueRecordByHQL(hql);
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }
     
     
    public static Long getCountOfSkoolaExams() {
      Long count = 0l;
       
        try {
                String hql = "";
                
                hql = "select count(e) from Exam e";
                count =(Long) service.getUniqueRecordByHQL(hql);
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public static Exam getExamByFullName(String examNameInFull) {
        Exam exam = null;
        try{
            String hql = "";
            Collection<Exam> examCol =  service.getAllRecordsByHQL(hql);
            if(examCol != null && !examCol.isEmpty()){
            
                if(examCol.size() == 1){
                    exam = examCol.iterator().next();
                }  
            }
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return exam;
    }

    public ImageType getImageTypeByName(String imageTypeName) {
        ImageType imageType = null;
        try {

            String hql = "select i from ImageType i where i.name ='" + imageTypeName + "'";
            imageType = (ImageType) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return imageType;
    }

    public Image getImageBySchoolIdAndImageTypeId(Long schoolId, Long imageTypeId) {
        Image image = null;
        try {

            String hql = "select i from Image i where i.school.id ='" + schoolId + "' and i.imageType.id='" + imageTypeId + "";
            image = (Image) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return image;
    }

    public Collection<Course> getCoursesByCourseIdLocationIdStudyTypeIdMaxPriceMinPrice(
            Long courseIdLong, Long courseLocationIdLong,
            Long courseStudyTypeIdLong, Long minPriceLong, Long maxPriceLong,
            Long startIndexLong, Long endIndexLong) {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<Course> getCoursesByCourseIdLocationId(Long courseIdLong,
            Long courseLocationIdLong, Long startIndexLong, Long endIndexLong) {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<Course> getCoursesByCourseIdStudyTypeId(
            Long courseIdLong, Long courseStudyTypeIdLong, Long startIndexLong,
            Long endIndexLong) {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<Course> getCoursesByCourseIdLocationIdStudyTypeId(
            Long courseIdLong, Long courseLocationIdLong,
            Long courseStudyTypeIdLong, Long startIndexLong, Long endIndexLong) {
        // TODO Auto-generated method stub
        return null;
    }

    public static String getSettingValue(String settingName, String defaultValue,
            boolean createIfNotExist) {
        Setting setting = null;
        String query = "select s from Setting s where upper(s.name) = upper('" + settingName + "')";
        setting = (Setting) service.getUniqueRecordByHQL(query);
        if (setting != null) {
            return setting.getValue();
        }
        if (createIfNotExist) {
            setting = new Setting();
            setting.setName(settingName);
            setting.setDescription(settingName);
            setting.setValue(defaultValue);

            service.createNewRecord(setting);
        }
        return defaultValue;
    }

    public static PortalUser getPortalUserByEmailAddress(String emailAddress) {
        PortalUser portalUser = null;
        try {

            String hql = "select p from PortalUser p where upper(p.email) = upper('" + emailAddress + "')";
            portalUser = (PortalUser) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return portalUser;
    }

    public static PortalUser getPortalUserByPhoneNumber(String phoneNumber) {
        PortalUser portalUser = null;
        try {

            String option1 = SkoolaConstants.NIGERIA_PHONE_CODE + phoneNumber;
            String option2 = SkoolaConstants.NIGERIA_PHONE_CODE + phoneNumber.substring(0);
            String hql = "select p from PortalUser p where p.phoneNumber ='" + option1 + "' "
                    + "or p.phoneNumber = '" + option2 + "' or p.phoneNumber = '" + phoneNumber + "'";
            portalUser = (PortalUser) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return portalUser;
    }

    public static AccountCreationRequest getAccountCreationRequestByPhoneNumber(String phoneNumber) {
        AccountCreationRequest accr = null;
        try {
            String option1 = SkoolaConstants.NIGERIA_PHONE_CODE + phoneNumber;
            String option2 = SkoolaConstants.NIGERIA_PHONE_CODE + phoneNumber.substring(0);
            String hql = "select a from AccountCreationRequest a where a.phoneNumber ='" + option1 + "' "
                    + "or a.phoneNumber = '" + option2 + "' or a.phoneNumber = '" + phoneNumber + "'";
            accr = (AccountCreationRequest) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return accr;
    }

    public static AccountCreationRequest getAccountCreationRequestByEmailAddress(String emailAddress) {
        AccountCreationRequest accr = null;
        try {

            String hql = "select a from AccountCreationRequest a where upper(a.email) = upper('" + emailAddress + "')";
            accr = (AccountCreationRequest) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return accr;
    }

    public static AccountCreationRequest getAccountCreationRequestByActivationCode(String activationCode) {
        AccountCreationRequest accr = null;
        try {
            String hql = "select a from AccountCreationRequest a where upper(a.activationCode) = upper('" + activationCode + "')";
            accr = (AccountCreationRequest) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return accr;

    }

    public static PortalUser getPortalUserByUserId(String userId) {
        PortalUser portalUser = null;
        try {

            String hql = "select p from PortalUser p where upper(p.userId) = upper('" + userId + "')";
            portalUser = (PortalUser) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return portalUser;

    }

    /*public static PortalUser getPortalUserByEmail(String email){
		PortalUser portalUser = null;
		try{
			String hql = "select p from PortalUser p where upper(p.email) = upper('"+email.trim()+"')";
			portalUser = (PortalUser) service.getUniqueRecordByHQL(hql);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return portalUser;
		
	}*/
    public static Role getRoleByName(String roleName) {
        Role role = null;
        try {

            String hql = "select r from Role r where upper(r.name) = upper('" + roleName + "')";
            role = (Role) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return role;

    }

    public static Long getCountOfCoursesBySubjectGroupId(Long subjectGroupId) {
        Long count = 0l;
        try {

            String hql = "select count(c) from Course c where c.subject.id in ( select s.id from Subject s where s.subjectGroup.id = " + subjectGroupId + ")";
            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return count;
    }

    public static Long getCountOfUniversitiesBySubjectGroupId(Long subjectGroupId) {
        Long count = 0l;
        try {

            String hql = "select count(sc) from School sc where sc.id in (select c.school.id from Course c where c.subject.id in ( select s.id from Subject s where s.subjectGroup.id  = " + subjectGroupId + "))";
            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return count;
    }

    public static Collection<Course> getCoursesBySearchParameter(String courseId, String maximumTuition, String minimumTuition,
            String courseTypeId, String countryId, String searchIndex, String subjectId, String type) {
        Collection<Course> courseCol = new ArrayList<Course>();
        String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
        String dollarExchangeRate = CustomService.getSettingValue("DOLLAR_EXCHANGE_RAGE", "325", true);
        try {

            StringBuilder sb = new StringBuilder();

            if (maximumTuition != null && !maximumTuition.isEmpty() && minimumTuition != null && !minimumTuition.isEmpty()) {
                BigDecimal minimumFeeInCents = new BigDecimal(minimumTuition);
                BigDecimal dollarToNairaExchangeRate = new BigDecimal(dollarExchangeRate);
                minimumFeeInCents = minimumFeeInCents.divide(dollarToNairaExchangeRate, 0, RoundingMode.CEILING);
                minimumFeeInCents = (minimumFeeInCents.multiply(new BigDecimal(100)));

                BigDecimal maximumFeeInCents = new BigDecimal(maximumTuition);
                dollarToNairaExchangeRate = new BigDecimal(dollarExchangeRate);
                maximumFeeInCents = maximumFeeInCents.divide(dollarToNairaExchangeRate, 0, RoundingMode.CEILING);
                maximumFeeInCents = (maximumFeeInCents.multiply(new BigDecimal(100)));

                sb.append("select c from Course c where c.feeInCents >= '" + minimumFeeInCents.longValue() + "' and c.feeInCents <= '" + maximumFeeInCents + "'");
            }

            if (courseTypeId != null && !(courseTypeId.isEmpty())) {

                sb.append("and c.courseType.id = " + Long.valueOf(courseTypeId) + "");
            }

            if (countryId != null && !(countryId.isEmpty())) {

                sb.append(" and c.school.id in ( select s.id from School s where s.country.id = " + Long.valueOf(countryId) + ")");
            }

            if (type != null && !type.isEmpty() && subjectId != null && !subjectId.isEmpty()) {
                if (type.equalsIgnoreCase("Subject")) {
                    sb.append(" and c.subject.id = " + Long.valueOf(subjectId));
                } else if (type.equalsIgnoreCase("SubjectGroup")) {
                    sb.append(" and c.subject.id in ( select s.id from Subject s where s.subjectGroup.id = " + subjectId + ")");
                }
            }

            String hql = sb.toString();
            System.out.println(" final hql = " + hql);
            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }
            Collection<Course> queryResult = service.getAllRecordsByHQL(hql, Integer.valueOf(searchIndex), Integer.valueOf(maximumResultSetSize));

            courseCol = queryResult;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return courseCol;
    }

    public static Long getCountOfCoursesBySearchParameters(String courseId, String maximumTuition, String minimumTuition,
            String courseTypeId, String countryId, String searchIndex, String subjectId, String type) {
        Long resultSize = 0l;
        String dollarExchangeRate = CustomService.getSettingValue("DOLLAR_EXCHANGE_RAGE", "325", true);

        try {

            StringBuilder sb = new StringBuilder();

            if (maximumTuition != null && !maximumTuition.isEmpty() && minimumTuition != null && !minimumTuition.isEmpty()) {
                BigDecimal minimumFeeInCents = new BigDecimal(minimumTuition);
                BigDecimal dollarToNairaExchangeRate = new BigDecimal(dollarExchangeRate);
                minimumFeeInCents = minimumFeeInCents.divide(dollarToNairaExchangeRate, 0, RoundingMode.CEILING);
                minimumFeeInCents = (minimumFeeInCents.multiply(new BigDecimal(100)));

                BigDecimal maximumFeeInCents = new BigDecimal(maximumTuition);
                dollarToNairaExchangeRate = new BigDecimal(dollarExchangeRate);
                maximumFeeInCents = maximumFeeInCents.divide(dollarToNairaExchangeRate, 0, RoundingMode.CEILING);
                maximumFeeInCents = (maximumFeeInCents.multiply(new BigDecimal(100)));

                sb.append("select count(c) from Course c where c.feeInCents >= '" + minimumFeeInCents.longValue() + "' and c.feeInCents <= '" + maximumFeeInCents + "'");
            }

            if (courseTypeId != null && !(courseTypeId.isEmpty())) {

                sb.append("and c.courseType.id = " + Long.valueOf(courseTypeId) + "");
            }

            if (countryId != null && !(countryId.isEmpty())) {

                sb.append(" and c.school.id in ( select s.id from School s where s.country.id = " + Long.valueOf(countryId) + ")");
            }

            if (type != null && !type.isEmpty() && subjectId != null && !subjectId.isEmpty()) {
                if (type.equalsIgnoreCase("Subject")) {
                    sb.append(" and c.subject.id = " + Long.valueOf(subjectId));
                } else if (type.equalsIgnoreCase("SubjectGroup")) {
                    sb.append(" and c.subject.id in ( select s.id from Subject s where s.subjectGroup.id = " + subjectId + ")");
                }
            }

            String hql = sb.toString();
            System.out.println(" final hql = " + hql);
            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }
            resultSize = (Long) service.getUniqueRecordByHQL(hql);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return resultSize;
    }

    public Feeschedule getFeesScheduleByProcessTypeId(Long processTypeId) {
        Feeschedule feesSchedule = null;
        try {
            String hql = "select f from Feeschedule f where f.processType.id = '" + processTypeId + "'";
            feesSchedule = (Feeschedule) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return feesSchedule;
    }

    public Feeschedule getFeesScheduleByCode(String code) {
        Feeschedule feesSchedule = null;
        try {

            String hql = "select f from Feeschedule f where upper(f.code) = upper('" + code + "')";
            feesSchedule = (Feeschedule) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return feesSchedule;
    }

    public ProcessType getProcessTypeByName(String processTypeName) {
        ProcessType processType = null;
        try {

            String hql = "select p from ProcessType p where upper(p.name) =upper('" + processTypeName + "')";
            processType = (ProcessType) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return processType;
    }

    public PaymentHistory getPaymentHistoryByReferenceId(String paymentReference) {
        PaymentHistory paymentHistory = null;
        try {
            String hql = "select p from PaymentHistory p where upper(p.referenceId) = upper('" + paymentReference + "')";
            paymentHistory = (PaymentHistory) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return paymentHistory;
    }

    public Collection<PaymentHistory> getPaymentHistoryByCreatedByProcessTypeAndRecordId(Long createdBy, Long processTypeId, Long recordId) {
        Collection<PaymentHistory> paymentHistoryCol = new ArrayList<PaymentHistory>();
        try {
            String hql = "select p from PaymentHistory p where p.createdBy.id='" + createdBy + "' and p.recordId ='" + recordId + "' and p.processType.id='" + processTypeId + "'";
            paymentHistoryCol = service.getAllRecordsByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return paymentHistoryCol;
    }

    public static Collection<PaymentHistory> getPaginatedPaymentHistoryListByPortalUserIdAndSearchIndex(Long portalUserId, Long searchIndex) {
        Collection<PaymentHistory> paymentHistoryCol = new ArrayList<PaymentHistory>();
        try {
            String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
            String hql = "select p from PaymentHistory p where p.createdBy.id = " + portalUserId + " order by p.dateCreated desc";
            paymentHistoryCol = service.getAllRecordsByHQL(hql, searchIndex.intValue(), Long.valueOf(maximumResultSetSize).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return paymentHistoryCol;
    }

    public static Long getCountOfPaymentHistoryListByPortalUserId(Long portalUserId) {
        Long count = 0l;
        try {
            String hql = "select count(p) from PaymentHistory p where p.createdBy.id = " + portalUserId + " ";
            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public static Collection<ConsultationRequest> getPaginatedConsultationRequestListByPortalUserIdAndSearchIndex(Long portalUserId, Long searchIndex) {
        Collection<ConsultationRequest> consultationRequestCol = new ArrayList<ConsultationRequest>();
        try {

            String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
            String hql = "select c from ConsultationRequest c where c.createdBy.id = " + portalUserId + " order by c.dateCreated  desc";
            consultationRequestCol = service.getAllRecordsByHQL(hql, searchIndex.intValue(), Long.valueOf(maximumResultSetSize).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return consultationRequestCol;
    }

    public static Long getCountOfConsultationRequestListByPortalUserId(Long portalUserId) {
        Long count = 0l;
        try {
            String hql = "select count(c) from ConsultationRequest c where c.createdBy.id = " + portalUserId + " ";
            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public static Collection<Role> getAllPortalUserRolePortalUserId(Long portalUserId) {
        Collection<Role> queryResultCol = new ArrayList<Role>();
        try {
            String hql = "select r from Role r where r.id in (select p.role.id from PortalUserRoleMap p where p.portalUser.id =" + portalUserId + ")";
            queryResultCol = service.getAllRecordsByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return queryResultCol;
    }

    public static Collection<PortalUser> getAllPortalUsersByRoleId(Long roleId, Long searchIndex) {
        Collection<PortalUser> portalUserCol = new ArrayList<PortalUser>();
        try {
            String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
            String hql = "select p from PortalUser p where p.id in (select pu.portalUser.id from PortalUserRoleMap pu where pu.role.id = " + roleId + ") order by p.firstname asc";
            portalUserCol = service.getAllRecordsByHQL(hql, searchIndex.intValue(), Long.valueOf(maximumResultSetSize).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return portalUserCol;
    }

    public static Long getCountOfPortalUsersByRoleId(Long roleId) {
        Long resultCount = 0l;
        try {

            String hql = "select count(p) from PortalUser p where p.id in (select pu.portalUser.id from PortalUserRoleMap pu where pu.role.id = " + roleId + ")";
            resultCount = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultCount;
    }

    public static Collection<PortalUser> getAllPortalUsersNotStudents(Long studentRoleId, Long searchIndex) {
        Collection<PortalUser> portalUserCol = new ArrayList<PortalUser>();
        try {
            String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
            String hql = "select p from PortalUser p where p.id in (select pu.portalUser.id from PortalUserRoleMap pu where pu.role.id != " + studentRoleId + ") order by p.firstname asc";
            portalUserCol = service.getAllRecordsByHQL(hql, searchIndex.intValue(), Long.valueOf(maximumResultSetSize).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return portalUserCol;
    }

    public static Long getCountOfPortalUsersNotStudents(Long studentRoleId) {
        Long resultCount = 0l;
        try {

            String hql = "select count(p) from PortalUser p where p.id in (select pu.portalUser.id from PortalUserRoleMap pu where pu.role.id != " + studentRoleId + ")";
            resultCount = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultCount;
    }

    public static Collection<ConsultationRequest> getPaginatedConsultationRequestListByConsultationStatusConstant(ConsultationStatusConstant consultationStatusConstant, Long searchIndex) {
        Collection<ConsultationRequest> consultationRequestCol = new ArrayList<ConsultationRequest>();
        try {

            String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
            String hql = "";
            if (consultationStatusConstant == null) {
                hql = "select c from ConsultationRequest c order by c.dateCreated  desc";
            } else {
                hql = "select c from ConsultationRequest c where c.consultationStatus = '" + consultationStatusConstant + "' order by c.dateCreated  desc";
            }
            consultationRequestCol = service.getAllRecordsByHQL(hql, searchIndex.intValue(), Long.valueOf(maximumResultSetSize).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return consultationRequestCol;
    }

    public static Long getCountOfConsultationRequestListByConsultationStatusConstant(ConsultationStatusConstant consultationStatusConstant) {
        Long count = 0l;
        try {
            String hql = "";
            if (consultationStatusConstant == null) {
                hql = "select count(c) from ConsultationRequest c";
            } else {
                hql = "select count(c) from ConsultationRequest c where c.consultationStatus = '" + consultationStatusConstant + "'";
            }

            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public static Long getUserSignUpCountByDateRange(Timestamp startDate, Timestamp endDate) {
        Long count = 0l;
        try {
            String hql = "";

            hql = "select count(p) from PortalUser p where p.dateCreated >= '" + startDate + "' and p.dateCreated <= '" + endDate + "'";

            //System.out.println(" query for user signup in time range = " + hql);
            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public static Long getConsultationRequestCountByDateRangeAndConsultationStatus(Timestamp startDate, Timestamp endDate, ConsultationStatusConstant consultationStatusConstant) {
        Long count = 0l;
        try {
            String hql = "";

            hql = "select count(c) from ConsultationRequest c where c.consultationStatus = '" + consultationStatusConstant + "' and c.dateCreated >= '" + startDate + "' and c.dateCreated <= '" + endDate + "'";

            count = (Long) service.getUniqueRecordByHQL(hql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }

    public static Collection<Course> getPaginatedSkoolaCoursesByIndexSubjectIdAndCourseTypeId(Long searchIndexValue,
            Long subjectIdValue, Long courseTypeIdValue, Long schoolIdValue) {
        Collection<Course> courseCol = new ArrayList<Course>();
        try {

            String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);
            StringBuilder hql = new StringBuilder();

            hql.append("select c from Course c where c.id is not null ");
            if (subjectIdValue != null) {
                hql.append(" and c.subject.id = " + subjectIdValue + " ");
            }
            if (courseTypeIdValue != null) {
                hql.append(" and c.courseType.id = " + courseTypeIdValue + "");
            }
            if (schoolIdValue != null) {
                hql.append(" and c.school.id = " + schoolIdValue + "");
            }
            System.out.println(" hql = " + hql.toString());
            courseCol = service.getAllRecordsByHQL(hql.toString(), searchIndexValue.intValue(), Long.valueOf(maximumResultSetSize).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return courseCol;
    }

    public static Long getCountOfSkoolaCoursesBySubjectIdAndCourseTypeId(Long subjectIdValue, Long courseTypeIdValue, Long schoolIdValue) {
        Long courseCount = 0l;
        try {
            StringBuilder hql = new StringBuilder();

            hql.append("select count(c) from Course c where c.id is not null ");
            if (subjectIdValue != null) {
                hql.append(" and c.subject.id = " + subjectIdValue + " ");
            }
            if (courseTypeIdValue != null) {
                hql.append(" and c.courseType.id = " + courseTypeIdValue + "");
            }
            if (schoolIdValue != null) {
                hql.append(" and c.school.id = " + schoolIdValue + "");
            }

            courseCount = (Long) service.getUniqueRecordByHQL(hql.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return courseCount;
    }

}

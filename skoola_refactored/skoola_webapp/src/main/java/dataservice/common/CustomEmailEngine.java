package dataservice.common;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.sun.mail.smtp.SMTPTransport;
import dataservice.util.Constant;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class CustomEmailEngine {

    private Logger logger = Logger.getLogger(CustomEmailEngine.class);
    private CustomService skoolaService = CustomService.getInstance();
    private String senderEmail = CustomService.getSettingValue(Constant.NOTIFICATION_EMAIL_ADDRESS, "noreply@skoola.com", true);
    private String password = CustomService.getSettingValue(Constant.NOTIFICATION_EMAIL_PASSWORD, "*#n0r3ply#", true);
    private String smtpHost = CustomService.getSettingValue(Constant.NOTIFICATION_SMTP_HOST, "smtp.mailgun.org", true);
    private String mailEnginURL = CustomService.getSettingValue(Constant.MAIL_ENGINE_URL, "smtp.mailgun.com", true);

    public void sendMessageWithHTMLTemplate(String recieverEmail, String mailSubject, Map<String, Object> inputs, String templateFileName) {

        File templatePath = new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + "emailtemplates");
        if (!templatePath.exists()) {
            templatePath.mkdir();
        }
        try {
            // System.out.println("template path: "+templateDirPath);
            Configuration cfg = new Configuration();
            cfg.setDirectoryForTemplateLoading(templatePath);
            Template template = cfg.getTemplate(templateFileName);

            Writer out = new StringWriter();
            template.process(inputs, out);
            String[] recipient = {recieverEmail};
            System.out.println("1");
            sendEmailToUser(recipient, mailSubject, out.toString());

        } catch (TemplateException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    private void sendEmailToUser(final String[] recipient, final String mailSubject, final String mailBody) {
        Thread t = new Thread() {

            public void run() {
                System.out.println(">>>>Send mail thread started..." + recipient[0]);
                Properties props = new Properties();
                props = setMailServerProperties(props);
                try {
                    createEmailMessage(recipient, mailSubject, mailBody, props);
                } catch (Exception e) {
                    System.out.println("Unable to send mail: " + e.getMessage());
                }
            }
        };
        t.start();

    }

    private Properties setMailServerProperties(Properties props) {

        props.put("mail.smtps.host", smtpHost);
        props.put("mail.smtps.auth", "true");

        return props;
    }

    private void createEmailMessage(String[] recipent, String mailSubject, String mailBody, Properties props) throws AddressException, MessagingException {

        try {
            Session session = Session.getInstance(props, null);

            String[] toEmails = recipent;
            String emailSubject = mailSubject;
            String emailBody = mailBody;
            Message emailMessage = new MimeMessage(session);
            emailMessage.setFrom(new InternetAddress(senderEmail));
            emailMessage.setSentDate(new Date());

            for (int i = 0; i < toEmails.length; i++) {
                emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
            }

            emailMessage.setSubject(emailSubject);
            emailMessage.setContent(emailBody, "text/html");// for a html email

            SMTPTransport t = (SMTPTransport) session.getTransport("smtps");
            t.connect(mailEnginURL, senderEmail, password);
            t.sendMessage(emailMessage, emailMessage.getAllRecipients());
            System.out.println(" Email sent!!!");
            System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (Exception e) {
            System.out.println("Email NOT Sent!!!");
            e.printStackTrace();
        }

    }

}

package dataservice.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;

public class SkoolaUtil {
	
	public static byte[] getMessageDigest(String digestAlgorithm,String textVariable){
		//String digest = "";
		byte[] dg = null;
		try{
			MessageDigest md = MessageDigest.getInstance(digestAlgorithm);
			String param = textVariable;
	
			md.update(param.getBytes("UTF-8")); // Change this to "UTF-16" if needed
			dg = md.digest();
			//digest = new String(dg);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return dg;
	}
	
	
	 public static String convertFromCentsToNaira(Long amountInCents,String dollarExchangeRate){
	    	String result = "";
	    	if(amountInCents != null && amountInCents != 0){
				BigDecimal courseFee = new BigDecimal(amountInCents);
				BigDecimal dollarToNairaExchangeRate = new BigDecimal(dollarExchangeRate);
				BigDecimal courseFeeInNaira = (courseFee.divide(new BigDecimal(100),2,RoundingMode.CEILING)).multiply(dollarToNairaExchangeRate);    
				result = BWUtil.getFormattedKoboPrice( courseFeeInNaira.longValue() * 100);
				
			}
	    	return result;
	    }
	

}

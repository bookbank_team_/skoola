package dataservice.payment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.dg.skoola.ServiceLocator;


import com.dg.skoola.entity.ConsultationRequest;
import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.Feeschedule;
import com.dg.skoola.entity.PaymentCardDetail;
import com.dg.skoola.entity.PaymentHistory;
import com.dg.skoola.entity.PaymentResponseStore;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.PortalUserPaymentDetailMap;
import com.dg.skoola.entity.ProcessType;
import com.dg.skoola.entity.Setting;
import com.dg.skoola.enumeration.ConsultationStatusConstant;
import com.dg.skoola.enumeration.PaymentStatusConstant;
import com.dg.skoola.enumeration.ProcessTypeConstant;

import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;
import dataservice.common.CustomService;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

import sun.misc.BASE64Encoder;


public class PaymentHandler {

    //private HashValidator hashValidator = HashValidator.getInstance();
    private static Logger log = Logger.getLogger(PaymentHandler.class);
    private static SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
    private static CustomService skoolaCustomService = CustomService.getInstance();
    private static BASE64Encoder encoder = new BASE64Encoder();
    private static Gson gson = new Gson();
    private static JSONParser jsonParser = new JSONParser();

   
    public static String createPaymentHistory(String data) {
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return gson.toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String processTypeId = (String) requestObject.get("processTypeId");
                String recordId = (String) requestObject.get("recordId");
                String portalUserId = (String) requestObject.get("portalUserId");
                String feesScheduleId = (String) requestObject.get("feesScheduleId");

                System.out.println(" action = " + action);
                System.out.println(" processTypeId = " + processTypeId);
                System.out.println(" recordId = " + recordId);
                System.out.println(" portalUserId = " + portalUserId);
                System.out.println(" feesScheduleId = " + feesScheduleId);

                if (action.equalsIgnoreCase("createPaymentHistory") && !processTypeId.isEmpty() && !portalUserId.isEmpty() && !recordId.isEmpty()) {

                    ProcessType processType = (ProcessType) skoolaService.getRecordById(ProcessType.class, Long.valueOf(processTypeId));

                    if (processType == null) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("Process Type cannot be null, contact support");

                        return gson.toJson(responseObject);
                    }

                    Feeschedule feeschedule = (Feeschedule) skoolaService.getRecordById(Feeschedule.class, Long.valueOf(feesScheduleId));

                    PortalUser portalUser = (PortalUser) skoolaService.getRecordById(PortalUser.class, Long.valueOf(portalUserId));

                    String payStackSecretKey = "";
                    String payStackPaymentURL = "";

                    try {
                        payStackSecretKey = skoolaCustomService.getSettingValue("PAY_STACK_PAYMENT_API_SECRET_KEY", "Bearer sk_test_470832b29008cc54862a18aff3718b198061779a", true);
                        payStackPaymentURL = skoolaCustomService.getSettingValue("PAY_STACK_PAYMENT_URL", "https://api.paystack.co/transaction/initialize", true);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    if (portalUser == null) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("User cannot be null, contact support");

                        return gson.toJson(responseObject);
                    }

                    String uniqueReferenceId = "";
                    do {
                        uniqueReferenceId = RandomStringUtils.randomAlphanumeric(11).toUpperCase();
                    } while (skoolaCustomService.getPaymentHistoryByReferenceId(uniqueReferenceId) != null);

                    PaymentHistory paymentHistory = new PaymentHistory();
                    paymentHistory.setAmountInKobo(feeschedule.getFeeInKobo());
                    paymentHistory.setDateCreated(new Timestamp(new Date().getTime()));
                    paymentHistory.setCreatedBy(portalUser);
                    paymentHistory.setPaymentStatus(PaymentStatusConstant.PENDING);
                    paymentHistory.setProcessType(processType);
                    paymentHistory.setRecordId(Long.valueOf(recordId));
                    paymentHistory.setReferenceId(uniqueReferenceId);
                    paymentHistory.setPaymentChannel("N/A");
                    paymentHistory.setLastUpdated(new Timestamp(new Date().getTime()));

                    paymentHistory = (PaymentHistory) skoolaService.createNewRecord(paymentHistory);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("paymentHistoryReferenceId", paymentHistory.getReferenceId().toString());
                    jsonObject.put("payStackSecretKey", payStackSecretKey);
                    jsonObject.put("payStackPaymentURL", payStackPaymentURL);
                    jsonObject.put("amountInKobo", paymentHistory.getAmountInKobo().toString());

                    System.out.println(" amount created =  amount " + paymentHistory.getAmountInKobo().toString());

                    responseObject = new SkoolaJSONResponseObject();
                    responseObject.setData(jsonObject.toJSONString());
                    responseObject.setSuccessful(true);
                    responseObject.setMessage("Successful");

                    return gson.toJson(responseObject);

                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new Gson().toJson(responseObject);
    }


    public static String updatePaymentHistoryWithPayStackResponse(String data) {
        System.out.println(" update_payment_history_with_paystack_response ");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        ProcessType processType = null;

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return gson.toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String paymentReferenceId = (String) requestObject.get("paymentReferenceId");
                String payStackResponse = (String) requestObject.get("payStackResponse");

                if (action.equalsIgnoreCase("updatePaymentHistoryWithPayStackPaymentResponse") && !paymentReferenceId.isEmpty()) {

                    PaymentHistory paymentHistory = skoolaCustomService.getPaymentHistoryByReferenceId(paymentReferenceId.trim());

                    if (paymentHistory == null) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("Hash Validation Failed");

                        return gson.toJson(responseObject);
                    }
                    processType = (ProcessType) skoolaService.getRecordById(ProcessType.class, paymentHistory.getProcessType().getId());

                    String verificationResponse = payStackResponse;
                    System.out.println(" paystack response = " + payStackResponse);
                    if (verificationResponse != null && !verificationResponse.isEmpty()) {

                        JSONObject verificationResponseJSON = (JSONObject) new JSONParser().parse(verificationResponse);

                        Boolean status = (Boolean) verificationResponseJSON.get("status");
                        String message = (String) verificationResponseJSON.get("message");

                        if (status && message.equalsIgnoreCase("Verification successful")) {

                            JSONObject dataJSONObject = (JSONObject) verificationResponseJSON.get("data");
                            JSONObject authorizationJSONObject = (JSONObject) dataJSONObject.get("authorization");
                            JSONObject customerJSONObject = (JSONObject) dataJSONObject.get("customer");
                            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ dataJSONObject =" + dataJSONObject);
                            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ authorizationJSONObject =" + authorizationJSONObject);
                            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ customerJSONObject =" + customerJSONObject);
                            String paymentResponseJSON = verificationResponse;
                            String transactionDate = dataJSONObject.get("transaction_date").equals(null) ? "" : (String) dataJSONObject.get("transaction_date");
                            String paymentStatus = dataJSONObject.get("status").equals(null) ? "" : (String) dataJSONObject.get("status");
                            String referenceId = dataJSONObject.get("reference").equals(null) ? "" : (String) dataJSONObject.get("reference");
                            String authorizationCode = authorizationJSONObject.get("authorization_code").equals(null) ? "" : (String) authorizationJSONObject.get("authorization_code");
                            String cardType = authorizationJSONObject.get("card_type").equals(null) ? "" : (String) authorizationJSONObject.get("card_type");
                            String lastFourDigits = authorizationJSONObject.get("last4").equals(null) ? "" : (String) authorizationJSONObject.get("last4");
                            String bank = authorizationJSONObject.get("bank").equals(null) ? "" : (String) authorizationJSONObject.get("bank");
                            String firstName = customerJSONObject.get("first_name") == null ? "" : (String) customerJSONObject.get("first_name");
                            String lastName = customerJSONObject.get("last_name") == null ? "" : (String) customerJSONObject.get("last_name");
                            String email = customerJSONObject.get("email") == null ? "" : (String) customerJSONObject.get("email");

                            Date date = new Date();

                            try {
                                date = sdf.parse(transactionDate);

                            } catch (Exception ex) {
                                //ex.printStackTrace();
                            }
                            if (paymentStatus.equalsIgnoreCase("success") && referenceId.equalsIgnoreCase(paymentHistory.getReferenceId())) {

                                paymentHistory.setPaymentStatus(PaymentStatusConstant.SUCCESSFUL);
                                paymentHistory.setLastUpdated(new Timestamp(new Date().getTime()));
                                paymentHistory.setDatePaid(new Timestamp(date.getTime()));
                                skoolaService.updateRecord(paymentHistory);

                                if (updateUserApplicationRequests(paymentHistory.getRecordId(), paymentHistory.getProcessType().getId())) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("paymentHistoryReferenceId", paymentHistory.getReferenceId());

                                    responseObject = new SkoolaJSONResponseObject();
                                    responseObject.setData(jsonObject.toJSONString());
                                    responseObject.setSuccessful(true);
                                    responseObject.setMessage("Successful");
                                }
                                System.out.println(" payment status is success");

                                if (!authorizationCode.isEmpty() && !cardType.isEmpty() && !lastFourDigits.isEmpty()) {

                                    try {
                                        PortalUser portalUser = (PortalUser) skoolaService.getRecordById(PortalUser.class, paymentHistory.getCreatedBy().getId());

                                        PaymentCardDetail cardDetail = new PaymentCardDetail();
                                        cardDetail.setFirstName(firstName);
                                        cardDetail.setLastName(lastName);
                                        cardDetail.setEmail(email);
                                        cardDetail.setAuthorizationCode(authorizationCode);
                                        cardDetail.setCardType(cardType);
                                        cardDetail.setLastFourDigits(lastFourDigits);
                                        cardDetail.setBank(bank);

                                        cardDetail = (PaymentCardDetail) skoolaService.createNewRecord(cardDetail);

                                        PortalUserPaymentDetailMap map = new PortalUserPaymentDetailMap();
                                        map.setPaymentCardDetail(cardDetail);
                                        map.setPortalUser(portalUser);

                                        map = (PortalUserPaymentDetailMap) skoolaService.createNewRecord(map);
                                    } catch (Exception e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    System.out.println(" authoriazatio detail is saved ");
                                }

                                PaymentResponseStore paymentResponseStore = new PaymentResponseStore();
                                paymentResponseStore.setDateCreated(new Timestamp(new Date().getTime()));
                                paymentResponseStore.setResponse(paymentResponseJSON);

                                skoolaService.createNewRecord(paymentResponseStore);

                                System.out.println(" payment response is stored");
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("processTypeName", processType.getName().getValue());
                                jsonObject.put("paymentStatus", "successful");

                                responseObject = new SkoolaJSONResponseObject();
                                responseObject.setData(jsonObject.toJSONString());
                                responseObject.setSuccessful(true);
                                responseObject.setMessage("Successful");
                                System.out.println(" response sent");
                                return gson.toJson(responseObject);

                            } else {
                                System.out.println(" payment status is not success");
                            }
                        } else {
                            System.out.println(" payment verification status was not successful ");
                        }
                    }
                }
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return gson.toJson(responseObject);
    }

   
    public static String updatePaymentHistoryWithPaystackAccessCode(String data) {
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();
        System.out.println(" inside update access codes ");
        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return gson.toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String paymentHistoryReferenceId = (String) requestObject.get("paymentHistoryReferenceId");
                String payStackAccessCode = (String) requestObject.get("accessCode");

                if (action.equalsIgnoreCase("updatePaymentHistoryWithPaystackAccessCode") && !paymentHistoryReferenceId.isEmpty() && !payStackAccessCode.isEmpty()) {

                    PaymentHistory paymentHistory = skoolaCustomService.getPaymentHistoryByReferenceId(paymentHistoryReferenceId.trim());

                    if (paymentHistory == null) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("Hash Validation Failed");

                        return gson.toJson(responseObject);
                    }
                    paymentHistory.setPaystackAccessCode(payStackAccessCode);
                    skoolaService.updateRecord(paymentHistory);

                    Collection<PaymentHistory> duplicateRequestsCol
                            = skoolaCustomService.getPaymentHistoryByCreatedByProcessTypeAndRecordId(paymentHistory.getCreatedBy().getId(), paymentHistory.getProcessType().getId(), paymentHistory.getRecordId());
                    Boolean paidFor = false;
                    System.out.println(" no. of duplications = " + duplicateRequestsCol.size());
                    if (duplicateRequestsCol != null && !duplicateRequestsCol.isEmpty()) {
                        duplicateRequestsCol.remove(paymentHistory);
                        for (PaymentHistory p : duplicateRequestsCol) {
                            if (p.getPaymentStatus() == PaymentStatusConstant.PENDING) {
                                p.setPaymentStatus(PaymentStatusConstant.CANCELED);
                                skoolaService.updateRecord(p);
                            }/*else if(p.getPaymentStatus() == PaymentStatusConstant.SUCCESSFUL){
			    				paidFor = true;
			    			}*/
                        }
                    }

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("paymentHistoryReferenceId", paymentHistory.getReferenceId());
                    jsonObject.put("paidFor", paidFor);

                    responseObject = new SkoolaJSONResponseObject();
                    responseObject.setData(jsonObject.toJSONString());
                    responseObject.setSuccessful(true);
                    responseObject.setMessage("Successful");

                    return gson.toJson(responseObject);
                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new Gson().toJson(responseObject);
    }

  
    public static String getProcessTypeByName(String data) {
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return gson.toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String processTypeName = (String) requestObject.get("processTypeName");

                if (action.equalsIgnoreCase("getProcessTypeById")) {

                    ProcessType processType = skoolaCustomService.getProcessTypeByName(processTypeName);

                    if (processType == null) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("An error occured, please contact the administrator.");

                        return gson.toJson(responseObject);
                    } else {

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("processTypeId", processType.getId().toString());
                        jsonObject.put("processTypeName", processType.getName());

                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setData(jsonObject.toJSONString());
                        responseObject.setSuccessful(true);
                        responseObject.setMessage("Successful");

                        return gson.toJson(responseObject);

                    }
                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new Gson().toJson(responseObject);
    }


    public static String getSettingByName(String data) {
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return gson.toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String settingName = (String) requestObject.get("settingName");

                if (action.equalsIgnoreCase("getSettingByName")) {

                    String settingValue = "";

                    if (settingName.equalsIgnoreCase("PAY_STACK_PAYMENT_API_SECRET_KEY")) {
                        settingValue = skoolaCustomService.getSettingValue("PAY_STACK_PAYMENT_API_SECRET_KEY", "Bearer sk_test_470832b29008cc54862a18aff3718b198061779a", true);
                    } else if (settingName.equalsIgnoreCase("PAY_STACK_PAYMENT_URL")) {
                        settingValue = skoolaCustomService.getSettingValue("PAY_STACK_PAYMENT_URL", "https://api.paystack.co/transaction/initialize", true);
                    } else if (settingName.equalsIgnoreCase("PAYSTACK_TRANSACTION_VERIFICATION_URL")) {
                        settingValue = skoolaCustomService.getSettingValue("PAYSTACK_TRANSACTION_VERIFICATION_URL", "https://api.paystack.co/transaction/verify/", true);
                    }

                    if (settingValue == null || settingValue.isEmpty()) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("An error occured, please contact the administrator.");

                        return gson.toJson(responseObject);
                    } else {

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("settingValue", settingValue);

                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setData(jsonObject.toJSONString());
                        responseObject.setSuccessful(true);
                        responseObject.setMessage("Successful");

                        return gson.toJson(responseObject);

                    }
                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new Gson().toJson(responseObject);
    }

  
    public static String getFeeScheduleByProcessTypeById(String data) {
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return new Gson().toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String processTypeId = (String) requestObject.get("processTypeId");

                if (action.equalsIgnoreCase("getFeeScheduleByProcessTypeId")) {

                    ProcessType processType = (ProcessType) skoolaService.getRecordById(ProcessType.class, Long.valueOf(processTypeId));

                    if (processType == null) {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        responseObject.setMessage("An error occured, contact administrator!");
                        return gson.toJson(skoolaJSONRequestObject);
                    }

                    Feeschedule feesSchedule = skoolaCustomService.getFeesScheduleByProcessTypeId(Long.valueOf(processType.getId()));

                    if (feesSchedule != null) {

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("feeInKobo", feesSchedule.getFeeInKobo().toString());
                        jsonObject.put("processType", processType.getName());
                        jsonObject.put("processTypeId", processType.getId().toString());
                        jsonObject.put("feesScheduleId", feesSchedule.getId());

                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(true);
                        responseObject.setMessage("Successful");
                        responseObject.setData(jsonObject.toJSONString());

                        return gson.toJson(responseObject);

                    }
                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return gson.toJson(responseObject);
    }

  
    public static String getFeeScheduleByCode(String data) {
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject responseObject = new SkoolaJSONResponseObject();
        String wsData = skoolaJSONRequestObject.getData();

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return new Gson().toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(wsData);

                String action = (String) requestObject.get("action");
                String code = (String) requestObject.get("code");

                if (action.equalsIgnoreCase("getFeeScheduleByCode")) {

                    Feeschedule feesSchedule = skoolaCustomService.getFeesScheduleByCode(code);

                    if (feesSchedule != null) {
                        ProcessType processType = (ProcessType) skoolaService.getRecordById(ProcessType.class, feesSchedule.getProcessType().getId());

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("feeInKobo", feesSchedule.getFeeInKobo().toString());
                        jsonObject.put("processType", processType.getName());
                        jsonObject.put("processTypeId", processType.getId().toString());
                        jsonObject.put("feesScheduleId", feesSchedule.getId().toString());

                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(true);
                        responseObject.setMessage("Successful");
                        responseObject.setData(jsonObject.toJSONString());

                        return gson.toJson(responseObject);

                    }
                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return gson.toJson(responseObject);
    }

    public static void main(String[] arg) {
        System.out.println(RandomStringUtils.randomAlphanumeric(11).toUpperCase());
    }

    public static String verifyPayStackPaymentStatus(String paymentReference) {
        String verificationResponse = "";
        try {
            String paystackVerificationURL = skoolaCustomService.getSettingValue("PAYSTACK_TRANSACTION_VERIFICATION_URL", "https://api.paystack.co/transaction/verify/", true);
            String payStackSecretKey = skoolaCustomService.getSettingValue("PAY_STACK_PAYMENT_API_SECRET_KEY", "Bearer sk_test_470832b29008cc54862a18aff3718b198061779a", true);

            paystackVerificationURL = paystackVerificationURL + paymentReference;

            URI uri = new URI(paystackVerificationURL);

            uri = URIUtils.rewriteURI(uri);
            System.out.println(" url beaing called  ========== " + uri);
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost(uri);

            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Authorization", payStackSecretKey);

            HttpResponse httpResponse = client.execute(httpPost);
            System.out.println("Response Code : " + httpResponse.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            verificationResponse = result.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return verificationResponse;
    }

    public static Boolean updateUserApplicationRequests(Long recordId, Long processTypeId) {
        try {

            ProcessType processTypeInHouseConsultation = skoolaCustomService.getProcessTypeByName(ProcessTypeConstant.BOOK_IN_HOUSE_CONSULTATION.getValue());
            ProcessType processTypeVirtualConsultation = skoolaCustomService.getProcessTypeByName(ProcessTypeConstant.BOOK_VIRTUAL_CONSULTATION.getValue());
            ProcessType processTypeSixMonthsExamCoaching = skoolaCustomService.getProcessTypeByName(ProcessTypeConstant.BOOK_EXAM_COACHING_6_MONTH.getValue());
            ProcessType processTypeOneMonthExamCoaching = skoolaCustomService.getProcessTypeByName(ProcessTypeConstant.BOOK_EXAM_COACHING_1_MONTH.getValue());
            ProcessType processTypeAccomodationServices = skoolaCustomService.getProcessTypeByName(ProcessTypeConstant.BOOK_ACCOMMODATION_SERVICES.getValue());

            ProcessType currentProcessType = (ProcessType) skoolaService.getRecordById(ProcessType.class, processTypeId);
            System.out.println(" current process type in update user application requests = " + currentProcessType.getName());
            if (currentProcessType.equals(processTypeInHouseConsultation)) {
                ConsultationRequest consultationRequest = (ConsultationRequest) skoolaService.getRecordById(ConsultationRequest.class, recordId);
                consultationRequest.setConsultationStatus(ConsultationStatusConstant.PAID);
                skoolaService.updateRecord(consultationRequest);

            } else if (currentProcessType.equals(processTypeVirtualConsultation)) {
                ConsultationRequest consultationRequest = (ConsultationRequest) skoolaService.getRecordById(ConsultationRequest.class, recordId);
                consultationRequest.setConsultationStatus(ConsultationStatusConstant.PAID);
                skoolaService.updateRecord(consultationRequest);

            } else if (currentProcessType.getName() == processTypeSixMonthsExamCoaching.getName()) {

            } else if (currentProcessType.getName() == processTypeOneMonthExamCoaching.getName()) {

            } else if (currentProcessType.getName() == processTypeAccomodationServices.getName()) {

            }

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

}

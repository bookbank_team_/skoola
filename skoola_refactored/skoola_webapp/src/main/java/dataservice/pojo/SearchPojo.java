package dataservice.pojo;

import java.util.List;

public class SearchPojo {
	
	private String courseId;
	private String courseName;
	private String courseLocationId;
	private String courseStudyTypeId;
	private String minPrice;
	private String maxPrice;
	private String startIndex;
	private String endIndex;
	private List<CoursePojo> searchResult;
	private String totalResultCount;
	
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseLocationId() {
		return courseLocationId;
	}
	public void setCourseLocationId(String courseLocationId) {
		this.courseLocationId = courseLocationId;
	}
	public String getCourseStudyTypeId() {
		return courseStudyTypeId;
	}
	public void setCourseStudyTypeId(String courseStudyTypeId) {
		this.courseStudyTypeId = courseStudyTypeId;
	}
	public String getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	public String getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}
	public String getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(String startIndex) {
		this.startIndex = startIndex;
	}
	public String getEndIndex() {
		return endIndex;
	}
	public void setEndIndex(String endIndex) {
		this.endIndex = endIndex;
	}
	public List<CoursePojo> getSearchResult() {
		return searchResult;
	}
	public void setSearchResult(List<CoursePojo> searchResult) {
		this.searchResult = searchResult;
	}
	public String getTotalResultCount() {
		return totalResultCount;
	}
	public void setTotalResultCount(String totalResultCount) {
		this.totalResultCount = totalResultCount;
	}
	
	
	
	

}

package dataservice.search;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import sun.misc.BASE64Encoder;

import com.dg.skoola.ServiceLocator;


import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.Course;
import com.dg.skoola.entity.CourseType;
import com.dg.skoola.entity.Image;
import com.dg.skoola.entity.ImageType;
import com.dg.skoola.entity.School;
import com.dg.skoola.entity.Subject;
import com.dg.skoola.entity.SubjectGroup;
import com.dg.skoola.enumeration.ImageTypeConstant;
import com.dg.skoola.service.SkoolaService;
import com.google.gson.Gson;
import common.SkoolaConstants;
import dataservice.common.CustomService;
import dataservice.common.SkoolaUtil;
import pojo.CoursePojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

//import com.liferay.portal.kernel.search.WildcardQuery;

public class SearchHandler {

    //private HashValidator hashValidator = HashValidator.getInstance();
    private static Logger log = Logger.getLogger(SearchHandler.class);
    private static  SkoolaService skoolaService = ServiceLocator.getInstance().getSkoolaService();
    private CustomService skoolaCustomService = CustomService.getInstance();
    private static BASE64Encoder encoder = new BASE64Encoder();
    private static Gson gson = new Gson();
    private static JSONParser jsonParser = new JSONParser();

 
    public static String test(String data) {
        System.out.println(" data = " + data);
        SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        System.out.println(" inside test  ");
        SkoolaJSONResponseObject SkoolaJSONResponseObject = new SkoolaJSONResponseObject();
        SkoolaJSONResponseObject.setData("Welcome to Skoola SearchHandler ");
        SkoolaJSONResponseObject.setSuccessful(true);

        try {
            skoolaService.getAllRecords(Country.class);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new Gson().toJson(SkoolaJSONResponseObject);
    }

    /*   
    
    @POST @Path("/getAllCountry")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllCountry(){
    	System.out.println(" Inside get all country records  ");
    	String result = "";
    	//List<Country> list = new ArrayList<Country>();
    	Collection<Country> allCountryCol = skoolaService.getAllRecords(Country.class);
    	//new State().get
    	if(allCountryCol != null && !allCountryCol.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(Country c : allCountryCol){
    			list.add(new Gson().toJson(c));
    		}
    		obj.put("country", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
    
    
    
    @POST @Path("/getAllCourse")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllCourse(){
    	System.out.println(" Inside get all course records  ");
    	String result = "";
    	//List<Country> list = new ArrayList<Country>();
    	Collection<Course> allCourseCol = skoolaService.getAllRecords(Course.class);
    	//new State().get
    	if(allCourseCol != null && !allCourseCol.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(Course c : allCourseCol){
    			list.add(new Gson().toJson(c));
    		}
    		obj.put("course", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
    
    @POST @Path("/getAllStudyType")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllStudyType(){
    	System.out.println(" Inside get all course records  ");
    	String result = "";
    	
    	
    	//List<Country> list = new ArrayList<Country>();
    	Collection<StudyType> allStudyTypeCol = skoolaService.getAllRecords(StudyType.class);
    	//new State().get
    	if(allStudyTypeCol != null && !allStudyTypeCol.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(StudyType c : allStudyTypeCol){
    			list.add(new Gson().toJson(c));
    		}
    		obj.put("studyType", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
     */

    public static String getCourseById(String data) {
        System.out.println(" Inside get  course by Id  ");
        String result = "";

        SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);

        System.out.println("" + " inside reindex lucene");
        SkoolaJSONResponseObject responseObject = null;
        String wsData = SkoolaJSONRequestObject.getData();
        String hash = SkoolaJSONRequestObject.getHash();
        String apiKey = SkoolaJSONRequestObject.getApi_key();

        try {
            if (wsData == null) {

                System.out.println(" hash validation failed");
                responseObject = new SkoolaJSONResponseObject();
                responseObject.setSuccessful(false);
                responseObject.setMessage("Hash Validation Failed");

                return new Gson().toJson(responseObject);
            } else {

                System.out.println(" hash validation passed");
                JSONParser parser = new JSONParser();
                JSONObject requestObject = (JSONObject) parser.parse(data);

                String action = "";
                String courseId = "";

                if (requestObject.containsKey("action") && requestObject.containsKey("courseId")) {
                    action = (String) requestObject.get("action");
                    courseId = (String) requestObject.get("courseId");
                    if (action.equalsIgnoreCase("getCourseById")) {

                        try {
                            Course course = (Course) skoolaService.getRecordById(Course.class, Long.valueOf(courseId));

                            if (course == null) {
                                responseObject = new SkoolaJSONResponseObject();
                                responseObject.setSuccessful(false);
                                responseObject.setMessage("No result found!");
                            }

                            School school = (School) skoolaService.getRecordById(School.class, course.getSchool().getId());
                            CourseType courseType = (CourseType) skoolaService.getRecordById(CourseType.class, course.getCourseType().getId());
                            Subject courseSubject = (Subject) skoolaService.getRecordById(Subject.class, course.getSubject().getId());
                            SubjectGroup courseSubjectGroup = null;
                            if (courseSubject != null && courseSubject.getSubjectGroup() != null) {
                                courseSubjectGroup = (SubjectGroup) skoolaService.getRecordById(SubjectGroup.class, courseSubject.getSubjectGroup().getId());
                            }

                            CoursePojo pojo = new CoursePojo();
                            pojo.setId(course.getId());
                            pojo.setCourseDuration(course.getDuration());
                            pojo.setCourseSummary(course.getCourseSummary());
                            pojo.setIeltsRequired(course.getIeltsRequired());
                            pojo.setCourseRequirements(course.getCourseRequirements());
                            pojo.setProgramModules(course.getProgramModules());
                            pojo.setCourseVenue(course.getCourseVenue());
                            pojo.setCourseFeeInCents(course.getFeeInCents().toString());
                            pojo.setCourseSubjectName(courseSubject == null ? " " : courseSubject.getName());
                            pojo.setCourseSubjectGroupName(courseSubjectGroup == null ? " " : courseSubjectGroup.getName());
                            pojo.setCourseType(courseType == null ? " " : courseType.getName());
                            pojo.setCourseSchoolName(school == null ? " " : school.getName());

                            responseObject = new SkoolaJSONResponseObject();
                            responseObject.setSuccessful(true);
                            responseObject.setData(new Gson().toJson(pojo));
                            return new Gson().toJson(responseObject);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            responseObject = new SkoolaJSONResponseObject();
                            responseObject.setMessage(e.getMessage());
                            responseObject.setSuccessful(false);
                            return new Gson().toJson(responseObject);

                        }

                    } else {
                        responseObject = new SkoolaJSONResponseObject();
                        responseObject.setSuccessful(false);
                        return new Gson().toJson(responseObject);
                    }

                }

            }
        } catch (Exception e) {
            responseObject = new SkoolaJSONResponseObject();
            responseObject.setSuccessful(false);
            responseObject.setMessage(e.getMessage());
            log.error(e);
            e.printStackTrace();
        }
        return new Gson().toJson(new SkoolaJSONResponseObject());
    }

    /*    
    @POST @Path("/get_all_skoola_courses")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllSkoolaCourses(String data){
    	System.out.println(" inside get all skoola courses >>>");
    	SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
    	SkoolaJSONResponseObject skoolaJSONResponseObject =  new SkoolaJSONResponseObject();
       
    	try {
    		
    		if(!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)){
    			skoolaJSONResponseObject.setMessage("Invalid Request!");
    			skoolaJSONResponseObject.setSuccessful(false);
    			return gson.toJson(skoolaJSONResponseObject);
    		}
    		
    		
    		Collection<Course> skoolaCourseCol = skoolaService.getAllRecords(Course.class, 0 , 500);
    		
    		JSONArray listArray = new JSONArray();
    		JSONObject jsonObject = new JSONObject();
    		if(skoolaCourseCol != null && !skoolaCourseCol.isEmpty()){
    			for(Course course : skoolaCourseCol){
    				
    				CourseType courseType = (CourseType) skoolaService.getRecordById(CourseType.class, course.getCourseType().getId());
    			
    				School school = (School) skoolaService.getRecordById(School.class, course.getSchool().getId());
    			
    				CourseCategory courseCategory = (CourseCategory) skoolaService.getRecordById(CourseCategory.class, course.getCourseCategory().getId());
    			
    				CoursePojo pojo = new CoursePojo();
    				pojo.setCourseCategory(courseCategory.getName().getValue());
    				pojo.setCourseSchool(school.getName());
    				pojo.setCourseType(courseType.getName().getValue());
    				pojo.setCourseDuration(course.getDuration());
    				pojo.setCourseFeeInCents(course.getFeeInCents().toString());
    				pojo.setCourseFeeInWords(course.getFeeInWords());
    				pojo.setName(course.getName());
    				pojo.setId(course.getId());
    				
    				listArray.add(gson.toJson(pojo));
    			}
    		}
    		
    		jsonObject.put("course_list", listArray);
    		skoolaJSONResponseObject.setMessage("");
    		skoolaJSONResponseObject.setData(jsonObject.toJSONString());
    		skoolaJSONResponseObject.setSuccessful(true);
    		System.out.println(" response = " + skoolaJSONResponseObject);
    		return new Gson().toJson(skoolaJSONResponseObject);
    		
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	
    	return new Gson().toJson(skoolaJSONResponseObject);
    }
    
     */

    public static String getAllCountry(String data) {
        System.out.println(" inside get all country >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<Country> countryCol = skoolaService.getAllRecords(Country.class);
            JSONArray jsonArray = new JSONArray();

            JSONArray listArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (countryCol != null && !countryCol.isEmpty()) {
                for (Country country : countryCol) {
                    JSONObject obj = new JSONObject();
                    obj.put("name", country.getName());
                    obj.put("id", country.getId().toString());
                    listArray.add(obj);
                }
            }

            jsonObject.put("country_list", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);
            System.out.println(" response = " + skoolaJSONResponseObject);
            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

  
    public static String getAllSubjectGroupStatistics(String data) {
        System.out.println(" inside get all course type >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<SubjectGroup> subjectGroupCol = skoolaService.getAllRecords(SubjectGroup.class);
            JSONArray jsonArray = new JSONArray();

            JSONArray listArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (subjectGroupCol != null && !subjectGroupCol.isEmpty()) {
                for (SubjectGroup subjectGroup : subjectGroupCol) {

                    try {
                        Long countOfCourses = CustomService.getCountOfCoursesBySubjectGroupId(subjectGroup.getId());
                        Long countOfUniversities = CustomService.getCountOfUniversitiesBySubjectGroupId(subjectGroup.getId());

                        System.out.println(" count of courses  = " + countOfCourses);
                        System.out.println(" count of countOfUniversities  = " + countOfUniversities);
                        System.out.println(" subject  = " + subjectGroup.getName());

                        JSONObject obj = new JSONObject();
                        obj.put("id", subjectGroup.getId().toString());
                        obj.put("type", "subjectGroup");
                        obj.put("numberOfUniversities", countOfUniversities.toString());
                        obj.put("numberOfCourses", countOfCourses.toString());
                        obj.put("name", subjectGroup.getName());
                        obj.put("displayName", subjectGroup.getName());

                        listArray.add(obj);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        continue;
                    }
                }
            }

            jsonObject.put("subject_group_statistics_list", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);
           // System.out.println(" response = " + skoolaJSONResponseObject.getData());
            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }


    public static String getAllCourseType(String data) {
        System.out.println(" inside get all course type >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<CourseType> courseTypeCol = skoolaService.getAllRecords(CourseType.class);
            JSONArray jsonArray = new JSONArray();

            JSONArray listArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (courseTypeCol != null && !courseTypeCol.isEmpty()) {
                for (CourseType courseType : courseTypeCol) {
                    JSONObject obj = new JSONObject();
                    obj.put("name", courseType.getName());
                    obj.put("id", courseType.getId().toString());
                    listArray.add(obj);
                }
            }

            jsonObject.put("course_type_list", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);

            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

 
    public static String getAllSkoolaCourseSubjectCategories(String data) {
        System.out.println(" inside get all skoola course categories >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<SubjectGroup> subjectGroupCol = skoolaService.getAllRecords(SubjectGroup.class);
            JSONArray jsonArray = new JSONArray();

            JSONArray listArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (subjectGroupCol != null && !subjectGroupCol.isEmpty()) {
                for (SubjectGroup subjectGroup : subjectGroupCol) {
                    JSONObject obj = new JSONObject();
                    obj.put("name", subjectGroup.getName());
                    obj.put("id", subjectGroup.getId().toString());
                    listArray.add(obj);
                }
            }

            jsonObject.put("course_subject_group_list", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);

            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

 
    public static String getAllSkoolaSchools(String data) {
        System.out.println(" get_paginated_skoola_schools >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();
        String maximumResultSetSize = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            JSONObject jsonObject = null;
            String searchIndex = "";
            Boolean isPaginated = false;

            if (skoolaJSONRequestObject.getData() != null && !skoolaJSONRequestObject.getData().isEmpty()) {
                jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());
                searchIndex = (String) jsonObject.get("searchIndex");
                isPaginated = (Boolean) jsonObject.get("isPaginated");
            }

            System.out.println("1");
            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }

            Collection<School> schoolCol = new ArrayList<School>();

            if (isPaginated) {
                schoolCol = skoolaService.getAllRecords(School.class, Long.valueOf(searchIndex).intValue(), Long.valueOf(maximumResultSetSize).intValue());
            } else {
                schoolCol = skoolaService.getAllRecords(School.class);

            }

            System.out.println("2");

            JSONArray listArray = new JSONArray();
            jsonObject = new JSONObject();
            if (schoolCol != null && !schoolCol.isEmpty()) {
                for (School school : schoolCol) {

                    Country country = null;

                    if (school.getCountry() != null) {
                        country = (Country) skoolaService.getRecordById(Country.class, school.getCountry().getId());
                    }

                    JSONObject obj = new JSONObject();
                    obj.put("name", school.getName() == null ? "" : school.getName());
                    obj.put("id", school.getId().toString());
                    obj.put("address", school.getAddress() == null ? "" : school.getAddress());
                    obj.put("yearEstablished", school.getYearEstablished() == null ? "" : school.getYearEstablished());
                    obj.put("logoUrl", school.getLogoUrl() == null ? "" : school.getLogoUrl());
                    obj.put("country", country == null ? "" : country.getName());

                    listArray.add(obj);
                }
            }
            System.out.println(" listArray = " + listArray);
            jsonObject.put("school_search_result", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);

            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }


    public static String getAllCourseSubjectType(String data) {
        System.out.println(" inside get all course subject type >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<Subject> subjectCol = skoolaService.getAllRecords(Subject.class);
            JSONArray jsonArray = new JSONArray();

            JSONArray listArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (subjectCol != null && !subjectCol.isEmpty()) {
                for (Subject subject : subjectCol) {
                    JSONObject obj = new JSONObject();
                    obj.put("name", subject.getName());
                    obj.put("id", subject.getId().toString());
                    obj.put("type", "subject");
                    listArray.add(obj);
                }
            }

            jsonObject.put("course_subject_type_list", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);

            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

 
    public static String getAllCourseSubjectGroupType(String data) {
        System.out.println(" inside get all course subject group type >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            Collection<SubjectGroup> subjectGroupCol = skoolaService.getAllRecords(SubjectGroup.class);
            JSONArray jsonArray = new JSONArray();

            JSONArray listArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            if (subjectGroupCol != null && !subjectGroupCol.isEmpty()) {
                for (SubjectGroup subjectGroup : subjectGroupCol) {
                    JSONObject obj = new JSONObject();
                    obj.put("name", subjectGroup.getName());
                    obj.put("id", subjectGroup.getId().toString());
                    obj.put("type", "subjectGroup");
                    listArray.add(obj);
                }
            }

            jsonObject.put("course_subject_group_type_list", listArray);
            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(jsonObject.toJSONString());
            skoolaJSONResponseObject.setSuccessful(true);

            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

  
    public static String getCoursesBySearchParameters(String data) {
        System.out.println(" inside get_courses_by_parameters >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();

        String dollarExchangeRate = CustomService.getSettingValue("DOLLAR_EXCHANGE_RAGE", "325", true);

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            String subjectId = (String) jsonObject.get("subjectId");
            String type = (String) jsonObject.get("type");
            String countryId = (String) jsonObject.get("countryId");
            String courseTypeId = (String) jsonObject.get("courseTypeId");
            String minimumTuition = (String) jsonObject.get("minimumTuition");
            String maximumTuition = (String) jsonObject.get("maximumTuition");
            String searchIndex = (String) jsonObject.get("searchIndex");

            System.out.println(" minimumTuition" + minimumTuition);
            System.out.println(" maximumTuition =" + maximumTuition);

            if ((subjectId == null || subjectId.isEmpty()) && (countryId == null || countryId.isEmpty())
                    && (courseTypeId == null || courseTypeId.isEmpty())
                    && (minimumTuition == null || minimumTuition.isEmpty())
                    && (maximumTuition == null || maximumTuition.isEmpty())
                    && (searchIndex == null || searchIndex.isEmpty())
                    && (type == null || type.isEmpty())) {

                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            if (searchIndex == null || searchIndex.isEmpty()) {
                searchIndex = "0";
            }

            Collection<Course> queryResult = CustomService.getCoursesBySearchParameter(subjectId, maximumTuition, minimumTuition, courseTypeId, countryId, searchIndex, subjectId, type);

            Long resultCount = CustomService.getCountOfCoursesBySearchParameters(subjectId, maximumTuition, minimumTuition, courseTypeId, countryId, searchIndex, subjectId, type);

            System.out.println(" queryResultCount = " + queryResult == null ? "0" : queryResult.size());
            System.out.println(" count = " + resultCount);
            if (resultCount == null) {
                resultCount = 0l;
            }

            if (queryResult == null || queryResult.isEmpty()) {
                skoolaJSONResponseObject.setMessage("Sorry! we could not find courses that match your search parameters. "
                        + " Please refine you search and try again.");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            JSONObject jsonResultObject = new JSONObject();
            JSONArray jArray = new JSONArray();

            for (Course course : queryResult) {

                if (course.getSubject() == null) {
                    continue;
                }

                if (course.getSchool() == null) {
                    continue;
                }

                if (course.getCourseType() == null) {
                    continue;
                }

                Subject courseSubject = (Subject) skoolaService.getRecordById(Subject.class, course.getSubject().getId());
                School school = (School) skoolaService.getRecordById(School.class, course.getSchool().getId());
                CourseType courseType = (CourseType) skoolaService.getRecordById(CourseType.class, course.getCourseType().getId());

                JSONObject jObj = new JSONObject();

                jObj.put("courseSubject", courseSubject.getName());
                jObj.put("courseSubjectId", courseSubject.getId().toString());

                jObj.put("schoolName", school.getName());
                jObj.put("schoolId", school.getId().toString());
                jObj.put("schoolLogoURL", school.getLogoUrl() == null ? "" : school.getLogoUrl());

                jObj.put("courseTypeName", courseType.getName());
                jObj.put("courseTypeId", courseType.getId().toString());

                jObj.put("courseName", course.getName());
                jObj.put("courseId", course.getId().toString());

                jObj.put("courseDuration", course.getDuration());
                jObj.put("courseFeeInWords", course.getFeeInWords());
                jObj.put("courseFeeInDollars", String.valueOf(course.getFeeInCents() / 100));

                String formattedCourseFee = SkoolaUtil.convertFromCentsToNaira(course.getFeeInCents(), dollarExchangeRate);

                jObj.put("courseFeeInNaira", formattedCourseFee);

                jArray.add(jObj);

            }
            String searchParameterSubjectName = "";
            String searchParameterCountryName = "";
            String searchParameterCourseTypeName = "";

            if (subjectId != null && !subjectId.isEmpty()) {
                if (type != null && !type.isEmpty()) {
                    if (type.equalsIgnoreCase("Subject")) {
                        Subject subject = (Subject) skoolaService.getRecordById(Subject.class, Long.valueOf(subjectId));
                        searchParameterSubjectName = subject.getName();
                    } else if (type.equalsIgnoreCase("SubjectGroup")) {
                        SubjectGroup subjectGroup = (SubjectGroup) skoolaService.getRecordById(SubjectGroup.class, Long.valueOf(subjectId));
                        searchParameterSubjectName = subjectGroup.getName();
                    }
                }

            }

            if (courseTypeId != null && !courseTypeId.isEmpty()) {
                CourseType courseType = (CourseType) skoolaService.getRecordById(CourseType.class, Long.valueOf(courseTypeId));
                searchParameterCourseTypeName = courseType.getName();
            }

            if (countryId != null && !countryId.isEmpty()) {
                Country country = (Country) skoolaService.getRecordById(Country.class, Long.valueOf(countryId));
                searchParameterCountryName = country.getName();
            }

            jsonResultObject.put("searchParameterSubjectName", searchParameterSubjectName);
            jsonResultObject.put("searchParameterCountryName", searchParameterCountryName);
            jsonResultObject.put("searchParameterCourseTypeName", searchParameterCourseTypeName);

            jsonResultObject.put("search_result", jArray);
            jsonResultObject.put("courseId", subjectId);
            jsonResultObject.put("countryId", countryId);
            jsonResultObject.put("courseTypeId", courseTypeId);
            jsonResultObject.put("subjectId", subjectId);
            jsonResultObject.put("type", type);
            jsonResultObject.put("courseTypeId", courseTypeId);
            jsonResultObject.put("minimumTuition", minimumTuition);
            jsonResultObject.put("maximumTuition", maximumTuition);
            jsonResultObject.put("searchIndex", searchIndex);
            jsonResultObject.put("search_result_count", resultCount.toString());

            skoolaJSONResponseObject.setMessage("Search successful!");
            skoolaJSONResponseObject.setSuccessful(true);
            skoolaJSONResponseObject.setData(jsonResultObject.toJSONString());
            return gson.toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

  
    public static String getCourseDetail(String data) {
        System.out.println(" inside get course detail >>>");
        SkoolaJSONRequestObject skoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
        SkoolaJSONResponseObject skoolaJSONResponseObject = new SkoolaJSONResponseObject();
        String dollarExchangeRate = CustomService.getSettingValue("DOLLAR_EXCHANGE_RAGE", "325", true);

        try {

            if (!skoolaJSONRequestObject.getApi_key().equalsIgnoreCase(SkoolaConstants.API_KEY)) {
                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                return gson.toJson(skoolaJSONResponseObject);
            }

            JSONObject jsonObject = (JSONObject) jsonParser.parse(skoolaJSONRequestObject.getData());

            String courseId = (String) jsonObject.get("courseId");

            if ((courseId == null || courseId.isEmpty())) {

                skoolaJSONResponseObject.setMessage("Invalid Request!");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            Course course = (Course) skoolaService.getRecordById(Course.class, Long.valueOf(courseId));

            if (course == null) {
                skoolaJSONResponseObject.setMessage("Sorry! your request cannot be processed at the moment, kindly contact support");
                skoolaJSONResponseObject.setSuccessful(false);
                skoolaJSONResponseObject.setData(skoolaJSONRequestObject.getData());
                return gson.toJson(skoolaJSONResponseObject);
            }

            Subject courseSubject = null;
            SubjectGroup courseSubjectGroup = null;
            School school = null;
            CourseType courseType = null;

            if (course.getSubject() != null) {
                courseSubject = (Subject) skoolaService.getRecordById(Subject.class, course.getSubject().getId());

                if (courseSubject != null && courseSubject.getSubjectGroup() != null) {
                    courseSubjectGroup = (SubjectGroup) skoolaService.getRecordById(SubjectGroup.class, courseSubject.getSubjectGroup().getId());
                }
            }

            if (course.getSchool() != null) {
                school = (School) skoolaService.getRecordById(School.class, course.getSchool().getId());
            }

            if (course.getCourseType() != null) {
                courseType = (CourseType) skoolaService.getRecordById(CourseType.class, course.getCourseType().getId());

            }

            Long course_id = course == null ? 0l : course.getId();
            String course_name = course.getName() == null ? "" : course.getName();
            String course_type = courseType == null ? "" : courseType.getName();
            String course_category = courseSubjectGroup == null ? "" : courseSubjectGroup.getName();
            String course_category_name = courseSubjectGroup == null ? "" : courseSubjectGroup.getName();
            String course_subject = courseSubject == null ? "" : courseSubject.getName();
            String course_duration = course.getDuration() == null ? "" : course.getDuration();
            String course_fee_in_words = course.getFeeInWords() == null ? "" : course.getFeeInWords();
            String course_fee_in_cents = course.getFeeInCents() == null ? "" : course.getFeeInCents().toString();
            String course_venue = course.getCourseVenue() == null ? "" : course.getCourseVenue();
            String course_requirements = course.getCourseRequirements() == null ? "" : course.getCourseRequirements();
            Boolean course_is_ielts_required = course.getIeltsRequired() == null ? false : course.getIeltsRequired();
            String course_module = course.getProgramModules() == null ? "" : course.getProgramModules();
            String course_summary = course.getCourseSummary() == null ? "" : course.getCourseSummary();
            String course_school = school == null ? "" : school.getName();
            String course_type_id = courseType == null ? "" : courseType.getId().toString();
            String course_category_id = courseSubjectGroup == null ? "" : courseSubjectGroup.getId().toString();
            String course_school_id = school == null ? "" : school.getId().toString();
            String course_subject_id = courseSubject == null ? "" : courseSubject.getId().toString();

            CoursePojo pojo = new CoursePojo();
            pojo.setId(course_id);
            pojo.setCourseDuration(course_duration);
            pojo.setCourseFeeInCents(course_fee_in_cents);
            pojo.setCourseFeeInNaira(SkoolaUtil.convertFromCentsToNaira(course.getFeeInCents(), dollarExchangeRate));
            pojo.setCourseFeeInWords(course_fee_in_words);
            pojo.setCourseRequirements(course_requirements);
            pojo.setCourseSchoolName(course_school);
            pojo.setCourseSubjectName(course_subject);
            pojo.setCourseSubjectGroupName(course_category);
            pojo.setCourseSummary(course_summary);
            pojo.setCourseType(course_type);
            pojo.setCourseVenue(course_venue);
            pojo.setIeltsRequired(course_is_ielts_required);
            pojo.setName(course_name);
            pojo.setProgramModules(course_module);
            pojo.setCourseTypeId(course_type_id);
            pojo.setCourseSubjectId(course_subject_id);
            pojo.setCourseSchoolId(course_school_id);
            pojo.setCourseSubjectGroupName(course_category_name);
            pojo.setCourseSubjectGroupId(course_category_id);

            skoolaJSONResponseObject.setMessage("");
            skoolaJSONResponseObject.setData(new Gson().toJson(pojo));
            skoolaJSONResponseObject.setSuccessful(true);
            return new Gson().toJson(skoolaJSONResponseObject);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new Gson().toJson(skoolaJSONResponseObject);
    }

    /* @POST @Path("/searchCourses")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getSearchCourses(String data){
    	System.out.println(" Inside search courses.  ");
    	String result = "";
    	
    	SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(data, SkoolaJSONRequestObject.class);
    	
    	System.out.println("" + " inside reindex lucene");
    	 SkoolaJSONResponseObject responseObject = null;
    	 String wsData =  SkoolaJSONRequestObject.getData();
    	 String hash =  SkoolaJSONRequestObject.getHash();
    	 String apiKey =  SkoolaJSONRequestObject.getApi_key();
    	 
    	 
    	 try {
             if (wsData == null) {
            	 
            	 System.out.println(" hash validation failed");
                 responseObject = new SkoolaJSONResponseObject();
                 responseObject.setSuccessful(false);
                 responseObject.setMessage( "Hash Validation Failed");

                 return new Gson().toJson(responseObject);
             } else {

            	 System.out.println(" hash validation passed");

                 SearchPojo searchPojo = new Gson().fromJson(wsData, SearchPojo.class);

                 if (searchPojo != null){
                	 
                	String courseId = searchPojo.getCourseId() == null ? "" : searchPojo.getCourseId();            		
            		String courseLocationId = searchPojo.getCourseLocationId()  == null ? "" : searchPojo.getCourseLocationId();
            		String courseStudyTypeId = searchPojo.getCourseStudyTypeId()  == null ? "" : searchPojo.getCourseStudyTypeId();
            		String minPrice = searchPojo.getMinPrice()  == null ? "" : searchPojo.getMinPrice();
            		String maxPrice = searchPojo.getMaxPrice()  == null ? "" : searchPojo.getMaxPrice();
            		String startIndex = searchPojo.getStartIndex()  == null ? "" : searchPojo.getStartIndex();
            		String endIndex = searchPojo.getEndIndex()  == null ? "" : searchPojo.getEndIndex();
            		
            		Collection<Course> searchResult = new ArrayList<Course>();
            		
            		Long courseIdLong = Long.valueOf(courseId);
            		Long courseLocationIdLong = Long.valueOf(courseLocationId);
            		Long courseStudyTypeIdLong = Long.valueOf(courseStudyTypeId);
            		Long minPriceLong = Long.valueOf(minPrice);
            		Long maxPriceLong = Long.valueOf(maxPrice);
            		Long startIndexLong = Long.valueOf(startIndex);
            		Long endIndexLong = Long.valueOf(endIndex);
            		
            		if(!courseId.isEmpty() && !courseLocationId.isEmpty() && !courseStudyTypeId.isEmpty() && !minPrice.isEmpty() && !maxPrice.isEmpty()){
            			
            			searchResult = skoolaCustomService.getCoursesByCourseIdLocationIdStudyTypeIdMaxPriceMinPrice
            					(courseIdLong,courseLocationIdLong,courseStudyTypeIdLong,minPriceLong,maxPriceLong,startIndexLong,endIndexLong);
            			
            		}else if(!courseId.isEmpty() && !courseLocationId.isEmpty() && courseStudyTypeId.isEmpty() && minPrice.isEmpty() && maxPrice.isEmpty()){
            		

            			searchResult = skoolaCustomService.getCoursesByCourseIdLocationId
            					(courseIdLong,courseLocationIdLong,startIndexLong,endIndexLong);
            			
            			
            		}else if(!courseId.isEmpty() && courseLocationId.isEmpty() && !courseStudyTypeId.isEmpty() && minPrice.isEmpty() && maxPrice.isEmpty()){
            		
            			searchResult = skoolaCustomService.getCoursesByCourseIdStudyTypeId
            					(courseIdLong,courseStudyTypeIdLong,startIndexLong,endIndexLong);
            			
            			
            		}else if(!courseId.isEmpty() && !courseLocationId.isEmpty() && !courseStudyTypeId.isEmpty() && minPrice.isEmpty() && maxPrice.isEmpty()){
            		
            			searchResult = skoolaCustomService.getCoursesByCourseIdLocationIdStudyTypeId
            					(courseIdLong,courseLocationIdLong,courseStudyTypeIdLong,startIndexLong,endIndexLong);
            			
            		}

            		
                     if(searchResult != null && !searchResult.isEmpty()){

                    	 for(Course course : searchResult){
	                    	 try {
								Course cus = (Course) skoolaService.getRecordById(Course.class, Long.valueOf(courseId));
								 
								 if(cus == null){
									 responseObject = new SkoolaJSONResponseObject();
								     responseObject.setSuccessful(false);
								     responseObject.setMessage( "No result found!");
								 }
								 
								 StudyType studyType = (StudyType) skoolaService.getRecordById(StudyType.class, Long.valueOf(cus.getStudyType().getId()));
								 School school = (School) skoolaService.getRecordById(School.class, cus.getSchool().getId());
								 SchoolType schoolType = (SchoolType) skoolaService.getRecordById(SchoolType.class, school.getId());
								 
								 
								 ImageType imageType = skoolaCustomService.getImageTypeByName(ImageTypeConstant.SCHOOL_LOGO_IMAGE.getValue());
								 Image schoolLogoImage = skoolaCustomService.getImageBySchoolIdAndImageTypeId(school.getId(), imageType.getId());
								
								 
								 CoursePojo pojo = new CoursePojo();
								 pojo.setId(cus.getId());
								 pojo.setName(cus.getName());
								// pojo.setDurationInMonths(cus.getDurationInMonths().toString());
								// pojo.setSchoolName(school.getName());
								// pojo.setSchoolLogo(schoolLogoImage.getImageId().toString());
								// pojo.setStudyType(studyType.getDisplayName());
								// pojo.setSchoolType(schoolType.getDisplayName());
								// pojo.setAward(cus.getAward().getValue());
			 
								 responseObject = new SkoolaJSONResponseObject();
								 responseObject.setSuccessful(true);
								 responseObject.setData(new Gson().toJson(pojo));
								 return new Gson().toJson(responseObject);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								 responseObject = new SkoolaJSONResponseObject();
								 responseObject.setMessage(e.getMessage());
		                         responseObject.setSuccessful(false);
		                         return new Gson().toJson(responseObject);
								
							}
                    	 
                    	 }
                     }else{
                    	 responseObject = new SkoolaJSONResponseObject();
                         responseObject.setSuccessful(false);
                         return new Gson().toJson(responseObject);
                     }
                 	
                 }

             }
         } catch (Exception e) {
             responseObject = new SkoolaJSONResponseObject();
             responseObject.setSuccessful(false);
             responseObject.setMessage( e.getMessage());
             log.error(e);
             e.printStackTrace();
         }
         return new Gson().toJson(new SkoolaJSONResponseObject());
    }
     */
 /*
    @POST @Path("/reIndexLucene")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String reIndexLucene(String wsData) {

    	System.out.println(" data = " + wsData);
    	SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(wsData, SkoolaJSONRequestObject.class);
    	
    	System.out.println("" + " inside reindex lucene");
    	 SkoolaJSONResponseObject responseObject = null;
    	 String data =  SkoolaJSONRequestObject.getData();
    	 String hash =  SkoolaJSONRequestObject.getHash();
    	 String apiKey =  SkoolaJSONRequestObject.getApi_key();

         try {
             if (!hashValidator.isValidRequestData(data, hash, apiKey)) {
            	 
            	 System.out.println(" hash validation failed");
                 responseObject = new SkoolaJSONResponseObject();
                 responseObject.setSuccessful(false);
                 responseObject.setMessage( "Hash Validation Failed");

                 return new Gson().toJson(responseObject);
             } else {

            	 System.out.println(" hash validation passed");
                 JSONParser parser = new JSONParser();
                 JSONObject requestObject = (JSONObject) parser.parse(data);

                 String action = "";

                 if (requestObject.containsKey("action")){
                     action = (String) requestObject.get("action");
                     if(action.equalsIgnoreCase("startIndexing")){
                    	 indexLucene();
                    	 
                    	 responseObject = new SkoolaJSONResponseObject();
                         responseObject.setSuccessful(true);
                         return new Gson().toJson(responseObject);
                    	 
                    	 
                     }else{
                    	 responseObject = new SkoolaJSONResponseObject();
                         responseObject.setSuccessful(false);
                         return new Gson().toJson(responseObject);
                     }
                 	
                 }

             }
         } catch (Exception e) {
             responseObject = new SkoolaJSONResponseObject();
             responseObject.setSuccessful(false);
             responseObject.setMessage( e.getMessage());
             log.error(e);
             e.printStackTrace();
         }
         return new Gson().toJson(new SkoolaJSONResponseObject());
    }
    
   
    
    @POST @Path("/getAllMedicalSpeciality")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllMedicalRecords(){
    	System.out.println(" Inside get all medical speciality ");
    	String result = "";
    	
    	Collection<MedicalSpeciality> allMedicalSpeciality = skoolaService.getAllRecords(MedicalSpeciality.class);
    	
    	if(allMedicalSpeciality != null && !allMedicalSpeciality.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(MedicalSpeciality m : allMedicalSpeciality){
    			list.add(new Gson().toJson(m));
    		}
    		obj.put("medicalspeciality", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
    
    
    
    @POST @Path("/getAllState")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllStateRecords(){
    	System.out.println(" Inside get all state records  ");
    	String result = "";
    	
    	Collection<State> allStateCol = skoolaService.getAllRecords(State.class);
    	//new State().get
    	if(allStateCol != null && !allStateCol.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(State s : allStateCol){
    			list.add(new Gson().toJson(s));
    		}
    		obj.put("state", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
    
    
    @POST @Path("/getAllCity")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllCityRecords(){
    	System.out.println(" Inside get all city records  ");
    	String result = "";
    	
    	Collection<City> allCityCol = skoolaService.getAllRecords(City.class);
    	
    	if(allCityCol != null && !allCityCol.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(City c : allCityCol){
    			list.add(new Gson().toJson(c));
    		}
    		obj.put("city", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
    
    
    @POST @Path("/getAllLga")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String getAllLgaRecords(){
    	System.out.println(" Inside get all lga records  ");
    	String result = "";
    	
    	Collection<Lga> allLgaCol = skoolaService.getAllRecords(Lga.class);
    	
    	if(allLgaCol != null && !allLgaCol.isEmpty()){
    		
    		JSONObject obj = new JSONObject();
    		
    		JSONArray list = new JSONArray();
    		for(Lga l : allLgaCol){
    			list.add(new Gson().toJson(l));
    		}
    		obj.put("lga", list);
    		
    		
    		return obj.toJSONString();
    	}
    	
    	return result;
    }
    
    
    
    
    
    
    
    
    
    @POST @Path("/searchLuceneByFieldParam")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    public String searchLuceneByFieldParameter( String wsData){
    	

    	System.out.println(" inside search lucene by field param ");
    	System.out.println(" data = " + wsData);
    	SkoolaJSONRequestObject SkoolaJSONRequestObject = new Gson().fromJson(wsData, SkoolaJSONRequestObject.class);
    	
    	System.out.println("" + " inside reindex lucene");
    	 SkoolaJSONResponseObject responseObject = null;
    	 String data =  SkoolaJSONRequestObject.getData();
    	 String hash =  SkoolaJSONRequestObject.getHash();
    	 String apiKey =  SkoolaJSONRequestObject.getApi_key();

         try {
             if (!hashValidator.isValidRequestData(data, hash, apiKey)) {
            	 
            	 System.out.println(" hash validation failed");
                 responseObject = new SkoolaJSONResponseObject();
                 responseObject.setSuccessful(false);
                 responseObject.setMessage( "Hash Validation Failed");

                 return new Gson().toJson(responseObject);
             } else {

            	 System.out.println(" hash validation passed");
                 JSONParser parser = new JSONParser();
                 JSONObject requestObject = (JSONObject) parser.parse(data);

                 String action = requestObject.containsKey("action") ? (String) requestObject.get("action") : "";
                 String field = requestObject.containsKey("field") ? (String) requestObject.get("field") : "";
                 String seachParameter = requestObject.containsKey("seachParameter") ? (String) requestObject.get("seachParameter") : "";
                 String index = requestObject.containsKey("index") ? (String) requestObject.get("index") : "";

                     if(action.equalsIgnoreCase("search_lucene")){
                    	 
                    	 if(field.equalsIgnoreCase(LUCENE_FIELD_MEDICAL_SPECIALITY_SPECIALITY)){
                    		 System.out.println(" search field is speciality");
                    		 if(index.isEmpty()){
                    			 Collection<SearchResultPojo> luceneSearchResult = searchIndexByMedicalSpecialityField(seachParameter);
                    			 
                    			 String jsonpackage = transformSearchResultPojoToJSONPackage(luceneSearchResult);

                    			 responseObject = new SkoolaJSONResponseObject();
                    			 responseObject.setData(jsonpackage);
                    			 responseObject.setMessage("query result " + luceneSearchResult.size());
                    			 responseObject.setSuccessful(true);
                    			 
                    			 return new Gson().toJson(responseObject);
 
                    		 }else{
                    			 
                    		 }
                    		 
                    	 }else if(field.equalsIgnoreCase(LUCENE_FIELD_LOCATION)){
                    		 System.out.println(" search field is location");
                    		 if(index.isEmpty()){
                    			 Collection<SearchResultPojo> luceneSearchResult = searchIndexByLocationField(seachParameter);
                    			 
                    			 String jsonpackage = transformSearchResultPojoToJSONPackage(luceneSearchResult);

                    			 responseObject = new SkoolaJSONResponseObject();
                    			 responseObject.setData(jsonpackage);
                    			 responseObject.setMessage("query result " + luceneSearchResult.size());
                    			 responseObject.setSuccessful(true);
                    			 
                    			 return new Gson().toJson(responseObject);
 
                    		 }else{
                    			 
                    		 }
                    		 
                    	 }
                    	 
                    	
                    	 
                     }else{
                    	 responseObject = new SkoolaJSONResponseObject();
                         responseObject.setSuccessful(false);
                         return new Gson().toJson(responseObject);
                     }
                 	
              

             }
         } catch (Exception e) {
             responseObject = new SkoolaJSONResponseObject();
             responseObject.setSuccessful(false);
             responseObject.setMessage( e.getMessage());
             log.error(e);
             e.printStackTrace();
         }
         return new Gson().toJson(new SkoolaJSONResponseObject());

    }
     */
}

/**
 * Copyright (C) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package conf;

import ninja.AssetsController;
import ninja.Results;
import ninja.Router;
import ninja.application.ApplicationRoutes;
import ninja.utils.NinjaConstant;
import ninja.utils.NinjaProperties;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.inject.Inject;

import common.SkoolaConstants;
import common.WSUtil;
import controllers.ApplicationController;
import controllers.SkoolaHomeController;
import controllers.account.AccountController;
import controllers.admin.AdminController;
import controllers.careerconsultation.CareerConsultationController;
import controllers.payments.PaymentController;
import controllers.search.SearchController;
import controllers.signin.SigninController;
import controllers.signup.SignUpController;

public class Routes implements ApplicationRoutes {

    private Logger log = Logger.getLogger(Routes.class);

    @Inject
    NinjaProperties ninjaProperties;

    @Override
    public void init(Router router) {
        setApplicationMode();

        router.GET().route("/test").with(ApplicationController.class, "testSkoolaService");
        router.GET().route("/").with(SkoolaHomeController.class, "index");

        router.GET().route("/search").with(SkoolaHomeController.class, "search");
        router.GET().route("/viewCoursesUnderACategory").with(SkoolaHomeController.class, "viewCoursesUnderACategory");

        router.GET().route("/viewCountryDetail").with(SkoolaHomeController.class, "viewCountryDetail");
        router.GET().route("/moreDestinations").with(SkoolaHomeController.class, "moreDestinations");

        router.GET().route("/viewExamDetail").with(SkoolaHomeController.class, "viewExamDetail");
        router.GET().route("/viewMoreExams").with(SkoolaHomeController.class, "viewMoreExams");

        router.GET().route("/login").with(SkoolaHomeController.class, "login");
        router.GET().route("/signup").with(SkoolaHomeController.class, "signup");

        router.GET().route("/universtiyRanking").with(SkoolaHomeController.class, "universtiyRanking");
        router.GET().route("/studyAbroadGuides").with(SkoolaHomeController.class, "studyAbroadGuides");
        router.GET().route("/CountryInfo").with(SkoolaHomeController.class, "CountryInfo");
        router.GET().route("/ApplicationHelp").with(SkoolaHomeController.class, "ApplicationHelp");

        // router.GET().route("/getAllSkoolaCourses").with(SearchController.class, "getAllSkoolaCourses");
        router.GET().route("/getAllSubjectGroupType").with(SearchController.class, "getAllSubjectGroupType");
        router.GET().route("/getAllSubjectType").with(SearchController.class, "getAllSubjectType");
        router.GET().route("/complete_registration_request").with(SignUpController.class, "completeRegistrationRequest");
        router.POST().route("/create_signup_request").with(SignUpController.class, "createSignUpRequest");
        router.POST().route("/signin").with(SigninController.class, "userSignin");
        router.GET().route("/signout").with(SigninController.class, "userSignout");
        router.GET().route("/user_profile_page").with(AccountController.class, "handleShowCurrentUserProfile");
        router.GET().route("/skoola_about_us").with(SkoolaHomeController.class, "handleShowAboutUsPage");
        router.GET().route("/skoola_accommdation_info_page").with(SkoolaHomeController.class, "handleShowAccommodationInfoPage");
        router.GET().route("/skoola_exam_coaching_info").with(SkoolaHomeController.class, "handleShowExamInfoPage");

        // router.POST().route("/search_courses_by_parameters").with(SearchController.class, "getCoursesBySearchParameters"); 
        router.GET().route("/search_courses_by_parameters").with(SearchController.class, "getCoursesBySearchParameters");
        router.GET().route("/show_course_detail").with(SearchController.class, "showCourseDetail");
        router.GET().route("/go_back_to_course_search_result").with(SearchController.class, "goBackToCourseSearchResult");
        router.GET().route("/go_to_carreer_consultation").with(CareerConsultationController.class, "handleGoToCarreerConsultation");
        router.POST().route("/go_to_carreer_consultation").with(CareerConsultationController.class, "handleGoToCarreerConsultation");
        router.GET().route("/go_back_to_course_detail").with(SearchController.class, "handleGoBackToCourseDetail");
        router.POST().route("/pay_for_career_consultation").with(CareerConsultationController.class, "handleCreateCarreerConsultation");
        router.GET().route("/create_payment_history").with(PaymentController.class, "intializePaymentForCareerConsultation");
        router.GET().route("/payment_callback").with(PaymentController.class, "paymentCallBack");
        router.GET().route("/appy_for_course").with(SearchController.class, "applyForACourse");

        // User Account Actions
        router.GET().route("/my_career_consultation_requests").with(AccountController.class, "handleShowMyCareerConsultationRequests");
        router.GET().route("/my_payment_history").with(AccountController.class, "handleShowMyPaymentHistory");
        router.GET().route("/my_messages").with(AccountController.class, "handleShowMyMessages");

        // Admin Area Actions
        router.GET().route("/admin_dashboard").with(AdminController.class, "handleShowAdminDashBoard");
        router.GET().route("/admin_application_users").with(AdminController.class, "handleShowApplicationUsers");
        router.GET().route("/admin_backend_users").with(AdminController.class, "handleShowApplicationUsers");
        router.GET().route("/admin_consultation_applications").with(AdminController.class, "handleShowConsultationApplication");
        router.GET().route("/admin_skoola_courses").with(AdminController.class, "handleShowSkoolaCourses");
        router.POST().route("/admin_skoola_courses").with(AdminController.class, "handleShowSkoolaCourses");
        router.GET().route("/admin_skoola_exams").with(AdminController.class, "handleShowSkoolaExams");
        router.GET().route("/admin_skoola_universities").with(AdminController.class, "handleShowSkoolaUniversities");
        router.GET().route("/admin_skoola_countries").with(AdminController.class, "handleShowSkoolaCountries");
        router.GET().route("/admin_edit_skoola_course").with(AdminController.class, "handleShowEditCourse");
        router.POST().route("/admin_create_or_update_skoola_course").with(AdminController.class, "handleCreateORUpdateSkoolaCourse");
        router.GET().route("/get_skoola_course_subjects_by_subject_category").with(AdminController.class, "handleGetSkoolaCourseSubjetBySubjectCategoryId");
        router.GET().route("/admin_skoola_course_subjects").with(AdminController.class, "handleShowSkoolaCourseSubjects");
        router.POST().route("/admin_skoola_course_subjects").with(AdminController.class, "handleShowSkoolaCourseSubjects");
        router.GET().route("/admin_skoola_course_subject_category").with(AdminController.class, "handleShowSkoolaCourseSubjectCategory");
        router.POST().route("/admin_skoola_course_subject_category").with(AdminController.class, "handleShowSkoolaCourseSubjectCategory");
        router.GET().route("/admin_edit_skoola_course_subject").with(AdminController.class, "handleEditSkoolaCourseSubject");
        router.GET().route("/admin_edit_skoola_course_subject_category").with(AdminController.class, "handleEditSkoolaCourseSubjectCategory");
        router.POST().route("/admin_create_or_update_skoola_subject").with(AdminController.class, "handleCreateORUpdateSkoolaSubject");
        router.POST().route("/admin_create_or_update_skoola_subject_category").with(AdminController.class, "handleCreateORUpdateSkoolaSubjectCategory");
        router.GET().route("/admin_edit_skoola_country").with(AdminController.class, "handleEditSkoolaCountry");
        router.POST().route("/admin_create_or_update_skoola_country").with(AdminController.class, "handleCreateORUpdateSkoolaCountry");
        router.GET().route("/admin_edit_skoola_school").with(AdminController.class, "handleEditSkoolaSchool");
        router.POST().route("/admin_create_or_update_skoola_schools").with(AdminController.class, "handleCreateORUpdateSkoolaSchools");
        router.POST().route("/admin_create_or_update_skoola_exam").with(AdminController.class, "handleCreateORUpdateSkoolaExam");
        router.GET().route("/admin_edit_skoola_exam").with(AdminController.class, "handleEditSkoolaExam");
        router.POST().route("/admin_activate_deactivate_user").with(AdminController.class, "handleActivateDeactivteUser");
        router.POST().route("/admin_reset_user_password").with(AdminController.class, "handleResetUserPassword");
        
        router.GET().route("/more_destinations").with(SkoolaHomeController.class, "handleShowMoreDestinations");
        router.GET().route("/country_detail").with(SkoolaHomeController.class, "handleShowCountryDetail");
        router.GET().route("/more_exams").with(SkoolaHomeController.class, "handleShowMoreExams");
        router.GET().route("/exam_detail").with(SkoolaHomeController.class, "handleShowExamDetail");
         
        
        
        
        

        ///////////////////////////////////////////////////////////////////////
        // Assets (pictures / javascript)
        ///////////////////////////////////////////////////////////////////////    
        router.GET().route("/assets/webjars/{fileName: .*}").with(AssetsController.class, "serveWebJars");
        router.GET().route("/assets/{fileName: .*}").with(AssetsController.class, "serveStatic");

        ///////////////////////////////////////////////////////////////////////
        // Index / Catchall shows index page
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/.*").with(SkoolaHomeController.class, "index");
    }

    private void setApplicationMode() {

        System.setProperty(NinjaConstant.MODE_KEY_NAME, NinjaConstant.MODE_PROD);

    }

}

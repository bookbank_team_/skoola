package controllers.signup;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.form.SignupFormPojo;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.session.FlashScope;
import ninja.utils.NinjaProperties;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.SecurityUtil;
import common.SkoolaConstants;
import common.WSUtil;
import dataservice.signup.SignUp;

@Singleton
public class SignUpController {

    private Logger log = Logger.getLogger(SignUpController.class);

    @Inject
    NinjaProperties ninjaProperties;

  //  private WSUtil wsUtil = new WSUtil();

    public Result createSignUpRequest(Context context, FlashScope flashScope, SignupFormPojo signupFormPojo) {

        String firstName = signupFormPojo.getFirstName();
        String lastName = signupFormPojo.getLastName();
        String email = signupFormPojo.getEmail();
        String phoneNumber = signupFormPojo.getPhoneNumber();
        String password1 = context.getParameter("password1");
        String password2 = context.getParameter("password2");

        JSONObject obj = new JSONObject();

        obj.put("firstName", firstName);
        obj.put("lastName", lastName);
        obj.put("email", email);
        obj.put("phoneNumber", phoneNumber);
        obj.put("password", password1);

        if (firstName == null || firstName.isEmpty()) {
            flashScope.error("Enter a valid firstname.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (lastName == null || lastName.isEmpty()) {
            flashScope.error("Enter a valid lastname.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (email == null || email.isEmpty()) {
            flashScope.error("Enter a valid email.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (phoneNumber == null || phoneNumber.isEmpty()) {
            flashScope.error("Enter a valid phone number.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (password1 == null || password1.isEmpty()) {
            flashScope.error("Enter a valid password.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (password1.length() < 6) {
            flashScope.error("The minimum lenght of your password should be six (6)");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (password2 == null || password2.isEmpty()) {
            flashScope.error("Enter a valid password confirmation.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (!password1.equalsIgnoreCase(password2)) {
            flashScope.error("Your passwords don't match.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        if (password1.equalsIgnoreCase("password")) {
            flashScope.error("Your password cannot be password.");
            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_SIGNUP_CREATE_SIGNUP_REQUEST;
        String apiKey = SkoolaConstants.API_KEY;

        requestObject.setApi_key(apiKey);
        requestObject.setData(obj.toJSONString());
        requestObject.setHash("");

        String requestString = new Gson().toJson(requestObject);

        String response = SignUp.createSignUpRequest(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);

        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        System.out.println(" response object = " + responseObject);
        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());

            try {
                obj = (JSONObject) new JSONParser().parse(responseObject.getData());
                signupFormPojo.setFirstName((String) obj.get("firstName"));
                signupFormPojo.setLastName((String) obj.get("lastName"));
                signupFormPojo.setEmail((String) obj.get("email"));
                signupFormPojo.setPhoneNumber((String) obj.get("phoneNumber"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE).render(signupFormPojo);
        }

        flashScope.success(responseObject.getMessage());
        return Results.html().template(SkoolaConstants.SIGNUP_REGISTRATION_CONFIRMATION_PAGE);

    }

    public Result completeRegistrationRequest(Context context, FlashScope flashScope) {

        String activationCode = context.getParameter("code");
        if (activationCode == null || activationCode.isEmpty()) {
            flashScope.error("Sorry, your activation code is invalid");
            return Results.html().template(SkoolaConstants.HOME_PAGE);
        }

        JSONObject obj = new JSONObject();
        obj.put("activationCode", activationCode);

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_SIGNUP_ACTIVATE_USER_ACCOUNT;
        String apiKey = SkoolaConstants.API_KEY;

        requestObject.setApi_key(apiKey);
        requestObject.setData(obj.toJSONString());
        requestObject.setHash("");

        String requestString = new Gson().toJson(requestObject);

        String response = SignUp.activateUserAccount(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);

        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());
            return Results.html().template(SkoolaConstants.HOME_PAGE);
        }

        return Results.html().template(SkoolaConstants.SIGNUP_ACCOUNT_ACTIVATION_PAGE);
    }

}

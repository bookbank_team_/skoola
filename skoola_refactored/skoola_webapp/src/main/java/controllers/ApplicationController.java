/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package controllers;

import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import ninja.Result;
import ninja.Results;
import ninja.utils.NinjaProperties;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.SecurityUtil;
import common.WSUtil;
import ninja.Context;

@Singleton
public class ApplicationController {

    @Inject
    NinjaProperties ninjaProperties;

    //private WSUtil wsUtil = new WSUtil();

    public Result index() {

        return Results.html();

    }

    public Result testSkoolaService(Context context) {

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        
        requestObject.setData(context.getHostname());

        return Results.json().render(requestObject);

    }

    public static class SimplePojo {

        public String content;

    }
}

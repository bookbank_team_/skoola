package controllers.payments;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.HttpUtils;
import common.SkoolaConstants;
import common.WSUtil;
import controllers.signup.SignUpController;
import dataservice.payment.PaymentHandler;
import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;
import pojo.ActiveSessionPojo;
import pojo.CoursePojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import ninja.Context;

@Singleton
public class PaymentController {
	
	private Logger log = Logger.getLogger(PaymentController.class);
	@Inject
	NinjaCache ninjaCache;
	//private WSUtil wsUtil = new WSUtil();
	
	@Inject 
    NinjaProperties ninjaProperties;
	
	
	
    public Result intializePaymentForCareerConsultation(FlashScope flashScope,Context context) {
 
    	
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_CREATE_PAYMENT_HISTORY; 
        String apiKey = SkoolaConstants.API_KEY; 
        Session session = context.getSession();
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
        
        ActiveSessionPojo activeSessionPojo = null;
		HashMap<String,ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
		if(userSessionMap != null){
			activeSessionPojo = userSessionMap.get(session.getId());
		}
		if(activeSessionPojo == null){
			flashScope.error("An error occured please try again");
			Results.redirect(SkoolaConstants.HOME_PAGE);
		}
        
     
		
		String processTypeId = (String) ninjaCache.get(session.getId()+"_processTypeId");
		String feescheduleId = (String) ninjaCache.get(session.getId()+"_feesScheduleId");
		String consultationRequestId = (String) ninjaCache.get(session.getId()+"_consultationRequestId");
		//String feeInKobo =  (String)ninjaCache.get(session.getId()+"_feeInKobo");
		
		String portalUserId = activeSessionPojo.getPortalUserId();
		
		if(processTypeId == null || processTypeId.isEmpty() || feescheduleId == null || feescheduleId.isEmpty() 
			|| consultationRequestId == null || consultationRequestId.isEmpty() || portalUserId == null || portalUserId.isEmpty()){
			flashScope.error("An error occured please try again");
       	 	return Results.html().template(SkoolaConstants.HOME_PAGE); 
		}
		
        
        JSONObject jsonObject = new JSONObject();
        try {
        	jsonObject.put("action", "createPaymentHistory");
			jsonObject.put("processTypeId", processTypeId);
			jsonObject.put("recordId", consultationRequestId);
			jsonObject.put("portalUserId", portalUserId);
			jsonObject.put("feesScheduleId", feescheduleId);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        requestObject.setData(jsonObject.toString());
        String requestString = new Gson().toJson(requestObject);
       
        String response =  PaymentHandler.createPaymentHistory(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
        }
        
        try {
	        JSONObject jObject =   new JSONObject(responseObject.getData());
	    
			String paymentHistoryReferenceId = jObject.getString("paymentHistoryReferenceId");
			String payStackSecretKey = jObject.getString("payStackSecretKey");
			String payStackPaymentURL = jObject.getString("payStackPaymentURL");
			String amountInKobo = jObject.getString("amountInKobo");
			
		
			System.out.println(" payment url = " + payStackPaymentURL);
			System.out.println(" amount in kobo sent to paystack  = " + amountInKobo);
			URI uri = new URI(payStackPaymentURL);
			
			uri = URIUtils.rewriteURI(uri);
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(uri);
			String callbackUrl = "http://" + context.getHostname() + SkoolaConstants.PAYMENT_CALLBACK_URL;
			
			System.out.println(" this is the callback url ***************** = " + callbackUrl);
			String json = "{\"reference\": \""+paymentHistoryReferenceId+"\", \"amount\":"+amountInKobo+", \"email\": \""+activeSessionPojo.getEmail()+"\",\"callback_url\":\""+callbackUrl+"\"}";
			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("Authorization", payStackSecretKey);

			HttpResponse httpResponse = client.execute(httpPost);
			System.out.println("Response Code : " + httpResponse.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			JSONObject payStackResponsePojo =   new JSONObject(result.toString());
			JSONObject dataObject = payStackResponsePojo.getJSONObject("data");
			String paymentGatewayURL = dataObject.getString("authorization_url");
			String accessCode = dataObject.getString("access_code");
			
			 JSONObject updatePaymentHistoryWithAccessCodeJsonObject = new JSONObject();
		        try {
		        	updatePaymentHistoryWithAccessCodeJsonObject.put("action", "updatePaymentHistoryWithPaystackAccessCode");
		        	updatePaymentHistoryWithAccessCodeJsonObject.put("paymentHistoryReferenceId", paymentHistoryReferenceId);
		        	updatePaymentHistoryWithAccessCodeJsonObject.put("accessCode", accessCode);
					
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
		      methodUrl = SkoolaConstants.URL_UPDATE_PAYMENT_HISTORY_WITH_PAYSTACK_ACCESSCODE;
		      
		      requestObject.setData(updatePaymentHistoryWithAccessCodeJsonObject.toString());
		      requestString = new Gson().toJson(requestObject);
		       
		      response = PaymentHandler.updatePaymentHistoryWithPaystackAccessCode(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
		      responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
		        if(!responseObject.getSuccessful()){
		        	flashScope.error(responseObject.getMessage());
		        	 return Results.html().template(SkoolaConstants.HOME_PAGE);
		        }
		        
		        jObject =   new JSONObject(responseObject.getData());
			    
				paymentHistoryReferenceId = jObject.getString("paymentHistoryReferenceId");
				Boolean paidFor = jObject.getBoolean("paidFor");
				
				if(paidFor){
					flashScope.error("This consultation request has been paid for!!!");
					return Results.redirect("/go_to_carreer_consultation");
				}
			
		        
			
			return Results.redirect(paymentGatewayURL);
			
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
		
        return Results.json();

    }
    


    public Result paymentCallBack(FlashScope flashScope,Context context) {
    	System.out.println(" inside payment callback url ");
    	SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_SETTING_BY_NAME; 
        String apiKey = SkoolaConstants.API_KEY; 
        Session session = context.getSession();
        
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");
    
    	String paymentReferenceId = context.getParameter("trxref");
    	String payStackSecretKeyValue = "";
    	String transactionVerificationURLValue = "";
    	
    	if(paymentReferenceId == null || paymentReferenceId.isEmpty()){
    		flashScope.error("Sorry an error occured and your payment was unsuccessful , please contact our support team.");
			return Results.redirect("/go_to_carreer_consultation");
    	}

        JSONObject jsonObject = new JSONObject();
        try {
        	jsonObject.put("action", "getSettingByName");
			jsonObject.put("settingName", "PAYSTACK_TRANSACTION_VERIFICATION_URL");
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        requestObject.setData(jsonObject.toString());
        String requestString = new Gson().toJson(requestObject);
       
        String response = PaymentHandler.getSettingByName(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	return Results.html().template(SkoolaConstants.HOME_PAGE);
        }
        try {
			JSONObject jObject =   new JSONObject(responseObject.getData());
			transactionVerificationURLValue = jObject.getString("settingValue");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        
        
        jsonObject = new JSONObject();
        try {
        	jsonObject.put("action", "getSettingByName");
			jsonObject.put("settingName", "PAY_STACK_PAYMENT_API_SECRET_KEY");
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        requestObject.setData(jsonObject.toString());
        requestString = new Gson().toJson(requestObject);
       
        response = PaymentHandler.getSettingByName(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	return Results.redirect(SkoolaConstants.HOME_PAGE);
        }
        try {
        	JSONObject jObject =   new JSONObject(responseObject.getData());
			payStackSecretKeyValue = jObject.getString("settingValue");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
        
		String payStackResponse = verifyPayStackPaymentStatus(paymentReferenceId, transactionVerificationURLValue, payStackSecretKeyValue);
		
		System.out.println( "************************************************8  Pay Stack Response  = " + payStackResponse);
		
        methodUrl = SkoolaConstants.URL_UPDATE_PAYMENT_HISTORY_WITH_PAYSTACK_PAYMENT_RESPONSE;
        jsonObject = new JSONObject();
        try {
        	jsonObject.put("action", "updatePaymentHistoryWithPayStackPaymentResponse");
			jsonObject.put("paymentReferenceId", paymentReferenceId);
			jsonObject.put("payStackResponse", payStackResponse);
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        requestObject.setData(jsonObject.toString());
        requestString = new Gson().toJson(requestObject);
       
       response = PaymentHandler.updatePaymentHistoryWithPayStackResponse(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl , requestString);
       // log.info(" response  = " + response);
        System.out.println(" response from call back  = " + response );
        responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if(!responseObject.getSuccessful()){
        	flashScope.error(responseObject.getMessage());
        	return Results.redirect(SkoolaConstants.HOME_PAGE);
        }
        
        try {       	
			JSONObject jObject =   new JSONObject(responseObject.getData());

			String processTypeName = jObject.getString("processTypeName");
			String paymentStatus = jObject.getString("paymentStatus");

			flashScope.success("Your payment was successful. Your reference number is : " + paymentReferenceId.toUpperCase());

			if(processTypeName.equalsIgnoreCase(SkoolaConstants.PROCESS_TYPE_BOOK_IN_HOUSE_CONSULTATION ) || 
					processTypeName.equalsIgnoreCase(SkoolaConstants.PROCESS_TYPE_BOOK_VIRTUAL_CONSULTATION)){
				return Results.redirect("/user_profile_page");
			}
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	    
    	return Results.redirect(SkoolaConstants.HOME_PAGE);
    }
    
    public String verifyPayStackPaymentStatus(String paymentReference,String versificationUrl , String secretkey){
    	String verificationResponse = "";
    	try{
    		String paystackVerificationURL = versificationUrl;
    		String payStackSecretKey = secretkey;
    		
    		paystackVerificationURL = paystackVerificationURL + paymentReference;
    		
    		URI uri = new URI(paystackVerificationURL);
			
			uri = URIUtils.rewriteURI(uri);
			System.out.println(" url beaing called  ========== " +uri  );
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(uri);
		
			/*httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");*/
			httpGet.setHeader("Authorization", payStackSecretKey);

			HttpResponse httpResponse = client.execute(httpGet);
			System.out.println("Response Code : " + httpResponse.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			verificationResponse = result.toString();
			
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return verificationResponse;
    }
    
    public static void main(String [] args){
    	String paystackVerificationURL = "https://api.paystack.co/transaction/verify/";
		String payStackSecretKey = "Bearer sk_test_470832b29008cc54862a18aff3718b198061779a";
		
		StringBuffer result;
		try {
			paystackVerificationURL = paystackVerificationURL + "RLEM6YG5MSZ";
			
			URI uri = new URI(paystackVerificationURL);
			
			uri = URIUtils.rewriteURI(uri);
			System.out.println(" url beaing called  ========== " +uri  );
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(uri);

			httpGet.setHeader("Authorization", payStackSecretKey);

			HttpResponse httpResponse = client.execute(httpGet);
			System.out.println("Response Code : " + httpResponse.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

			result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			System.out.println(result.toString());
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
   

}

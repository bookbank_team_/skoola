package controllers.careerconsultation;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.AppUtil;
import common.SkoolaConstants;
import common.WSUtil;
import dataservice.careerconsultation.CareerConsultationHandler;
import dataservice.payment.PaymentHandler;
import filters.sessionfilter.SessionFilter;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.Router;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;
import pojo.ActiveSessionPojo;
import pojo.CoursePojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;

@Singleton
public class CareerConsultationController {

    private Logger log = Logger.getLogger(CareerConsultationController.class);
    @Inject
    NinjaProperties ninjaProperties;

    @Inject
    Router router;

  //  private WSUtil wsUtil = new WSUtil();
    private AppUtil appUtil = new AppUtil();

    @Inject
    NinjaCache ninjaCache;

    @FilterWith(SessionFilter.class)
    public Result handleCreateCarreerConsultation(FlashScope flashScope, Context context) {
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_CREATE_CONSULTATION_REQUEST;
        String apiKey = SkoolaConstants.API_KEY;
        Session session = context.getSession();
        Result result = Results.html();

        requestObject.setApi_key(apiKey);
        requestObject.setHash("");

        try {
            String consultationMode = context.getParameter("consultation_mode");
            System.out.println(" consultation mode = " + consultationMode);
            if (consultationMode == null || consultationMode.isEmpty()) {
                System.out.println(" redirecting back to course detail ");
                return result.redirect("/go_back_to_course_detail");
            }

            CoursePojo pojo = (CoursePojo) ninjaCache.get(session.getId() + "_coursePojo");

            if (pojo != null) {
                result.render(pojo);
            } else {
                flashScope.error("Sorry an error occured, please try again.");
                return result.html().template(SkoolaConstants.HOME_PAGE);
            }

            ActiveSessionPojo activeSessionPojo = null;
            HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
            if (userSessionMap != null) {
                activeSessionPojo = userSessionMap.get(session.getId());
            }
            if (activeSessionPojo != null) {

                result.render(activeSessionPojo);
            } else {
                flashScope.error("Sorry an error occured, please try again.");
                return result.html().template(SkoolaConstants.HOME_PAGE);
            }

            methodUrl = SkoolaConstants.URL_CREATE_CONSULTATION_REQUEST;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "createConsultationRequest");
            jsonObject.put("portalUserId", activeSessionPojo.getPortalUserId());
            jsonObject.put("courseId", pojo.getId().toString());
            jsonObject.put("consultationMode", consultationMode);

            requestObject.setData(jsonObject.toString());

            String requestString = new Gson().toJson(requestObject);
            String response = CareerConsultationHandler.createConsultationRequest(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
            log.info(" response from get create consulation request   = " + response);
            SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
            log.info(" response object = " + responseObject);
            if (!responseObject.getSuccessful()) {
                flashScope.error(responseObject.getMessage());
                return result.redirect("/go_back_to_course_detail");
            }

            JSONObject feesScheduleJsonObject = new JSONObject(responseObject.getData());

            System.out.println(" storing consulation request id ");
            String consultationRequestId = feesScheduleJsonObject.getString("consultationRequestId");
            if (consultationRequestId != null && !consultationRequestId.isEmpty()) {
                ninjaCache.add(session.getId() + "_consultationRequestId", consultationRequestId, SkoolaConstants.CACHE_VARIABLE_EXPIRATION_TIME);
            } else {
                flashScope.error(responseObject.getMessage());
                return result.redirect("/go_back_to_course_detail");
            }

            methodUrl = SkoolaConstants.URL_GET_FEESCHEDULE_BY_CODE;
            jsonObject = new JSONObject();
            jsonObject.put("action", "getFeeScheduleByCode");
            if (consultationMode.equalsIgnoreCase("VIRTUAL")) {
                jsonObject.put("code", SkoolaConstants.FEES_SCHEDULE_CODE_VIRTUAL_CAREER_CONSULTATION);
            } else if (consultationMode.equalsIgnoreCase("IN_HOUSE")) {
                jsonObject.put("code", SkoolaConstants.FEES_SCHEDULE_CODE_IN_HOUSE_CAREER_CONSULTATION);
            }

            requestObject.setData(jsonObject.toString());

            requestString = new Gson().toJson(requestObject);
            response = PaymentHandler.getFeeScheduleByCode(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
            System.out.println(" response get feeschedule = " + response);
            responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
            if (!responseObject.getSuccessful()) {
                flashScope.error(responseObject.getMessage());
                return result.redirect("/go_back_to_course_detail");
            }

            feesScheduleJsonObject = new JSONObject(responseObject.getData());

            String feeInKobo = feesScheduleJsonObject.getString("feeInKobo");
            String processType = feesScheduleJsonObject.getString("processType");
            String processTypeId = feesScheduleJsonObject.getString("processTypeId");
            String feesScheduleId = feesScheduleJsonObject.getString("feesScheduleId");

            if (feeInKobo != null && !feeInKobo.isEmpty()
                    && processType != null && !processType.isEmpty()
                    && processTypeId != null && !processTypeId.isEmpty()
                    && feesScheduleId != null && !feesScheduleId.isEmpty()) {
                ninjaCache.add(session.getId() + "_feeInKobo", feeInKobo, SkoolaConstants.CACHE_VARIABLE_EXPIRATION_TIME);
                ninjaCache.add(session.getId() + "_processType", processType, SkoolaConstants.CACHE_VARIABLE_EXPIRATION_TIME);
                ninjaCache.add(session.getId() + "_processTypeId", processTypeId, SkoolaConstants.CACHE_VARIABLE_EXPIRATION_TIME);
                ninjaCache.add(session.getId() + "_feesScheduleId", feesScheduleId, SkoolaConstants.CACHE_VARIABLE_EXPIRATION_TIME);
            } else {
                flashScope.error(responseObject.getMessage());
                return result.redirect("/go_back_to_course_detail");
            }

            System.out.println("before going payment history feeinkobo is " + feeInKobo);
            System.out.println("before going payment history processType is " + processType);
            System.out.println("before going payment history processTypeId is " + processTypeId);
            System.out.println("before going payment history feesScheduleId is " + feesScheduleId);
            //result.redirectTemporary("http://www.google.com");
            return Results.redirect("/create_payment_history");

        } catch (JsonSyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result.redirect("/go_back_to_course_detail");
    }

    @FilterWith(SessionFilter.class)
    public Result handleGoToCarreerConsultation(FlashScope flashScope, Context context) {
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_CREATE_CONSULTATION_REQUEST;
        String apiKey = SkoolaConstants.API_KEY;
        Session session = context.getSession();
        Result result = Results.html();

        requestObject.setApi_key(apiKey);
        requestObject.setHash("");

        CoursePojo pojo = (CoursePojo) ninjaCache.get(session.getId() + "_coursePojo");
        System.out.println(" course pojo = " + pojo);
        if (pojo != null) {
            result.render(pojo);
        } else {
            flashScope.error("Sorry an error occured, please try again.");
            return result.html().template(SkoolaConstants.HOME_PAGE);
        }

        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }
        if (activeSessionPojo != null) {

            result.render(activeSessionPojo);
        } else {
            flashScope.error("Sorry an error occured, please try again.");
            return result.html().template(SkoolaConstants.HOME_PAGE);
        }
        /* String action = (String) requestObject.get("action");
			    String portalUserId = (String) requestObject.get("portalUserId");
			    String courseId = (String) requestObject.get("courseId");
			    String consultationMode = (String) requestObject.get("consultationMode");
         */
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.html().template(SkoolaConstants.CONSULTATION_APPLICATION_PAGE);
    }

}

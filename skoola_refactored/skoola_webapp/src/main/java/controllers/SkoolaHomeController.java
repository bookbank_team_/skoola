package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import pojo.ActiveSessionPojo;
import pojo.CountryPojo;
import pojo.CourseSubjectGroupPojo;
import pojo.CourseTypePojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.form.SigninFormPojo;
import pojo.form.SignupFormPojo;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.Router;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.AppUtil;
import common.SkoolaConstants;
import common.WSUtil;
import dataservice.search.SearchHandler;
import dataservice.setting.Setting;

@Singleton
public class SkoolaHomeController {

    private Logger log = Logger.getLogger(SkoolaHomeController.class);
    private AppUtil appUtil = new AppUtil();

    @Inject
    NinjaProperties ninjaProperties;

    @Inject
    Router router;

   // private WSUtil wsUtil = new WSUtil();

    @Inject
    NinjaCache ninjaCache;

    public Result index(Context context, FlashScope flashScope) {

        Session session = context.getSession();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_COUNTRY;
        String apiKey = SkoolaConstants.API_KEY;

 

        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String countriesJSONString = session.get("countriesJSONString");

        JSONArray countryJArray = new JSONArray();
        JSONObject countryJObject = new JSONObject();

        Collection<CountryPojo> countryCol = new ArrayList<CountryPojo>();
        Collection<CourseTypePojo> courseTypeCol = new ArrayList<CourseTypePojo>();
        Collection<CourseSubjectGroupPojo> courseSubjectGroupStatPojoCol = new ArrayList<CourseSubjectGroupPojo>();

        try {

            if (countriesJSONString != null && !countriesJSONString.isEmpty()) {

                countryJObject = new JSONObject(countriesJSONString);
                //log.infoln("getting values from session 1");
            } else {

                String requestString = new Gson().toJson(requestObject);

                String response = SearchHandler.getAllCountry(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
                //  log.info("get all countries response $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ response  = " + response);
                SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
                if (responseObject != null && !responseObject.getSuccessful()) {
                    flashScope.error(responseObject.getMessage());
                    return Results.html().template(SkoolaConstants.HOME_PAGE);
                }

                countryJObject = new JSONObject(responseObject.getData());
                session.put("countriesJSONString", responseObject.getData());
            }

            countryJArray = (JSONArray) countryJObject.get("country_list");
            //log.infoln("country jArray = "+ courseTypeJArray);
            for (int i = 0; i < countryJArray.length(); i++) {
                JSONObject jsonObject = countryJArray.getJSONObject(i);
                //CountryPojo pojo = new Gson().fromJson(jsonObject.toString(), CountryPojo.class);

                //log.infoln( " country object = " + jsonObject);
                //log.infoln(" country id = "+ jsonObject.getString("id") );
                //log.infoln(" country id = "+ jsonObject.getString("name") );
                CountryPojo pojo = new CountryPojo();
                pojo.setDescription("");
                pojo.setCountryId(jsonObject.getString("id"));
                pojo.setName(jsonObject.getString("name"));

                countryCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {

            String courseTypeJSONString = session.get("courseTypeJSONString");

            JSONArray courseTypeJArray = new JSONArray();
            JSONObject courseTypeJObject = new JSONObject();

            methodUrl = SkoolaConstants.URL_GET_ALL_COURSE_TYPE;

            requestObject.setApi_key(apiKey);
            requestObject.setData("");
            requestObject.setHash("");

            if (courseTypeJSONString != null && !courseTypeJSONString.isEmpty()) {

                courseTypeJObject = new JSONObject(courseTypeJSONString);
                //log.infoln("getting values from session 1");
            } else {
                String requestString = new Gson().toJson(requestObject);

                String response = SearchHandler.getAllCourseType(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
                // log.infoln(" response  = " + response);
                SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
                if (responseObject != null && !responseObject.getSuccessful()) {
                    flashScope.error(responseObject.getMessage());
                    return Results.html().template(SkoolaConstants.HOME_PAGE);
                }

                courseTypeJObject = new JSONObject(responseObject.getData());
                session.put("courseTypeJSONString", responseObject.getData());
            }

            courseTypeJArray = (JSONArray) courseTypeJObject.get("course_type_list");
            //log.infoln("jArray = " + countriesTypeJArray);
            for (int i = 0; i < courseTypeJArray.length(); i++) {
                //log.infoln(" object = " + jArray.getJSONObject(i));

                JSONObject jsonObject = courseTypeJArray.getJSONObject(i);
                //log.infoln("jsonobject = " + jsonObject);
                CourseTypePojo pojo = new CourseTypePojo();
                pojo.setDescription("");
                pojo.setId(jsonObject.getString("id"));
                pojo.setName(jsonObject.getString("name"));

                courseTypeCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {

            String subjectGroupStatisticsJSONString = (String) ninjaCache.get("subjectGroupStatisticsJSONString");

            JSONArray subjectGroupStatisticsJArray = new JSONArray();
            JSONObject subjectGroupStatisticsJObject = new JSONObject();

            methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_GROUP_STATISTICS;

            requestObject.setApi_key(apiKey);
            requestObject.setData("");
            requestObject.setHash("");

            if (subjectGroupStatisticsJSONString != null && !subjectGroupStatisticsJSONString.isEmpty()) {

                subjectGroupStatisticsJObject = new JSONObject(subjectGroupStatisticsJSONString);
                System.out.println(" Subject Group Statistics   = " + subjectGroupStatisticsJArray);
            } else {
                String requestString = new Gson().toJson(requestObject);

                String response = SearchHandler.getAllSubjectGroupStatistics(requestString); //wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
                 log.info(" response @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  = " + response);
                SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
                if (responseObject != null && !responseObject.getSuccessful()) {
                    flashScope.error(responseObject.getMessage());
                    return Results.html().template(SkoolaConstants.HOME_PAGE);
                }

                subjectGroupStatisticsJObject = new JSONObject(responseObject.getData());
                ninjaCache.set("subjectGroupStatisticsJSONString", responseObject.getData());
                //session.put("subjectGroupStatisticsJSONString", responseObject.getData());
            }

            subjectGroupStatisticsJArray = (JSONArray) subjectGroupStatisticsJObject.get("subject_group_statistics_list");
            
            
             
            for (int i = 0; i < subjectGroupStatisticsJArray.length(); i++) {

                JSONObject jsonObject = subjectGroupStatisticsJArray.getJSONObject(i);

                CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
                pojo.setCourseSubjectGroupDisplayName(jsonObject.getString("displayName"));
                pojo.setCourseSubjectGroupName(jsonObject.getString("name"));
                pojo.setId(jsonObject.getString("id"));
                pojo.setNumberOfCourses(jsonObject.getString("numberOfCourses"));
                pojo.setNumberOfUniversities(jsonObject.getString("numberOfUniversities"));
                pojo.setType(jsonObject.getString("type"));

                courseSubjectGroupStatPojoCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

       

        Result result = Results.html();
        result.render("countryCol", countryCol);
        result.render("courseTypeCol", courseTypeCol);
        result.render("courseSubjectGroupStatPojoCol", courseSubjectGroupStatPojoCol);
        if (activeSessionPojo != null) {
            result.render(activeSessionPojo);
        }

        return result;
    }

    public Result search() {
        return Results.html();
    }

    public Result viewCoursesUnderACategory() {
        return Results.html().template("views/CourseSearchResults/search.ftl.html");
    }

    public Result viewCountryDetail() {
        return Results.html().template("views/CountryDetail/country-details.ftl.html");
    }

    public Result moreDestinations() {
        return Results.html().template("views/MoreDestinations/countries.ftl.html");

    }

    public Result viewExamDetail() {
        return Results.html().template("views/ExamDetails/exam-details.ftl.html");
    }

    public Result viewMoreExams() {
        return Results.html().template("views/MoreExams/exams.ftl.html");
    }

    public Result login(FlashScope flashScope, Context context) {

        // update settings
        System.out.println(" calling update app settings ");
        updateApplicationSettings();

        Session session = context.getSession();
        SigninFormPojo pojo = new SigninFormPojo();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }
        Result result = Results.html();

        result = handleRetrieveCourseCategoriesData(context, flashScope);
        result.template(SkoolaConstants.SIGNIN_PAGE);
        result.render(pojo);
        if (activeSessionPojo != null) {
            result.render(activeSessionPojo);
        }
        return result;
    }

    public Result signup(FlashScope flashScope, Context context) {
        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }
        SignupFormPojo signupFormPojo = new SignupFormPojo();
        Result result = Results.html();

        result = handleRetrieveCourseCategoriesData(context, flashScope);
        result.template(SkoolaConstants.SIGNUP_REGISTRATION_PAGE);
        result.render(signupFormPojo);
        if (activeSessionPojo != null) {
            result.render(activeSessionPojo);
        }
        return result;
    }

    public Result universtiyRanking() {
        return Results.html().template("views/SkoolaHomeController/index.ftl.html");
    }

    public Result studyAbroadGuides() {
        return Results.html().template("views/SkoolaHomeController/index.ftl.html");
    }

    public Result CountryInfo() {
        return Results.html().template("views/SkoolaHomeController/index.ftl.html");
    }

    public Result ApplicationHelp() {
        return Results.html().template("views/SkoolaHomeController/index.ftl.html");
    }

    public Result handleShowAboutUsPage(Context context, FlashScope flashScope) {

        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache
                .get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        Result result = Results.html();
        result.render(activeSessionPojo);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template("views/About/about.ftl.html");
    }

    public Result handleShowAccommodationInfoPage(Context context, FlashScope flashScope) {
        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache
                .get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }
        Result result = Results.html();
        result.render(activeSessionPojo);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template("views/Accommodation/accommodation.ftl.html");
    }

    public Result handleShowExamInfoPage(Context context, FlashScope flashScope) {
        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache
                .get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        Result result = Results.html();
        result.render(activeSessionPojo);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template("views/ExamCoaching/exams.ftl.html");
    }

    private Result handleRetrieveCourseCategoriesData(Context context, FlashScope flashScope) {
        Result result = Results.html();
        Collection<CourseSubjectGroupPojo> courseSubjectGroupStatPojoCol = new ArrayList<CourseSubjectGroupPojo>();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String apiKey = SkoolaConstants.API_KEY;
        try {

            String subjectGroupStatisticsJSONString = (String) ninjaCache.get("subjectGroupStatisticsJSONString");

            JSONArray subjectGroupStatisticsJArray = new JSONArray();
            JSONObject subjectGroupStatisticsJObject = new JSONObject();

            String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_GROUP_STATISTICS;

            requestObject.setApi_key(apiKey);
            requestObject.setData("");
            requestObject.setHash("");

            if (subjectGroupStatisticsJSONString != null && !subjectGroupStatisticsJSONString.isEmpty()) {

                subjectGroupStatisticsJObject = new JSONObject(subjectGroupStatisticsJSONString);
                //log.infoln("getting values from session 1");
            } else {
                String requestString = new Gson().toJson(requestObject);

                String response = SearchHandler.getAllSubjectGroupStatistics(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
                // log.infoln(" response  = " + response);
                SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
                if (responseObject != null && !responseObject.getSuccessful()) {
                    flashScope.error(responseObject.getMessage());
                    return Results.html().template(SkoolaConstants.HOME_PAGE);
                }

                subjectGroupStatisticsJObject = new JSONObject(responseObject.getData());
                ninjaCache.set("subjectGroupStatisticsJSONString", responseObject.getData());
                //session.put("subjectGroupStatisticsJSONString", responseObject.getData());
            }

            subjectGroupStatisticsJArray = (JSONArray) subjectGroupStatisticsJObject.get("subject_group_statistics_list");
            for (int i = 0; i < subjectGroupStatisticsJArray.length(); i++) {

                JSONObject jsonObject = subjectGroupStatisticsJArray.getJSONObject(i);

                CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
                pojo.setCourseSubjectGroupDisplayName(jsonObject.getString("displayName"));
                pojo.setCourseSubjectGroupName(jsonObject.getString("name"));
                pojo.setId(jsonObject.getString("id"));
                pojo.setNumberOfCourses(jsonObject.getString("numberOfCourses"));
                pojo.setNumberOfUniversities(jsonObject.getString("numberOfUniversities"));
                pojo.setType(jsonObject.getString("type"));

                courseSubjectGroupStatPojoCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result.render("courseSubjectGroupStatPojoCol", courseSubjectGroupStatPojoCol);
    }

    private void updateApplicationSettings() {
        try {
           // WSUtil wsUtil = new WSUtil();

            SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
            String methodUrl = SkoolaConstants.URL_CREATE_UPDATE_APPLICATION_SETTINGS;
            String apiKey = SkoolaConstants.API_KEY;

            JSONObject obj = new JSONObject();
            obj.put("action", "create_update_application_setting");

            requestObject.setApi_key(apiKey);
            requestObject.setData(obj.toString());
            requestObject.setHash("");

            String requestString = new Gson().toJson(requestObject);

            String response = Setting.createUpdateDefaultSuperAdminUser(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);

            SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

            System.out.println(" response object = " + responseObject);
            if (!responseObject.getSuccessful()) {
                log.info("ERROR!! Settings not updated.");
            } else {
                log.info("SUCCESS!! Settings updated.");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    
    public Result handleShowMoreDestinations(Context context,FlashScope flashScope){
        
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template(SkoolaConstants.MORE_COUNTRIES_PAGE);
    }
    
    
    public Result handleShowCountryDetail(Context context,FlashScope flashScope){
        
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template(SkoolaConstants.COUNTRY_DETAILS_PAGE);
    }
    
    
    public Result handleShowMoreExams(Context context,FlashScope flashScope){
        
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template(SkoolaConstants.MORE_EXAMS_PAGE);
    }
    
    
    public Result handleShowExamDetail(Context context,FlashScope flashScope){
        
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        return result.template(SkoolaConstants.EXAM_DETAILS_PAGE);
    }

}

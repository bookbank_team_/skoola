package controllers.admin;

import com.dg.skoola.entity.Country;
import com.dg.skoola.entity.Exam;
import com.dg.skoola.entity.PortalUser;
import com.dg.skoola.entity.School;
import com.dg.skoola.entity.Subject;
import com.dg.skoola.entity.SubjectGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.AppUtil;
import common.SkoolaConstants;
import common.WSUtil;
import controllers.account.AccountController;
import dataservice.accounts.backend.BackEndAccountHandler;
import dataservice.common.CustomService;
import dataservice.search.SearchHandler;

import filters.sessionfilter.ApplicationRoleFilter;
import filters.sessionfilter.SessionFilter;
import ninja.Context;
import ninja.FilterWith;

import ninja.Context;

import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;
import org.apache.commons.lang3.text.WordUtils;
import pojo.ActiveSessionPojo;
import pojo.AjaxResponse;
import pojo.ApplicationUserPojo;
import pojo.ConsultationRequestPojo;
import pojo.CoursePojo;
import pojo.CourseSubjectGroupPojo;
import pojo.CourseSubjectPojo;
import pojo.CourseTypePojo;

import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.form.SigninFormPojo;

@Singleton
public class AdminController {

    private Logger log = Logger.getLogger(AdminController.class);

    @Inject
    NinjaProperties ninjaProperties;
    @Inject
    NinjaCache ninjaCache;

    //private WSUtil wsUtil = new WSUtil();
    private AppUtil appUtil = new AppUtil();

    @FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
    public Result handleShowAdminDashBoard(FlashScope flashScope, Context context) {

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ADMIN_DASHBOARD_STATISTICS;
        String apiKey = SkoolaConstants.API_KEY;
        Result result = Results.html();
        ActiveSessionPojo activeSessionPojo = (ActiveSessionPojo) context.getAttribute("activeSessionPojo");
        result.render(activeSessionPojo);
        //result = appUtil.renderApplicationSession(result, context,ninjaCache);

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String requestString = new Gson().toJson(requestObject);

        String response = BackEndAccountHandler.getAdminDashBoardStatistics(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        try {
            JSONObject jObject = new JSONObject(responseObject.getData());
            String totalSignUpsToday = jObject.getString("totalSignUpsToday");
            String totalSignUpsThisWeek = jObject.getString("totalSignUpsThisWeek");
            String totalPaidConsultationsToday = jObject.getString("totalPaidConsultationsToday");
            String totalPaidConsultatioinsThisWeek = jObject.getString("totalPaidConsultatioinsThisWeek");

            //System.out.println(" totalSignUpsToday = " + totalSignUpsToday);
            //System.out.println(" totalSignUpsThisWeek = " + totalSignUpsThisWeek);
            //System.out.println(" totalPaidConsultationsToday = " + totalPaidConsultationsToday);
            //System.out.println(" totalPaidConsultatioinsThisWeek = " + totalPaidConsultatioinsThisWeek);
            result.render("totalSignUpsToday", totalSignUpsToday);
            result.render("totalSignUpsThisWeek", totalSignUpsThisWeek);
            result.render("totalPaidConsultationsToday", totalPaidConsultationsToday);
            result.render("totalPaidConsultatioinsThisWeek", totalPaidConsultatioinsThisWeek);
            result.html().template(SkoolaConstants.ADMIN_DASHBOARD);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;

    }


    public Result handleShowApplicationUsers(FlashScope flashScope, Context context) {
        Session session = context.getSession();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_APPLICATION_USERS_RESULT;
        String apiKey = SkoolaConstants.API_KEY;
        Result result = Results.html();

        result = appUtil.renderApplicationSession(result, context, ninjaCache);

        String requestURI = context.getRoute().getUri();
        Boolean isAdminRoleType = false;

        if (requestURI.equalsIgnoreCase("/admin_application_users")) {
            isAdminRoleType = false;
        } else if (requestURI.equalsIgnoreCase("/admin_backend_users")) {
            isAdminRoleType = true;
        }

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");

        JSONObject jsonRequestObject = new JSONObject();
        try {
            jsonRequestObject.put("searchIndex", searchIndex);

            jsonRequestObject.put("isAdminRoleType", isAdminRoleType);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        requestObject.setData(jsonRequestObject.toString());
        String requestString = new Gson().toJson(requestObject);

        String response = BackEndAccountHandler.getPaginatedApplicationUsersResult(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        //System.out.println(" appuser response string  = " + response);
        Collection<ApplicationUserPojo> applicationUserCol = new ArrayList<ApplicationUserPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        try {

            if (responseObject.getSuccessful()) {

                jObject = new JSONObject(responseObject.getData());
                jArray = (JSONArray) jObject.get("application_user_result");

                searchIndex = (String) jObject.get("search_index");
                searchResultCount = (String) jObject.get("search_result_count");

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = jArray.getJSONObject(i);

                    ApplicationUserPojo pojo = new ApplicationUserPojo();
                    pojo.setDateCreated((String) jsonObject.get("dateCreated"));
                    pojo.setEmail(jsonObject.getString("email"));
                    pojo.setFirstName(jsonObject.getString("firstName"));
                    pojo.setLastName(jsonObject.getString("lastName"));
                    pojo.setPhoneNumber(jsonObject.getString("phoneNumber"));
                    pojo.setRole(jsonObject.getString("role"));
                    pojo.setUserName(jsonObject.getString("userName"));
                    pojo.setStatus(jsonObject.getString("status"));
                    pojo.setUserId(jsonObject.getString("userId").toUpperCase());
                    pojo.setActive(jsonObject.getString("activated"));
                    pojo.setPasswordReset(jsonObject.getString("passwordReset"));

                    applicationUserCol.add(pojo);
                }
                result.render("applicationUserCol", applicationUserCol);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        result.template(SkoolaConstants.ADMIN_APPLICATION_USERS);
        result.render("searchIndex", searchIndex);
        result.render("searchResultCount", searchResultCount);

        result.render("SEARCH_MAXIMUM_RESULTSET_SIZE", SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE); //
        return result;
    }
    
    public Result handleActivateDeactivteUser (FlashScope flashScope, Context context) {
        
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        
        String action = context.getParameter("action") == null ? "" : context.getParameter("action");
        String appUserId = context.getParameter("appUserId") == null ? "" : context.getParameter("appUserId");
        
        if(action.isEmpty() || appUserId.isEmpty()){
            flashScope.error("Sorry!! An error occurred, please contact support.");
            return handleShowApplicationUsers(flashScope, context);
        }
        
        PortalUser portalUser = CustomService.getPortalUserByUserId(appUserId);
        
        if(portalUser == null){
            flashScope.error("Sorry!! Please select a valid user.");
            return handleShowApplicationUsers(flashScope, context);
        }
        
        
        if(action.equalsIgnoreCase("activated")){
            portalUser.setActive(Boolean.TRUE);
        }else if(action.equalsIgnoreCase("deactivated")){
             portalUser.setActive(Boolean.FALSE);
        }
        CustomService.service.updateRecord(portalUser);
        
        String userName = WordUtils.capitalizeFully(portalUser.getFirstname()) + " " + WordUtils.capitalizeFully(portalUser.getLastname());
        flashScope.success(" User : " +userName+ " was " + action +" successfully ");
        return handleShowApplicationUsers(flashScope, context);
       
    }
    
    
    
    public Result handleResetUserPassword (FlashScope flashScope, Context context) {
        
        return Results.html();
    }

    @FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
    public Result handleShowConsultationApplication(FlashScope flashScope, Context context) {
        Session session = context.getSession();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_CAREER_CONSULTATION_APPLICATION_REQUESTS_RESULT;
        String apiKey = SkoolaConstants.API_KEY;
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");
        String requestStatus = context.getParameter("requestStatus") == null ? "" : context.getParameter("requestStatus");

        //System.out.println(" search index = " + searchIndex);
        //System.out.println(" request status = " + requestStatus);
        JSONObject jsonRequestObject = new JSONObject();
        try {
            jsonRequestObject.put("searchIndex", searchIndex);
            jsonRequestObject.put("requestStatus", requestStatus);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        requestObject.setData(jsonRequestObject.toString());
        String requestString = new Gson().toJson(requestObject);

        String response = BackEndAccountHandler.getPaginatedCareerConsultationRequestByStatus(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        Collection<ConsultationRequestPojo> consultationRequestCol = new ArrayList<ConsultationRequestPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        try {

            if (responseObject.getSuccessful()) {

                jObject = new JSONObject(responseObject.getData());
                jArray = (JSONArray) jObject.get("consultation_request_result");

                searchIndex = (String) jObject.get("search_index");
                searchResultCount = (String) jObject.get("search_result_count");
                requestStatus = (String) jObject.get("request_status");

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = jArray.getJSONObject(i);
                    //System.out.println(" jsonobjec of consultation request : " + jsonObject);
                    ConsultationRequestPojo pojo = new ConsultationRequestPojo();
                    pojo.setDateCreated((String) jsonObject.get("dateCreated"));
                    pojo.setEmail(jsonObject.getString("email"));
                    pojo.setFirstName(jsonObject.getString("firstName"));
                    pojo.setLastName(jsonObject.getString("lastName"));
                    pojo.setPhoneNumber(jsonObject.getString("phoneNumber"));
                    pojo.setConsultationStatus(jsonObject.getString("consultationStatus"));
                    pojo.setConsultationMode(jsonObject.getString("consultationMode"));
                    pojo.setCourseName(jsonObject.getString("courseName"));
                    pojo.setSubjectName(jsonObject.getString("courseSubject"));
                    pojo.setId(jsonObject.getString("id"));

                    consultationRequestCol.add(pojo);
                }
                result.render("consultationRequestCol", consultationRequestCol);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        result.template(SkoolaConstants.ADMIN_CAREER_CONSULTATION_APPLICATION);
        result.render("searchIndex", searchIndex);
        result.render("searchResultCount", searchResultCount);
        result.render("requestStatus", requestStatus);
        result.render("SEARCH_MAXIMUM_RESULTSET_SIZE", SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
        return result;
    }

    @FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
    public Result handleShowSkoolaCourses(FlashScope flashScope, Context context) {

        System.out.println(" inside show skoola courses ");
        Session session = context.getSession();
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_SKOOLA_COURSES;
        String apiKey = SkoolaConstants.API_KEY;
        Result result = Results.html();

        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllCourseType(result, flashScope, session);
        result = getAllSubjectType(result, flashScope, session);
        result = appUtil.handleGetAllSkoolaSchools(result, context, flashScope, ninjaCache, ninjaProperties);

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");
        String status = context.getParameter("status") == null ? "" : context.getParameter("status");
        String subjectIdParameter = context.getParameter("subjectIdParameter") == null ? "" : context.getParameter("subjectIdParameter");
        String courseTypeIdParameter = context.getParameter("courseTypeIdParameter") == null ? "" : context.getParameter("courseTypeIdParameter");
        String schoolIdParameter = context.getParameter("schoolIdParameter") == null ? "" : context.getParameter("schoolIdParameter");

        //System.out.println(" ***************************** school parameter = " + schoolIdParameter);
        JSONObject jsonRequestObject = new JSONObject();
        try {
            jsonRequestObject.put("searchIndex", searchIndex);
            jsonRequestObject.put("status", status);
            jsonRequestObject.put("subjectIdParameter", subjectIdParameter);
            jsonRequestObject.put("courseTypeIdParameter", courseTypeIdParameter);
            jsonRequestObject.put("schoolIdParameter", schoolIdParameter);
        } catch (JSONException e) {

            e.printStackTrace();
        }
        requestObject.setData(jsonRequestObject.toString());
        String requestString = new Gson().toJson(requestObject);

        String response = BackEndAccountHandler.getPaginatedSkoolaCourses(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        Collection<CoursePojo> courseCol = new ArrayList<CoursePojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";

        try {

            if (responseObject.getSuccessful()) {

                jObject = new JSONObject(responseObject.getData());
                jArray = (JSONArray) jObject.get("course_result");

                searchIndex = (String) jObject.get("search_index");
                searchResultCount = String.valueOf(jObject.get("resultCount"));

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = jArray.getJSONObject(i);

                    Long id = jsonObject.getString("courseId") == null ? 0l : Long.valueOf(jsonObject.getString("courseId"));
                    String name = jsonObject.getString("courseName") == null ? "" : jsonObject.getString("courseName");
                    String courseDuration = jsonObject.getString("courseDuration") == null ? "" : jsonObject.getString("courseDuration");
                    String courseSummary = jsonObject.getString("courseSummary") == null ? "" : jsonObject.getString("courseSummary");
                    Boolean ieltRequired = jsonObject.getBoolean("ieltsRequired") == false ? false : true;
                    String courseRequirements = jsonObject.getString("courseRequirements") == null ? "" : jsonObject.getString("courseRequirements");
                    String programModules = jsonObject.getString("programModules") == null ? "" : jsonObject.getString("programModules");
                    String courseVenue = jsonObject.getString("courseVenue") == null ? "" : jsonObject.getString("courseVenue").replace(",", " ").replace("\n", " ");
                    String courseSubject = jsonObject.getString("courseSubject") == null ? "" : jsonObject.getString("courseSubject");
                    String courseSubjectId = jsonObject.getString("courseSubjectId") == null ? "" : jsonObject.getString("courseSubjectId");
                    String courseSubjectGroupName = jsonObject.getString("courseSubjectGroupName") == null ? "" : jsonObject.getString("courseSubjectGroupName");
                    String courseTypeName = jsonObject.getString("courseTypeName") == null ? "" : jsonObject.getString("courseTypeName");
                    String schoolId = jsonObject.getString("schoolId") == null ? "" : jsonObject.getString("schoolId");
                    String schoolLogoURL = jsonObject.getString("schoolLogoURL") == null ? "" : jsonObject.getString("schoolLogoURL");
                    String schoolName = jsonObject.getString("schoolName") == null ? "" : jsonObject.getString("schoolName");
                    String feeInNaira = jsonObject.getString("feeInNaira") == null ? "" : jsonObject.getString("feeInNaira");
                    String courseFeeInWords = jsonObject.getString("courseFeeInWords") == null ? "" : jsonObject.getString("courseFeeInWords");

                    CoursePojo pojo = new CoursePojo();
                    pojo.setId(id);
                    pojo.setName(name);
                    pojo.setCourseDuration(courseDuration);
                    pojo.setCourseSummary(courseSummary);
                    pojo.setIeltsRequired(ieltRequired);
                    pojo.setCourseRequirements(courseRequirements);
                    pojo.setProgramModules(programModules);
                    pojo.setCourseVenue(courseVenue);
                    pojo.setCourseSubjectName(courseSubject);
                    pojo.setCourseSubjectId(courseSubjectId);
                    pojo.setCourseSubjectGroupName(courseSubjectGroupName);
                    pojo.setCourseType(courseTypeName);
                    pojo.setCourseSchoolId(schoolId);
                    pojo.setCourseSchoolLogoUrl(schoolLogoURL);
                    pojo.setCourseSchoolName(schoolName);
                    pojo.setCourseFeeInNaira(feeInNaira);
                    pojo.setCourseFeeInWords(courseFeeInWords);

                    courseCol.add(pojo);
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //System.out.println(" size of courseCol = " + courseCol.size());
        result.render("searchIndex", searchIndex);
        result.render("courseCol", courseCol);
        result.render("status", status);
        result.render("searchResultCount", searchResultCount);
        result.render("subjectIdParameter", subjectIdParameter);
        result.render("courseTypeIdParameter", courseTypeIdParameter);
        result.render("schoolIdParameter", schoolIdParameter);
        result.render("SEARCH_MAXIMUM_RESULTSET_SIZE", SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
        result.template(SkoolaConstants.ADMIN_SKOOLA_COURSES);
        return result;
    }

    @FilterWith({SessionFilter.class, ApplicationRoleFilter.class})

    public Result getAllSubjectType(Result result, FlashScope flashScope, Session session) {

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_TYPE;
        String apiKey = SkoolaConstants.API_KEY;

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String requestString = new Gson().toJson(requestObject);

        String response = SearchHandler.getAllCourseSubjectType(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());
            return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        }
        Collection<CourseSubjectPojo> courseSubjectCol = new ArrayList<CourseSubjectPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try {

            jObject = new JSONObject(responseObject.getData());

            jArray = (JSONArray) jObject.get("course_subject_type_list");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jsonObject = jArray.getJSONObject(i);

                CourseSubjectPojo pojo = new CourseSubjectPojo();
                pojo.setSubjectDisplayName(jsonObject.getString("name"));
                pojo.setId(jsonObject.getString("id"));
                pojo.setSubjectName(jsonObject.getString("name"));
                pojo.setType("Subject");

                courseSubjectCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        result.render("courseSubjectCol", courseSubjectCol);
        return result;
    }

    @FilterWith({SessionFilter.class, ApplicationRoleFilter.class})

    public Result getAllSkoolaCourseSubjectCategories(Result result, FlashScope flashScope, Session session) {

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_SKOOLA_COURSE_SUBJECT_CATEGORIES;
        String apiKey = SkoolaConstants.API_KEY;

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String requestString = new Gson().toJson(requestObject);

        String response = SearchHandler.getAllCourseSubjectGroupType(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        // //System.out.println(" response object for course subject group type " + response );
        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());
            return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        }
        Collection<CourseSubjectGroupPojo> courseSubjectGroupCol = new ArrayList<CourseSubjectGroupPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try {

            jObject = new JSONObject(responseObject.getData());

            jArray = (JSONArray) jObject.get("course_subject_group_type_list");
            for (int i = 0; i < jArray.length(); i++) {

                JSONObject jsonObject = jArray.getJSONObject(i);

                String courseSubjectGroupDisplayName = jsonObject.getString("name") == null ? "" : jsonObject.getString("name");
                String courseSubjectGroupName = jsonObject.getString("name") == null ? "" : jsonObject.getString("name");
                String id = jsonObject.getString("id") == null ? "" : jsonObject.getString("id");

                CourseSubjectGroupPojo pojo = new CourseSubjectGroupPojo();
                pojo.setCourseSubjectGroupDisplayName(courseSubjectGroupDisplayName);
                pojo.setCourseSubjectGroupName(courseSubjectGroupName);
                pojo.setId(id);

                courseSubjectGroupCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.info(" courseSubjectGroupCol $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ = " + courseSubjectGroupCol);
        result.render("courseSubjectGroupCol", courseSubjectGroupCol);
        return result;
    }

    @FilterWith({SessionFilter.class, ApplicationRoleFilter.class})
    public Result getAllSkoolaCountriesInJSONFormat(Result result, FlashScope flashScope, Session session) {

        Collection<Country> countryCol = CustomService.service.getAllRecords(Country.class);
        JSONArray jArray = new JSONArray();
        if (countryCol != null && !countryCol.isEmpty()) {

            try {

                for (Country country : countryCol) {

                    JSONObject jsonObject = new JSONObject();

                    String name = country.getName() == null ? "" : country.getName();
                    String code = country.getCode() == null ? "" : country.getCode();
                    String livingConditions = country.getLivingConditions() == null ? "" : country.getLivingConditions();
                    String studyInformation = country.getStudyInformation() == null ? "" : country.getStudyInformation();
                    String aboutCountryInformation = country.getAboutCountryInfomation() == null ? "N/A" : country.getAboutCountryInfomation();
                    Integer quickFactPopulationFigure = country.getQuickFactPopulationFigure() == null ? 0 : country.getQuickFactPopulationFigure();
                    Integer quickFactInternationStudents = country.getQuickFactInternationStudents() == null ? 0 : country.getQuickFactInternationStudents();
                    Integer quickFactCourses = country.getQuickFactCourses() == null ? 0 : country.getQuickFactCourses();
                    Integer quickFactInstitutions = country.getQuickFactInstitutions() == null ? 0 : country.getQuickFactInstitutions();

                    jsonObject.put("id", country.getId().toString());
                    jsonObject.put("name", name);
                    jsonObject.put("code", code);
                    jsonObject.put("livingConditions", livingConditions);
                    jsonObject.put("studyInformation", studyInformation);
                    jsonObject.put("aboutCountryInformation", aboutCountryInformation);
                    jsonObject.put("quickFactPopulationFigure", quickFactPopulationFigure);
                    jsonObject.put("quickFactInternationStudents", quickFactInternationStudents);
                    jsonObject.put("quickFactCourses", quickFactCourses);
                    jsonObject.put("quickFactInstitutions", quickFactInstitutions);

                    jArray.put(jsonObject);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        result.render("countryJSONArray", jArray);
        return result;
    }

    public Result getAllSkoolaSchools(Result result, FlashScope flashScope, Session session) {

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_SUBJECT_TYPE;
        String apiKey = SkoolaConstants.API_KEY;

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        requestObject.setData(new JSONObject().toString());

        String requestString = new Gson().toJson(requestObject);

        String response = SearchHandler.getAllCourseSubjectType(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());
            return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        }
        Collection<CourseSubjectPojo> courseSubjectCol = new ArrayList<CourseSubjectPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try {

            jObject = new JSONObject(responseObject.getData());

            jArray = (JSONArray) jObject.get("course_subject_type_list");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jsonObject = jArray.getJSONObject(i);

                CourseSubjectPojo pojo = new CourseSubjectPojo();
                pojo.setSubjectDisplayName(jsonObject.getString("name"));
                pojo.setId(jsonObject.getString("id"));
                pojo.setSubjectName(jsonObject.getString("name"));
                pojo.setType("Subject");

                courseSubjectCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        result.render("courseSubjectCol", courseSubjectCol);
        return result;
    }

    public Result getAllCourseType(Result result, FlashScope flashScope, Session session) {

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_ALL_COURSE_TYPE;
        String apiKey = SkoolaConstants.API_KEY;

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String requestString = new Gson().toJson(requestObject);

        String response = SearchHandler.getAllCourseType(requestString); //wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);
        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());
            return result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        }
        Collection<CourseTypePojo> courseTypeCol = new ArrayList<CourseTypePojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        try {

            jObject = new JSONObject(responseObject.getData());
            jArray = (JSONArray) jObject.get("course_type_list");

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jsonObject = jArray.getJSONObject(i);
                CourseTypePojo pojo = new CourseTypePojo();
                pojo.setDescription("");
                pojo.setId(jsonObject.getString("id"));
                pojo.setName(jsonObject.getString("name"));

                courseTypeCol.add(pojo);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        result.render("courseTypeCol", courseTypeCol);
        return result;

    }

    public Result handleShowBackEndUsers(FlashScope flashScope, Context context) {
        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        Result result = Results.html();
        result.template(SkoolaConstants.ADMIN_BACKEND_USERS);
        result.render(activeSessionPojo);
        return result;
    }

   

   

    public Result handleShowEditCourse(FlashScope flashScope, Context context) {
        Session session = context.getSession();
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllCourseType(result, flashScope, session);
        result = getAllSubjectType(result, flashScope, session);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);
        result = appUtil.handleGetAllSkoolaSchools(result, context, flashScope, ninjaCache, ninjaProperties);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String status = context.getParameter("status") == null ? "" : context.getParameter("status");
        String subjectIdParameter = context.getParameter("subjectIdParameter") == null ? "" : context.getParameter("subjectIdParameter");
        String courseTypeIdParameter = context.getParameter("courseTypeIdParameter") == null ? "" : context.getParameter("courseTypeIdParameter");
        String schoolIdParameter = context.getParameter("schoolIdParameter") == null ? "" : context.getParameter("schoolIdParameter");
        String courseId = context.getParameter("courseId") == null ? "" : context.getParameter("courseId");
        String operation = context.getParameter("operation") == null ? "" : context.getParameter("operation");
        Boolean edit_mode = false;
        //System.out.println(" school id param  = " + schoolIdParameter);

        //System.out.println(" #######################  status = " + status);
        if (operation.equalsIgnoreCase("edit")) {

            if (courseId == null || courseId.isEmpty()) {
                flashScope.error("Invalid request : course Id is not available.");
                return Results.redirect("/admin_skoola_courses");
            }

            SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
            String methodUrl = SkoolaConstants.URL_GET_COURSE_DETAIL;
            String apiKey = SkoolaConstants.API_KEY;

            requestObject.setApi_key(apiKey);
            requestObject.setData("");
            requestObject.setHash("");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("courseId", courseId);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            requestObject.setData(jsonObject.toString());
            String requestString = new Gson().toJson(requestObject);

            String response = SearchHandler.getCourseDetail(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
            SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

            if (!responseObject.getSuccessful()) {
                flashScope.error(responseObject.getMessage());
                return Results.redirect("/admin_skoola_courses");
            }

            CoursePojo pojo = null;
            try {
                pojo = new Gson().fromJson(responseObject.getData(), CoursePojo.class);
            } catch (JsonSyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (pojo == null) {
                flashScope.error("Invalide Response : ");
                return Results.redirect("/admin_skoola_courses");
            }

            edit_mode = true;
            result.render("coursePojo", pojo);

        }

        result.render("edit_mode", edit_mode);
        result.render("course_searchIndex", searchIndex);
        result.render("course_status", status);
        result.render("course_subjectIdParameter", subjectIdParameter);
        result.render("course_courseTypeIdParameter", courseTypeIdParameter);
        result.render("course_schoolIdParameter", schoolIdParameter);
        result.render("course_courseId", courseId);
        result.template(SkoolaConstants.ADMIN_EDIT_SKOOLA_COURSE);
        return result;
    }

    public Result handleCreateORUpdateSkoolaCourse(FlashScope flashScope, Context context) {
        //System.out.println(" 0");
        Session session = context.getSession();
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllCourseType(result, flashScope, session);
        result = getAllSubjectType(result, flashScope, session);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);
        result = appUtil.handleGetAllSkoolaSchools(result, context, flashScope, ninjaCache, ninjaProperties);

        //System.out.println(" 1 ");
        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String status = context.getParameter("status") == null ? "" : context.getParameter("status");
        String subjectIdParameter = context.getParameter("subjectIdParameter") == null ? "" : context.getParameter("subjectIdParameter");
        String courseTypeIdParameter = context.getParameter("courseTypeIdParameter") == null ? "" : context.getParameter("courseTypeIdParameter");
        String schoolIdParameter = context.getParameter("schoolIdParameter") == null ? "" : context.getParameter("schoolIdParameter");
        String courseId = context.getParameter("courseId") == null ? "" : context.getParameter("courseId");
        String operation = context.getParameter("operation") == null ? "" : context.getParameter("operation");

        String cu_course_name = context.getParameter("cu_course_name") == null ? "" : context.getParameter("cu_course_name");
        String cu_course_type = context.getParameter("cu_course_type") == null ? "" : context.getParameter("cu_course_type");
        String cu_course_subject_group = context.getParameter("cu_course_subject_group") == null ? "" : context.getParameter("cu_course_subject_group");
        String cu_course_subject = context.getParameter("cu_course_subject") == null ? "" : context.getParameter("cu_course_subject");
        String cu_course_school = context.getParameter("cu_course_school") == null ? "" : context.getParameter("cu_course_school");
        String cu_course_venue = context.getParameter("cu_course_venue") == null ? "" : context.getParameter("cu_course_venue");
        String cu_course_duration = context.getParameter("cu_course_duration") == null ? "" : context.getParameter("cu_course_duration");
        String cu_course_fee_in_figures = context.getParameter("cu_course_fee_in_figures") == null ? "" : context.getParameter("cu_course_fee_in_figures").trim();
        String cu_course_fee_in_dollars = context.getParameter("cu_course_fee_in_dollars") == null ? "" : context.getParameter("cu_course_fee_in_dollars").trim();
        String cu_course_requirements = context.getParameter("cu_course_requirements") == null ? "" : context.getParameter("cu_course_requirements");
        String cu_ielt_required = context.getParameter("cu_ielt_required") == null ? "" : context.getParameter("cu_ielt_required");
        String cu_course_summary = context.getParameter("cu_course_summary") == null ? "" : context.getParameter("cu_course_summary");
        String cu_course_module = context.getParameter("cu_course_module") == null ? "" : context.getParameter("cu_course_module");

        //System.out.println(" cu_course_type  =" + cu_course_type);
        //System.out.println(" cu_course_subject = " + cu_course_subject);
        //System.out.println(" cu_course_name = " + cu_course_name);
        //System.out.println(" subjectIdParameter =  " + subjectIdParameter);
        //System.out.println(" courseTypeIdParameter = " + courseTypeIdParameter);
        //System.out.println(" schoolIdParameter = " + schoolIdParameter);
        if (cu_course_type.isEmpty() || cu_course_subject.isEmpty() || cu_course_name.isEmpty()
                || (cu_course_school.isEmpty())) {
            flashScope.error("Please Enter valid  values in the required fields.");
            //System.out.println("$$$$$$$$$$$$$$$$$$$$$ returning because parameter is empty ");
            return handleShowEditCourse(flashScope, context);
        }

        //System.out.println(" 3 ");
        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_CREATE_OR_UPDATE_COURSE_DETAIL;
        String apiKey = SkoolaConstants.API_KEY;

        //System.out.println(" 4 ");
        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("courseId", courseId);
            jsonObject.put("cu_course_name", cu_course_name);
            jsonObject.put("cu_course_type", cu_course_type);
            jsonObject.put("cu_course_subject_group", cu_course_subject_group);
            jsonObject.put("cu_course_subject", cu_course_subject);
            jsonObject.put("cu_course_school", cu_course_school);
            jsonObject.put("cu_course_venue", cu_course_venue);
            jsonObject.put("cu_course_duration", cu_course_duration);
            jsonObject.put("cu_course_fee_in_figures", cu_course_fee_in_figures);
            jsonObject.put("cu_course_fee_in_dollars", cu_course_fee_in_dollars);
            jsonObject.put("cu_course_requirements", cu_course_requirements);
            jsonObject.put("cu_ielt_required", cu_ielt_required);
            jsonObject.put("cu_course_summary", cu_course_summary);
            jsonObject.put("cu_course_module", cu_course_module);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return handleShowEditCourse(flashScope, context);
        }

        //System.out.println(" 5 ");
        requestObject.setData(jsonObject.toString());
        String requestString = new Gson().toJson(requestObject);

        String response = BackEndAccountHandler.createOrUpdateSkoolaCourse(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        //System.out.println(" 6 ");
        if (!responseObject.getSuccessful()) {
            flashScope.error(responseObject.getMessage());
            return handleShowEditCourse(flashScope, context);
        }

        //System.out.println(" 7 ");
        if (operation.equalsIgnoreCase("edit")) {
            flashScope.success("Course was updated successfully !!!");
        } else if (operation.equalsIgnoreCase("create")) {
            flashScope.success("Course was created successfully !!!");
        }

        result.render("searchIndex", searchIndex);
        result.render("status", status);
        result.render("subjectIdParameter", subjectIdParameter);
        result.render("courseTypeIdParameter", courseTypeIdParameter);
        result.render("schoolIdParameter", schoolIdParameter);
        result.render("courseId", courseId);
        return handleShowSkoolaCourses(flashScope, context);
    }

    public Result handleGetSkoolaCourseSubjetBySubjectCategoryId(FlashScope flashScope, Context context) {

        String courseCategoryId = context.getParameter("courseGroupId");
        AjaxResponse ajaxResponse = new AjaxResponse();
        ajaxResponse.setSuccessful(Boolean.FALSE);
        System.out.println(" course category id  = " + courseCategoryId);
        Collection<Subject> subjectCol = CustomService.getSkoolaSubjectBySubjectCategoryId(Long.valueOf(courseCategoryId));

        if (subjectCol != null && !subjectCol.isEmpty()) {

            JSONArray jsonArray = new JSONArray();
            for (Subject subject : subjectCol) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("text", subject.getName() == null ? "" : subject.getName());
                jsonObject.put("value", subject.getId().toString());

                jsonArray.put(jsonObject);
            }

            ajaxResponse.setSuccessful(Boolean.TRUE);
            ajaxResponse.setResult(jsonArray.toString());
        }

        return Results.json().render(ajaxResponse);
    }

    public Result handleShowSkoolaCourseSubjects(FlashScope flashScope, Context context) {
        log.info(" ############### inside show skoola subjects ");
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, context.getSession());
        String SEARCH_MAXIMUM_RESULTSET_SIZE = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String courseCategoryId = context.getParameter("courseCategoryId") == null ? "" : context.getParameter("courseCategoryId");

        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        Long si = Long.valueOf(searchIndex);
        Long courseCatId = null;

        if (!courseCategoryId.isEmpty()) {
            courseCatId = Long.valueOf(courseCategoryId);
        }

        Collection<Subject> resultSetCol = CustomService.getSkoolaCourseSubjectByCourseCategoryIdAndSearchIndexParameter(courseCatId, si);
        Long searchResultCount = CustomService.getCountOfSkoolaCourseSubjectByCourseCategoryId(courseCatId);

        log.info("%%%%%%%%%%%%%%%%%%%%%%%% searchResultCount = " + searchResultCount);
        if (resultSetCol == null || resultSetCol.isEmpty()) {
            flashScope.error("An error occured, please contact support.");
            return handleShowAdminDashBoard(flashScope, context);
        }

        if (searchResultCount == null) {
            searchResultCount = 0l;
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for (Subject subject : resultSetCol) {
            JSONObject jObj = new JSONObject();
            jObj.put("name", subject.getName());
            jObj.put("id", subject.getId().toString());

            jsonArray.put(jObj);
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("searchResultCount", searchResultCount);
        jsonObject.put("courseCategoryId", courseCategoryId);
        jsonObject.put("SEARCH_MAXIMUM_RESULTSET_SIZE", SEARCH_MAXIMUM_RESULTSET_SIZE);
        jsonObject.put("result", jsonArray);
        jsonObject.put("status", "successful");
        result.render("json", jsonObject);

        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_COURSE_SUBJECT).render(jsonObject);

    }

    public Result handleEditSkoolaCourseSubject(FlashScope flashScope, Context context) {
        // log.info("$$$$  inside edit skoola subjects ");
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, context.getSession());

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String courseCategoryId = context.getParameter("courseCategoryId") == null ? "" : context.getParameter("courseCategoryId");
        String subjectId = context.getParameter("subjectId") == null ? "" : context.getParameter("subjectId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");
        log.info("edit_mode = " + edit_mode);
        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        JSONObject subjectObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        if (edit_mode.equalsIgnoreCase("true")) {
            if (subjectId == null || subjectId.isEmpty()) {
                flashScope.error("An error occured, please contact support.");
                return handleShowSkoolaCourseSubjects(flashScope, context);
            }

            Subject subject = (Subject) CustomService.service.getRecordById(Subject.class, Long.valueOf(subjectId));

            if (subject == null) {
                flashScope.error("Please select a valid Subject before you edit.");
                return handleShowSkoolaCourseSubjects(flashScope, context);
            }

            SubjectGroup subjectGroup = (SubjectGroup) CustomService.service.getRecordById(SubjectGroup.class, Long.valueOf(subject.getSubjectGroup().getId()));

            String code = subject.getCode() == null ? "" : subject.getCode();
            String description = subject.getDescription() == null ? "" : subject.getDescription();
            String displayName = subject.getDisplayName() == null ? "" : subject.getDisplayName();
            Boolean isActive = subject.getIsActive() == null ? false : subject.getIsActive();
            String name = subject.getName() == null ? "" : subject.getName();
            Long subjectGroupId = subjectGroup == null ? 0L : subjectGroup.getId();

            subjectObject.put("code", code);
            subjectObject.put("description", description);
            subjectObject.put("isActive", isActive);
            subjectObject.put("name", WordUtils.capitalizeFully(name));
            subjectObject.put("displayName", WordUtils.capitalizeFully(displayName));
            subjectObject.put("subjectGroupId", subjectGroupId.toString());

            jsonObject.put("subjectObject", subjectObject);
        } else {
            jsonObject.put("subjectObject", "");
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("subjectId", subjectId);
        jsonObject.put("courseCategoryId", courseCategoryId);

        jsonObject.put("status", "successful");
        jsonObject.put("edit_mode", edit_mode);
        result.render("json", jsonObject);
        // log.info("@@@@"  + edit_mode);
        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_COURSE_EDIT_SUBJECT).render(jsonObject);

    }

    public Result handleCreateORUpdateSkoolaSubject(FlashScope flashScope, Context context) {

        Session session = context.getSession();
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String subjectId = context.getParameter("subjectId") == null ? "" : context.getParameter("subjectId");
        String courseCategoryId = context.getParameter("courseCategoryId") == null ? "" : context.getParameter("courseCategoryId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");

        String subject_category = context.getParameter("subject_category") == null ? "" : context.getParameter("subject_category");
        String subject_name = context.getParameter("subject_name") == null ? "" : context.getParameter("subject_name");
        String display_name = context.getParameter("display_name") == null ? "" : context.getParameter("display_name");
        String subject_description = context.getParameter("subject_description") == null ? "" : context.getParameter("subject_description");
        Boolean isActive = context.getParameter("subject_active") == null ? false : (context.getParameter("subject_active").equalsIgnoreCase("1") ? true : false);

        String code = context.getParameter("code") == null ? "" : context.getParameter("code");

        if (subject_category.isEmpty() || subject_name.isEmpty() || edit_mode.isEmpty()) {
            flashScope.error("Please Enter valid values in the required fields.");
            return handleShowEditCourse(flashScope, context);
        }

        Subject subject = null;

        if (edit_mode.equalsIgnoreCase("edit")) {
            subject = (Subject) CustomService.service.getRecordById(Subject.class, Long.valueOf(subjectId));
        } else {
            subject = new Subject();
        }

        SubjectGroup subjectGroup = (SubjectGroup) CustomService.service.getRecordById(SubjectGroup.class, Long.valueOf(subject_category));
        if (subjectGroup == null) {
            flashScope.error("Please Enter valid  values in the required fields.");
            return handleShowEditCourse(flashScope, context);
        }

        subject.setCode(code);
        subject.setDescription(subject_description);
        subject.setDisplayName(display_name.trim());
        subject.setIsActive(isActive);
        subject.setName(subject_name.trim());
        subject.setSubjectGroup(subjectGroup);

        if (edit_mode.equalsIgnoreCase("create")) {
            subject = (Subject) CustomService.service.createNewRecord(subject);
        } else {
            CustomService.service.updateRecord(subject);
        }

        if (edit_mode.equalsIgnoreCase("create")) {
            flashScope.success("Subject was created successfully !!!");
        } else {
            flashScope.success("Subject was updated successfully !!!");
        }

        result.render("searchIndex", searchIndex);
        result.render("courseCategoryId", courseCategoryId);
        return handleShowSkoolaCourseSubjects(flashScope, context);
    }

    public Result handleShowSkoolaCourseSubjectCategory(FlashScope flashScope, Context context) {
        log.info(" inside show skoola subject categories ");
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        String SEARCH_MAXIMUM_RESULTSET_SIZE = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");

        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        Long si = Long.valueOf(searchIndex);

        Collection<SubjectGroup> resultSetCol = CustomService.service.getAllRecords(SubjectGroup.class, Integer.valueOf(searchIndex), Integer.valueOf(SEARCH_MAXIMUM_RESULTSET_SIZE));
        Long searchResultCount = CustomService.getCountOfSkoolaCourseSubjectGroup();

        if (resultSetCol == null || resultSetCol.isEmpty()) {
            flashScope.error("An error occured, please contact support.");
            return handleShowAdminDashBoard(flashScope, context);
        }

        if (searchResultCount == null) {
            searchResultCount = 0l;
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for (SubjectGroup subjectGroup : resultSetCol) {
            JSONObject jObj = new JSONObject();
            jObj.put("name", subjectGroup.getName());
            jObj.put("id", subjectGroup.getId().toString());
            jObj.put("description", subjectGroup.getDescription());

            jsonArray.put(jObj);
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("searchResultCount", searchResultCount);
        jsonObject.put("SEARCH_MAXIMUM_RESULTSET_SIZE", SEARCH_MAXIMUM_RESULTSET_SIZE);
        jsonObject.put("result", jsonArray);
        jsonObject.put("status", "successful");

        result.render("json", jsonObject);
        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_COURSE_SUBJECT_CATEGORY);

    }

    public Result handleEditSkoolaCourseSubjectCategory(FlashScope flashScope, Context context) {

        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, context.getSession());

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String subjectGroupId = context.getParameter("subjectGroupId") == null ? "" : context.getParameter("subjectGroupId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");
        log.info("edit_mode = " + edit_mode);
        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        JSONObject subjectGroupObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        if (edit_mode.equalsIgnoreCase("true")) {
            if (subjectGroupId == null || subjectGroupId.isEmpty()) {
                flashScope.error("An error occured, please contact support.");
                return handleShowSkoolaCourseSubjectCategory(flashScope, context);
            }

            SubjectGroup subjectGroup = (SubjectGroup) CustomService.service.getRecordById(SubjectGroup.class, Long.valueOf(subjectGroupId));

            if (subjectGroup == null) {
                flashScope.error("Please select a valid Subject before you edit.");
                return handleShowSkoolaCourseSubjectCategory(flashScope, context);
            }

            String description = subjectGroup.getDescription() == null ? "" : subjectGroup.getDescription();
            String displayName = subjectGroup.getDisplayName() == null ? "" : subjectGroup.getDisplayName();
            String name = subjectGroup.getName() == null ? "" : subjectGroup.getName();

            subjectGroupObject.put("description", description);
            subjectGroupObject.put("name", WordUtils.capitalizeFully(name));
            subjectGroupObject.put("displayName", WordUtils.capitalizeFully(displayName));
            subjectGroupObject.put("subjectGroupId", subjectGroupId.toString());

            jsonObject.put("subjectObject", subjectGroupObject);
        } else {
            jsonObject.put("subjectObject", "");
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("subjectGroupId", subjectGroupId);

        jsonObject.put("status", "successful");
        jsonObject.put("edit_mode", edit_mode);
        result.render("json", jsonObject);

        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_COURSE_EDIT_SUBJECT_CATEGORY).render(jsonObject);

    }

    public Result handleCreateORUpdateSkoolaSubjectCategory(FlashScope flashScope, Context context) {

        Session session = context.getSession();
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String subjectGroupId = context.getParameter("subjectGroupId") == null ? "" : context.getParameter("subjectGroupId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");

        String subject_group_name = context.getParameter("subject_group_name") == null ? "" : context.getParameter("subject_group_name");
        String display_name = context.getParameter("display_name") == null ? "" : context.getParameter("display_name");
        String subject_description = context.getParameter("subject_description") == null ? "" : context.getParameter("subject_description");

        if (subject_group_name.isEmpty() || edit_mode.isEmpty()) {
            flashScope.error("Please Enter valid values in the required fields.");
            return handleShowSkoolaCourseSubjectCategory(flashScope, context);
        }

        SubjectGroup subjectGroup = null;

        if (edit_mode.equalsIgnoreCase("edit")) {
            subjectGroup = (SubjectGroup) CustomService.service.getRecordById(SubjectGroup.class, Long.valueOf(subjectGroupId));
        } else {
            subjectGroup = new SubjectGroup();
        }

        subjectGroup.setDescription(subject_description);
        subjectGroup.setDisplayName(display_name.trim());
        subjectGroup.setName(subject_group_name.trim());

        if (edit_mode.equalsIgnoreCase("create")) {
            subjectGroup = (SubjectGroup) CustomService.service.createNewRecord(subjectGroup);
        } else {
            CustomService.service.updateRecord(subjectGroup);
        }

        if (edit_mode.equalsIgnoreCase("create")) {
            flashScope.success("Subject was created successfully !!!");
        } else {
            flashScope.success("Subject was updated successfully !!!");
        }

        result.render("searchIndex", searchIndex);
        return handleShowSkoolaCourseSubjectCategory(flashScope, context);
    }

    public Result handleShowSkoolaCountries(FlashScope flashScope, Context context) {
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        String SEARCH_MAXIMUM_RESULTSET_SIZE = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");

        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;

        Collection<Country> resultSetCol = CustomService.service.getAllRecords(Country.class, Integer.valueOf(searchIndex), Integer.valueOf(SEARCH_MAXIMUM_RESULTSET_SIZE));
        Long searchResultCount = CustomService.getCountOfSkoolaCountries();

        if (resultSetCol == null || resultSetCol.isEmpty()) {
            flashScope.error("An error occured, please contact support.");
            return handleShowAdminDashBoard(flashScope, context);
        }

        if (searchResultCount == null) {
            searchResultCount = 0l;
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for (Country country : resultSetCol) {
            JSONObject jObj = new JSONObject();
            jObj.put("name", country.getName());
            jObj.put("id", country.getId().toString());
            jObj.put("code", country.getCode() == null ? "" : country.getCode());
            jsonArray.put(jObj);
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("searchResultCount", searchResultCount);
        jsonObject.put("SEARCH_MAXIMUM_RESULTSET_SIZE", SEARCH_MAXIMUM_RESULTSET_SIZE);
        jsonObject.put("result", jsonArray);

        result.render("json", jsonObject);
        result.template(SkoolaConstants.ADMIN_SKOOLA_COUNTRIES);
        return result;
    }

    public Result handleEditSkoolaCountry(FlashScope flashScope, Context context) {

        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, context.getSession());

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String countryId = context.getParameter("countryId") == null ? "" : context.getParameter("countryId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");
        log.info("edit_mode = " + edit_mode);
        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        JSONObject countryJSONObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        if (edit_mode.equalsIgnoreCase("true")) {
            if (countryId == null || countryId.isEmpty()) {
                flashScope.error("An error occured, please contact support.");
                return handleShowSkoolaCountries(flashScope, context);
            }

            Country country = (Country) CustomService.service.getRecordById(Country.class, Long.valueOf(countryId));

            if (country == null) {
                flashScope.error("Please select a valid Subject before you edit.");
                return handleShowSkoolaCountries(flashScope, context);
            }

            String name = country.getName() == null ? "" : country.getName();
            String code = country.getCode() == null ? "" : country.getCode();
            String livingConditions = country.getLivingConditions() == null ? "" : country.getLivingConditions();
            String studyInformation = country.getStudyInformation() == null ? "" : country.getStudyInformation();
            String aboutCountryInformation = country.getAboutCountryInfomation() == null ? "" : country.getAboutCountryInfomation();
            Integer quickFactPopulationFigure = country.getQuickFactPopulationFigure() == null ? 0 : country.getQuickFactPopulationFigure();
            Integer quickFactInternationalStudents = country.getQuickFactInternationStudents() == null ? 0 : country.getQuickFactInternationStudents();
            Integer quickFactCourses = country.getQuickFactCourses() == null ? 0 : country.getQuickFactCourses();
            Integer quickFactInstitutions = country.getQuickFactInstitutions() == null ? 0 : country.getQuickFactInstitutions();

            countryJSONObject.put("name", WordUtils.capitalizeFully(name));
            countryJSONObject.put("code", code);
            countryJSONObject.put("livingConditions", livingConditions);
            countryJSONObject.put("studyInformation", studyInformation);
            countryJSONObject.put("aboutCountryInformation", aboutCountryInformation);
            countryJSONObject.put("quickFactPopulationFigure", quickFactPopulationFigure);
            countryJSONObject.put("quickFactInternationalStudents", quickFactInternationalStudents);
            countryJSONObject.put("quickFactCourses", quickFactCourses);
            countryJSONObject.put("quickFactInstitutions", quickFactInstitutions);

            jsonObject.put("countryObject", countryJSONObject);
        } else {
            jsonObject.put("countryObject", "");
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("countryId", countryId);

        jsonObject.put("status", "successful");
        jsonObject.put("edit_mode", edit_mode);
        result.render("json", jsonObject);

        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_EDIT_COUNTRY).render(jsonObject);

    }

    public Result handleCreateORUpdateSkoolaCountry(FlashScope flashScope, Context context) {

        Session session = context.getSession();
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String countryId = context.getParameter("countryId") == null ? "" : context.getParameter("countryId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");

        String name = context.getParameter("name") == null ? "" : context.getParameter("name");
        String code = context.getParameter("code") == null ? "" : context.getParameter("code");
        String livingConditions = context.getParameter("livingConditions") == null ? "" : context.getParameter("livingConditions");
        String studyInformation = context.getParameter("studyInformation") == null ? "" : context.getParameter("studyInformation");
        String aboutCountryInformation = context.getParameter("aboutCountryInformation") == null ? "" : context.getParameter("aboutCountryInformation");
        Integer quickFactPopulationFigure = context.getParameterAsInteger("quickFactPopulationFigure") == null ? 0 : context.getParameterAsInteger("quickFactPopulationFigure");
        Integer quickFactInternationalStudents = context.getParameterAsInteger("quickFactInternationalStudents") == null ? 0 : context.getParameterAsInteger("quickFactInternationalStudents");
        Integer quickFactCourses = context.getParameterAsInteger("quickFactCourses") == null ? 0 : context.getParameterAsInteger("quickFactCourses");
        Integer quickFactInstitutions = context.getParameterAsInteger("quickFactInstitutions") == null ? 0 : context.getParameterAsInteger("quickFactInstitutions");

        if (name.isEmpty() || edit_mode.isEmpty()) {
            flashScope.error("Please Enter valid values in the required fields.");
            return handleShowSkoolaCountries(flashScope, context);
        }

        Country country = null;

        if (edit_mode.equalsIgnoreCase("edit")) {
            country = (Country) CustomService.service.getRecordById(Country.class, Long.valueOf(countryId));
        } else {
            country = new Country();
        }

        country.setAboutCountryInfomation(aboutCountryInformation);
        country.setCode(code.trim());
        country.setLivingConditions(livingConditions);
        country.setName(name.trim());
        country.setQuickFactCourses(quickFactCourses);
        country.setQuickFactInstitutions(quickFactInstitutions);
        country.setQuickFactInternationStudents(quickFactInternationalStudents);
        country.setQuickFactPopulationFigure(quickFactPopulationFigure);
        country.setStudyInformation(studyInformation);

        if (edit_mode.equalsIgnoreCase("create")) {
            country = (Country) CustomService.service.createNewRecord(country);
        } else {
            CustomService.service.updateRecord(country);
        }

        if (edit_mode.equalsIgnoreCase("create")) {
            flashScope.success("Country was created successfully !!!");
        } else {
            flashScope.success("Country was updated successfully !!!");
        }

        result.render("searchIndex", searchIndex);
        return handleShowSkoolaCountries(flashScope, context);
    }
    
    
     public Result handleShowSkoolaUniversities(FlashScope flashScope, Context context) {

        // log.info(" inside handle show handleShowSkoolaUniversities");
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCountriesInJSONFormat(result, flashScope, context.getSession());
        String SEARCH_MAXIMUM_RESULTSET_SIZE = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String countryId = context.getParameter("countryId") == null ? "" : context.getParameter("countryId");
        
        log.info(" country $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ = " + countryId );
         log.info(" searchIndex $$$$$$$$$$$$$$$$$$$$$$$$$$$4 = " + searchIndex );
        
        Long countryIdValue = null;
        if(countryId != null && !countryId.isEmpty()){
            countryIdValue = Long.valueOf(countryId);
        }

        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;

        Collection<School> resultSetCol = CustomService.getSchoolsByCountryIdAndSearchIndex(countryIdValue, Integer.valueOf(searchIndex));
        Long searchResultCount = CustomService.getSchoolsCountByCountryId(countryIdValue);

        if (resultSetCol == null || resultSetCol.isEmpty()) {
            flashScope.error("An error occured, please contact support.");
            return handleShowAdminDashBoard(flashScope, context);
        }

        if (searchResultCount == null) {
            searchResultCount = 0l;
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for (School school : resultSetCol) {
            Country country = null;
            if(school.getCountry() != null){
                country = (Country) CustomService.service.getRecordById(Country.class, school.getCountry().getId());
            }
            
            JSONObject jObj = new JSONObject();
            jObj.put("name", school.getName());
            jObj.put("id", school.getId().toString());
            jObj.put("country",country == null ? "" : country.getName());
            jObj.put("universityWebSiteURL", school.getUniversityWebsiteURL() == null ? "N/A" : school.getUniversityWebsiteURL());
            jsonArray.put(jObj);
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("countryId",countryId);
        jsonObject.put("searchResultCount", searchResultCount);
        jsonObject.put("SEARCH_MAXIMUM_RESULTSET_SIZE", SEARCH_MAXIMUM_RESULTSET_SIZE);
        jsonObject.put("result", jsonArray);

        result.render("json", jsonObject);
        result.template(SkoolaConstants.ADMIN_SKOOLA_UNIVERSITIES);
        return result;
  
    }
     
     
      public Result handleEditSkoolaSchool(FlashScope flashScope, Context context) {

        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCountriesInJSONFormat(result, flashScope, context.getSession());

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String schoolId = context.getParameter("schoolId") == null ? "" : context.getParameter("schoolId");
        String countryId = context.getParameter("countryId") == null ? "" : context.getParameter("countryId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");
        log.info("edit_mode = " + edit_mode);
        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        JSONObject schoolJSONObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        if (edit_mode.equalsIgnoreCase("true")) {
            if (schoolId == null || schoolId.isEmpty()) {
                flashScope.error("An error occured, please contact support.");
                return handleShowSkoolaUniversities(flashScope, context);
            }

            School school = (School) CustomService.service.getRecordById(School.class, Long.valueOf(schoolId));

            if (school == null) {
                flashScope.error("Please select a valid School before you edit.");
                return handleShowSkoolaUniversities(flashScope, context);
            }
            
            Country schoolCountry = null;
            
            if(school.getCountry() != null){
                schoolCountry = (Country) CustomService.service.getRecordById(Country.class, school.getCountry().getId());
            }

            String name = school.getName() == null ? "N/A" : school.getName();
            String address = school.getAddress() == null ? "N/A" : school.getAddress();
            String yearEstablished = school.getYearEstablished() == null ? "N/A" : school.getYearEstablished();
            String logoUrl = school.getLogoUrl() == null ? "N/A" : school.getLogoUrl();
            String universityType = school.getUniversityType() == null ? "N/A" : school.getUniversityType();
            String universitySetting = school.getUniversitySetting() == null ? "N/A" : school.getUniversitySetting();
            String highestDegreeOffered = school.getHighestDegreeOffered() == null ? "N/A" : school.getHighestDegreeOffered();
            String region = school.getRegion() == null ? "N/A" : school.getRegion();
            String semester = school.getSemester() == null ? "N/A" : school.getSemester();
            String overview = school.getOverview() == null ? "N/A" : school.getOverview();
            String averageFees = school.getAverageFees() == null ? "N/A" : school.getAverageFees();
            String generalRequirements = school.getGeneralRequirements() == null ? "N/A" : school.getGeneralRequirements();
            String universityWebsiteURL = school.getUniversityWebsiteURL() == null ? "N/A" : school.getUniversityWebsiteURL();
            String schoolCountryId = schoolCountry == null ? "" : schoolCountry.getId().toString();
         
            schoolJSONObject.put("name", WordUtils.capitalizeFully(name));
            schoolJSONObject.put("address", address);
            schoolJSONObject.put("yearEstablished", yearEstablished);
            schoolJSONObject.put("logoUrl", logoUrl);
            schoolJSONObject.put("universityType", universityType);
            schoolJSONObject.put("universitySetting", universitySetting);
            schoolJSONObject.put("highestDegreeOffered", highestDegreeOffered);
            schoolJSONObject.put("region", region);
            schoolJSONObject.put("semester", semester);
            schoolJSONObject.put("overview", overview);
            schoolJSONObject.put("averageFees", averageFees);
            schoolJSONObject.put("generalRequirements", generalRequirements);
            schoolJSONObject.put("universityWebsiteURL", universityWebsiteURL);
            schoolJSONObject.put("schoolCountryId", schoolCountryId);

            jsonObject.put("schoolJSONObject", schoolJSONObject);
        } else {
            jsonObject.put("schoolJSONObject", "");
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("countryId", countryId);
        jsonObject.put("schoolId",schoolId);

        jsonObject.put("status", "successful");
        jsonObject.put("edit_mode", edit_mode);
        result.render("json", jsonObject);

        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_EDIT_SCHOOL).render(jsonObject);

    }
      
      
      
     public Result handleCreateORUpdateSkoolaSchools(FlashScope flashScope, Context context) {

        Session session = context.getSession();
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        result = getAllSkoolaCourseSubjectCategories(result, flashScope, session);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String countryId = context.getParameter("countryId") == null ? "" : context.getParameter("countryId");
        String schoolId = context.getParameter("schoolId") == null ? "" : context.getParameter("schoolId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");

         String name = context.getParameter("name") == null ? "" : context.getParameter("name");
         String address = context.getParameter("address") == null ? "" : context.getParameter("address");
         String countryIdElement = context.getParameter("countryIdElement") == null ? "" : context.getParameter("countryIdElement");
         String yearEstablished = context.getParameter("yearEstablished") == null ? "" : context.getParameter("yearEstablished");
         String logoUrl = context.getParameter("logoUrl") == null ? "" : context.getParameter("logoUrl");
         String universityWebSiteURL = context.getParameter("universityWebSiteURL") == null ? "" : context.getParameter("universityWebSiteURL");
         String universityType = context.getParameter("universityType") == null ? "" : context.getParameter("universityType");
         String highestDegreeOffered = context.getParameter("highestDegreeOffered") == null ? "" : context.getParameter("highestDegreeOffered");
         String averageFees = context.getParameter("averageFees") == null ? "" : context.getParameter("averageFees");
         String region = context.getParameter("region") == null ? "" : context.getParameter("region");
         String semester = context.getParameter("semester") == null ? "" : context.getParameter("semester");
         String generalRequirements = context.getParameter("generalRequirements") == null ? "" : context.getParameter("generalRequirements");
         String overview = context.getParameter("overview") == null ? "" : context.getParameter("overview");
         String universitySetting = context.getParameter("universitySetting") == null ? "" : context.getParameter("universitySetting");

         log.info(" logo url = " + logoUrl);
         log.info("universityWebSiteURL " + universityWebSiteURL);
         log.info(" edit_mode" + edit_mode);
        if (name.isEmpty() || logoUrl.isEmpty() || universityWebSiteURL.isEmpty() || edit_mode.isEmpty() ) {
            flashScope.error("Please Enter valid values in the required fields.");
            return handleShowSkoolaUniversities(flashScope, context);
        }
        
        Country country  = (Country) CustomService.service.getRecordById(Country.class, Long.valueOf(countryIdElement));
        
        if(country == null){
            flashScope.error("Please Select a valid country.");
            return handleShowSkoolaUniversities(flashScope, context);
        }

        School school = null;

        if (edit_mode.equalsIgnoreCase("edit")) {
            school = (School) CustomService.service.getRecordById(School.class, Long.valueOf(schoolId));
        } else {
            school = new School();
        }

       school.setAddress(address);
       school.setAverageFees(averageFees);
       school.setCountry(country);
       school.setGeneralRequirements(generalRequirements);
       school.setHighestDegreeOffered(highestDegreeOffered);
       school.setLogoUrl(logoUrl);
       school.setName(name.trim());
       school.setOverview(overview);
       school.setRegion(region);
       school.setSemester(semester);
       school.setUniversitySetting(universitySetting);
       school.setUniversityType(universityType);
       school.setUniversityWebsiteURL(universityWebSiteURL);
       school.setYearEstablished(yearEstablished);
       

        if (edit_mode.equalsIgnoreCase("create")) {
            school = (School) CustomService.service.createNewRecord(school);
        } else {
            CustomService.service.updateRecord(school);
        }

        if (edit_mode.equalsIgnoreCase("create")) {
            flashScope.success("School was created successfully !!!");
        } else {
            flashScope.success("School was updated successfully !!!");
        }

        result.render("countryId",countryId);
        result.render("searchIndex", searchIndex);
        return handleShowSkoolaUniversities(flashScope, context);
    }
     
     
      public Result handleShowSkoolaExams(FlashScope flashScope, Context context) {
        log.info("  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$inside show exams ");
        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
        String SEARCH_MAXIMUM_RESULTSET_SIZE = CustomService.getSettingValue("SEARCH_MAXIMUM_RESULTSET_SIZE", "10", true);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;

        Collection<Exam> resultSetCol = CustomService.service.getAllRecords(Exam.class,Integer.valueOf(searchIndex), Integer.valueOf(SEARCH_MAXIMUM_RESULTSET_SIZE));
        Long searchResultCount = CustomService.getCountOfSkoolaExams();

        if (resultSetCol == null || resultSetCol.isEmpty()) {
            flashScope.error("Sorry!! no results where found.");
           // return handleShowAdminDashBoard(flashScope, context);
        }

        if (searchResultCount == null) {
            searchResultCount = 0l;
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        if(resultSetCol != null && !resultSetCol.isEmpty()){
            for (Exam exam : resultSetCol) {
                String examFeeInFigures = exam.getExamFeeInFigures() == null ? "" : exam.getExamFeeInFigures();
                String examNameAbbreivated = exam.getExamNameAbbreivated() == null ? "" : exam.getExamNameAbbreivated();
                String examNameInFull = exam.getExamNameInFull() == null ? "" : exam.getExamNameInFull();
                String faqs = exam.getFaqs() == null ? "" : exam.getFaqs();
                String overview = exam.getOverview() == null ? "" : exam.getOverview();
                String testFormat = exam.getTestFormat() == null ? "" : exam.getTestFormat();
                String examFeeInKobo = exam.getExamFeeInKobo() == null ? "" : String.valueOf(exam.getExamFeeInKobo()/100);
                String examId = exam.getId().toString();
                String examActive = exam.getActive() == null ? "false" : exam.getActive() ? "true" : "false";



                JSONObject jObj = new JSONObject();
                jObj.put("examFeeInFigures", examFeeInFigures);
                jObj.put("examNameAbbreivated", examNameAbbreivated);
                jObj.put("examNameInFull", examNameInFull);
                jObj.put("faqs", faqs);
                jObj.put("overview", overview);
                jObj.put("testFormat", testFormat);
                jObj.put("examFeeInKobo", examFeeInKobo);
                jObj.put("examActive", examActive);
                jObj.put("id",examId);

                jsonArray.put(jObj);
            }
        }
        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("searchResultCount", searchResultCount);
        jsonObject.put("SEARCH_MAXIMUM_RESULTSET_SIZE", SEARCH_MAXIMUM_RESULTSET_SIZE);
        jsonObject.put("result", jsonArray);
        result.render("json",jsonObject);
        result.template(SkoolaConstants.ADMIN_SKOOLA_EXAMS);
        log.info("returning ###############################################");
        return result;
    }
      
      
      
      
     public Result handleCreateORUpdateSkoolaExam(FlashScope flashScope, Context context) {

        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);

        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String examId = context.getParameter("examId") == null ? "" : context.getParameter("examId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");

        
        log.info(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% edit mode =" + edit_mode);
         String examFeeInFigures  = context.getParameter("examFeeInFigures") == null ? "" : context.getParameter("examFeeInFigures");
         String examNameAbbreivated  = context.getParameter("examNameAbbreivated") == null ? "" : context.getParameter("examNameAbbreivated");
         String examNameInFull  = context.getParameter("examNameInFull") == null ? "" : context.getParameter("examNameInFull");
         String faqs  = context.getParameter("faqs") == null ? "" : context.getParameter("faqs");
         String overview  = context.getParameter("overview") == null ? "" : context.getParameter("overview");
         String testFormat  = context.getParameter("testFormat") == null ? "" : context.getParameter("testFormat");
         String examFeeInKobo  = context.getParameter("examFeeInKobo") == null ? "" : context.getParameter("examFeeInKobo");
         String emailActive = context.getParameter("emailActive") == null ? "" : context.getParameter("emailActive");
         
         

        if (examNameInFull.isEmpty() || examFeeInKobo.isEmpty() || examNameAbbreivated.isEmpty() || edit_mode.isEmpty() || emailActive.isEmpty() ) {
            flashScope.error("Please Enter valid values in the required fields.");
            return handleShowSkoolaExams(flashScope, context);
        }
        
       try{
            
           Long testAmount = Long.parseLong(examFeeInKobo);
       
       }catch(Exception ex){
           ex.printStackTrace();
            flashScope.error("Please enter a valid amount ");
            return handleShowSkoolaExams(flashScope, context);
       }
       
       
       Boolean email_active = false;
       if(emailActive.equalsIgnoreCase("1")){
           email_active = true;
       }else{
           email_active = false;
       }
       

        Exam exam = null;

        if (edit_mode.equalsIgnoreCase("edit")) {
            exam = (Exam) CustomService.service.getRecordById(Exam.class, Long.valueOf(examId));
        } else {
            exam = new Exam();
            
            Exam testExam = CustomService.getExamByFullName(examNameInFull);
            if(testExam != null){
                 flashScope.error("An Exam with this Name already exists.");
                return handleShowSkoolaExams(flashScope, context);
            }
        }

        exam.setExamFeeInFigures(examFeeInFigures);
        exam.setExamFeeInKobo(Long.valueOf(examFeeInKobo) * 100);
        exam.setExamNameAbbreivated(examNameAbbreivated.toUpperCase().trim());
        exam.setExamNameInFull(WordUtils.capitalizeFully(examNameInFull).trim());
        exam.setFaqs(faqs);
        exam.setOverview(overview);
        exam.setTestFormat(testFormat);
        exam.setActive(email_active);
       

        if (edit_mode.equalsIgnoreCase("create")) {
            exam = (Exam) CustomService.service.createNewRecord(exam);
        } else {
            CustomService.service.updateRecord(exam);
        }

        if (edit_mode.equalsIgnoreCase("create")) {
            flashScope.success("Exam was created successfully !!!");
        } else {
            flashScope.success("Exam was updated successfully !!!");
        }

        result.render("examId",examId);
        result.render("searchIndex", searchIndex);
        return handleShowSkoolaExams(flashScope, context);
    }
     
     
     
      public Result handleEditSkoolaExam(FlashScope flashScope, Context context) {

        Result result = Results.html();
        result = appUtil.renderApplicationSession(result, context, ninjaCache);
      
        String searchIndex = context.getParameter("searchIndex") == null ? "" : context.getParameter("searchIndex");
        String examId = context.getParameter("examId") == null ? "" : context.getParameter("examId");
        String edit_mode = context.getParameter("edit_mode") == null ? "" : context.getParameter("edit_mode");
        log.info("edit_mode = " + edit_mode);
        searchIndex = searchIndex.isEmpty() ? "0" : searchIndex;
        JSONObject examJSONObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        if (edit_mode.equalsIgnoreCase("true")) {
            if (examId == null || examId.isEmpty()) {
                flashScope.error("An error occured, please contact support.");
                return handleShowSkoolaExams(flashScope, context);
            }

            Exam exam = (Exam) CustomService.service.getRecordById(Exam.class, Long.valueOf(examId));

            if (exam == null) {
                flashScope.error("Please select a valid School before you edit.");
                return handleShowSkoolaExams(flashScope, context);
            }
           

            String overview = exam.getOverview() == null ? "N/A" : exam.getOverview();
            String testFormat = exam.getTestFormat() == null ? "N/A" : exam.getTestFormat();
            String examFeeInKobo = exam.getExamFeeInKobo() == null ? "N/A" : String.valueOf(exam.getExamFeeInKobo()/100);
            String faqs = exam.getFaqs() == null ? "N/A" : exam.getFaqs();
            String examNameAbbreviated = exam.getExamNameAbbreivated() == null ? "N/A" : exam.getExamNameAbbreivated();
            String examNameInFull = exam.getExamNameInFull() == null ? "N/A" : exam.getExamNameInFull();
            String examFeeInFigures = exam.getExamFeeInFigures() == null ? "N/A" : exam.getExamFeeInFigures();
            String examActive = exam.getActive() == null ? "false" : exam.getActive() ? "true" : "false";
         
            examJSONObject.put("examNameAbbreviated", examNameAbbreviated.toUpperCase());
            examJSONObject.put("examNameInFull", WordUtils.capitalizeFully(examNameInFull));
            examJSONObject.put("examFeeInFigures", WordUtils.capitalizeFully(examFeeInFigures));
            examJSONObject.put("faqs", faqs);
            examJSONObject.put("examFeeInKobo", examFeeInKobo);
            examJSONObject.put("testFormat", testFormat);
            examJSONObject.put("overview", overview);
            examJSONObject.put("examActive",examActive);
            

            jsonObject.put("examJSONObject", examJSONObject);
        } else {
            jsonObject.put("examJSONObject", "");
        }

        jsonObject.put("searchIndex", searchIndex);
        jsonObject.put("examId",examId);

        jsonObject.put("status", "successful");
        jsonObject.put("edit_mode", edit_mode);
        result.render("json", jsonObject);

        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%555 before edit");
        return result.html().template(SkoolaConstants.ADMIN_SKOOLA_EDIT_EXAM).render(jsonObject);

    }


}

package controllers.account;

import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.cache.NinjaCache;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.utils.NinjaProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pojo.ActiveSessionPojo;
import pojo.ConsultationRequestPojo;
import pojo.CountryPojo;
import pojo.PaymentHistoryPojo;
import pojo.SkoolaJSONRequestObject;
import pojo.SkoolaJSONResponseObject;
import pojo.form.SigninFormPojo;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import common.AppUtil;
import common.SkoolaConstants;
import common.WSUtil;
import controllers.signup.SignUpController;
import dataservice.accounts.UserAccountHandler;

@Singleton
public class AccountController {

    private Logger log = Logger.getLogger(AccountController.class);
    @Inject
    NinjaProperties ninjaProperties;
    @Inject
    NinjaCache ninjaCache;

   // private WSUtil wsUtil = new WSUtil();

    public Result handleShowCurrentUserProfile(FlashScope flashScope, SigninFormPojo signinFormPojo, Context context) {
        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        Result result = Results.html();
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        result.template(SkoolaConstants.ACCOUNT_PROFILE_PAGE);
        result.render(activeSessionPojo);
        result.render(SkoolaConstants.CURRENT_PAGE_KEY, SkoolaConstants.USER_ACCOUNT_PAGE_DASHBOARD);
        return result;

    }

    public Result handleShowMyCareerConsultationRequests(FlashScope flashScope, SigninFormPojo signinFormPojo, Context context) {
        Session session = context.getSession();

        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_CONSULTATION_REQUEST_BY_PORTALUSER_USERID;
        String apiKey = SkoolaConstants.API_KEY;
        Result result = Results.html();
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String userId = activeSessionPojo.getUserUniqueId();
        String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");

        JSONObject jsonRequestObject = new JSONObject();
        try {
            jsonRequestObject.put("userId", userId);
            jsonRequestObject.put("searchIndex", searchIndex);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        requestObject.setData(jsonRequestObject.toString());

        String requestString = new Gson().toJson(requestObject);

        String response = UserAccountHandler.getPaginatedConsultationRequestByPortalUserUserId(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        Collection<ConsultationRequestPojo> consultationRequestCol = new ArrayList<ConsultationRequestPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        try {

            if (responseObject.getSuccessful()) {

                jObject = new JSONObject(responseObject.getData());
                jArray = (JSONArray) jObject.get("consultation_request_result");

                searchIndex = (String) jObject.get("search_index");
                searchResultCount = (String) jObject.get("search_result_count");

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = jArray.getJSONObject(i);

                    ConsultationRequestPojo pojo = new ConsultationRequestPojo();
                    pojo.setConsultationDate((String) jsonObject.get("consultationDate"));
                    pojo.setConsultationMode(jsonObject.getString("consultationMode"));
                    pojo.setConsultationStatus(jsonObject.getString("consultationStatus"));
                    pojo.setDateCreated(jsonObject.getString("dateCreated"));
                    pojo.setCourseName(jsonObject.getString("courseName"));
                    pojo.setId(jsonObject.getString("id"));

                    consultationRequestCol.add(pojo);
                }
                result.render("consultationRequestCol", consultationRequestCol);
            }

            result.template(SkoolaConstants.ACCOUNT_PROFILE_PAGE);
            result.render(activeSessionPojo);
            result.render("searchIndex", searchIndex);
            result.render("SEARCH_MAXIMUM_RESULTSET_SIZE", SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
            result.render("searchResultCount", searchResultCount);
            result.render(SkoolaConstants.CURRENT_PAGE_KEY, SkoolaConstants.USER_ACCOUNT_PAGE_MY_CAREER_CONSULTATION);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;

    }

    public Result handleShowMyPaymentHistory(FlashScope flashScope, SigninFormPojo signinFormPojo, Context context) {

        Session session = context.getSession();

        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        SkoolaJSONRequestObject requestObject = new SkoolaJSONRequestObject();
        String methodUrl = SkoolaConstants.URL_GET_PAGINATED_PAYMENT_HISTORY_BY_PORTALUSER_USERID;
        String apiKey = SkoolaConstants.API_KEY;
        Result result = Results.html();
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);

        requestObject.setApi_key(apiKey);
        requestObject.setData("");
        requestObject.setHash("");

        String userId = activeSessionPojo.getUserUniqueId();
        String searchIndex = context.getParameter("searchIndex") == null ? "0" : context.getParameter("searchIndex");

        JSONObject jsonRequestObject = new JSONObject();
        try {
            jsonRequestObject.put("userId", userId);
            jsonRequestObject.put("searchIndex", searchIndex);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        requestObject.setData(jsonRequestObject.toString());

        String requestString = new Gson().toJson(requestObject);

        String response = UserAccountHandler.getPaginatedPaymentHistoryByPortalUserUserId(requestString);//wsUtil.sendDataToWebservice(ninjaProperties, methodUrl, requestString);
        // log.info(" response  = " + response);
        SkoolaJSONResponseObject responseObject = new Gson().fromJson(response, SkoolaJSONResponseObject.class);

        Collection<PaymentHistoryPojo> paymentHistoryCol = new ArrayList<PaymentHistoryPojo>();

        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();
        String searchResultCount = "0";
        try {

            if (responseObject.getSuccessful()) {
                //flashScope.error(responseObject.getMessage());
                // return Results.html().template(SkoolaConstants.HOME_PAGE);

                jObject = new JSONObject(responseObject.getData());
                jArray = (JSONArray) jObject.get("payment_history_result");
                searchIndex = (String) jObject.get("search_index");
                searchResultCount = (String) jObject.get("search_result_count");

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = jArray.getJSONObject(i);

                    PaymentHistoryPojo pojo = new PaymentHistoryPojo();
                    pojo.setPaymentStatus(jsonObject.getString("paymentStatus"));
                    pojo.setAmountInNaira(jsonObject.getString("amountInNaira"));
                    pojo.setDateCreated(jsonObject.getString("dateCreated"));
                    pojo.setReferenceId(jsonObject.getString("referenceId"));
                    pojo.setProcessName(jsonObject.getString("processName"));
                    pojo.setId(jsonObject.getString("id"));

                    paymentHistoryCol.add(pojo);
                }

                result.render("paymentHistoryCol", paymentHistoryCol);

            }

            result.template(SkoolaConstants.ACCOUNT_PROFILE_PAGE);
            result.render(activeSessionPojo);
            result.render("searchResultCount", searchResultCount);
            result.render("searchIndex", searchIndex);
            result.render("SEARCH_MAXIMUM_RESULTSET_SIZE", SkoolaConstants.SEARCH_MAXIMUM_RESULTSET_SIZE);
            result.render(SkoolaConstants.CURRENT_PAGE_KEY, SkoolaConstants.USER_ACCOUNT_PAGE_MY_PAYMENT_HISTORY);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;

    }

    public Result handleShowMyMessages(FlashScope flashScope, SigninFormPojo signinFormPojo, Context context) {
        Session session = context.getSession();
        ActiveSessionPojo activeSessionPojo = null;
        HashMap<String, ActiveSessionPojo> userSessionMap = (HashMap<String, ActiveSessionPojo>) ninjaCache.get(SkoolaConstants.ACTIVE_SESSION_MAP);
        if (userSessionMap != null) {
            activeSessionPojo = userSessionMap.get(session.getId());
        }

        Result result = Results.html();
        result = new AppUtil().handleRetrieveCourseCategoriesData(result, context, flashScope, ninjaCache, ninjaProperties);
        result.template(SkoolaConstants.ACCOUNT_PROFILE_PAGE);
        result.render(activeSessionPojo);
        result.render(SkoolaConstants.CURRENT_PAGE_KEY, SkoolaConstants.USER_ACCOUNT_PAGE_MY_MESSAGES);
        return result;

    }

}

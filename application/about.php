<?php include('inc/header.php');?>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <div id="imageslider">
            <img src="../public/img/about/word_class_education.jpg" title="World class education at your fingertips"/>
            <img data-src="../public/img/about/bridging_the_gap.jpg" title="Bridging the gaps in modern learning" />
            <img data-src="../public/img/about/go_further_with_less_stress.jpg" title="Go further, with less stress and more help" />
        </div>

        <section class="white_background-spacing">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-md-9 col-sm-12 col-xs-12 col-centered">
                        <div class="section-heading text-center margin_bottom_30"><h2>What kind of education do you dream of?</h2></div>
                        <p>Ignore everything you have ever heard of studying abroad, because most of it has been wrong. You don’t have to worry about the process if you’ve never done it before because we’ll sort it out. It’s not about what we do, it’s about what you want. Imagine pushing all boundaries you could ever think of and doing precisely what you want with your life, and more. Well, technology has made it possible so you no longer have to deal with second best or not extraordinary.</p>

                        <p>At Skoola, you will not pay the exorbitant amounts typically charged by traditional agencies. And you will definitely get more out of our services. How? For starters, our database has courses from 100s of universities around the world, and counting, which puts access to education at your fingertips.</p>

                        <p>We cover everything involved in the study abroad process. This means that we will help you search for schools and courses, prepare your application, apply for your visa, get preparatory exam coaching, write exams, make friends, find a house, get a taxi, collaborate, learn, rinse and repeat! Not enough? well, if you’re a working professional to busy to study abroad but dreaming of a fantastic qualification, we will find you an online course at a world-class university so you do not have to compromise on what matters most in life.</p>

                        <p>Apart from the reduced price, you are at the centre of our business, because we exist to make your life easier. Our customer service is guaranteed to provide detailed information on everything possibly related to studying abroad and education. At the end of the day, we want you to get a world-class education, so we are meticulous and we pay attention.</p>

                    </div>
                </div>
            </div>
        </section>


        <section class="about_no_talk">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-md-9 col-sm-12 col-xs-12 col-centered">
                        <div class="section-heading text-center margin_bottom_30"><h2>No, this is not just talk... </h2></div>
                        <p>We’re not in the business of making false claims. So, how are we innovative? our database of thousands of courses is always being updated, so that whenever you access our platform, you have accurate information. We take our relationship with you very seriously, so we can’t compromise on your trust by giving you wrong or outdated information.</p>
                        <p>Still not convinced that you matter? Every time you call our lines, we won’t keep you waiting more than 3 minutes, before a representative comes through. And emails? 1 hour turnaround times during peak and working hours. At off peak times, give us 6 hours or less. When we say you come first, we mean it.</p>
                     </div>
                </div>
            </div>
        </section>


        <section class="white_background-spacing blue-text">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-md-9 col-sm-12 col-xs-12 col-centered">
                        <div class="section-heading text-center margin_bottom_30"><h2>The Big W’s</h2></div>
                        <p>Everyone is busy and we know you have to get back to work, so here’s the TL;DR version of us:</p>

                        <p><b>Who</b> – Ridiculously honest technology business that cares about people and really shows it.</p>

                        <p><b>What</b> – Our online education platform gives real people access to study abroad services. 10,000+ courses, 500+ universities, and counting.</p>

                        <p><b>When</b> – Whenever you feel like getting a fantastic degree or doing a short course to give you an edge over your colleagues and peers.</p>

                        <p><b>Where</b> – We currently serve all of Nigeria virtually, but if you ever want to stop by, we’ll be here crunching numbers to serve you better: The Waterfront. 12B Admiralty Way, Lekki Phase I, Lagos.</p>

                        <p><b>Why</b> – Well, there should be no limits to what you can achieve in your life. You can do anything you want; a world-class education truly made simple.</p>
                    </div>
                </div>
            </div>
        </section>





        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>






    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>

    <!--Ideal Image Slider-->
    <script src="../public/js/plugins/ideal-image-slider/ideal-image-slider.min.js"></script>
    <script src="../public/js/plugins/ideal-image-slider/extensions/bullet-nav/iis-bullet-nav.js"></script>
    <script src="../public/js/plugins/ideal-image-slider/extensions/captions/iis-captions.js"></script>

    <!--Date Picker-->
    <script src="../public/js/plugins/datepicker/moment.js"></script>
    <script src="../public/js/plugins/datepicker/pikaday.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slider = new IdealImageSlider.Slider('#imageslider');
            slider.addBulletNav();
            slider.addCaptions();
            slider.start();

            var movein_picker = new Pikaday({
                field: document.getElementById('pick-mov-in-date'),
                format: 'DD-MM-YYYY'
            });

            var moveout_picker = new Pikaday({
                field: document.getElementById('pick-mov-out-date'),
                format: 'DD-MM-YYYY'
            });

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            $('.selectbox').selectize({create: false});


            //Bootstrap typeahead
            var courses, remoteHost;
            $.support.cors = true;
            remoteHost = 'http://apps.dev/skoola/application/data/courses.json';
            courses = new Bloodhound({
                //identify: function(o) { return o.id_str; },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                dupDetector: function(a, b) { return a.id_str === b.id_str; },
                prefetch: remoteHost + '?prefetch',
                remote: {
                    url: remoteHost + '?keyword=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            $('.typeahead').typeahead({
                    items: 8,
                    minLength: 1,
                    autoSelect: true,
                    source:courses.ttAdapter()
            });


            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



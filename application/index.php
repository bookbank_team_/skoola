<?php include('inc/header.php');?>

<style>
    .special-select .selectize-dropdown,
    .special-select .selectize-input,
    .special-select .selectize-input input {
        line-height: 28px !important;
    }
</style>

<link href="../public/js/plugins/slick/slick.css" rel="stylesheet">
<link href="../public/js/plugins/slick/slick-theme.css" rel="stylesheet">



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section class="homeSearchBoxContainer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 margin_top_50">
                        <div class="searchBoxMessage">
                            <h1>Study Abroad made <span>simple</span></h1>
                            <p>Find and secure admission from institutions <span>all over the world</span></p>
                        </div>
                        <!--form-->
                        <div class="row row-centered margin_top_20">
                            <div class="col-lg-10 col-md-11 col-centered">
                                <form role="form" method="get" action="search_results.php" class="homeSearchForm">
                                    <div class="row row-centered">
                                        <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 nopadding">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg typeahead" style="display: block;width 100%" name="keyword" data-provide="typeahead" autocomplete="off"  placeholder="Enter Course Name">
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 nopadding margin_bottom_10">
                                            <div class="form-group text-left special-select">
                                                <select class="selectbox " placeholder="Location">
                                                    <option value="">Location</option>
                                                    <option value="1">UK</option>
                                                    <option value="2">US</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-8 col-xs-6 nopadding margin_bottom_10">
                                            <div class="form-group text-left special-select">
                                                <select class="selectbox " placeholder="Choose Study Level">
                                                    <option value="">Choose Study Level</option>
                                                    <option value="1">Postgraduate</option>
                                                    <option value="2">Undergraduate</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 nopadding">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-warning btn-lg"><i class="icon-search"></i> Search</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <!--steps in styuding abroad-->
        <section class="margin_top_30">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 section-heading margin_bottom_20 text-center">
                        <h2>Study Abroad in 3 Simple Steps</h2>
                    </div>

                    <div class="col-lg-4 col-md-4 margin_bottom_10">
                        <div class="steps">
                            <span class="line"></span>
                            <span class="sp_icon">
                                <img src="../public/img/steps-study.png"/>
                            </span>
                            <h3>Create Account</h3>
                            <p>Register for an account and complete your profile with accurate details</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 margin_bottom_10">
                        <div class="steps">
                            <span class="line"></span>
                            <span class="sp_icon">
                                <img src="../public/img/steps-search.png"/>
                            </span>
                            <h3>School Search</h3>
                            <p>Search for courses and programs in 500+ universities around the world</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 margin_bottom_10">
                        <div class="steps">
                            <span class="sp_icon">
                                <img src="../public/img/steps-application.png"/>
                            </span>
                            <h3>Book & Apply</h3>
                            <p>Book an appointment - over the phone or in person - to start your application</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="categoriesSection margin_top_50">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-md-12 col-centered">
                        <div class="categoryHeadings margin_top_40 margin_bottom_20">
                            <h2>Need help picking a course?</h2>
                            <p>You can start here...and we will guide you</p>
                        </div>


                        <div class="row">
                            <div class="col-md-4 eq-col-sm-4 col-sm-4 col-xs-6">
                                <a href="" class="eachCategory">
                                    <img src="../public/img/category-icons/health_medicine.png"/>
                                    <h4>Health & Medicine</h4>
                                    <p>20 Courses</p>
                                    <p>10 Universities</p>
                                </a>
                            </div>
                            <div class="col-md-4 eq-col-sm-4 col-sm-4 col-xs-6">
                                <a href="" class="eachCategory">
                                    <img src="../public/img/category-icons/science.png"/>
                                    <h4>Science</h4>
                                    <p>40 Courses</p>
                                    <p>50 Universities</p>
                                </a>
                            </div>
                            <div class="col-md-4 eq-col-sm-4 col-sm-4 col-xs-6">
                                <a href="" class="eachCategory">
                                    <img src="../public/img/category-icons/engineering.png"/>
                                    <h4>Engineering</h4>
                                    <p>15 Courses</p>
                                    <p>30 Universities</p>
                                </a>
                            </div>
                            <div class="col-md-4 eq-col-sm-4 col-sm-4 col-xs-6">
                                <a href="" class="eachCategory">
                                    <img src="../public/img/category-icons/business.png"/>
                                    <h4>Business</h4>
                                    <p>25 Courses</p>
                                    <p>30 Universities</p>
                                </a>
                            </div>
                            <div class="col-md-4 eq-col-sm-4 col-sm-4 col-xs-6">
                                <a href="" class="eachCategory">
                                    <img src="../public/img/category-icons/creative_arts.png"/>
                                    <h4>Creative Arts</h4>
                                    <p>25 Courses</p>
                                    <p>30 Universities</p>
                                </a>
                            </div>
                            <div class="col-md-4 eq-col-sm-4 col-sm-4 col-xs-6">
                                <a href="" class="eachCategory">
                                    <img src="../public/img/category-icons/social_studies.png"/>
                                    <h4>Social Studies</h4>
                                    <p>35 Courses</p>
                                    <p>40 Universities</p>
                                </a>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </section>


        <section class="where-to-study">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="section-heading text-center margin_bottom_30"><h2>Where would you like to go?</h2></div>

                        <div class="row">

                                <div class="col-md-15 col-sm-4 col-xs-6">
                                    <a href="" class="custom-btn-dark uk">UK</a>
                                </div>
                                <div class="col-md-15 col-sm-4 col-xs-6">
                                    <a href="" class="custom-btn-dark usa">USA</a>
                                </div>
                                <div class="col-md-15 col-sm-4 col-xs-6">
                                    <a href="" class="custom-btn-dark australia">Australia</a>
                                </div>
                                <div class="col-md-15 col-sm-4 col-xs-6">
                                    <a href="" class="custom-btn-dark canada">Canada</a>
                                </div>
                                <div class="col-md-15 col-sm-4 col-xs-12">
                                    <a href="countries.php" class="custom-btn-dark last">More Destinations</a>
                                </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>


        <section class="examSection">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12 margin_top_20">

                        <!--Heading-->
                        <div class="section-heading margin_bottom_30 text-center">
                            <h2>Entry Exams</h2>
                        </div>

                        <div class="slick-carousel">

                            <a href="exam-details.php" class="eachExams">
                                <h4>GRE <small>&reg;</small></h4>
                                <!--<p>Graduate Record Exam</p>-->
                            </a>

                            <a href="exam-details.php" class="eachExams">
                                <h4>GMAT <small>&reg;</small></h4>
                                <!--<p>Graduate Management Admission Test</p>-->
                            </a>

                            <a href="exam-details.php" class="eachExams">
                                <h4>SAT <small>&reg;</small></h4>
                                <!--<p>Scholastic Assessment Test</p>-->
                            </a>

                            <a href="exam-details.php" class="eachExams">
                                <h4>TOEFL <small>&reg;</small></h4>
                                <!--<p>Test of English as a Foreign Language</p>-->
                            </a>

                            <a href="exam-details.php" class="eachExams">
                                <h4>IELTS  <small>&reg;</small></h4>
                                <!--<p>International English Language Testing System</p>-->
                            </a>

                            <a href="exam-details.php" class="eachExams">
                                <h4>PTE <small>&reg;</small></h4>
                                <!--<p>Pearson Tests of English</p>-->
                            </a>

                        </div>

                    </div>
                </div>
            </div>
        </section>



        <section class="advice_tips margin_top_30 margin_bottom_60">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-heading text-center"><h2>Application Services</h2></div>

                        <div class="row margin_top_30 row-centered">

                            <div class="col-md-6 col-sm-8 col-xs-12 col-centered">
                                <div class="row">
                                    <div class="eq-col-sm-6 col-xs-6">
                                        <a href="" class="each-tips">
                                            <img src="../public/img/app-services/preparation.png"/>
                                            <h3>Exam Preparation</h3>
                                        </a>
                                    </div>

                                    <div class="eq-col-sm-6 col-xs-6">
                                        <a href="" class="each-tips">
                                            <img src="../public/img/app-services/accomodation.png"/>
                                            <h3>Accommodation</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="advice_tips margin_top_30 margin_bottom_60">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-heading text-center"><h2>Advice and Tips</h2></div>

                        <div class="row margin_top_30">
                            <div class="eq-col-sm-3 col-xs-6 col-sm-3">
                                <a href="" class="each-tips">
                                    <img src="../public/img/advice-icons/rank.png"/>
                                    <h3>University Rankings</h3>
                                </a>
                            </div>

                            <div class="eq-col-sm-3 col-xs-6 col-sm-3">
                                <a href="" class="each-tips">
                                    <img src="../public/img/advice-icons/guides.png"/>
                                    <h3>Study Abroad Guides</h3>
                                </a>
                            </div>

                            <div class="eq-col-sm-3 col-xs-6 col-sm-3">
                                <a href="" class="each-tips">
                                    <img src="../public/img/advice-icons/info.png"/>
                                    <h3>Country Info</h3>
                                </a>
                            </div>

                            <div class="eq-col-sm-3 col-xs-6 col-sm-3">
                                <a href="" class="each-tips">
                                    <img src="../public/img/advice-icons/help.png"/>
                                    <h3>Application Help</h3>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/bloodhound/bloodhound.min.js"></script>
    <script src="../public/js/plugins/typeahead/bootstrap3-typeahead.min.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>
    <script src="../public/js/plugins/slick/slick.min.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            $('.slick-carousel').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            $('.selectbox').selectize({create: false});


            //Bootstrap typeahead
            var courses, remoteHost;
            $.support.cors = true;
            remoteHost = 'http://apps.dev/skoola/application/data/courses.json';
            courses = new Bloodhound({
                //identify: function(o) { return o.id_str; },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                dupDetector: function(a, b) { return a.id_str === b.id_str; },
                prefetch: remoteHost + '?prefetch',
                remote: {
                    url: remoteHost + '?keyword=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            $('.typeahead').typeahead({
                    items: 8,
                    minLength: 1,
                    autoSelect: true,
                    source:courses.ttAdapter()
            });


            //sticky header
            $(".main-navigation").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



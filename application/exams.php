    <?php include('inc/header.php');?>



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Entry Exams</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="examSection no-background">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12 margin_top_20">

                        <div class="row row-margin">
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>GRE</h4>
                                    <p>Graduate Record Exam</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>GMAT</h4>
                                    <p>Graduate Management Admission Test</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>SAT</h4>
                                    <p>Scholastic Assessment Test</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>TOEFL</h4>
                                    <p>Test of English as a Foreign Language</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>USMLE</h4>
                                    <p>Medical Licensing Examination</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>IELTS</h4>
                                    <p>International English Language Testing System</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>
                            <div class="eq-col-md-4 eq-col-sm-4 margin_bottom_15">
                                <div class="eachExams">
                                    <h4>PTE</h4>
                                    <p>Pearson Tests of English</p>
                                    <a href="exam-details.php">Learn More</a>
                                </div>
                            </div>

                            <div class="eq-col-md-8 eq-col-sm-8 margin_bottom_15">
                                <div class="eachExams" style="background:white;">
                                    <h4>Why Skoola?</h4>
                                    <p>We are the number 1 and most trusted platform for overseas study. Thousands of students use Skoola to find the right schools; make applications & get admitted to the best schools in Europe, US, Asia and South America</p>
                                    <a href="register.php">Sign Up</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/responsiveTabs.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            fakewaffle.responsiveTabs(['xs']);

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



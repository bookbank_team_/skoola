    <?php include('inc/header.php');?>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation-account.php');?>
        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>My Documents / Certificates</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="margin_top_30 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-push-4 margin_bottom_15">

                        <div class="innerContentWrapper">
                            <h4>My Documents <small>- Certificates -</small></h4>

                            <div class="my-document-attachment">
                                <div class="attachment-icon">
                                    <img src="../public/img/account/certificate.png" class="img-responsive"/>
                                </div>
                                <div class="attachment-info">
                                    <h4>Msc. - University of Liverpool</h4>
                                    <div class="r-meta-data">Attached: <span>Nov 12th, 2015</span> | Last Updated: <span>Nov 16th, 2015</span> | Size: <span>540Kb</span></div>
                                    <div>
                                        <form class="inline-block">
                                            <span class="file-input btn btn btn-default btn-file btn-sm">
                                           <i class="icon-edit"></i> Edit <input  name="resume" type="file"></span>
                                        </form>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                            </div>

                            <div class="my-document-attachment">
                                <div class="attachment-icon">
                                    <img src="../public/img/account/certificate.png" class="img-responsive"/>
                                </div>
                                <div class="attachment-info">
                                    <h4>BEng. - University of Ilorin</h4>
                                    <div class="r-meta-data">Attached: <span>Nov 12th, 2015</span> | Last Updated: <span>Nov 16th, 2015</span> | Size: <span>64Kb</span></div>
                                    <div>
                                        <form class="inline-block">
                                            <span class="file-input btn btn btn-default btn-file btn-sm">
                                           <i class="icon-edit"></i> Edit <input  name="resume" type="file"></span>
                                        </form>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                            </div>

                            <div class="addNewDocument text-center">
                                <a href="#" class="addDocBtn"><i class="icon-plus"></i> Add New Certificate</a>
                                <div class="addFormContainer" style="display:none;">
                                    <h4>New Certificate <button class="btn btn-xs btn-default pull-right closeAddFormContainer">Cancel</button></h4>
                                    <form enctype="multipart/form-data">
                                        <div class="form-group">
                                            <input type="text" name="name" id="inputID" class="form-control" placeholder="Certificate Title" required="required" >
                                        </div>
                                        <div class="form-group">
                                        <span class="file-input btn btn btn-default btn-file btn-sm">
                                           <i class="icon-plus"></i> Attach Certificate <input  name="resume" type="file"/>
                                        </span>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-warning">Upload</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--this only shows if the user is yet to upload their resume-->
                            <div class="empty-resume">
                                <img src="../public/img/account/resume.png"/>
                                <p>No certificate is linked to your account yet. Please use the form below to add certificate</p>
                                <form enctype="multipart/form-data">
                                    <div class="form-group">
                                        <input type="text" name="name" id="inputID" class="form-control" placeholder="Certificate Title" required="required" >
                                    </div>
                                    <div class="form-group">
                                        <span class="file-input btn btn btn-default btn-file btn-sm">
                                           <i class="icon-plus"></i> Attach Certificate <input  name="resume" type="file"/>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-warning">Upload</button>
                                    </div>
                                </form>
                            </div>


                        </div>

                    </div>

                    <div class="col-sm-4 col-sm-pull-8">
                        <div class="sideBarBox">
                            <h4>My Documents</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Resume/CV</a></li>
                                <li><a href="">Transcript</a></li>
                                <li><a href="">Passport Datapage</a></li>
                                <li><a href="">Certificates</a></li>
                                <li><a href="">Personal Statement</a></li>
                                <li><a href="">Reference Letters</a></li>

                            </ul>
                            <h4>My Application</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Schedule Application Appointment</a></li>
                                <!--<li><a href=""></a></li>-->
                            </ul>
                            <h4>My Messages</h4>
                            <ul class="side-menu-navigation">

                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <!--<script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>-->
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            $('.changePicture :file').on('fileselect', function(event, numFiles, label) {
                //var input = $(this).parents('.input-group').find(':text'),
                //    log = numFiles > 1 ? numFiles + ' files selected' : label;
                //
                //if( input.length ) {
                //    input.val(log);
                //} else {
                //    if( log ) alert(log);
                //}
            });


            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            $('.addDocBtn').click(function(){
                $(this).fadeOut(function(){$(this).hide()});
                $('.addFormContainer').fadeIn(function(){
                    $('.addFormContainer').show();
                });
            });

            $('.closeAddFormContainer').click(function(){
                $('.addFormContainer').fadeOut('fast', function(){ $('.addFormContainer').hide();});
                $('.addDocBtn').fadeIn(function(){ $('.addDocBtn').show();});

            })

            //$('.selectbox').selectize({create: false});

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



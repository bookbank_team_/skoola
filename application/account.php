    <?php include('inc/header.php');?>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation-account.php');?>
        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Account</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="margin_top_30 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-push-4 margin_bottom_15">

                        <div class="innerContentWrapper">
                            <h4>My Applications</h4>

                            <div class="addNewDocument text-center">
                                <a href="search_results.php" class="addDocBtn"><i class="icon-search"></i> Find Your Favorite Course</a>
                            </div>

                            <div class="my-document-attachment">
                                <div class="attachment-icon">
                                    <img src="../public/img/account/application.png" class="img-responsive"/>
                                </div>
                                <div class="attachment-info">
                                    <h4>Software Engineering <small>University of Liverpool</small></h4>
                                    <div class="r-meta-data">
                                        <span class="label label-primary">PhD</span>
                                        <span class="label label-warning">Application In Progress...</span> |
                                        <span class="label label-success">Application Completed</span> |
                                        <span class="label label-danger">Application Cancelled</span>
                                    </div>
                                    <div class="r-meta-data">
                                        Applied on <span>Nov 3rd, 2015</span>
                                    </div>
                                    <div>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-eye"></i> View</a>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-cancel-circle"></i> Cancel</a>
                                    </div>
                                </div>
                            </div>

                            <div class="my-document-attachment">
                                <div class="attachment-icon">
                                    <img src="../public/img/account/application.png" class="img-responsive"/>
                                </div>
                                <div class="attachment-info">
                                    <h4>Project Management <small>University of Liverpool</small></h4>
                                    <div class="r-meta-data">
                                        <span class="label label-primary">PhD</span>
                                        <span class="label label-warning">Application In Progress...</span> |
                                        <span class="label label-success">Application Completed</span> |
                                        <span class="label label-danger">Application Cancelled</span>
                                    </div>
                                    <div class="r-meta-data">
                                        Applied on <span>Nov 3rd, 2015</span>
                                    </div>
                                    <div>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-eye"></i> View</a>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-cancel-circle"></i> Cancel</a>
                                    </div>
                                </div>
                            </div>

                            <div class="empty-resume">
                                <img src="../public/img/account/application.png"/>
                                <p>New Application? Click on the button below.</p>
                                <a class="btn btn-default" data-toggle="modal" href="#new-application">Start New Application</a>
                            </div>



                        </div>

                    </div>

                    <div class="col-sm-4 col-sm-pull-8">
                        <div class="sideBarBox">
                            <h4>My Documents</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Resume/CV</a></li>
                                <li><a href="">Transcript</a></li>
                                <li><a href="">Passport Datapage</a></li>
                                <li><a href="">Certificates</a></li>
                                <li><a href="">Personal Statement</a></li>
                                <li><a href="">Reference Letters</a></li>

                            </ul>
                            <h4>My Application</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Schedule Application Appointment</a></li>
                                <!--<li><a href=""></a></li>-->
                            </ul>
                            <h4>My Messages</h4>
                            <ul class="side-menu-navigation">

                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!--Application Modal-->
    <div class="modal fade" id="new-application">
    	<div class="modal-dialog">
    		<div class="modal-content">
                <form method="post" action="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">New Application</h4>
                    </div>
                    <div class="modal-body">
                        <div class="application-modal">

                            <h4 class="apm-title">Confirm and Book with the following sections:</h4>

                            <div class="each-sections">
                                <div class="title">Personal Details</div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="tel" name="phone_no" id="phone_no" class="form-control" placeholder="Phone No" required="required" >
                                </div>
                            </div>


                            <div class="each-sections">
                                <div class="title">Application Details</div>
                                <div class="form-group">
                                    <label>Study Level</label>
                                    <select class="selectbox" placeholder="Study Level">
                                        <option value="">Study Level</option>
                                        <option value="1">PhD.</option>
                                        <option value="2">Masters</option>
                                        <option value="3">Undergraduate</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Course</label>
                                    <select class="selectbox" placeholder="Course">
                                        <option value="">Course</option>
                                        <option value="1">Engineering</option>
                                        <option value="2">Accounting</option>
                                        <option value="3">Management</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>University</label>
                                    <select class="selectbox" placeholder="University">
                                        <option value="">University</option>
                                        <option value="1">University of Liverpool</option>
                                        <option value="2">Western University</option>
                                        <option value="3">Oxford University</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <p><b>Unsure of your course details</b>? No need to worry, we can discuss this during your consultation session</p>
                                </div>

                                <div class="form-group">
                                    <label>Would you like</label><br/>
                                    <div class="custom-radiobox inline-radio">
                                        <input type="radio" id="inhouse-option" name="consultation">
                                        <label for="inhouse-option">In-house consultation</label>
                                        <div class="check"></div>
                                    </div>
                                    <div class="custom-radiobox inline-radio">
                                        <input type="radio" id="virtual-option" name="consultation">
                                        <label for="virtual-option">Virtual consultation</label>
                                        <div class="check"></div>
                                    </div>
                                </div>

                                <div class="form-group text-right">
                                    <label>Consultation Fee</label>
                                    <div class="consultation-fee">NGN 15,000</div>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Confirm & Pay</button>
                    </div>
                </form>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            $('.changePicture :file').on('fileselect', function(event, numFiles, label) {
                //var input = $(this).parents('.input-group').find(':text'),
                //    log = numFiles > 1 ? numFiles + ' files selected' : label;
                //
                //if( input.length ) {
                //    input.val(log);
                //} else {
                //    if( log ) alert(log);
                //}
            });


            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            $('.selectbox').selectize({create: false});

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



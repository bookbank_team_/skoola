    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Your Application to University of Liverpool</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_50">
            <div class="container">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="customWizard">
                            <div class="row">
                                <div class="col-sm-4 step">
                                    <div>
                                        <span class="active">1</span>
                                        <span>Application Information</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span>2</span>
                                        <span>Profile Picture</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span>3</span>
                                        <span>Pricing</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="innerContentWrapper">
                            <h4>Application Information</h4>

                            <div class="row">
                                <div class="col-sm-7">
                                    <form method="post" action="picture.php">
                                        <div class="form-group">
                                            <label>Select the country where you'll like to study</label>
                                            <select class="selectbox" placeholder="Country">
                                                <option value="">Country</option>
                                                <option value="1">Canada</option>
                                                <option value="2">UK</option>
                                                <option value="3">USA</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Select the University you want to apply to</label>
                                            <select class="selectbox" placeholder="University">
                                                <option value="">University</option>
                                                <option value="1">University of Liverbool</option>
                                                <option value="2">Aberden University</option>
                                                <option value="3">University of Huddersfield</option>
                                            </select>
                                        </div>

                                        <!--<div class="form-group">-->
                                        <!--    <label>Which subject area are you interested in ?</label>-->
                                        <!--    <select class="selectbox" placeholder="Category">-->
                                        <!--        <option value="">Category</option>-->
                                        <!--        <option value="1">Arts & Humanities</option>-->
                                        <!--        <option value="2">Education</option>-->
                                        <!--        <option value="3">Engineering</option>-->
                                        <!--    </select>-->
                                        <!--</div>-->

                                        <div class="form-group">
                                            <label>Select Specific Course</label>
                                            <select class="selectbox" placeholder="Course">
                                                <option value="">Course</option>
                                                <option value="1">Software Engineering</option>
                                                <option value="2">Management</option>
                                                <option value="3">Information System Management</option>
                                            </select>
                                        </div>

                                        <!--<div class="form-group">-->
                                        <!--    <label>Select Type/Level</label>-->
                                        <!--    <select class="selectbox" placeholder="Type/Level">-->
                                        <!--        <option value="">Type/Level</option>-->
                                        <!--        <option value="1">Undergraduate</option>-->
                                        <!--        <option value="2">Master</option>-->
                                        <!--        <option value="3">PhD</option>-->
                                        <!--    </select>-->
                                        <!--</div>-->

                                        <!--<div class="form-group">-->
                                        <!--    <label>When do you want to start your study in abroad</label>-->
                                        <!--    <select class="selectbox" placeholder="Year">-->
                                        <!--        <option value="">Year</option>-->
                                        <!--        <option value="1">2015</option>-->
                                        <!--        <option value="2">2016</option>-->
                                        <!--        <option value="3">2017</option>-->
                                        <!--    </select>-->
                                        <!--</div>-->
                                        <!---->
                                        <!--<div class="form-group">-->
                                        <!--    <label>What do you need help with ?</label>-->
                                        <!--    <select class="selectbox" placeholder="Help Needed">-->
                                        <!--        <option value="">Help Needed</option>-->
                                        <!--        <option value="1">Full Application</option>-->
                                        <!--        <option value="2">Accomodation</option>-->
                                        <!--        <option value="3">Admission Only</option>-->
                                        <!--    </select>-->
                                        <!--</div>-->

                                        <!--<div class="form-group">-->
                                        <!--    <label>Who will sponsor your application abroad</label>-->
                                        <!--    <select class="selectbox" placeholder="Sponsor">-->
                                        <!--        <option value="">Sponsor</option>-->
                                        <!--        <option value="self">Self</option>-->
                                        <!--        <option value="parent">Parent</option>-->
                                        <!--        <option value="employer">Employer</option>-->
                                        <!--        <option value="other">Other</option>-->
                                        <!--    </select>-->
                                        <!--</div>-->

                                        <!--<div class="form-group sponsor_info_more" style="display:none;">-->
                                        <!--    <label>Sponsor's Name</label>-->
                                        <!--    <input type="text" name="sponsor" id="sponsor" class="form-control" placeholder="Sponsor Name" >-->
                                        <!--</div>-->
                                        <!---->
                                        <!--<div class="form-group sponsor_info_more" style="display:none;">-->
                                        <!--    <label>Sponsor's No</label>-->
                                        <!--    <input type="text" name="sponsor" id="sponsor" class="form-control" placeholder="Sponsor No" >-->
                                        <!--</div>-->
                                        <!---->
                                        <!--<div class="form-group">-->
                                        <!--    <label>Tell us about yourself</label>-->
                                        <!--    <textarea  name="about" rows="5" placeholder="About You" class="form-control"></textarea>-->
                                        <!--</div>-->

                                        <div class="form-group text-right">
                                            <button type="submit" class="btn btn-warning">Continue</button>
                                        </div>


                                    </form>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>




        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            var $select = $('.selectbox').selectize({
                create: false,
                //onChange: function(value) {
                //    var item = sponsor.getValue();
                //    if(item != null && item != ''){
                //        if(item == 'parent' || item == 'employer' || item == 'other'){
                //            //enable the sponsor's name and phone number
                //            $('.sponsor_info_more').show();
                //        }
                //        else{
                //            $('.sponsor_info_more').hide();
                //        }
                //    }
                //}
            });

            //var sponsor = $select[7].selectize;

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



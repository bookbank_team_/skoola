    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>


    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Your Application to University of Liverpool</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_50">
            <div class="container">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="customWizard">
                            <div class="row">
                                <div class="col-sm-4 step">
                                    <div>
                                        <span class="done">1</span>
                                        <span>Application Information</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span class="active">2</span>
                                        <span>Profile Picture</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span>3</span>
                                        <span>Pricing</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="innerContentWrapper">
                            <h4>Add Your Picture <small>(So we can help you better)</small></h4>

                            <div class="row">
                                <div class="col-sm-7">
                                    <form method="post" action="pricing.php">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img src="../public/img/users/default.png" class="img-responsive"/>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group margin_top_30">
                                                    <!--<input type="file" class="form-control">-->
                                                    <span class="file-input btn btn btn-default btn-file btn-block btn-sm">
                                                Click here to select Image <input  name="picture" type="file">
                                            </span>
                                                </div>

                                                <div class="form-group text-right">
                                                    <a href="application.php" class="btn btn-primary">Back</a>
                                                    <button type="submit" class="btn btn-warning">Skip</button>
                                                </div>

                                            </div>
                                        </div>




                                    </form>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>




        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            $('.changePicture :file').on('fileselect', function(event, numFiles, label) {
                //var input = $(this).parents('.input-group').find(':text'),
                //    log = numFiles > 1 ? numFiles + ' files selected' : label;
                //
                //if( input.length ) {
                //    input.val(log);
                //} else {
                //    if( log ) alert(log);
                //}
            });



            var $select = $('.selectbox').selectize({
                create: false,
                onChange: function(value) {
                    var item = sponsor.getValue();
                    if(item != null && item != ''){
                        if(item == 'parent' || item == 'employer' || item == 'other'){
                            //enable the sponsor's name and phone number
                            $('.sponsor_info_more').show();
                        }
                        else{
                            $('.sponsor_info_more').hide();
                        }
                    }
                    //alert('Item is '+item);
                }
            });

            var sponsor = $select[7].selectize;

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



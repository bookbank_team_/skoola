    <?php include('inc/header.php');?>
    <style>
        .input-group-addon {
            outline:none !important;
        }
    </style>


    <?php $leaveLoginJoinBtn =  true; ?>


    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>


        <section class="margin_top_40 margin_bottom_30">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-sm-5 col-md-4 col-xs-12 col-centered">
                        <div class="loginBox sideBarBox">
                            <h3>Sign Up</h3>

                            <div class="margin_bottom_20">
                                <a href="" class="socials facebook"><i class="icon-facebook"></i> Continue with Facebook</a>
                            </div>
                            <h6 class="text-center">OR</h6>
                            <form class="" method="post" action="register-step-2.php">
                                <div class="form-group">
                                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" required="required" >
                                </div>
                                <div class="form-group">
                                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name" required="required" >
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required" >
                                </div>
                                <div class="form-group">
                                    <input type="tel" name="phone_no" id="phone_no" class="form-control" placeholder="Phone No" required="required" >
                                </div>
                                <div class="form-group">
                                    <input id="password"
                                           class="form-control"
                                           type="password"
                                           placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-warning btn-block btn-lg" value="Sign Up">
                                </div>
                                <p class="fontsize_12">Creating an account means you agree with Skoola's <a href="">Terms of Service</a>, and <a href="">Privacy Policy</a>.</p>

                                <p class="fontsize_13">Already have an account? <a href="login.php" class="semibold">LOG IN</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>





    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/bootstrap-show-password.min.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            $('#password').password();

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



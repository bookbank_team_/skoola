    <?php include('inc/header.php');?>
    <style>
        .input-group-addon {
            outline:none !important;
        }
    </style>


    <?php $leaveLoginJoinBtn =  true; ?>


    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>


        <section class="margin_top_40">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-sm-6 col-xs-12 col-centered">
                        <div class="loginBox sideBarBox">
                            <h3><i class="icon-key-outline"></i> Reset Password</h3>

                            <form class="">
                                <div class="form-group">
                                    <input id="password"
                                           class="form-control"
                                           type="password"
                                           placeholder="New Password">
                                </div>
                                <div class="form-group text-right">
                                    <input type="submit" class="btn btn-warning" value="Reset Password">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>

    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/bootstrap-show-password.min.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            $('#password').password();

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



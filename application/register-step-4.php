    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Complete Your Profile</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_50">
            <div class="container">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="customWizard">
                            <div class="row">
                                <div class="col-sm-4 step done">
                                    <div>
                                        <span>1</span>
                                        <span>Contact Information</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step done">
                                    <div>
                                        <span>2</span>
                                        <span>Your Interest</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step active">
                                    <div>
                                        <span>3</span>
                                        <span>Your Background</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="innerContentWrapper">
                            <h4>Your Background</h4>

                            <div class="row">
                                <div class="col-sm-7">
                                    <form method="post" action="account.php">

                                        <div class="form-group">
                                            <label>Highest obtained degree</label>
                                            <select class="selectbox" placeholder="Highest obtained degree">
                                                <option value="">Highest obtained degree</option>
                                                <option value="1">BSc.</option>
                                                <option value="2">HND</option>
                                                <option value="3">OND</option>
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label>Country of Study</label>
                                            <select class="selectbox" placeholder="Which country do you study?">
                                                <option value="">Which country do you study?</option>
                                                <option value="1">USA</option>
                                                <option value="2">UK</option>
                                                <option value="3">Canada</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Accomplishment Note</label>
                                            <textarea class="form-control" placeholder="Accomplishment Note"></textarea>
                                        </div>



                                        <div class="form-group">
                                            <label>English Proficient</label>
                                            <select class="selectbox" placeholder="English Proficiency">
                                                <option value="">English Proficiency</option>
                                                <option value="1">Yes</option>
                                                <option value="1">No</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Work Experience <small>(years)</small></label>
                                            <input type="text" class="form-control" placeholder="Years of Experience"/>

                                        </div>
                                        

                                        <div class="form-group text-right">
                                            <button type="submit" class="btn btn-warning">Continue</button>
                                        </div>


                                    </form>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>




        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>


    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});



            var $select = $('.selectbox').selectize({
                create: false,
                //onChange: function(value) {
                //    var item = sponsor.getValue();
                //    if(item != null && item != ''){
                //        if(item == 'parent' || item == 'employer' || item == 'other'){
                //            //enable the sponsor's name and phone number
                //            $('.sponsor_info_more').show();
                //        }
                //        else{
                //            $('.sponsor_info_more').hide();
                //        }
                //    }
                //}
            });

            //var sponsor = $select[7].selectize;

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



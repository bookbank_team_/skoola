    <?php include('inc/header.php');?>

    <!--mobile navigation-->
    <?php $leaveLoginJoinBtn =  true; ?>


    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>


        <section class="margin_top_40 margin_bottom_30">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-sm-5 col-md-4 col-xs-12 col-centered">
                        <div class="loginBox sideBarBox">
                            <h3>Log In</h3>

                            <div class="margin_bottom_20">
                                <a href="" class="socials facebook"><i class="icon-facebook"></i> Continue with Facebook</a>
                            </div>
                            <h6 class="text-center">OR</h6>
                            <form class="" method="post" action="account.php">

                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required" >
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="required" >
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-warning btn-block btn-lg" value="Log In">
                                </div>
                                <p class="fontsize_12">Creating an account means you agree with Skoola's <a href="">Terms of Service</a>, and <a href="">Privacy Policy</a>.</p>

                                <p class="fontsize_13"><a href="forgot_password.php" class="semibold">FORGOT PASSWORD</a> | <a href="register.php" class="semibold">SIGN UP</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>





    </section>


    <!--mobile navigation menu-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



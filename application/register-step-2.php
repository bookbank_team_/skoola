    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>
    <link href="../public/js/plugins/datepicker/pikaday.css" rel="stylesheet">

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Complete Your Profile</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_50">
            <div class="container">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="customWizard">
                            <div class="row">
                                <div class="col-sm-4 step active">
                                    <div>
                                        <span>1</span>
                                        <span>Contact Information</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span>2</span>
                                        <span>Your Interest</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span>3</span>
                                        <span>Your Background</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="innerContentWrapper">
                            <h4>Contact Information</h4>

                            <div class="row">
                                <div class="col-sm-7">
                                    <form method="post" action="register-step-3.php">

                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" placeholder="Name" name="" class="form-control"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" placeholder="Email" name="" class="form-control"/>
                                        </div>



                                        <div class="form-group">
                                            <label>Gender</label><br/>
                                            <div class="custom-radiobox inline-radio">
                                                <input type="radio" id="m-option" name="gender">
                                                <label for="m-option">Male</label>
                                                <div class="check"></div>
                                            </div>
                                            <div class="custom-radiobox inline-radio">
                                                <input type="radio" id="f-option" name="gender">
                                                <label for="f-option">Female</label>
                                                <div class="check"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Date of Birth</label>
                                            <input type="text" placeholder="Date of Birth" class="form-control" id="pick-dob"/>
                                        </div>


                                        <div class="form-group">
                                            <label>Nationality</label>
                                            <select class="selectbox" placeholder="Nationality">
                                                <option value="">Nationality</option>
                                                <option value="1">Nigerian</option>
                                                <option value="2">Canadan</option>
                                                <option value="3">USA</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Telephone</label>
                                            <input type="text" placeholder="Telephone" name="" class="form-control"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Country of Residence</label>
                                            <select class="selectbox" placeholder="Country of Residence">
                                                <option value="">Country of Residence</option>
                                                <option value="1">Nigeria</option>
                                                <option value="2">Ghana</option>
                                                <option value="3">South Africa</option>
                                            </select>
                                        </div>


                                        <div class="form-group text-right">
                                            <button type="submit" class="btn btn-warning">Continue</button>
                                        </div>


                                    </form>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>




        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>

    <!--Date Picker-->
    <script src="../public/js/plugins/datepicker/moment.js"></script>
    <script src="../public/js/plugins/datepicker/pikaday.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});



            //Date Picker
            var maxYear = new Date().getFullYear() - 12;
            var dob_picker = new Pikaday({
                field: document.getElementById('pick-dob'),
                format: 'DD-MM-YYYY',
                yearRange: [1920, maxYear],
            });

            var $select = $('.selectbox').selectize({
                create: false,
                //onChange: function(value) {
                //    var item = sponsor.getValue();
                //    if(item != null && item != ''){
                //        if(item == 'parent' || item == 'employer' || item == 'other'){
                //            //enable the sponsor's name and phone number
                //            $('.sponsor_info_more').show();
                //        }
                //        else{
                //            $('.sponsor_info_more').hide();
                //        }
                //    }
                //}
            });

            //var sponsor = $select[7].selectize;

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



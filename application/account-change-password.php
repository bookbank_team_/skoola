    <?php include('inc/header.php');?>

    <style>
        .input-group-addon {
            outline:none !important;
        }
    </style>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation-account.php');?>
        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Change Password</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="margin_top_30 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-push-4 margin_bottom_15">

                        <div class="innerContentWrapper">
                            <h4>Change Password</h4>
                            <form>
                                <div class="form-group">
                                    <input type="password" name="password" id="inputID" class="form-control" placeholder="Current Password" required="required" >
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="New Password" required="required" >
                                </div>
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-warning">Change Password</button>
                                </div>
                            </form>

                        </div>

                    </div>

                    <div class="col-sm-4 col-sm-pull-8">
                        <div class="sideBarBox">
                            <h4>My Documents</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Resume/CV</a></li>
                                <li><a href="">Transcript</a></li>
                                <li><a href="">Passport Datapage</a></li>
                                <li><a href="">Certificates</a></li>
                                <li><a href="">Personal Statement</a></li>
                                <li><a href="">Reference Letters</a></li>
                            </ul>
                            <h4>My Application</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Schedule Application Appointment</a></li>
                                <!--<li><a href=""></a></li>-->
                            </ul>
                            <h4>My Messages</h4>
                            <ul class="side-menu-navigation">

                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <!--<script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>-->
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/bootstrap-show-password.min.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            $('#password').password();

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            //$('.selectbox').selectize({create: false});

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



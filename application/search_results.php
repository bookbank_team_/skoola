    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>
    <link href="../public/js/plugins/ionslider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="../public/js/plugins/ionslider/css/ion.rangeSlider.skinHTML5.css" rel="stylesheet">



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Search results for <span>engineering</span></h2>
                    </div>
                </div>
            </div>
        </section>


        <!--Search form -->
        <section class="margin_top_30 margin_bottom_30">
            <div class="container">
                <div class="row row-centered">
                    <div class="col-lg-10 col-centered">
                        <form role="form" method="get" action="search_results.php" class="homeSearchForm">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 nopadding">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg typeahead" style="display: block;width 100%" name="keyword" data-provide="typeahead" autocomplete="off"  placeholder="Enter Course Name">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 nopadding margin_bottom_10">
                                    <div class="form-group text-left special-select">
                                        <select class="selectbox " placeholder="Location">
                                            <option value="">Location</option>
                                            <option value="1">UK</option>
                                            <option value="2">US</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-8 col-xs-6 nopadding margin_bottom_10">
                                    <div class="form-group text-left special-select">
                                        <select class="selectbox " placeholder="Choose Study Level">
                                            <option value="">Choose Study Level</option>
                                            <option value="1">Postgraduate</option>
                                            <option value="2">Undergraduate</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 nopadding">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-warning btn-lg"><i class="icon-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5 nopadding col-xs-12">
                                    <div class="form-group special-select">
                                        <select class="selectbox" placeholder="Select Type">
                                            <option value="">Select Type</option>
                                            <option value="1">Type 1</option>
                                            <option value="2">Type 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-5  col-xs-12">
                                    <div class="form-group">
                                        <input name="price_range" id="price-range">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_10 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <!--<div class="col-lg-4 col-md-4 col-sm-4">-->
                    <!--    <div class="sideBarBox">-->
                    <!--        <h3>Filter Results</h3>-->
                    <!---->
                    <!--        <form method="" action="">-->
                    <!--            <div class="form-group">-->
                    <!--                <input type="text" class="form-control" name="cp"  placeholder="Course/Program">-->
                    <!--            </div>-->
                    <!---->
                    <!--            <div class="form-group">-->
                    <!--                <select class="selectbox " placeholder="Country">-->
                    <!--                    <option value="">Country</option>-->
                    <!--                    <option value="1">Canada</option>-->
                    <!--                    <option value="2">UK</option>-->
                    <!--                    <option value="3">USA</option>-->
                    <!--                </select>-->
                    <!--            </div>-->
                    <!---->
                    <!---->
                    <!---->
                    <!--            <div class="form-group text-right">-->
                    <!--                <button type="submit" class="btn btn-warning">Search</button>-->
                    <!--            </div>-->
                    <!---->
                    <!--        </form>-->
                    <!---->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="incontentWrapper">
                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-4 margin_bottom_20">
                                    <div class="programListing">
                                        <div class="listingBody">

                                            <div><a href="school.php"><img src="../public/img/institutions/university-of-liverpool.png"/></a></div>

                                            <h3>Electronic / Communication Engineering</h3>
                                            <div class="margin_top_10">
                                                <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                            </div>

                                            <h4 class="price">N3.42M <small>Annual Fees</small></h4>

                                        </div>
                                        <div class="listingFooter text-right">
                                            <a href="course.php" class="btn btn-default">Know More</a>
                                            <a href="application.php" class="btn btn-warning">Apply</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 margin_bottom_20">
                                    <div class="programListing">
                                        <div class="listingBody">

                                            <div><a href="school.php"><img src="../public/img/institutions/university-of-hudderfield.png"/></a></div>

                                            <h3>Electronic Engineering</h3>
                                            <div class="margin_top_10">
                                                <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                            </div>


                                            <h4 class="price">N3.42M <small>Annual Fees</small></h4>

                                        </div>
                                        <div class="listingFooter text-right">
                                            <a href="course.php" class="btn btn-default">Know More</a>
                                            <a href="application.php" class="btn btn-warning">Apply</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-4 margin_bottom_20">
                                    <div class="programListing">
                                        <div class="listingBody">

                                            <div><a href="school.php"><img src="../public/img/institutions/university-of-liverpool.png"/></a></div>

                                            <h3>Computer and Information Security</h3>
                                            <div class="margin_top_10">
                                                <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                            </div>

                                            <h4 class="price">N3.42M <small>Annual Fees</small></h4>

                                        </div>
                                        <div class="listingFooter text-right">
                                            <a href="course.php" class="btn btn-default">Know More</a>
                                            <a href="application.php" class="btn btn-warning">Apply</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 margin_bottom_20">
                                    <div class="programListing">
                                        <div class="listingBody">

                                            <div><a href="school.php"><img src="../public/img/institutions/university-of-hudderfield.png"/></a></div>

                                            <h3>Information Systems and Technology</h3>
                                            <div class="margin_top_10">
                                                <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                            </div>



                                            <h4 class="price">N3.42M <small>Annual Fees</small></h4>

                                        </div>
                                        <div class="listingFooter text-right">
                                            <a href="course.php" class="btn btn-default">Know More</a>
                                            <a href="application.php" class="btn btn-warning">Apply</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-4 margin_bottom_20">
                                    <div class="programListing">
                                        <div class="listingBody">

                                            <div><a href="school.php"><img src="../public/img/institutions/university-of-liverpool.png"/></a></div>

                                            <h3>Computer and Information Security</h3>
                                            <div class="margin_top_10">
                                                <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                            </div>

                                            <h4 class="price">N3.42M <small>Annual Fees</small></h4>

                                        </div>
                                        <div class="listingFooter text-right">
                                            <a href="course.php" class="btn btn-default">Know More</a>
                                            <a href="application.php" class="btn btn-warning">Apply</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 margin_bottom_20">
                                    <div class="programListing">
                                        <div class="listingBody">

                                            <div><a href="school.php"><img src="../public/img/institutions/university-of-hudderfield.png"/></a></div>

                                            <h3>Information Systems and Technology</h3>
                                            <div class="margin_top_10">
                                                <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                            </div>



                                            <h4 class="price">N3.42M <small>Annual Fees</small></h4>

                                        </div>
                                        <div class="listingFooter text-right">
                                            <a href="course.php" class="btn btn-default">Know More</a>
                                            <a href="application.php" class="btn btn-warning">Apply</a>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <ul class="pagination">
                                    	<li><a href="#">&laquo;</a></li>
                                    	<li><a href="#">1</a></li>
                                    	<li><a href="#">2</a></li>
                                    	<li><a href="#">3</a></li>
                                    	<li><a href="#">4</a></li>
                                    	<li><a href="#">5</a></li>
                                    	<li><a href="#">&raquo;</a></li>
                                    </ul>
                                </div>
                            </div>




                        </div>

                    </div>
                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/plugins/ionslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    <script src="../public/js/plugins/bloodhound/bloodhound.min.js"></script>
    <script src="../public/js/plugins/typeahead/bootstrap3-typeahead.min.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            $("#price-range").ionRangeSlider({
                type: "double",
                from: 500000,
                to: 3000000,
                min: 100000,
                max: 5000000,
                grid: true,
                prefix: '₦',
                step: 5000,
                force_edges: true
            });


            //Bootstrap typeahead
            var courses, remoteHost;
            $.support.cors = true;
            remoteHost = 'http://apps.dev/skoola/application/data/courses.json';
            courses = new Bloodhound({
                //identify: function(o) { return o.id_str; },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                dupDetector: function(a, b) { return a.id_str === b.id_str; },
                prefetch: remoteHost + '?prefetch',
                remote: {
                    url: remoteHost + '?keyword=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            $('.typeahead').typeahead({
                items: 8,
                minLength: 1,
                autoSelect: true,
                source:courses.ttAdapter()
            });


            $('.selectbox').selectize({create: false});

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



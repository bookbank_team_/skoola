<!--footer-->
<section class="skfooter">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 footerBox">
                <h4>Sitemap</h4>
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="login.php">Log In</a></li>
                    <li><a href="register.php">Sign Up</a></li>
                    <li><a href="exams.php">Entry Exam</a></li>
                    <li><a href="account.php">My Application</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 footerBox contacts">
                <h4>Contact Us</h4>
                <p><span>+234-809-029-9997</span><p>
                <p><b>ask&#64;skoola.com</b></p>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 footerBox">
                <h4>About Us</h4>
                <ul>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contact Us</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Terms and Conditions</a></li>
                    <li><a href="">FAQs</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 footerBox">
                <h4>Follow Us</h4>
                <div class="footerSocials">
                    <a href=""><img src="../public/img/socials/facebook.png"/></a>
                    <a href=""><img src="../public/img/socials/twitter.png"/></a>
                    <a href=""><img src="../public/img/socials/googleplus.png"/></a>
                    <a href=""><img src="../public/img/socials/linkedin.png"/></a>
                </div>
            </div>

        </div>
        <div class="row footerCopyright">
            <div class="col-lg-12">
                <p>Copyright 2015, All right reserved</p>
            </div>
        </div>
    </div>
</section>

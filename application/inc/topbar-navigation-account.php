<!--TopBar-->
<section class="topBar">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-2 col-lg-push-3 col-md-push-3 col-sm-push-3">
                <div class="topNavigation">
                    <ul class="yamm custom-nav hidden-xs visible-sm visible-lg visible-md">
                        <?php if(!isset($leaveLoginJoinBtn)): ?>
                            <li class="dropdown yamm-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Search for a Course <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="mega-menu-content-box">
                                                        <h3>Search for a Course</h3>
                                                        <h4>By Popular Subject <a href="" class="pull-right">See All</a></h4>
                                                        <ul>
                                                            <li><a href="">Agriculture and Veterinary Medicine</a></li>
                                                            <li><a href="">Applied and Pure Sciences</a></li>
                                                            <li><a href="">Architecture and Construction</a></li>
                                                            <li><a href="">Business and Management</a></li>
                                                            <li><a href="">Computer Science and IT</a></li>
                                                            <li><a href="">Creative Arts and Design</a></li>
                                                            <li><a href="">Education and Training</a></li>
                                                            <li><a href="">Engineering</a></li>
                                                            <li><a href="">Personal Care and Fitness</a></li>
                                                            <li><a href="">Health and Medicine</a></li>
                                                            <li><a href="">Humanities</a></li>
                                                            <li><a href="">Law</a></li>
                                                            <li><a href="">MBA</a></li>
                                                            <li><a href="">Social Studies and Media</a></li>
                                                            <li><a href="">Travel and Hospitality</a></li>
                                                        </ul>

                                                        <h4 class="margin_top_20">By Countries <a href="" class="pull-right">See All</a></h4>
                                                        <ul>
                                                            <li><a href="">UK</a></li>
                                                            <li><a href="">Australia</a></li>
                                                            <li><a href="">USA</a></li>
                                                            <li><a href="">New Zealand</a></li>
                                                            <li><a href="">Singapore</a></li>
                                                            <li><a href="">Malaysia</a></li>
                                                            <li><a href="">Sweden</a></li>
                                                            <li><a href="">Ireland</a></li>
                                                            <li><a href="">Hongkong</a></li>
                                                            <li><a href="">Netherlands</a></li>
                                                            <li><a href="">Canada</a></li>
                                                        </ul>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown yamm-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Why Skoola? <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="mega-menu-content-box">
                                                        <h3>Why Skoola</h3>
                                                        <p>We are the number 1 and most trusted platform for overseas study. Thousands of students use Skoola to find the right the right schools; make applications & get admitted to the best schools in Europe, US, Asia and South America</p>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                <div class="skoolaDataStats">
                                                                    <h4>2,211+</h4>
                                                                    <p>COURSES / PROGRAMS COVERED</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                <div class="skoolaDataStats">
                                                                    <h4>690+</h4>
                                                                    <p>UNIVERSITIES / HIGHER INSTITUTIONS</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                <div class="skoolaDataStats">
                                                                    <h4>79</h4>
                                                                    <p>COUNTRIES</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 learn-more-box">
                                                    <h4>Learn More</h4>
                                                    <ul>
                                                        <li><a href="">Contact Us</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        <?php else: ?>
                            <li><a>&nbsp;</a></li>
                        <?php endif?>
                        <li class="dropdown account-dropdown"><a href="#" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><img src="../public/img/users/default.png" width="24px" class="img-circle"/> My Account <span class="caret"></span></a>
                            <ul class="dropdown-menu" style="z-index:3000;">
                                <li><a href="#">My Applications</a></li>
                                <li><a href="#">Profile</a></li>
                                <li><a href="#">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <div class="visible-xs mobileMenuBtn text-left">
                    <a href="#" class="toggle-button"><i class="icon-menu"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-10 col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
                <div class="logo">
                    <a href="index.php"><img src="../public/img/skoola-logo.png"/></a>
                </div>
            </div>
        </div>
    </div>
</section>
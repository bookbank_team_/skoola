<section id="menuWrapper" class="slidingMenuContainer">
    <div class="navheading">Quick Navigation <span class="close-menu pull-right">x Close</span></div>
    <ul class="mobileMenuNavigation">
        <li><a href="">How it Works</a></li>
        <li><a href="">About Skoola</a></li>
        <li><a href="">Contact Us</a></li>
        <li><a href="">Login</a></li>
        <li><a href="">Join Now</a></li>
        <li class="divider">My Account</li>
        <li><a href="">My Profile</a></li>
        <li><a href="">Applications</a></li>
        <li><a href="">Change Password</a></li>
        <li><a href="">Logout</a></li>
    </ul>
</section>
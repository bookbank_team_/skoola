<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Product"><html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Skoola</title>
    <meta name="description" content="Skoola" />
    <!--SOCIAL META TAGS-->
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="The Name or Title Here">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta itemprop="description" content="This is the page description">
    <meta itemprop="image" content="http://www.example.com/image.jpg">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="Page description less than 200 characters">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="http://www.example.com/image.html">
    <meta name="twitter:data1" content="$3">
    <meta name="twitter:label1" content="Price">
    <meta name="twitter:data2" content="Black">
    <meta name="twitter:label2" content="Color">
    <!-- Open Graph data -->
    <meta property="og:title" content="Title Here" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.example.com/" />
    <meta property="og:image" content="http://example.com/image.jpg" />
    <meta property="og:description" content="Description Here" />
    <meta property="og:site_name" content="Site Name, i.e. Moz" />
    <meta property="og:price:amount" content="15.00" />
    <meta property="og:price:currency" content="USD" />
    <!--END OF SOCIAL META TAGS-->

    <!--css -->
    <link href="../public/css/fontello/css/fontello-embedded.css" rel="stylesheet">
    <link href="../public/css/plugins/selective/selectize.css" rel="stylesheet">
    <link href="../public/css/skoola.css" rel="stylesheet">


    <!--jquery-->
    <script src="../public/js/jquery/jquery.min.js"></script>

</head>
<body>
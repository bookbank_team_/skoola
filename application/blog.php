    <?php include('inc/header.php');?>
    <link href="../public/js/plugins/lightbox/css/lightbox.min.css" rel="stylesheet">


    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Our Blog</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 margin_bottom_15">
                        <div class="blog-posts">

                            <div class="each-posts">
                                <h2><a href="">Welcome to Skoola Blog</a></h2>
                                <div class="post-metas">Posted on <a href="">March 30, 2016</a> by <a href="">Skoola</a> | <a href="">2 Comments</a></div>

                                <div class="attachment"><a href="../public/img/country-study-banner.jpg" data-lightbox="Image-1" data-title="Blog Caption"><img src="../public/img/country-study-banner.jpg"/></a></div>

                                <p>Uniquely plagiarize empowered processes before multimedia based results. Synergistically redefine innovative core competencies for excellent networks. Proactively transform goal-oriented human capital without mission-critical best practices. <a href="">Read More</a> </p>
                            </div>


                            <div class="each-posts">
                                <h2><a href="">How to Study Abroad</a></h2>
                                <div class="post-metas">Posted on <a href="">March 30, 2016</a> by <a href="">Skoola</a> | <a href="">2 Comments</a></div>

                                <div class="attachment"><a href="../public/img/country-study-banner.jpg" data-lightbox="Image-1" data-title="Blog Caption"><img src="../public/img/country-study-banner.jpg"/></a></div>

                                <p>Uniquely plagiarize empowered processes before multimedia based results. Synergistically redefine innovative core competencies for excellent networks. Proactively transform goal-oriented human capital without mission-critical best practices. <a href="">Read More</a> </p>
                            </div>



                            <!--<div class="no-blog-posts">No posts yet. Please check back shortly. <br/><a href="" class="btn btn-primary btn-sm">Study Abroad</a> </div>-->
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="sk-sidebar-widget">
                            <aside id="recent-comments-2" class="widget widget_recent_comments">
                                <h3 class="widget-title">Recent Comments</h3>
                                <ul id="recentcomments">
                                    <li class="recentcomments">
                                        <span class="comment-author-link"><a href="https://wordpress.org/" rel="external nofollow" class="url">Technology Harness</a></span> on <a href="http://apps.dev/skoolablog/index.php/2016/03/07/hello-world/#comment-1">Hello world!</a>
                                    </li>
                                    <li class="recentcomments">
                                        <span class="comment-author-link"><a href="https://wordpress.org/" rel="external nofollow" class="url">Outstanding Solutions</a></span> on <a href="http://apps.dev/skoolablog/index.php/2016/03/07/hello-world/#comment-1">Hello world!</a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/responsiveTabs.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>
    <script src="../public/js/plugins/lightbox/js/lightbox.min.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            fakewaffle.responsiveTabs(['xs']);

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



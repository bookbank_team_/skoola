    <?php include('inc/header.php');?>



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Countries</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="where-to-study">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="row">

                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark uk">UK</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark usa">USA</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark australia">Australia</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark canada">New Zealand</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark uk">Singapore</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark usa">Malaysia</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark australia">Sweden</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark canada">Hongkong</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark usa">Ireland</a>
                            </div>
                            <div class="col-md-15 col-sm-4 col-xs-6">
                                <a href="" class="custom-btn-dark australia">Netherlands</a>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/responsiveTabs.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            fakewaffle.responsiveTabs(['xs']);

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



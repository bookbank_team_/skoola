    <?php include('inc/header.php');?>



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Universities</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_30">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12 margin_top_20">

                        <div class="row row-margin">

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-hudderfield.png"/></div>
                                    <h3>University of Huddersfield Engineering</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">200 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-liverpool.png"/></div>
                                    <h3>University of Liverpool</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">500 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-hudderfield.png"/></div>
                                    <h3>University of Huddersfield Engineering</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">200 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-liverpool.png"/></div>
                                    <h3>University of Liverpool</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">500 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>


                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-hudderfield.png"/></div>
                                    <h3>University of Huddersfield Engineering</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">200 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-liverpool.png"/></div>
                                    <h3>University of Liverpool</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">500 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-hudderfield.png"/></div>
                                    <h3>University of Huddersfield Engineering</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">200 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>

                            <div class="eq-col-md-3 eq-col-sm-3 margin_bottom_15">
                                <a href="school.php" class="eachUniversity">
                                    <div><img src="../public/img/institutions/university-of-liverpool.png"/></div>
                                    <h3>University of Liverpool</h3>
                                    <div class="margin_top_10">
                                        <span class="label label-primary margin_bottom_5">UK</span>
                                        <span class="label label-primary margin_bottom_5">500 Views</span>
                                    </div>
                                    <p>English Courses Available</p>
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/responsiveTabs.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            fakewaffle.responsiveTabs(['xs']);

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



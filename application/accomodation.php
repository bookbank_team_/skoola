<?php include('inc/header.php');?>
<style>
    .special-select .selectize-dropdown,
    .special-select .selectize-input,
    .special-select .selectize-input input {
        line-height: 28px !important;
    }
</style>

    <link href="../public/js/plugins/datepicker/pikaday.css" rel="stylesheet">

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <div id="imageslider">
            <img src="../public/img/accommodation/slider/slide1.jpg" alt="Slide 1" />
            <img data-src="../public/img/accommodation/slider/slide2.jpg" alt="Slide 2" />
            <img data-src="../public/img/accommodation/slider/slide3.jpg" src="" alt="Slide 3" /></a>
            <img data-src="../public/img/accommodation/slider/slide4.jpg" src="" alt="Slide 4" />
        </div>

        <section class="margin_top_40 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="accomodation-category">
                            <h4>University/On-Campus Housing</h4>
                            <p>This is a popular option for most students studying in the US. It gives the student a great chance to imbibe the culture of the university, experience life from the inside, and make friends. In some cities, this might be cheaper than securing accommodation from outside your school.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="accomodation-category">
                            <h4>Student Halls of Residence</h4>
                            <p>Very popular in the UK, this is a more flexible choice for students who want to enjoy the city as well as their university. The residences are reserved for student use only, and so is a guaranteed way to make friends who may or may not attend your university. Most buildings have many features like free Wifi, gyms, social and games rooms, shops, and more.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="accomodation-category">
                            <h4>Independent Landlord</h4>
                            <p>If you prefer a private lifestyle or are a more mature student, you are not left out. There are many landlords who rent out rooms in their houses or entire buildings to students. This affords you the luxury of more quiet living away from the hustle and bustle of the campus. The price ranges for this option are also very wide, meaning there is something for everyone.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="accomodation-service margin_bottom_40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>The accommodation service from Skoola includes:</h4>
                        <ul>
                            <li><i class="icon-ok"></i> Initial meeting to determine your needs</li>
                            <li><i class="icon-ok"></i> Cost/budget planning</li>
                            <li><i class="icon-ok"></i> Recommendation of suitable packages</li>
                            <li><i class="icon-ok"></i> Handling of entire application process and guaranteed success.</li>
                        </ul>
                    </div>

                    <!--Form-->
                    <div class="col-sm-7 margin_top_20">
                        <form method="post">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" required="required" >
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name" required="required" >
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="tel" name="phone_no" id="phone_no" class="form-control" placeholder="Phone No" required="required" >
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required" >
                            </div>

                            <div class="form-group">
                                <label>Destination Country</label>
                                <select class="selectbox" placeholder="Destination Country">
                                    <option value="">Destination Country</option>
                                    <option value="1">USA</option>
                                    <option value="2">UK</option>
                                    <option value="3">Canada</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>University</label>
                                <select class="selectbox" placeholder="University">
                                    <option value="">University</option>
                                    <option value="1">University of Liverpool</option>
                                    <option value="2">Western University</option>
                                    <option value="3">Oxford University</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Move In Date</label>
                                <input type="text" placeholder="Date of Birth" class="form-control" id="pick-mov-in-date"/>
                            </div>

                            <div class="form-group">
                                <label>Move Out Date</label>
                                <input type="text" placeholder="Date of Birth" class="form-control" id="pick-mov-out-date"/>
                            </div>

                            <div class="form-group">
                                <label>Would you like</label><br/>
                                <div class="custom-radiobox inline-radio">
                                    <input type="radio" id="inhouse-option" name="consultation">
                                    <label for="inhouse-option">In-person consultation</label>
                                    <div class="check"></div>
                                </div>
                                <div class="custom-radiobox inline-radio">
                                    <input type="radio" id="virtual-option" name="consultation">
                                    <label for="virtual-option">Virtual consultation <small>(via phone call or email)</small></label>
                                    <div class="check"></div>
                                </div>
                            </div>

                            <div class="form-group text-right">
                                <button type="button" data-toggle="modal" href="#confirmatn" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </section>








        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


<!--Confirmation Modal-->
<div class="modal fade" id="confirmatn">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <div class="application-modal">
                        <h4 class="apm-title">Please confirm the following information</h4>

                        <div class="form-group">
                            <label>First Name</label>
                            <p>Andrew</p>
                        </div>

                        <div class="form-group">
                            <label>Last Name</label>
                            <p>Eze</p>
                        </div>

                        <div class="form-group">
                            <label>Phone Number</label>
                            <p>+234xxxxxxxxxxxx</p>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <p>andy@ngcareers.com</p>
                        </div>

                        <div class="form-group">
                            <label>Destination Country</label>
                            <p>UK</p>
                        </div>

                        <div class="form-group">
                            <label>University</label>
                            <p>University of Liverpool</p>
                        </div>

                        <div class="form-group">
                            <label>Move In Date</label>
                            <p>15-04-2016</p>
                        </div>

                        <div class="form-group">
                            <label>Move Out Date</label>
                            <p>30-05-2017</p>
                        </div>

                        <div class="form-group">
                            <label>Consultation</label>
                            <p>In-person Consultation</p>
                        </div>

                        <div class="form-group">
                            <label>Consultation Fee</label>
                            <div class="consultation-fee">NGN 15,000</div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm & Pay</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/bs-equalizer.js"></script>

    <!--Ideal Image Slider-->
    <script src="../public/js/plugins/ideal-image-slider/ideal-image-slider.min.js"></script>
    <script src="../public/js/plugins/ideal-image-slider/extensions/bullet-nav/iis-bullet-nav.js"></script>
    

    <!--Date Picker-->
    <script src="../public/js/plugins/datepicker/moment.js"></script>
    <script src="../public/js/plugins/datepicker/pikaday.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slider = new IdealImageSlider.Slider('#imageslider');
            slider.addBulletNav();
            slider.start();

            var movein_picker = new Pikaday({
                field: document.getElementById('pick-mov-in-date'),
                format: 'DD-MM-YYYY'
            });

            var moveout_picker = new Pikaday({
                field: document.getElementById('pick-mov-out-date'),
                format: 'DD-MM-YYYY'
            });

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            $('.selectbox').selectize({create: false});


            //Bootstrap typeahead
            var courses, remoteHost;
            $.support.cors = true;
            remoteHost = 'http://apps.dev/skoola/application/data/courses.json';
            courses = new Bloodhound({
                //identify: function(o) { return o.id_str; },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                dupDetector: function(a, b) { return a.id_str === b.id_str; },
                prefetch: remoteHost + '?prefetch',
                remote: {
                    url: remoteHost + '?keyword=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            $('.typeahead').typeahead({
                    items: 8,
                    minLength: 1,
                    autoSelect: true,
                    source:courses.ttAdapter()
            });


            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



    <?php include('inc/header.php');?>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation-account.php');?>

        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>My Documents / Transcripts</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="margin_top_30 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-push-4 margin_bottom_15">

                        <div class="innerContentWrapper">
                            <h4>My Documents <small>- Transcript -</small></h4>

                            <div class="my-document-attachment">
                                <div class="attachment-icon">
                                    <img src="../public/img/account/transcript.png" class="img-responsive"/>
                                </div>
                                <div class="attachment-info">
                                    <h4>Transcript</h4>
                                    <div class="r-meta-data">Attached: <span>Nov 12th, 2015</span> | Last Updated: <span>Nov 16th, 2015</span> | Size: <span>64Kb</span></div>
                                    <div>
                                        <form class="inline-block">
                                            <span class="file-input btn btn btn-default btn-file btn-sm">
                                           <i class="icon-edit"></i> Edit <input  name="resume" type="file"></span>
                                        </form>
                                        <a href="" class="btn btn-default btn-sm"><i class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                            </div>

                            <!--this only shows if the user is yet to upload their resume-->
                            <div class="empty-resume">
                                <img src="../public/img/account/transcript.png"/>
                                <p>No transcript is linked to your account. Please click the <b>Add Transcript</b> button below to attach your transcript</p>
                                <form enctype="multipart/form-data">
                                    <span class="file-input btn btn btn-default btn-file btn-sm">
                                       <i class="icon-plus"></i> Add Transcript <input  name="resume" type="file">
                                    </span>
                                </form>
                            </div>


                        </div>

                    </div>

                    <div class="col-sm-4 col-sm-pull-8">
                        <div class="sideBarBox">
                            <h4>My Documents</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Resume/CV</a></li>
                                <li><a href="">Transcript</a></li>
                                <li><a href="">Passport Datapage</a></li>
                                <li><a href="">Certificates</a></li>
                                <li><a href="">Personal Statement</a></li>
                                <li><a href="">Reference Letters</a></li>

                            </ul>
                            <h4>My Application</h4>
                            <ul class="side-menu-navigation margin_bottom_20">
                                <li><a href="">Schedule Application Appointment</a></li>
                                <!--<li><a href=""></a></li>-->
                            </ul>
                            <h4>My Messages</h4>
                            <ul class="side-menu-navigation">

                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <!--<script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>-->
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            $('.changePicture :file').on('fileselect', function(event, numFiles, label) {
                //var input = $(this).parents('.input-group').find(':text'),
                //    log = numFiles > 1 ? numFiles + ' files selected' : label;
                //
                //if( input.length ) {
                //    input.val(log);
                //} else {
                //    if( log ) alert(log);
                //}
            });


            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            //$('.selectbox').selectize({create: false});

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



<form action="" method="post">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" id="coursename" value="Software Engineering" placeholder="Name">
    </div>
    <div class="form-group">
        <label>Overview</label>
        <textarea placeholder="Overview" class="form-control" cols="10">Overview content</textarea>
    </div>
    <div class="form-group">
        <label>Subject/Fields</label>
        <select class="selectbox" placeholder="Subject/Fields">
            <option value="">Subject/Fields</option>
            <option value="avm">Agriculture and Veterinary Medicine</option>
            <option value="aps">Applied and Pure Sciences</option>
            <option value="ac">Architecture and Construction</option>
            <option value="bm">Business and Management</option>
            <option value="csi">Computer Science and IT</option>
            <option value="cad">Creative Arts and Design</option>
            <option value="et">Education and Training</option>
            <option value="eng" selected>Engineering</option>
            <option value="pcf">Personal Care and Fitness</option>
            <option value="hm">Health and Medicine</option>
            <option value="hm">Humanities</option>
            <option value="law">Law</option>
            <option value="mba">MBA</option>
            <option value="ssm">Social Studies and Media</option>
            <option value="ssm">Travel and Hospitality</option>
        </select>
    </div>
    <div class="form-group">
        <label>Requirements</label>
        <textarea placeholder="Overview" class="form-control" cols="10"></textarea>
    </div>
    <div class="form-group">
        <label>Duration</label>
        <select class="selectbox" placeholder="Duration">
            <option value="">Duration</option>
            <option value="1">a Year</option>
            <option value="2">2 Years</option>
            <option value="3">3 Years</option>
            <option value="4">4 Years</option>
            <option value="5">5 Years</option>
            <option value="6">6 Years</option>
        </select>
    </div>
    <div class="form-group">
        <label>Start Date</label>
        <input type="text" class="form-control pick-a-date" id="startdate" placeholder="Start Date">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="selectbox" placeholder="Status">
            <option value="">Status</option>
            <option value="1" selected>Enable</option>
            <option value="0">Disable</option>
        </select>
    </div>
    <div class="form-group text-right">
        <button class="btn btn-sm btn-primary" type="submit">Save</button>
    </div>
</form>



<script>
    $(document).ready(function(){
        //selectize
        $('.selectbox').selectize({create: false});

        //datepicker
        $('.pick-a-date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minDate: moment()
        });
    });
</script>

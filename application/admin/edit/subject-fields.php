<form action="" method="post">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" id="categoryname" value="Agriculture and Veterinary Medicine" placeholder="Name">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select class="selectbox">
            <option value="1" selected>Enable</option>
            <option value="0">Disable</option>
        </select>
    </div>
    <div class="form-group text-right">
        <button class="btn btn-sm btn-primary" type="submit">Save</button>
    </div>
</form>


<script>
    $(document).ready(function(){
        $('.selectbox').selectize({create: false});
    });

</script>

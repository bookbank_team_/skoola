<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../../public/admin/img/admin.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, Andrew Eze</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Administrator</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="index.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-user"></i> <span>Users</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-mortar-board"></i> <span>Application</span>
                </a>
            </li>
            <li>
                <a href="courses.php">
                    <i class="fa fa-book"></i> <span>Courses</span>
                </a>
            </li>
            <li>
                <a href="exams.php">
                    <i class="fa fa-file-text"></i> <span>Exam</span>
                </a>
            </li>
            <li>
                <a href="universities.php">
                    <i class="fa fa-university"></i> <span>Universities</span>
                </a>
            </li>

            <li>
                <a href="countries.php">
                    <i class="fa fa-flag"></i> <span>Countries</span>
                </a>
            </li>




        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
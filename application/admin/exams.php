        <?php include('inc/header.php');?>
        <link href="../../public/admin/js/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include('inc/sidebar.php'); ?>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">

                    <div class="custom-breadcrumb">
                        <div class="row">
                            <div class="col-sm-7 margin_bottom_10"><h3>Exams</h3></div>
                            <div class="col-sm-5 breadcrumb-links margin_bottom_10">
                                <a href=""> <i class="fa fa-tachometer"></i> Home</a> <b>&rsaquo;</b>
                                    <span>Exams</span>
                            </div>
                        </div>
                    </div>


                    <div class="contentbox margin_top_15">
                        <div class="box-header no-border-bottom">
                            <h4 class="title pull-left">Exams</h4>
                            <div class="box-tools pull-right">
                                <button class="btn btn-default btn-sm" data-toggle="modal" href="#modal-newexam">New Exam</button>
                                <a href="course-categories.php" class="btn btn-default btn-sm hidden-xs">Registered Users</a>
                                <!--<a href="subject-fields.php" class="btn btn-default btn-sm hidden-xs">Subjects/Fields</a>-->
                                <!--<div class="dropdown visible-xs-inline">-->
                                <!--  <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">-->
                                <!--    More-->
                                <!--    <span class="caret"></span>-->
                                <!--  </button>-->
                                <!--  <ul class="dropdown-menu" style="left:-90px;" role="menu" aria-labelledby="dropdownMenu1">-->
                                <!--    <li role="presentation"><a role="menuitem" tabindex="-1" href="course-categories.php">Course Categories</a></li>-->
                                <!--    <li role="presentation"><a role="menuitem" tabindex="-1" href="subject-fields.php">Subject/Fields</a></li>-->
                                <!--  </ul>-->
                                <!--</div>-->

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="box-body no-padding">
                            <table cellpadding="0" cellspacing="0" class="table table-striped footable">

                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th data-hide="phone">Fees</th>
                                        <th data-hide="phone,medium">Registered Users</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td class="important">GRE</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="important">GMAT</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="important">SAT</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="important">TOEFL</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="important">USMLE</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="important">IELTS</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="important">PTE</td>
                                        <td>N40,000.00</td>
                                        <td>10</td>
                                        <td class="text-right">
                                            <a href="exam-details.php" class="btn btn-sm btn-default"><i class="fa fa-eye"></i> <span class="hidden-xs">View</span></a>
                                            <a href="edit/exam.php" data-toggle="lightbox" data-title="Edit Exam" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i> <span class="hidden-xs">Edit</span></a>
                                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> <span class="hidden-xs">Delete</span></a>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <ul class="pagination">
                            	<li><a href="#">&laquo;</a></li>
                            	<li><a href="#">1</a></li>
                            	<li><a href="#">2</a></li>
                            	<li><a href="#">3</a></li>
                            	<li><a href="#">4</a></li>
                            	<li><a href="#">5</a></li>
                            	<li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>





                    <!--Modal-->
                    <div class="modal fade" id="modal-newexam">
                    	<div class="modal-dialog">
                    		<div class="modal-content">
                    			<div class="modal-header">
                    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    				<h4 class="modal-title">Add an Exam</h4>
                    			</div>
                                <form action="" method="post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" id="coursename" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Overview</label>
                                            <textarea placeholder="Overview" class="form-control" cols="10"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Test Formats</label>
                                            <textarea placeholder="Overview" class="form-control" cols="10"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Fees</label>
                                            <input type="text" class="form-control" id="coursename" placeholder="Fees">
                                        </div>
                                        <div class="form-group">
                                            <label>FAQ</label>
                                            <textarea placeholder="FAQ" class="form-control" cols="10"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="selectbox" placeholder="Status">
                                                <option value="">Status</option>
                                                <option value="1" selected>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>


                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                    </div>
                                </form>
                    		</div><!-- /.modal-content -->
                    	</div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->



                </section><!-- /.content -->

                <?php include('inc/footer.php'); ?>

            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- Bootstrap -->
        <script src="../../public/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../public/admin/js/common/app.js" type="text/javascript"></script>
        <script src="../../public/admin/js/plugins/footable/js/footable.min.js" type="text/javascript"></script>
        <script src="../../public/admin/js/plugins/selectize/standalone/selectize.min.js" type="text/javascript"></script>
        <script src="../../public/admin/js/plugins/lightbox/ekko-lightbox.min.js" type="text/javascript"></script>
        <script src="../../public/admin/js/plugins/daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="../../public/admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        <script>
            $(document).ready(function(){
                $('.footable').footable(
                    {
                        breakpoints: {
                            phone: 480,
                            medium: 760,
                            tablet: 1024
                        },
                    }
                );

                $('.selectbox').selectize({create: false});

                $('.pick-a-date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minDate: moment()
                });


            });

            $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
        </script>


</body>
</html>
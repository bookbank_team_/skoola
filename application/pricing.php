    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>


    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Your Application to University of Liverpool</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_50">
            <div class="container">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="customWizard">
                            <div class="row">
                                <div class="col-sm-4 step">
                                    <div>
                                        <span class="done">1</span>
                                        <span>Application Information</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span class="done">2</span>
                                        <span>Profile Picture</span>
                                    </div>
                                </div>
                                <div class="col-sm-4 step">
                                    <div>
                                        <span class="active">3</span>
                                        <span>Pricing</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="innerContentWrapper">
                            <h4>Select Pricing Plan for Application Fee</h4>

                            <div class="row row-centered margin_top_30">
                                <div class="col-sm-8 col-centered">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12 margin_bottom_15">
                                            <div class="pricing">
                                                <h4>Full Package</h4>

                                                <ul>
                                                    <li>Full Application Support</li>
                                                    <li>Phone/Face to Face Session</li>
                                                    <li>Dedicated Advisor</li>
                                                    <li>Visa Support</li>
                                                    <li>Travel Support</li>
                                                </ul>
                                                <h2>$250</h2>
                                                <div class="footer text-center">
                                                    <a href="" class="btn btn-warning">Choose Package</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="pricing">
                                                <h4>Tailored Package</h4>
                                                <ul>
                                                    <li>Application Support</li>
                                                    <li>Advisor</li>
                                                </ul>
                                                <h2>$85</h2>
                                                <div class="footer text-center">
                                                    <a href="" class="btn btn- btn-warning">Choose Package</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>




        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            var $select = $('.selectbox').selectize({
                create: false,
                onChange: function(value) {
                    var item = sponsor.getValue();
                    if(item != null && item != ''){
                        if(item == 'parent' || item == 'employer' || item == 'other'){
                            //enable the sponsor's name and phone number
                            $('.sponsor_info_more').show();
                        }
                        else{
                            $('.sponsor_info_more').hide();
                        }
                    }
                    //alert('Item is '+item);
                }
            });

            var sponsor = $select[7].selectize;

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



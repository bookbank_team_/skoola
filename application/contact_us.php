    <?php include('inc/header.php');?>
    <style>
        .special-select .selectize-dropdown,
        .special-select .selectize-input,
        .special-select .selectize-input input {
            line-height: 28px !important;
        }
    </style>

    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>

        <!--homepage search banner section-->
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Contact Us</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="margin_top_30 margin_bottom_50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-9">


                            <div class="section-heading margin_bottom_30">
                                <h2>Contact Us</h2>
                                <p>Please feel free to contact us using the form below</p>
                            </div>

                            <div class="row">
                                <div class="col-sm-10">

                                    <form method="post" action="">

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" placeholder="Email" name="" class="form-control"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" placeholder="Phone Number" name="" class="form-control"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" placeholder="Name" name="" class="form-control"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Subject</label>
                                            <input type="text" placeholder="Subject" name="" class="form-control"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Message</label>
                                            <textarea class="form-control" placeholder="Message"></textarea>
                                        </div>


                                        <div class="form-group text-right">
                                            <button type="submit" class="btn btn-warning">Send</button>
                                        </div>


                                    </form>


                                </div>
                            </div>


                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="social-widget">
                            <h3>Social Connect</h3>
                            <p>You can connect with us via our social media platforms</p>
                            <div class="social-btns">
                                <a href=""><img src="../public/img/socials/facebook.png"/></a>
                                <a href=""><img src="../public/img/socials/twitter.png"/></a>
                                <a href=""><img src="../public/img/socials/googleplus.png"/></a>
                                <a href=""><img src="../public/img/socials/linkedin.png"/></a>
                            </div>

                            <h4>You can also give us a call on</h4>
                            <b>+234-SKOOLA-CONNECT</b>

                        </div>
                    </div>
                </div>

            </div>
        </section>




        <?php include('inc/footer.php'); ?>


    </section>

    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});


            var $select = $('.selectbox').selectize({
                create: false,
                //onChange: function(value) {
                //    var item = sponsor.getValue();
                //    if(item != null && item != ''){
                //        if(item == 'parent' || item == 'employer' || item == 'other'){
                //            //enable the sponsor's name and phone number
                //            $('.sponsor_info_more').show();
                //        }
                //        else{
                //            $('.sponsor_info_more').hide();
                //        }
                //    }
                //}
            });

            //var sponsor = $select[7].selectize;

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



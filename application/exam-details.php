    <?php include('inc/header.php');?>



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>
        <section id="pageTitle">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Entry Exam / GMAT</h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="examInfoTopBackgroundContainer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="examName text-center">
                            <h2>GMAT</h2>
                            <p>(Graduate Management Admission Test)</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="margin_top-50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-sm-push-3 margin_bottom_20">
                        <div class="innerContentWrapper removePadding noBorder">
                            <!-- TAB NAVIGATION -->
                            <ul class="nav nav-tabs responsive" id="schoolTabs" role="tablist">
                                <li class="active"><a href="#tb-overview" role="tab" data-toggle="tab">Overview</a></li>
                                <li><a href="#tb-format" role="tab" data-toggle="tab">Test Format</a></li>
                                <li><a href="#tb-fees" role="tab" data-toggle="tab">Fees</a></li>
                                <li><a href="#tb-faq" role="tab" data-toggle="tab">FAQs</a></li>
                            </ul>
                            <!-- TAB CONTENT -->
                            <div class="tab-content responsive">
                                <div class="tab-pane fade active in schoolTabContent" id="tb-overview">
                                    <h4>Heading</h4>
                                    <p>Dramatically simplify seamless mindshare through error-free schemas. Quickly actualize pandemic data before stand-alone communities. Appropriately fashion end-to-end e-business with tactical web-readiness. Phosfluorescently pontificate cost effective applications via B2C intellectual capital.</p>
                                    <p>Assertively communicate plug-and-play partnerships and seamless leadership. Proactively e-enable excellent paradigms through professional mindshare. Rapidiously extend holistic synergy through empowered applications. Collaboratively predominate market positioning leadership vis-a-vis multimedia based infomediaries. Competently transition alternative ROI vis-a-vis cross-platform mindshare.</p>
                                    <p>Conveniently formulate go forward action items rather than.</p>

                                    <h5>Sub Heading</h5>
                                    <ul>
                                        <li>Avocado chili has to have a divided, thin marshmellow component.</li>
                                        <li>Place the chocolate in a bucket, and rub quickly with heated lime.</li>
                                        <li>When roasting ripe sauerkraut, be sure they are room temperature.</li>
                                        <li>Roast tofu patiently, then mix with honey and serve fast in bowl.</li>
                                        <li>For a springy ground cake, add some coffee and cumin.</li>
                                        <li>Beloved mans yearns most peace.</li>
                                    </ul>

                                    <h5>Sunt sensoremes aperto clemens</h5>
                                    <p>Collision course at the bridge that is when devastated space suits wobble. Why does the transformator go? Ferengis fly on pattern at deep space! Dosis are the particles of the cloudy nuclear flux.</p>


                                    <div class="margin_top_30">
                                        <a href="" class="btn btn-warning">Register & Pay</a>
                                        <a href="" class="btn btn-default">Ask Our Adviser</a>
                                    </div>
                                </div>
                                <div class="tab-pane fade schoolTabContent" id="tb-fees">
                                    <h4>All Fees</h4>
                                    <p>Rub the nachos with sun-dried vegemite, brine, garlic, and chipotle chile powder making sure to cover all of it. Everyone loves the asperity of butter kebab coverd with small marmalade. </p>
                                    <p>For a sticky sichuan-style cheesecake, add some ricotta and onion powder.</p>

                                    <div class="margin_top_30">
                                        <a href="" class="btn btn-warning">Register & Pay</a>
                                        <a href="" class="btn btn-default">Ask Our Adviser</a>
                                    </div>
                                </div>

                                <div class="tab-pane fade schoolTabContent" id="tb-format">
                                    <h4>Available Formats</h4>
                                    <p>Refrigerate squeezed turkey in a casserole with water for about an hour to bring down their viscosity. The thought is a strange therapist. Cut watermelon roughly, then mix with hollandaise sauce and serve equally in frying pan.</p>

                                    <div class="margin_top_30">
                                        <a href="" class="btn btn-warning">Register & Pay</a>
                                        <a href="" class="btn btn-default">Ask Our Adviser</a>
                                    </div>
                                </div>

                                <div class="tab-pane fade schoolTabContent" id="tb-faq">
                                    <h4>Axona pius solem est.</h4>
                                    <ul>
                                        <li>Everyone loves the mossiness of chickpeas curry flavord with delicious sugar.</li>
                                        <li>Combine nachos, bagel and broccoli. rub with gooey cumin and serve scraped with peanuts. Enjoy!</li>
                                        <li>Everyone loves the viscosity of ground beef curry jumbled with salty chocolate.</li>
                                        <li>Quartered, tasty pudding is best tossed with fluffy hollandaise sauce.</li>
                                        <li>What’s the secret to muddy and chopped meatballs? Always use packaged brine.</li>
                                        <li>Combine lettuce, lentils and steak. toss with sour cinnamon and serve crushed with tofu. Enjoy!</li>
                                        <li>Instead of marinating tangy mint sauce with caviar, use one quarter cup worcestershire sauce and one jar brine bucket.</li>
                                        <li>Remember: toasted squid tastes best when simmered in a casserole brushed with curry.</li>
                                    </ul>

                                    <p>Dramatically leverage other's standardized initiatives without installed base methods of empowerment. Collaboratively develop resource sucking experiences without plug-and-play mindshare. Collaboratively target granular technology for high-payoff data. Distinctively foster intuitive models before distinctive.</p>

                                    <div class="margin_top_30">
                                        <a href="" class="btn btn-warning">Register & Pay</a>
                                        <a href="" class="btn btn-default">Ask Our Adviser</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="col-sm-3 col-sm-pull-9 margin_bottom_50">
                        <div class="sideBarBox">
                            <h3>Entry Exams</h3>
                            <ul class="schoolMetaData show-divider hover-style">
                                <li><a href="exam-details.php">GRE <small>(Graduate Record Exam)</small></a></li>
                                <li><a href="exam-details.php">GMAT <small>(Management Admission Test)</small></a></li>
                                <li><a href="exam-details.php">SAT <small>(Scholastic Assessment Test)</small></a></li>
                                <li><a href="exam-details.php">TOEFL <small>(Test of English as a Foreign Language)</small></a></li>
                                <li><a href="exam-details.php">USMLE <small>(Medical Licensing Examination)</small></a></li>
                                <li><a href="exam-details.php">IELTS <small>(International English Language Testing System)</small></a></li>
                                <li><a href="exam-details.php">PTE <small>(Pearson Tests of English)</small></a></li>

                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>



    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/responsiveTabs.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            fakewaffle.responsiveTabs(['xs']);

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



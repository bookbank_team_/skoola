    <?php include('inc/header.php');?>



    <section id="contentWrapper">

        <!--top bar naviation-->
        <?php include('inc/topbar-navigation.php');?>


        <section class="schoolInfoTopBackgroundContainer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="schoolLogoName text-center">
                            <img src="../public/img/institutions/university-of-liverpool.png"/>
                            <h2>University of Liverpool</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="margin_top-50 margin_bottom_70">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-push-4 margin_bottom_15">
                        <span class="label label-default no-border-radius countryName">UNITED KINGDOM</span>
                        <div class="innerContentWrapper removePadding noBorder">

                            <!-- TAB NAVIGATION -->
                            <ul class="nav nav-tabs responsive" id="schoolTabs" role="tablist">
                                <li class="active"><a href="#tb-overview" role="tab" data-toggle="tab">Overview</a></li>
                                <li><a href="#tb-averagefees" role="tab" data-toggle="tab">Average Fees</a></li>
                                <li><a href="#tb-courses" role="tab" data-toggle="tab">Courses</a></li>
                                <li><a href="#tb-genreq" role="tab" data-toggle="tab">General Requirements</a></li>
                            </ul>

                            <!-- TAB CONTENT -->
                            <div class="tab-content responsive">
                                <div class="tab-pane fade active in schoolTabContent" id="tb-overview">
                                    <h4>Heading</h4>
                                    <img src="../public/img/placeholders/city.jpg"/>
                                    <p>Dramatically simplify seamless mindshare through error-free schemas. Quickly actualize pandemic data before stand-alone communities. Appropriately fashion end-to-end e-business with tactical web-readiness. Phosfluorescently pontificate cost effective applications via B2C intellectual capital.</p>
                                    <p>Assertively communicate plug-and-play partnerships and seamless leadership. Proactively e-enable excellent paradigms through professional mindshare. Rapidiously extend holistic synergy through empowered applications. Collaboratively predominate market positioning leadership vis-a-vis multimedia based infomediaries. Competently transition alternative ROI vis-a-vis cross-platform mindshare.</p>
                                    <p>Conveniently formulate go forward action items rather than.</p>

                                    <h5>Sub Heading</h5>
                                    <ul>
                                        <li>Avocado chili has to have a divided, thin marshmellow component.</li>
                                        <li>Place the chocolate in a bucket, and rub quickly with heated lime.</li>
                                        <li>When roasting ripe sauerkraut, be sure they are room temperature.</li>
                                        <li>Roast tofu patiently, then mix with honey and serve fast in bowl.</li>
                                        <li>For a springy ground cake, add some coffee and cumin.</li>
                                        <li>Beloved mans yearns most peace.</li>
                                    </ul>

                                    <h5>Sunt sensoremes aperto clemens</h5>
                                    <p>Collision course at the bridge that is when devastated space suits wobble. Why does the transformator go? Ferengis fly on pattern at deep space! Dosis are the particles of the cloudy nuclear flux.</p>


                                </div>
                                <div class="tab-pane fade schoolTabContent" id="tb-averagefees">


                                        <div class="eachFee">
                                            <h4>Software Engineering</h4>
                                            <p>An imminent form of pain is the fear.</p>
                                            <h4 class="text-primary">N2.0M <small>(Annually)</small></h4>
                                        </div>

                                        <div class="eachFee">
                                            <h4>MBA</h4>
                                            <p>An imminent form of pain is the fear.</p>
                                            <h4 class="text-primary">N5.0M <small>(Annually)</small></h4>
                                        </div>

                                </div>
                                <div class="tab-pane fade schoolTabContent" id="tb-courses">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 margin_bottom_20">
                                            <div class="programListing">
                                                <div class="listingBody">
                                                    <h3>Electronic / Communication Engineering</h3>
                                                    <div class="margin_top_10">
                                                        <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                        <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                                    </div>

                                                    <h3 class="price">N3.42M <small>Annual Fees</small></h3>

                                                </div>
                                                <div class="listingFooter text-right">
                                                    <a href="" class="btn btn-default">Know More</a>
                                                    <a data-toggle="modal" href="#new-application" class="btn btn-warning">Apply</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 margin_bottom_20">
                                            <div class="programListing">
                                                <div class="listingBody">
                                                    <h3>Electronic Engineering</h3>
                                                    <div class="margin_top_10">
                                                        <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                        <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                                    </div>

                                                    <h3 class="price">N3.42M <small>Annual Fees</small></h3>

                                                </div>
                                                <div class="listingFooter text-right">
                                                    <a href="" class="btn btn-default">Know More</a>
                                                    <a data-toggle="modal" href="#new-application" class="btn btn-warning">Apply</a>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6 col-md-6 col-sm-6 margin_bottom_20">
                                            <div class="programListing">
                                                <div class="listingBody">
                                                    <h3>Computer and Information Security</h3>
                                                    <div class="margin_top_10">
                                                        <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                        <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                                    </div>

                                                    <h3 class="price">N3.42M <small>Annual Fees</small></h3>

                                                </div>
                                                <div class="listingFooter text-right">
                                                    <a href="" class="btn btn-default">Know More</a>
                                                    <a data-toggle="modal" href="#new-application" class="btn btn-warning">Apply</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 margin_bottom_20">
                                            <div class="programListing">
                                                <div class="listingBody">
                                                    <h3>Information Systems and Technology</h3>
                                                    <div class="margin_top_10">
                                                        <span class="label label-default margin_bottom_5">B.Eng (Hons)</span>
                                                        <span class="label label-default margin_bottom_5">Duration: 48 Months</span>
                                                    </div>

                                                    <h3 class="price">N3.42M <small>Annual Fees</small></h3>

                                                </div>
                                                <div class="listingFooter text-right">
                                                    <a href="" class="btn btn-default">Know More</a>
                                                    <a data-toggle="modal" href="#new-application" class="btn btn-warning">Apply</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="tab-pane fade schoolTabContent" id="tb-genreq">
                                    <h4>Axona pius solem est.</h4>
                                    <ul>
                                        <li>Everyone loves the mossiness of chickpeas curry flavord with delicious sugar.</li>
                                        <li>Combine nachos, bagel and broccoli. rub with gooey cumin and serve scraped with peanuts. Enjoy!</li>
                                        <li>Everyone loves the viscosity of ground beef curry jumbled with salty chocolate.</li>
                                        <li>Quartered, tasty pudding is best tossed with fluffy hollandaise sauce.</li>
                                        <li>What’s the secret to muddy and chopped meatballs? Always use packaged brine.</li>
                                        <li>Combine lettuce, lentils and steak. toss with sour cinnamon and serve crushed with tofu. Enjoy!</li>
                                        <li>Instead of marinating tangy mint sauce with caviar, use one quarter cup worcestershire sauce and one jar brine bucket.</li>
                                        <li>Remember: toasted squid tastes best when simmered in a casserole brushed with curry.</li>
                                    </ul>

                                    <p>Dramatically leverage other's standardized initiatives without installed base methods of empowerment. Collaboratively develop resource sucking experiences without plug-and-play mindshare. Collaboratively target granular technology for high-payoff data. Distinctively foster intuitive models before distinctive.</p>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="col-sm-4 col-sm-pull-8">
                        <div class="sideBarBox">
                            <h3>Quick Lookup</h3>
                            <ul class="schoolMetaData">
                                <li>
                                    <span class="pull-left">Year Established:</span>
                                    <span class="pull-right">1963</span>
                                </li>
                                <li>
                                    <span class="pull-left">University Type:</span>
                                    <span class="pull-right"><a href="">Private</a></span>
                                </li>
                                <li>
                                    <span class="pull-left">University Setting:</span>
                                    <span class="pull-right">Sub-Urban</span>
                                </li>
                                <li>
                                    <span class="pull-left">Highest Degree Offered:</span>
                                    <span class="pull-right">N/A</span>
                                </li>
                                <li>
                                    <span class="pull-left">Region</span>
                                    <span class="pull-right"><a href="">Liverpool</a></span>
                                </li>
                                <li>
                                    <span class="pull-left">Semester</span>
                                    <span class="pull-right">Tremester</span>
                                </li>
                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php include('inc/footer.php'); ?>


    </section>


    <!--mobile navigation-->
    <?php include('inc/mobile-navigation.php');?>


    <!--Application Modal-->
    <div class="modal fade" id="new-application">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">New Application</h4>
                    </div>
                    <div class="modal-body">
                        <div class="application-modal">

                            <h4 class="apm-title">Confirm and Book with the following sections:</h4>

                            <div class="each-sections">
                                <div class="title">Personal Details</div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First Name" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="tel" name="phone_no" id="phone_no" class="form-control" placeholder="Phone No" required="required" >
                                </div>
                            </div>


                            <div class="each-sections">
                                <div class="title">Application Details</div>
                                <div class="form-group">
                                    <label>Study Level</label>
                                    <select class="selectbox" placeholder="Study Level">
                                        <option value="">Study Level</option>
                                        <option value="1">PhD.</option>
                                        <option value="2">Masters</option>
                                        <option value="3">Undergraduate</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Course</label>
                                    <select class="selectbox" placeholder="Course">
                                        <option value="">Course</option>
                                        <option value="1">Engineering</option>
                                        <option value="2">Accounting</option>
                                        <option value="3">Management</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>University</label>
                                    <select class="selectbox" placeholder="University">
                                        <option value="">University</option>
                                        <option value="1">University of Liverpool</option>
                                        <option value="2">Western University</option>
                                        <option value="3">Oxford University</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <p><b>Unsure of your course details</b>? No need to worry, we can discuss this during your consultation session</p>
                                </div>

                                <div class="form-group">
                                    <label>Would you like</label><br/>
                                    <div class="custom-radiobox inline-radio">
                                        <input type="radio" id="inhouse-option" name="consultation">
                                        <label for="inhouse-option">In-house consultation</label>
                                        <div class="check"></div>
                                    </div>
                                    <div class="custom-radiobox inline-radio">
                                        <input type="radio" id="virtual-option" name="consultation">
                                        <label for="virtual-option">Virtual consultation</label>
                                        <div class="check"></div>
                                    </div>
                                </div>

                                <div class="form-group text-right">
                                    <label>Consultation Fee</label>
                                    <div class="consultation-fee">NGN 15,000</div>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Confirm & Pay</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Javascript Libraries -->
    <script src="../public/js/plugins/slideoutjs/slideout.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/plugins/retinajs/retina.min.js"></script>
    <script src="../public/js/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script src="../public/js/plugins/sticky/jquery.sticky.js"></script>
    <script src="../public/js/responsiveTabs.js"></script>
    <script src="../public/js/plugins/selective/standalone/selectize.min.js"></script>

    <!--custom javascript libraries-->
    <script>
        $(document).ready(function(){

            $('.selectbox').selectize({create: false});

            //mobile menu
            var slideout = new Slideout({
                'panel': document.getElementById('contentWrapper'),
                'menu': document.getElementById('menuWrapper'),
                'padding': 240,
                'tolerance': 70
            });

            document.querySelector('.toggle-button').addEventListener('click', function() {slideout.toggle();});
            document.querySelector('.close-menu').addEventListener('click', function() {slideout.close();});

            fakewaffle.responsiveTabs(['xs']);

            //sticky header
            $(".topBar").sticky({ topSpacing: 0});

            //custom placeholder for old browsers
            $('input, textarea').placeholder({ customClass: 'customInputPlaceholder' });

        });
    </script>
</body>
</html>



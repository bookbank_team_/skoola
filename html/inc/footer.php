<!--footer-->
<section class="skfooter margin_top_60">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 footerBox">
                <h4>Skoola <small>Study Oversees</small></h4>
                <p>Skoola is the most trusted way for students to find and apply to schools abroad. We connect you with licensed agents and process your application successfully</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 footerBox">
                <h4>Courses by Category</h4>
                <ul>
                    <li><a href=""><i class="icon-circle-empty"></i> Arts & Humanities</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Education</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Engineering</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Environmental Sciences</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Law</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Medicine & Pharmacology</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Pure & Applied Sciences</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Social Sciences</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 footerBox">
                <h4>About Us</h4>
                <ul>
                    <li><a href=""><i class="icon-circle-empty"></i> How it works</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> News</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Contact Us</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Privacy Policy</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> Terms and Conditions</a></li>
                    <li><a href=""><i class="icon-circle-empty"></i> FAQs</a></li>
                </ul>
                <div class="footerSocials">
                    <a href=""><img src="../public/img/socials/facebook.png"/></a>
                    <a href=""><img src="../public/img/socials/twitter.png"/></a>
                    <a href=""><img src="../public/img/socials/googleplus.png"/></a>
                    <a href=""><img src="../public/img/socials/linkedin.png"/></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--copyright-->
<section class="footerCopyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright 2015, All right reserved</p>
            </div>
        </div>
    </div>
</section>
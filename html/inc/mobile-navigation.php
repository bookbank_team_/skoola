<section id="menuWrapper" class="slidingMenuContainer">
    <div class="navheading">Quick Navigation <span class="close-menu pull-right">x Close</span></div>
    <ul class="mobileMenuNavigation">
        <li><a href="">How it Works</a></li>
        <li><a href="">About Skoola</a></li>
        <li><a href="">Contact Us</a></li>
        <li><a href="login.html">Login</a></li>
        <li><a href="register.html">Join Now</a></li>
        <li class="divider">My Account</li>
        <li><a href="account.html">My Applications</a></li>
        <li><a href="account-profile.html">Profile</a></li>
        <li><a href="account-change-password.html">Change Password</a></li>
        <li><a href="">Logout</a></li>
    </ul>
</section>
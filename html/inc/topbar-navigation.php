<!--TopBar-->
<section class="topBar">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-2 col-lg-push-3 col-md-push-3 col-sm-push-3">
                <div class="topNavigation">
                    <ul class="hidden-xs visible-sm visible-lg visible-md">
                        <?php if(!isset($leaveLoginJoinBtn)): ?>
                        <li><a href="">How it Works</a></li>
                        <li><a href="">About Skoola</a></li>
                        <li><a href="">Contact Us</a></li>
                        <?php else: ?>
                            <li><a>&nbsp;</a></li>
                        <?php endif?>
                        <li><a href="login.php" class="btn btn-default"><i class="icon-user-outline"></i> Login</a></li>
                        <li><a href="register.php" class="btn btn-warning"><i class="icon-user-add-outline"></i> Join Now</a></li>
                    </ul>
                </div>
                <div class="visible-xs mobileMenuBtn text-left">
                    <a href="#" class="toggle-button"><i class="icon-menu"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-10 col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
                <div class="logo">
                    <a href="index.php"><img src="../public/img/skoola-logo.png"/></a>
                </div>
            </div>
        </div>
    </div>
</section>
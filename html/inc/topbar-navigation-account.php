<!--TopBar-->
<section class="topBar">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-2 col-lg-push-3 col-md-push-3 col-sm-push-3">
                <div class="topNavigation">
                    <ul class="hidden-xs visible-sm visible-lg visible-md">
                        <li><a href="">How it Works</a></li>
                        <li><a href="">About Skoola</a></li>
                        <li><a href="">Contact Us</a></li>
                        <li class="dropdown"><a href="#" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><img src="../public/img/users/default.png" width="24px" class="img-circle"/> My Account <span class="caret"></span></a>
                            <ul class="dropdown-menu" style="z-index:3000;">
                                <li><a href="account.html">My Applications</a></li>
                                <li><a href="account-profile.html">Profile</a></li>
                                <li><a href="account-change-password.html">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <div class="visible-xs mobileMenuBtn text-left">
                    <a href="#" class="toggle-button"><i class="icon-menu"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-10 col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
                <div class="logo">
                    <a href="index.html"><img src="../public/img/skoola-logo.png"/></a>
                </div>
            </div>
        </div>
    </div>
</section>
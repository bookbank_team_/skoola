/*
 * Grunt Script for Skoola Style
 */

'use strict';
module.exports = function(grunt) {

  grunt.initConfig({

  stylus: {
    options: {
      compress: true,
      paths: ['stylus', './node_modules/rupture', __dirname],
      urlfunc: 'embedurl',
      use: [
        require('rupture'),
      ],
    },

    bootstrap: {
      files: {
        'css/skoola.css': 'css/stylus/skoola.styl',
        'admin/css/style.css': 'css/stylus/admin-style.styl',
      }
    }
  },

  watch: {
    css: {
      files: 'css/stylus/**/*.styl',
      tasks: ['stylus', 'postcss'],
      options: {
        debounceDelay: 5
      }
    }
  },

  postcss: {
    options: {
      browsers: [
        'Android 2.3',
        'Android >= 4',
        'Chrome >= 20',
        'Firefox >= 24', // Firefox 24 is the latest ESR
        'Explorer >= 8',
        'iOS >= 6',
        'Opera >= 12',
        'Safari >= 6'
      ],
      processors: [

        require('pixrem')(), // add fallbacks for rem units
        require('autoprefixer')/*({browsers: 'last 2 versions'})*/, // add vendor prefixes
        //require( 'stylelint' )(),
        require('cssnano')({safe: true}) // minify the result
      ]
    },
    core: {
      options: {
        map: false,
        safe: true
      },
      src: ['css/skoola.css', 'admin/css/style.css'],
    }
  },

  cssmin: {
    bootstrap: {
      files: {
        "css/skoola.css": ["css/skoola.css"],
        "admin/css/style.css": ["admin/css/style.css"],
      }
    }
  },
  });

  // Load plugins here
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  
  grunt.registerTask('default', [
    'stylus:bootstrap',
    'cssmin:bootstrap',
    'postcss',
    'watch',
  ]);
};
